%========================================================
%   Experiment 2 - behaviour
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script produces the Fig. 2a with the histogram of 
% confidence ratings

clear
addpath ../conf/
addpath ../functions

conf = [];
conf.frate = 0.1;
conf.exp = 2;
conf.session = 1;
config_spikes;

avgnci = @(x) [mean(x) std(x)./sqrt(length(x))];

% list neurons
d = dir(['../conf/neurons/e2s1*_gauss' num2str(conf.frate) '_stim.mat']);
% load the first one (this is just for the behaviour)
neuron = load(['../conf/neurons/' d(1).name]);

% SDT
idhit = strcmp(neuron.behav.sdt,'hit');
idmiss = strcmp(neuron.behav.sdt,'miss');
idcrej = strcmp(neuron.behav.sdt,'crej');
idfa = strcmp(neuron.behav.sdt,'fa');

% get behaviour
resp = idhit | idfa;
confidence = neuron.behav.conf;
amp = neuron.behav.amp;
onset = neuron.behav.onset;

% remove bad trials
badtrials = false(1,length(idhit));
badtrials(conf.allbadtrials) = 1;

idhit(badtrials) = 0;
idmiss(badtrials) = 0;
idcrej(badtrials) = 0;
idfa(badtrials) = 0;

% summary
n = sum(idhit | idmiss | idcrej | idfa);
nhit = sum(idhit);
nmiss = sum(idmiss);
ncrej = sum(idcrej);
nfa = sum(idfa);

% SDT dprime / criterion
zfa = norminv((nfa+0.5)./((nfa+0.5)+ncrej+1));
zhr = norminv((nhit+0.5)/((nhit+0.5)+nmiss+1));

crit = -zfa;
dprime = zhr-zfa;
    
% output stats
fprintf('Percentage: hit: %.2f%%, miss: %.2f%%, crej.: %.2f%%, f.a.: %.2f%%\n',...
    nhit./n*100,nmiss./n*100,ncrej./n*100,nfa./n*100);

fprintf('Confidence: hit: %.2f±%.2f, miss: %.2f±%.2f, crej.: %.2f±%.2f, f.a.: %.2f±%.2f\n',...
    avgnci(confidence(idhit)),avgnci(confidence(idmiss)),avgnci(confidence(idcrej)),avgnci(confidence(idfa)));

myranksum(confidence(idhit),confidence(idfa),{'Conf (hit)','Conf (fa)'});
myranksum(confidence(idcrej),confidence(idmiss),{'Conf (crej)','Conf (miss)'});

fprintf('dprime: %.2f crit: %.2f\n',...
    dprime, crit);

fprintf('rate: %.2f%% hit | %.2f%% fa \n',...
    100*sum(idhit)./sum(idhit | idmiss),100*sum(idfa)./sum(idcrej | idfa));

[h,p,ci,st] = ttest2(amp(idhit),amp(idmiss));
fprintf('amplitude: %.3f hit | %.3f miss, t(%d)=%.2f, p=%.3f\n',...
    mean(amp(idhit)),mean(amp(idmiss)), ...
    st.df,st.tstat,p);

[h,p,ci,st] = ttest2(onset(idhit),onset(idmiss));
fprintf('onset: %.3f hit | %.3f miss, t(%d)=%.2f, p=%.3f\n',...
    mean(onset(idhit)),mean(onset(idmiss)), ...
    st.df,st.tstat,p);

describe(confidence(idhit),'conf (hits)');
describe(confidence(idmiss),'conf (miss)');
[tbl,chi2stat,pval] = crosstab(confidence(idhit | idmiss),idhit(idhit | idmiss));
fprintf('Chi^2 = %.2f, p = %d\n',chi2stat,pval);           

%% plot first order

h = figure(100); clf; hold on;
bar(1,sum(idhit),'FaceColor',conf.color.hit);
bar(2,sum(idmiss),'FaceColor',conf.color.miss);
bar(3,sum(idcrej),'FaceColor',conf.color.crej);
bar(4,sum(idfa),'FaceColor',conf.color.fa);

set(gca,'XTick',1:4,'XTickLabel',{},'YTick',0:20:60,'YTickLabel',{});
set(gcf,'Position',[50,800,120,120])
print('figs/exp_firstorder.eps','-depsc');

%% plot second order (confidence)
inclcrej = 1;
h = figure(101); clf; hold on;
hmiss = [sum(confidence==1 & idmiss),sum(confidence==2 & idmiss),sum(confidence==3 & idmiss)];
hhitlow = [sum(confidence==1 & idhit),sum(confidence==2 & idhit)];
hhithigh = sum(confidence==3 & idhit);
hcrej = [sum(confidence==3 & idcrej),sum(confidence==2 & idcrej),sum(confidence==1 & idcrej)];
bar(1:3,hmiss,'FaceColor',conf.color.miss);
bar(4:5,hhitlow,'FaceColor',conf.color.hit2);
bar(6,hhithigh,'FaceColor',conf.color.hit3);
if inclcrej
    plot([7 7],ylim(),'k--');
bar(8:10,hcrej,'FaceColor',conf.color.crej);
end
set(gca,'XTick',1:10,'XTickLabel',{},'YTick',0:10:60,'YTickLabel',{});
set(gcf,'Position',[200,800,200,120])
print('figs/v2_t2hist.eps','-depsc');
% save data
tbl = table(hmiss,[hhitlow hhithigh],hcrej,...
    'VariableNames',{'cntmiss','cnthit','cntcrej'});
writetable(tbl,'../../supdata/Fig.2a.xls');
%% plot confidence for different SDT conditions
ci = @(x) std(x)./sqrt(length(x));
chit = mean(confidence(idhit));
cmiss = mean(confidence(idmiss));
ccrej = mean(confidence(idcrej));
cfa = mean(confidence(idfa));
chit_ci = std(confidence(idhit));
cmiss_ci = std(confidence(idmiss));
ccrej_ci = std(confidence(idcrej));
cfa_ci = std(confidence(idfa));
figure(110); clf; hold on;
ms = 8;
lw = 1.5;
errorbar(1,chit,chit_ci,'o','LineWidth',lw,'MarkerSize',ms,'Color',conf.color.hit,'MarkerFaceColor',conf.color.hit);
errorbar(2,cmiss,cmiss_ci,'o','LineWidth',lw,'MarkerSize',ms,'Color',conf.color.miss,'MarkerFaceColor',conf.color.miss);
errorbar(3,ccrej,ccrej_ci,'o','LineWidth',lw,'MarkerSize',ms,'Color',conf.color.crej,'MarkerFaceColor',conf.color.crej);
errorbar(4,cfa,cfa_ci,'o','LineWidth',lw,'MarkerSize',ms,'Color',conf.color.fa,'MarkerFaceColor',conf.color.fa);

xlim([0.5,4.5]); ylim([0,3])
set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
print('figs/exp2_confidence.eps','-depsc');

%% plot (no) amplitude differences

h = figure(102); clf; hold on;
bar([mean(amp(idhit)) mean(amp(idmiss))],'FaceColor','k');
%set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:0.1:2,'YTickLabel',{});
set(gcf,'Position',[350,800,120,120])
print('figs/exp2_stimamp.eps','-depsc');