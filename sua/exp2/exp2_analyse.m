%========================================================
%   Experiment 2 - responsive neurons
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script produces most analyses for experiment 2:
% number of responsive neurons, plotting SUA and average firing rates
%
% /!\ it requires running exp2_prep.m and exp2_rndperm.m which could take a
% while.

conf.exp = 2;
conf.session = 1;
config_spikes
interpmainwheninteract = 0;
doplot = 1;

if ~exist('cells','var') || cells(1).exp ~= 2
    fprintf('Loading precomputed data\n');
    if exist('save/rndcells_exp2.mat','file')
        load('save/cells_exp2.mat')
    else
        error('Could not find save/cells_exp2.mat\nyou probably should run exp2_prep.m');
    end
end
if ~exist('rndcells','var') || rndcells(1).exp ~= 2
    fprintf('Loading precomputed shuffled data, might take a while\n');
    if exist('save/rndcells_exp2.mat','file')
        load('save/rndcells_exp2.mat');
    else
        error('Could not find save/rndcells_exp2.mat\nyou probably should run exp2_rndperm.m');
    end
end
t = linspace(-0.5,1.5,2001);


sua = [cells.sua];
rndsua = repmat([cells.sua].',[1,size(rndcells,2)]);

% p detection effect
detect = [cells.isdetect];
rnddetect = reshape([rndcells.isdetect],size(rndcells,1),[]);

% p confidence effect
confid = [cells.isconf];
rndconfid = reshape([rndcells.isconf],size(rndcells,1),[]);

% p interaction effect
interact = [cells.isboth];
rndinteract = reshape([rndcells.isboth],size(rndcells,1),[]);

% correlation coefficients
corconf = [cells.corrconf];
rndpcorconf = reshape([rndcells.pcorrconf],size(rndcells,1),[]);

corconfhit = [cells.corrconf_hit];
rndpcorconfhit = reshape([rndcells.pcorrconf_hit],size(rndcells,1),[]);

corconfmiss = [cells.corrconf_miss];
rndpcorconfmiss = reshape([rndcells.pcorrconf_miss],size(rndcells,1),[]);

% associated probabilities
pcorconf = [cells.pcorrconf];
rndpcorconf = reshape([rndcells.pcorrconf],size(rndcells,1),[]);

pcorconfhit = [cells.pcorrconf_hit];
rndcorconfhit = reshape([rndcells.corrconf_hit],size(rndcells,1),[]);

pcorconfmiss = [cells.pcorrconf_miss];
rndcorconfmiss = reshape([rndcells.corrconf_miss],size(rndcells,1),[]);

% choice probs.
cpdetect = [cells.cp_detect];
rndcpdetect = reshape([rndcells.cp_detect],size(rndcells,1),[]);

% p detect (ranksum)
pdetect2 = [cells.isdetect2];
rndpdetect2 = reshape([rndcells.isdetect2],size(rndcells,1),[]);

% p diff. vs. baseline
hitsig = [cells.ishitsig2];
rndhitsig = reshape([rndcells.ishitsig],size(rndcells,1),[]);

misssig = [cells.ismisssig2];
rndmisssig = reshape([rndcells.ismisssig],size(rndcells,1),[]);


% spike counts
frhit = [cells.spikes_hit];
rndfrhit = reshape([rndcells.spikes_hit],size(rndcells,1),[]);

frhitch = [cells.spikes_hitch];
rndfrhitch = reshape([rndcells.spikes_hitch],size(rndcells,1),[]);

frhitcl = [cells.spikes_hitcl];
rndfrhitcl = reshape([rndcells.spikes_hitcl],size(rndcells,1),[]);

frmiss = [cells.spikes_miss];
rndfrmiss = reshape([rndcells.spikes_miss],size(rndcells,1),[]);

frmissch = [cells.spikes_missch];
rndfrmissch = reshape([rndcells.spikes_missch],size(rndcells,1),[]);

frmisscl = [cells.spikes_misscl];
rndfrmisscl = reshape([rndcells.spikes_misscl],size(rndcells,1),[]);

frbase = [cells.spikes_base];
rndfrbase = reshape([rndcells.spikes_base],size(rndcells,1),[]);

%%
%% selective neurons
figure(101); clf;
nsua = sum(sua);
fprintf('-----------------\n');
fprintf('----- count -----\n');
fprintf('-----------------\n');

%%
if interpmainwheninteract
    iddetect = (detect < 0.05) & (sua==1);
    rndiddetect = (rnddetect < 0.05) & (rndsua==1);
    
    idconf = (confid < 0.05)  & (sua==1);
    rndidconf = (rndconfid < 0.05)  & (rndsua==1);
else
    iddetect = (detect < 0.05) & ~(interact < 0.05) & (sua==1);
    rndiddetect = (rnddetect < 0.05) & ~(rndinteract < 0.05) & (rndsua==1);
    
    idconf = (confid < 0.05) & ~(interact < 0.05) & (sua==1);
    rndidconf = (rndconfid < 0.05) & ~(rndinteract < 0.05) & (rndsua==1);
    
end
ndetect = sum(iddetect);
rndndetect = sum(rndiddetect);
reportcells(ndetect,nsua,'detection',mean(rndndetect >= ndetect));
figure(101); subplot(3,3,1); plotperm(rndndetect,ndetect,nsua,0.05); title('detection');

nconf = sum(idconf);
rndnconf = sum(rndidconf);
reportcells(nconf,nsua,'conf',mean(rndnconf >= nconf));
figure(101); subplot(3,3,2); plotperm(rndnconf,nconf,nsua,0.05); title('conf');

%%
idboth = (interact < 0.05) & (sua==1);
rndidboth = (rndinteract < 0.05) & (rndsua==1);
nboth = sum(idboth);
rndnboth = sum(rndidboth);
reportcells(nboth,nsua,'interaction',mean(rndnboth >= nboth));
figure(101); subplot(3,3,3); plotperm(rndnboth,nboth,nsua,0.05); title('interaction');


fprintf('-----------------\n');

%%
idboth_pos = (cpdetect>0);
rndidboth_pos = (rndcpdetect >0);
nboth_pos = sum(idboth_pos(idboth));
rndnboth_pos = sum(rndidboth_pos(idboth,:));
reportcells(nboth_pos,nboth,'interaction & positive CP ',mean(rndnboth_pos >= nboth_pos));
%figure(101); subplot(3,3,4); plotperm(rndnbothinv_evidhit,nbothinv_evidhit,nboth,0.05); title('interaction & corr conf hit');

%%
id_evidconfhit = (pcorconfhit < 0.05);
rndid_evidconfhit = (rndpcorconfhit < 0.05);
nbothinv_evidhit = sum(id_evidconfhit(idboth));
rndnbothinv_evidhit = sum(rndid_evidconfhit(idboth,:));
reportcells(nbothinv_evidhit,nboth,'interaction & corr conf hit ',mean(rndnbothinv_evidhit >= nbothinv_evidhit));
figure(101); subplot(3,3,4); plotperm(rndnbothinv_evidhit,nbothinv_evidhit,nboth,0.05); title('interaction & corr conf hit');

%%
id_evidconfmiss = (pcorconfmiss < 0.05);
rndid_evidconfmiss = (rndpcorconfmiss < 0.05);
nbothinv_evidmiss = sum(id_evidconfmiss(idboth));
rndnbothinv_evidmiss = sum(rndid_evidconfmiss(idboth,:));
reportcells(nbothinv_evidmiss,nboth,'interaction & corr conf miss',mean(rndnbothinv_evidmiss >= nbothinv_evidmiss));
figure(101); subplot(3,3,5); plotperm(rndnbothinv_evidmiss,nbothinv_evidmiss,nboth,0.05); title('interaction & corr conf miss');

%%
id_evidconfhit2 = (sign(cpdetect)==sign(corconfhit)) & (pcorconfhit < 0.05);
rndid_evidconfhit2 = (sign(rndcpdetect) == sign(rndcorconfhit)) & (rndpcorconfhit < 0.05);
nbothinv_evidhit2 = sum(id_evidconfhit2(idboth));
rndnbothinv_evidhit2 = sum(rndid_evidconfhit2(idboth,:));
reportcells(nbothinv_evidhit2,nboth,'interaction & corr conf hit (~cp) ',mean(rndnbothinv_evidhit2 >= nbothinv_evidhit2));
figure(101); subplot(3,3,6); plotperm(rndnbothinv_evidhit2,nbothinv_evidhit2,nboth,0.05); title('interaction & corr conf hit (~cp)');

%%
id_evidconfmiss2 = sign(cpdetect)~=sign(corconfmiss) & (pcorconfmiss < 0.05);
rndid_evidconfmiss2 = sign(rndcpdetect) ~= sign(rndcorconfmiss) & (rndpcorconfmiss < 0.05);
nbothinv_evidmiss2 = sum(id_evidconfmiss2(idboth));
rndnbothinv_evidmiss2 = sum(rndid_evidconfmiss2(idboth,:));
reportcells(nbothinv_evidmiss2,nboth,'interaction & corr conf miss (~-cp)',mean(rndnbothinv_evidmiss2 >= nbothinv_evidmiss2));
figure(101); subplot(3,3,7); plotperm(rndnbothinv_evidmiss2,nbothinv_evidmiss2,nboth,0.05); title('interaction & corr conf miss (~-cp)');


%%
idtrueconf = pcorconf<0.05 & sign(corconfhit) == sign(corconfmiss)  & (sua==1);
rndidtrueconf= (rndpcorconf < 0.05) & sign(rndcorconfhit) == sign(rndcorconfmiss);
ntrueconf = sum(idtrueconf);
rndntrueconf = sum(rndidtrueconf);
reportcells(ntrueconf,nsua,'true conf',mean(rndntrueconf >= ntrueconf));
figure(101); subplot(3,3,8); plotperm(rndntrueconf,ntrueconf,nsua,0.05); title('true conf');

%%

idmissincr2 = sua & misssig<0.05 & (frmiss > frhit) & (frmiss > frbase);
rndidmissincr2 = rndsua & rndmisssig<0.05 & (rndfrmiss > rndfrhit) & (rndfrmiss > rndfrbase);
nmissincr2 = sum(idmissincr2(idboth));
rndnmissincr2 = sum(rndidmissincr2(idboth,:));
reportcells(nmissincr2,nboth,'detection + incr. from baseline for misses > hits (miss-coding)',mean(rndnmissincr2 >= nmissincr2));
figure(101); subplot(3,3,9); plotperm(rndnmissincr2,nmissincr2,nboth,0.05*0.5*0.5); title('detect + incr. baseline (misses) > hits');

%%
idhit2 = (pcorconfhit<0.05)   & (sua==1);
rndidhit2 = (rndpcorconfhit < 0.05) & (rndsua == 1);
nhit2= sum(idhit2);
rndnhit2 = sum(rndidhit2);
reportcells(nhit2,nsua,'corr conf (hits) (R2)',mean(rndnhit2 >= nhit2));
%%
idhitconf = (pcorconfhit<0.05)   & (sua==1);
rndidhitconf= (rndpcorconfhit < 0.05) & (rndsua == 1);
nhitconf = sum(idhitconf(idboth));
rndnhitconf = sum(rndidhitconf(idboth,:));
reportcells(nhitconf,sum(idboth),'detection + corr conf (hits) (R2)',mean(rndnhitconf >= nhitconf));
%figure(101); subplot(3,3,8); plotperm(rndntrueconf,ntrueconf,nsua,0.05); title('true conf');

%%
figure(101); set(gcf,'Position',[200,200,800,800])

%%

%%

fprintf('detection-selective neurons: ');
fprintf('%s ',cells(iddetect).id);
fprintf('\n');

fprintf('confidence-selective neurons: ');
fprintf('%s ',cells(idconf).id);
fprintf('\n');

fprintf('detection- and confidence-selective neurons: ');
fprintf('%s ',cells(idboth).id);
fprintf('\n');

%%
if doplot
    plot_exp2_gavg(cells(idboth &  ([cells.coeffboth]>0)),t,conf)
    %plot_exp2_gavg(cells(idboth),t,conf)
end
if doplot>=2
    %%
    plot_exp2(cells,'#380',t,conf)
    %plot_exp2(cells,'#330',t,conf)
    plot_exp2(cells,'#285',t,conf)
end

%%

%%
detect_cpdetect = [cells(iddetect).cp_detect];
detect_cpconf_hit = [cells(iddetect).cp_conf_hit];
detect_cpconf_miss = [cells(iddetect).cp_conf_miss];

conf_cpdetect = [cells(idconf).cp_detect];
conf_cpconf_hit = [cells(idconf).cp_conf_hit];
conf_cpconf_miss = [cells(idconf).cp_conf_miss];

both_cpdetect = [cells(idboth).cp_detect];
both_cpconf_hit = [cells(idboth).cp_conf_hit];
both_cpconf_miss = [cells(idboth).cp_conf_miss];


if doplot
    %% CP analysis
    ms = 4;
    colconf = conf.color.hit2;
    colresp = [219,94,229]./255;
    colboth = conf.color.crej;
    %idnone = ~(iddetect | idconf | idboth) & idsua;
    for j=1:3
        if j==1
            xtype = 'cp_conf_hit';
            ytype = 'cp_detect'; suffix = '';
        elseif j==2
            ytype = 'corrconf_miss'; suffix = '_conf';
            xtype = 'corrconf_hit';
        elseif j==3
            xtype = 'corrconf_miss'; suffix = '_confmiss';
            ytype = 'cp_detect';
        end
        cpx = [cells.(xtype)];
        cpy = [cells.(ytype)];
        
        
        bin = -0.8:0.1:0.8;
        figure(); clf; hold on;
        
        plot(cpx([cells.sua]==1),cpy([cells.sua]==1),'ko','MarkerFaceColor',conf.color.amp1);
        crt = [cells.cp_detect];
        sel = find([cells.sua]==1);
        for i=1:length(sel)
            if idboth(sel(i))
                plot(cpx(sel(i)),cpy(sel(i)),'ko','MarkerFaceColor',conf.color.amp4);
            end
            if pcorconfhit(sel(i)) < 0.05
                plot(cpx(sel(i)),cpy(sel(i)),'o','MarkerSize',6,'Color',conf.color.hit);
            end
            if pcorconfmiss(sel(i)) < 0.05
                plot(cpx(sel(i)),cpy(sel(i)),'o','MarkerSize',6,'Color',conf.color.miss);
            end
            
        end
        
        m = fitlm(cpy(sel).',cpx(sel).');
        x = (-0.6:0.1:0.6).';
        [y,yint] = m.predict(x);
        plot(x,y,'r','LineWidth',2);
        plot([x ; x(end:-1:1)],[yint(:,1) ; yint(end:-1:1,2)],'r','LineWidth',1);
        
        cpx2 = cpx(sel);
        cpy2 = cpy(sel);
        %crnd = nan(1,1000);
        crnd2 = nan(1,1000);
        
        rng('default');
        for i=1:1000
            %perm = randperm(length(cpy));
            %crnd(i) = corr(cpy(perm).',cpx.','type','Spearman');
            perm2 = randperm(length(cpy2));
            crnd2(i) = corr((cpx2(perm2)).',(cpy2).','type','Spearman');
        end
        cor = corr(cpy2.',cpx2.','type','Spearman');
        p = mean(crnd2 > cor);
        fprintf('Corr CP R = %.2f, p = %.3f\n',cor,p);
        
        axis([-0.8,0.8,-0.8,0.8]); axis square
        plot([0 0],ylim(),'k-');
        plot(xlim(),[0 0],'k-');
        set(gca,'XTick',-0.8:0.2:0.8,'YTick',-0.8:0.2:0.8,'XTickLabel',{},'YTickLabel',{});
        set(gcf,'Position',[500,700,360,250])
        
        print(['figs/exp2/exp2_choiceprob' suffix '.eps'],'-depsc');
        
        tbl = table(cpx2.',cpy2.',idboth(sel).'==1, pcorconfhit(sel).' < 0.05, pcorconfmiss(sel).' < 0.05,...
           'VariableNames',{xtype,ytype,'selective','corrconfhit','corrconfmiss'});
        if j==1
            writetable(tbl,'../../supdata/SFig.4a.xls');
        elseif j==2
            writetable(tbl,'../../supdata/SFig.4c.xls');
        elseif j==3
            writetable(tbl,'../../supdata/SFig.4b.xls');
        end

    end
end

