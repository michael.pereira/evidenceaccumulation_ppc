%========================================================
%   Experiment 2 - rebuttal
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script simulates SDT models for the responses to reviewer 2

clear;
conf = [];
conf.version = 1;
conf.session = 1;
config_spikes

data = [8,35,8,6,16,30];

type = 'gauss'; metathr = 1.5; dp = 1.64;

nrep = 1;
nhit = 52; % from data
nmiss = 51; % from data

fac = 0.2;
bias = 0.5;
metac = [-Inf,-1*metathr,-fac*metathr,0,bias*fac*metathr,bias*1*metathr, Inf];
labelconfid = [3,2,1,1,2,3];

%%
cnt1 = nan(2,nrep);
cnt2 = nan(6,nrep);
confid = nan(6,nrep);

for rep = 1:nrep
    if strcmp(type,'gauss')
        X = dp + randn(nhit+nmiss,1);
    elseif strcmp(type,'gamma')
        X = gamrnd(k,theta,nhit+nmiss,1);
    else
        error('Unknown type');
    end
    crit = median(X);
    X2 = (X - crit);
    cnt1(:,rep) = [sum(X2 <= metac(4)) sum(X2 > metac(4))];
    for i=1:length(metac)-1
        cnt2(i,rep) = sum(X2 > metac(i) & X2 <= metac(i+1));
    end
end
cnt2(1:3,:) = cnt2(3:-1:1,:);
labelconfid(1:3) = labelconfid(3:-1:1);

%% plot perceptual evidence from the model
figure();
subplot(121); hold on

[h,hc]= hist(X2,-4:0.2:4);%
bar(hc,h,'FaceColor',0.8*[1 1 1]);
for i=1:length(metac)
    if i<4
        plot(metac(i)*[1,1],ylim(),'Color',conf.color.miss,'LineWidth',1)
    else
        plot(metac(i)*[1,1],ylim(),'Color',conf.color.hit,'LineWidth',1)
    end
end
plot([0,0],ylim(),'k','LineWidth',2)
xlabel('X - c');

%% plot first-order
subplot(143); hold on;
b = bar([nmiss nhit]);
b.FaceColor = 'flat';
b.CData(1,:) = conf.color.miss;
b.CData(2,:) = conf.color.hit;
plot((cnt1.'),'ko','MarkerFaceColor','k');
xlabel('SDT');
set(gca,'XTick',1:2,'XTickLabel',{'miss','hit'});
ylim([0,60])

%% plot second order (confidence)
subplot(144); hold on;
b = bar(data);
b.FaceColor = 'flat';
b.CData(1:3,:) = repmat(conf.color.miss,3,1);
b.CData(4:6,:) = repmat(conf.color.hit,3,1);
plot((1:3),cnt2(1:3,:).','k','LineWidth',2);
plot(3+(1:3),cnt2(3+(1:3),:),'k','LineWidth',2);
xlabel('Confidence');
set(gca,'XTick',1:6,'XTickLabel',labelconfid);
set(gcf,'Position',[500,800,400,100]);
confmiss = 1/(nmiss)*(cnt2(1,:)+2*cnt2(2,:)+3*cnt2(3,:));
confhit = 1/(nhit)*(cnt2(4,:)+2*cnt2(5,:)+3*cnt2(6,:));
fprintf('Confidence: %.2f for misses, %.2f for hits\n',mean(confmiss.'),mean(confhit.'));
print('figs/rebuttal_sdt.png','-dpng');