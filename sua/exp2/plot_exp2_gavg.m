%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function plot_exp2_gavg(cells,t,conf)
%plot_exp3_gavg population plots
%   
short = 1;
if short == 1
lw = 1;
figsz = [100,400,150,120];
else
lw = 2;
figsz = [100,400,300,250];
end
%%
    if ~exist('figs/exp2','dir')
        mkdir('figs/exp1');
    end
    
   
    h = figure(151); clf; hold on;
    
    plottype = 3;

    cishade([cells.avgfmiss].',[],conf.color.miss,t,'LineWidth',lw);
    cishade([cells.avgfhit_conflow].',[],conf.color.hit2,t,'LineWidth',lw);
    cishade([cells.avgfhit_confhigh].',[],conf.color.hit3,t,'LineWidth',lw);

    y = ylim();

    plot([0 0],ylim(),'k--');
    xlim([-0.5,1.5])
    ylim(y);
    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
    set(gcf,'Position',figsz)
    print(['figs/exp2/exp2_gavg_frate.eps'],'-depsc');

    pause(0.1);
    
    % save data
    % save data
    miss_ = mean([cells.avgfmiss],2).';
    confhigh_ = mean([cells.avgfhit_confhigh],2).';
    conflow_ = mean([cells.avgfhit_conflow],2).';
    
    sel = t>= -0.5 & t < 1.5;
    tbl = table(t(sel).',confhigh_(sel).',conflow_(sel).',miss_(sel).','VariableNames',{'time','confhigh','conflow','miss'});
    
    writetable(tbl,'../../supdata/Fig.3b.xls');
    %%


pause(0.1);
end

