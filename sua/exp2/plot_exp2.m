%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function plot_exp2(cells,id,t,conf)
%plot_exp2 Helper function to plot data from single neurons
%   Inputs:
%       - cells: an array of structures computed by exp1_prep.m and stored
%       in save/cells_exp1.mat
%       - id: the index of the SUA/MUA for plotting
%       - t: time indices for the x-axis (see exp1_analyse.m)
%       - conf: the struct of configs from ../conf/config_spikes.m
%   
short = 1;
if short == 1
lw = 1;
figsz = [100,400,150,120];
else
lw = 2;
figsz = [100,400,300,250];
end
%%
    if ~exist('figs/exp2','dir')
        mkdir('figs/exp2');
    end
    
    cell = cells(strcmp({cells.id},id));

fprintf('= cell %s\n',cell.id);
fprintf('= SUA: %d\n',cell.sua);
fprintf('= SNR: %.1f\n',cell.snr);
fprintf('= ISI < 3 ms: %.2f %%\n',cell.isi*100);
fprintf('= Avg fire: %.1f\n',cell.fire);
fprintf('= CV2: %.2f\n',cell.cv2);
fprintf('= Bustiness: %.2f\n',cell.bi);
fprintf('= CP detect: %.2f (p = %.3f)\n',cell.cp_detect,cell.isdetect2);
fprintf('= CP conf hit: %.2f (p = %.3f)\n',cell.cp_conf_hit,cell.isconfhit2);
fprintf('= CP conf miss: %.2f (p = %.3f)\n',cell.cp_conf_miss,cell.isconfmiss2);


    
    h = figure(111); clf; hold on;
    
    if 1
    if size(cell.fhit_conflow,1)>5
        cishade(cell.fhit_conflow.',[],conf.color.hit2,t,'LineWidth',lw);
    end
    if size(cell.fhit_confhigh,1)>5
        cishade(cell.fhit_confhigh.',[],conf.color.hit3,t,'LineWidth',lw);
    end
    else
        cishade(cell.fhit.',[],conf.color.hit,t,'LineWidth',lw);

    end
    cishade(cell.fmiss.',[],conf.color.miss,t,'LineWidth',lw);
    xlim([-0.5,1.5])
    y = ylim();

    plot([0 0],ylim(),'k--');
    
    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
    set(gcf,'Position',figsz)
    print(['figs/exp2/exp2_' cell.id '_frate.eps'],'-depsc');

    % save data
    miss_ = mean(cell.fmiss,2).';
    confhigh_ = mean(cell.fhit_confhigh,2).';
    conflow_ = mean(cell.fhit_conflow,2).';
    
    sel = t>= -0.5 & t < 1.5;
    tbl = table(t(sel).',confhigh_(sel).',conflow_(sel).',miss_(sel).','VariableNames',{'time','confhigh','conflow','miss'});
    
    writetable(tbl,'../../supdata/Fig.2b_tmp.xls');
    %%
     
    h = figure(112); clf;
    hold on;
    raster(cell.raster.',t,[conf.color.miss2;conf.color.miss1;conf.color.hit2;conf.color.hit3],2)
    xlim([-0.5,1.5])
    plot([0 0],ylim(),'k--');
    %title(sprintf('firing rate: %.1f',allfire(n)));
   
    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:20:100,'YTickLabel',{});
    set(gcf,'Position',figsz+[160,0,0,0])
    %axis off
    
    print(['figs/exp2/exp2_' cell.id '_raster.eps'],'-depsc');

    %%
   
    %%
    h = figure(120); clf; hold on;
    t2 = linspace(-1,2,256);
    plot(t2,cell.ap,'k','LineWidth',lw)
    plot([t2 fliplr(t2)],[(cell.ap+cell.apstd) fliplr(cell.ap-cell.apstd)],'k','LineWidth',1)
    plot([1 1],[-100,-50],'k','LineWidth',lw);
    plot([1,2],[-50,-50],'k','LineWidth',lw);
    set(gca,'XTick',-1:2,'XTickLabel',{},'YTick',0:50:100,'YTickLabel',{});
    set(gcf,'Position',[260,600,150,100])
    axis off
    print(['figs/exp2/exp2_' cell.id '_ap.eps'],'-depsc');

    pause(0.1);
end

