%========================================================
%   Experiment 2 - random permutations
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the null distributions of the number
% of responsive neurons 

clear

rng('default');

addpath ../conf
addpath ../functions/
conf.exp = 2;
conf.session = 1;
config_spikes;

docue = 1;
conf.modelcomp = 0;

frate = 0.1;
d = dir(['../conf/neurons/e2*_gauss' num2str(frate) '_stim.mat']);
t = linspace(-0.5,1.5,2001);

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));
fprintf('     ');
for rnd = 1:conf.nperm
    fprintf('\b\b\b\b\b[%3d]',rnd);
    ch = 0;
    
    for n=1:length(d)
        %%
        neuron = load(['../conf/neurons/' d(n).name]);
        x = strsplit(d(n).name,'-');
        y = strsplit(x{2},'_');
        
        frate = neuron.frate;
        
        % get behav & ids
        id = getid(neuron.behav,conf.badtrials);
        
        % stats
        rndcells(n,rnd).exp = 2;
        rndcells(n,rnd).ses = neuron.stat.session;
        rndcells(n,rnd).id = y{1};
        rndcells(n,rnd).keep = sum(id.idall);
        rndcells(n,rnd).sua = neuron.stat.sua;
        rndcells(n,rnd).fire = neuron.stat.fire;
        rndcells(n,rnd).isi = neuron.stat.isi;
        rndcells(n,rnd).cv2 = neuron.stat.cv2;
        rndcells(n,rnd).bi = neuron.stat.bi;
        rndcells(n,rnd).snr = neuron.stat.snr;
        rndcells(n,rnd).spikeamp = neuron.stat.spikeamp;
        rndcells(n,rnd).peakvar = neuron.stat.peakvar;
        rndcells(n,rnd).ap = neuron.ap;
        rndcells(n,rnd).apstd = neuron.apstd;
        
        % detection test
        nspikes = sum(neuron.raster(selstat,:),1).';
        nspikesbl = (sum(neuron.raster(selbaseline,:),1).')./0.3;
        nsp_ = [nspikes; nspikesbl];

        % permute
        perm = randperm(length(nspikes));
        nspikes = nspikes(perm);
        perm2 = randperm(length(nsp_));
        nsp_ = nsp_(perm2);
        nspikes_ = nsp_(1:length(nspikes));
        nspikesbl_ = nsp_(length(nspikes)+1:end);
        
        [rndcells(n,rnd).corrconf,rndcells(n,rnd).pcorrconf] = corr(nspikes,id.conf,'type','Spearman');
        [rndcells(n,rnd).corrconf_hit,rndcells(n,rnd).pcorrconf_hit] = corr(nspikes(id.idhit),id.conf(id.idhit),'type','Spearman');
        [rndcells(n,rnd).corrconf_miss,rndcells(n,rnd).pcorrconf_miss] = corr(nspikes(id.idmiss),id.conf(id.idmiss),'type','Spearman');
        
        rndcells(n,rnd).isdetect2 = ranksum(nspikes(id.idhit),nspikes(id.idmiss));
        rndcells(n,rnd).isconfhit2 = ranksum(nspikes(id.idhitch),nspikes(id.idhitcl));
        rndcells(n,rnd).isconfmiss2 = ranksum(nspikes(id.idmissch),nspikes(id.idmisscl));
        rndcells(n,rnd).isconfall2 = ranksum(nspikes(id.idhitch | id.idmissch),nspikes(id.idhitcl | id.idmisscl));
        
        rndcells(n,rnd).ishitsig = signtest(nspikes_(id.idhit),nspikesbl_(id.idhit));
        rndcells(n,rnd).ismisssig = signtest(nspikes_(id.idmiss),nspikesbl_(id.idmiss));
        rndcells(n,rnd).isallsig = signtest(nspikes_(id.idstim),nspikesbl_(id.idstim));
        
        rndcells(n,rnd).spikes_base = sum(nspikesbl_(id.idstim))/sum(id.idstim);
        rndcells(n,rnd).spikes_hit = sum(nspikes_(id.idhit))/sum(id.idhit);
        rndcells(n,rnd).spikes_hitch = sum(nspikes_(id.idhitch))/sum(id.idhitch);
        rndcells(n,rnd).spikes_hitcl = sum(nspikes_(id.idhitcl))/sum(id.idhitcl);
  
        rndcells(n,rnd).spikes_miss = sum(nspikes_(id.idmiss))/sum(id.idmiss);
        rndcells(n,rnd).spikes_missch = sum(nspikes_(id.idmissch))/sum(id.idmissch);
        rndcells(n,rnd).spikes_misscl = sum(nspikes_(id.idmisscl))/sum(id.idmisscl);
        
        tbl = table(nspikes(id.idstim),(id.idhit(id.idstim)),(id.trueconf(id.idstim)),(id.conf(id.idstim)),'VariableNames',{'nspikes','resp','conf','binconf'});
        
        % same model
        model = respconfmodel(tbl,conf);
        for f = fieldnames(model).'
            rndcells(n,rnd).(cell2mat(f)) = model.(cell2mat(f));
        end
        
        % choice probabilities
        [~,~,~,auc_detect] = perfcurve(tbl.resp,tbl.nspikes,1);
        [~,~,~,auc_conf] = perfcurve(tbl.binconf,tbl.nspikes,1);
        [~,~,~,auc_conf_hit] = perfcurve(tbl.binconf(tbl.resp==1),tbl.nspikes(tbl.resp==1),1);
        [~,~,~,auc_conf_miss] = perfcurve(tbl.binconf(tbl.resp==0),tbl.nspikes(tbl.resp==0),0);
        
        rndcells(n,rnd).cp_detect = 2*auc_detect-1;
        rndcells(n,rnd).cp_conf = 2*auc_conf-1;
        rndcells(n,rnd).cp_conf_hit = 2*auc_conf_hit-1;
        rndcells(n,rnd).cp_conf_miss = 2*auc_conf_miss-1;
        
    end
    if mod(rnd,100) == 0
        save('save/rndcells_exp2.mat','rndcells');
    end
end
fprintf('\n [ analysis ] DONE\n');
rndconfsave = conf;
save('save/rndcells_exp2.mat','rndcells','rndconfsave','-v7.3');