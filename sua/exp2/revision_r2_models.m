%========================================================
%   Experiment 2 - rebuttal
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script simulates SUA firing rates for different models 
% for the responses to reviewer 2

clear;
addpath ../conf
addpath ../functions
conf = [];
conf.version = 1;
conf.session = 1;
config_spikes

mcomp = 0;

nrep = 200; % number of repetitions
n = [6,16,30 ; 8,35,8]; % trial balance (copied from data)
baserate = 10; % baseline firing rate in Hz
alleff = 0.2; % effect size

ci = @(x) std(x)./sqrt(length(x));
%%
% conditions
allbr =  [0, 1, 0,   0  ]; % resp
allbc =  [0, 0, 0.5, 0  ]; % conf
allbrc = [0, 0, 0,   0.5]; % resp x conf

ntyp = length(allbr);
tit = {'0','D','C','D x C'};

for eff = 1:length(alleff)
    figure(); clf;
    
    % preallocate
    pval = nan(ntyp,4,nrep);
    pval_detect = nan(ntyp,nrep);
    pval_confhit = nan(ntyp,nrep);
    pval_confmiss = nan(ntyp,nrep);
    pval_confall = nan(ntyp,nrep);
    
    % for all conditions (no effect / resp / conf / resp x conf)
    for typ = 1:ntyp
        % multiply baseline firing rate with effect size and condition
        br = baserate*alleff(eff)*allbr(typ);
        bc = baserate*alleff(eff)*allbc(typ);
        bcr = baserate*alleff(eff)*allbrc(typ);
        
        rng('default'); % for reproducibility
        
        % iterate
        for i=1:nrep
            fr = []; resp = []; confid = [];
            % for all levels of confidence
            for c=0:2
                % for all responses
                for r=0:1
                    rt = r;
                    ct = c;
                    % set firing rate
                    rate(r+1,c+1) = baserate+(rt)*br+(ct)*bc+(ct)*(rt)*bcr;
                    % generate Poisson spike count and aggregate
                    fr = [fr poissrnd(rate(r+1,c+1),[1,n(r+1,c+1)])];
                    % aggregate resp and conf
                    resp = [resp r*ones([1,n(r+1,c+1)])];
                    confid = [confid (c)*ones([1,n(r+1,c+1)])];
                end
            end
            conf.modelcomp = mcomp;

            % fit GLM for interaction
            tbl = table(fr.',resp.'==1,confid.','VariableNames',{'nspikes','resp','conf'});
            m = fitglm(tbl,'nspikes ~ resp*conf','Distribution','poisson','Link','log');
            pval(typ,:,i) = m.Coefficients.pValue;
       
            % other approach: difference in spike rate between hits and
            % misses
            pval_detect(typ,i) = ranksum(tbl.nspikes(resp==1),tbl.nspikes(resp==0));
            % correlation conf / spike rate for hits
            [~,pval_confhit(typ,i)] = corr(tbl.nspikes(resp==1),tbl.conf(resp==1),'type','Spearman');
            
        end
        
        %% plot example simulated rate for resp and conf
        subplot(3,ntyp,typ); hold on;
        mhit = [mean(tbl.nspikes(tbl.resp==1 & tbl.conf==0)) mean(tbl.nspikes(tbl.resp==1 & tbl.conf==1)) mean(tbl.nspikes(tbl.resp==1 & tbl.conf==2))];
        mmiss = [mean(tbl.nspikes(tbl.resp==0 & tbl.conf==0)) mean(tbl.nspikes(tbl.resp==0 & tbl.conf==1)) mean(tbl.nspikes(tbl.resp==0 & tbl.conf==2))];
        
        cihit = [ci(tbl.nspikes(tbl.resp==1 & tbl.conf==0)) ci(tbl.nspikes(tbl.resp==1 & tbl.conf==1)) ci(tbl.nspikes(tbl.resp==1 & tbl.conf==2))];
        cimiss = [ci(tbl.nspikes(tbl.resp==0 & tbl.conf==0)) ci(tbl.nspikes(tbl.resp==0 & tbl.conf==1)) ci(tbl.nspikes(tbl.resp==0 & tbl.conf==2))];
        
        errorbar((1:3)-0.05,rate(2,:),sqrt(rate(2,:)),'o-','Color',conf.color.hit);
        errorbar((1:3)+0.05,rate(1,:),sqrt(rate(2,:)),'o-','Color',conf.color.miss);
        ylim([5,17])
        xlabel('Confidence');
        ylabel(['Sim. rate [eff=' num2str(alleff(eff)) ']']);
        title(tit{typ});
        
        %% plot #times p<0.05 for GLM
        
        subplot(3,ntyp,ntyp+typ); hold on;
        pr = squeeze(pval(typ,:,:)<0.05);
        b = bar(mean(pr(2:end,:),2));
        b.FaceColor = 'flat';
        b.CData(1,:) = conf.color.amp1;
        b.CData(2,:) = conf.color.amp4;
        b.CData(3,:) = conf.color.amp7;
        hold on;
        errorbar(1:3,mean(pr(2:end,:).'),ci(pr(2:end,:).'),'k.');
        plot([0,4],0.05*[1 1],'r-');
        ylim([0,1])
        set(gca,'XTick',1:3,'XTickLabel',{'D','C','DxC'},'YTick',0:0.2:1);
        xtickangle(90)
        xlabel('GLM betas');
        ylabel('P(p<0.05)');
        text(1,0.9,['N = ' num2str(nrep)]);
        
        %% plot #times p<0.05 for single tests
        subplot(3,ntyp,ntyp*2+typ); hold on;
        pr_d = pval_detect(typ,:).'<0.05;
        pr_c = pval_confhit(typ,:).'<0.05;
        pr_dc = pval_detect(typ,:).'<0.05 & pval_confhit(typ,:).'<0.05;
        
        b = bar([mean(pr_d),mean(pr_c),mean(pr_dc)]);
        b.FaceColor = 'flat';
        b.CData(1,:) = conf.color.amp1;
        b.CData(2,:) = conf.color.amp4;
        b.CData(3,:) = conf.color.amp7;
        hold on;
        errorbar(1:3,[mean(pr_d),mean(pr_c),mean(pr_dc)],[ci(pr_d),ci(pr_c),ci(pr_dc)],'k.');
        plot([0,4],0.05*[1 1],'r-');
        ylim([0,1])
        set(gca,'XTick',1:3,'XTickLabel',{'D','C(hit)','D & C(hit)'},'YTick',0:0.2:1);
        xtickangle(90)
        ylabel('P(p<0.05)');
        text(1,0.9,['N = ' num2str(nrep)]);
        
        pause(0.1);
    end
    set(gcf,'Position',[500,600,800,450])
end