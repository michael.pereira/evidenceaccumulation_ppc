%========================================================
%   Experiment 2 - ECoG grid analysis
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This script performs the analyses of the ECoG grid

addpath ../conf
addpath ../functions/

conf.exp = 2;
conf.session = 1;
config_spikes;
addpath(genpath(conf.dir.toolbox_ielvis));

fs = conf.ecog.fs;%/dwnsamp;

global globalFsDir;
globalFsDir=conf.dir.brainmodel;

for type = {'lfp','hga'} % 'lfp' or 'hga' for high gamma
    
    % select
    if strcmp(type,'lfp')
        sel = 'trlfp_sm'; % load smoothed version
        selstat = 'trlfp'; % but do stats on the non-smoothed
    elseif strcmp(type,'hga')
        sel = 'trhga';
        selstat = 'trhga';
    else
        error('Unknown type');
    end
    
    f1 = [conf.dir.preprocdir '/e2s1/ecog' sel '_e2s1.mat'];
    fprintf('Loading %s\n',f1);
    d1 = load(f1);
    trials = d1.(sel);
    
    f1stat = [conf.dir.preprocdir '/e2s1/ecog' selstat '_e2s1.mat'];
    fprintf('Loading %s\n',f1stat);
    d1stat = load(f1stat);
    trialsstat = d1stat.(selstat);
    %sdt = d1.beh.sdt;
    
    selid = 0.5*fs+(1:2*fs);
    
    statid = 1.5*fs+(1:1*fs);
    
    d = dir(['../conf/neurons/e2*_gauss0.1_stim.mat']);
    neuron = load(['../conf/neurons/' d(1).name]);
    confidence = neuron.behav.conf;
    sdt = neuron.behav.sdt;
    idxstim = strcmp(sdt,'hit') | strcmp(sdt,'miss');
    
    idxhit = strcmp(sdt,'hit');
    idxhit1 = strcmp(sdt,'hit') & (confidence < 3);
    idxhit2 = strcmp(sdt,'hit') & (confidence == 3);
    idxmiss = strcmp(sdt,'miss');
    
    idxstim(conf.allbadtrials) = 0;
    idxhit(conf.allbadtrials) = 0;
    idxhit1(conf.allbadtrials) = 0;
    idxhit2(conf.allbadtrials) = 0;
    idxmiss(conf.allbadtrials) = 0;
    
    fprintf('hits: %d, misses: %d\n',sum(idxhit),sum(idxmiss));
    %%
    tr = [];
    
    [ntr,nt,nchan] = size(trials);
    t = linspace(-1,2,nt);
    figure(250); clf;
    dif = zeros(1,nchan);
    initrow = 21; row = initrow;
    for ch=1:nchan
        subplot(6,4,row); hold on;
        
        row = row - 4;
        if row < 1
            initrow = initrow+1;
            row = initrow;
        end
        
        hit1 = trials(idxhit1,:,ch);
        hit2 = trials(idxhit2,:,ch);
        miss = trials(idxmiss,:,ch);
        
        avg = mean(trialsstat(idxstim,statid,ch),2);
        tbl = table(avg,sdt(idxstim).',confidence(idxstim).','VariableNames',{'avg','resp','conf'});
        if strcmp(type,'lfp')
            m = fitlm(tbl,'avg ~ resp*conf');
        else
            m = fitglm(tbl,'avg ~ resp*conf','Distribution','gamma','Link','log');
        end
        coeff(ch) = m.Coefficients.Estimate(4);
        pval(ch) = m.Coefficients.pValue(4);
        
        lw = 1.5;
        
        cishade(miss,0.2,conf.color.miss,t,'LineWidth',lw);
        cishade(hit1,0.2,conf.color.hit2,t,'LineWidth',lw);
        cishade(hit2,0.2,conf.color.hit3,t,'LineWidth',lw);
        
        xlim([-0.5,1.5]);
        % ylim([-120,100]);
        plot([0 0],ylim(),'k--');
        set(gca,'XTick',-0.5:0.5:1.5);
        title(sprintf('ch %d',ch));
        xlabel('Time [s]');
        ylabel('Amplitude [{\mu}V]');
        
        pause(0.1);
    end
    set(gcf,'Position',[100,100,600,600])
    print(['figs/exp2_' type{1} '_allelecs.eps'],'-depsc');
    
    if 1
        %%
        
        elecNames=cell(nchan,1);
        for a=1:nchan
            elecNames{a}=sprintf('G%d',a);
        end
        
        %    coeff2(pval > 0.05/24) = -39;
        col = hot(51);
        %colors = col(40+min(round(coeff2),40),:);
        colors = col(1+min(round(abs(coeff)),50),:);
        
        colors(pval > 0.05/24,:) = 0;
        colors = [colors ; [76,168,41]./255];
        %elecNames(pval > 0.05) = [];
        elecNames{end+1} = 'Utah1';
        
        cfg = [];
        %cfg.surfType = 'pial';
        cfg.figId=102;
        cfg.elecNames=elecNames;
        cfg.pullOut=0;
        cfg.bidsDir = conf.dir.data;
        cfg.elecColors=colors;%[-coeff2.' ; 0];
        cfg.elecColorScale=[-40,40];
        cfg.elecCoord='LEPTO';
        cfg.elecCbar = 'n';
        cfg.showLabels='n';
        cfg.elecUnits='r';
        cfg.title='';
        cfg.view = 'l';
        cfg.elecSize=3;
        cfg.elecShape='sphere';
        %cfg.opaqueness = 0.7;
        
        figure();
        plotPialSurf('patient',cfg);
        view(-68,46);
        set(gcf,'Position',[100,100,800,800])
        print(['figs/exp2_' type{1} '_pial.png'],'-dpng');
    end
    
    
    
    if 1
        %%
        %for
        ch = [14]
        figure(); hold on;
        row = row - 4;
        if row < 1
            initrow = initrow+1;
            row = initrow;
        end
        
        hit = trials(idxhit,:,ch);
        hit1 = trials(idxhit1,:,ch);
        hit2 = trials(idxhit2,:,ch);
        miss = trials(idxmiss,:,ch);
        
        
        lw = 1;
        
        cishade(miss(:,selid),[],conf.color.miss,t(selid),'LineWidth',lw);
        
        cishade(hit1(:,selid),[],conf.color.hit2,t(selid),'LineWidth',lw);
        cishade(hit2(:,selid),[],conf.color.hit3,t(selid),'LineWidth',lw);
        
        plot(xlim(),[0 0],'k-');
        
        xlim([-0.5,1.5]);
        % ylim([-120,100]);
        plot([0 0],ylim(),'k--');
        set(gca,'XTick',-0.5:0.5:1.5);
        
        set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-100:20:100,'YTickLabel',{});
        set(gcf,'Position',[100,100,150,120])
        print(['figs/exp2_' type{1} '_ch ' num2str(ch) '.eps'],'-depsc');
        
        % save data
        miss_ = mean(miss(:,selid),1);
        hit1_ = mean(hit1(:,selid),1);
        hit2_ = mean(hit2(:,selid),1);
        
        tbl = table(t(selid).',hit1_.',hit2_.',miss_.','VariableNames',{'time','hit_lowconf','hit_highconf','miss'});
        writetable(tbl,'../../supdata/Fig.2e.xls');
        
        pause(0.1);
        % end
    end
end