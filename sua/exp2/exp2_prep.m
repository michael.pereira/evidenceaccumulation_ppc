%========================================================
%   Experiment 2 - responsive neurons
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the stats for each SUA and MUA and stores
% firing rates and rasters

clear

addpath ../admin
addpath ../functions/
conf.exp = 2;
conf.session = 1;
config_spikes;

docue = 1;
conf.modelcomp = 0;

frate = 0.1;
d = dir(['../conf/neurons/e2*_gauss' num2str(frate) '_stim.mat']);
t = linspace(-0.5,1.5,2001);

% window to do the stats
selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));

fprintf('     ');
for n=1:length(d)
    fprintf('\b\b\b\b\b[%3d]',n);
    %%
    neuron = load(['../conf/neurons/' d(n).name]);
    x = strsplit(d(n).name,'-');
    y = strsplit(x{2},'_');
    
    frate = neuron.frate;
  
    % get behav & ids
    id = getid(neuron.behav,conf.badtrials);
 
    % stats
    cells(n).exp = 2;
    cells(n).ses = neuron.stat.session;
    cells(n).id = y{1};
    cells(n).keep = sum(id.idall);
    cells(n).sua = neuron.stat.sua;
    cells(n).fire = neuron.stat.fire;
    cells(n).isi = neuron.stat.isi;
    cells(n).cv2 = neuron.stat.cv2;
    cells(n).bi = neuron.stat.bi;
    cells(n).snr = neuron.stat.snr;
    cells(n).spikeamp = neuron.stat.spikeamp;
    cells(n).peakvar = neuron.stat.peakvar;
    cells(n).ap = neuron.ap;
    cells(n).apstd = neuron.apstd;
       
    % spike counts
    nspikes = sum(neuron.raster(selstat,:),1).';
    nspikesbl = (sum(neuron.raster(selbaseline,:),1).')./0.3;
    
    % correlation with confidence
    [cells(n).corrconf,cells(n).pcorrconf] = corr(nspikes,id.conf,'type','Spearman');
    [cells(n).corrconf_hit,cells(n).pcorrconf_hit] = corr(nspikes(id.idhit),id.conf(id.idhit),'type','Spearman');
    [cells(n).corrconf_miss,cells(n).pcorrconf_miss] = corr(nspikes(id.idmiss),id.conf(id.idmiss),'type','Spearman');
    
    % differences 
    cells(n).isdetect2 = ranksum(nspikes(id.idhit),nspikes(id.idmiss));
    cells(n).isconfhit2 = ranksum(nspikes(id.idhitch),nspikes(id.idhitcl));
    cells(n).isconfmiss2 = ranksum(nspikes(id.idmissch),nspikes(id.idmisscl));
    cells(n).isconfall2 = ranksum(nspikes(id.idhitch | id.idmissch),nspikes(id.idhitcl | id.idmisscl));
    
    % test vs. baseline
    cells(n).ishitsig2 = signtest(nspikes(id.idhit),nspikesbl(id.idhit));
    cells(n).ismisssig2 = signtest(nspikes(id.idmiss),nspikesbl(id.idmiss));
    cells(n).isallsig2 = signtest(nspikes(id.idstim),nspikesbl(id.idstim));
    
    % spike rates
    cells(n).spikes_base = sum(nspikesbl(id.idstim))/sum(id.idstim);
    cells(n).spikes_hit = sum(nspikes(id.idhit))/sum(id.idhit);
    cells(n).spikes_hitch = sum(nspikes(id.idhitch))/sum(id.idhitch);
    cells(n).spikes_hitcl = sum(nspikes(id.idhitcl))/sum(id.idhitcl);
    
    cells(n).spikes_miss = sum(nspikes(id.idmiss))/sum(id.idmiss);
    cells(n).spikes_missch = sum(nspikes(id.idmissch))/sum(id.idmissch);
    cells(n).spikes_misscl = sum(nspikes(id.idmisscl))/sum(id.idmisscl);
    
    % fit GLM
    tbl = table(nspikes(id.idstim),(id.idhit(id.idstim)),(id.trueconf(id.idstim)),(id.conf(id.idstim)),'VariableNames',{'nspikes','resp','conf','binconf'});
    model = respconfmodel(tbl,conf);
    for f = fieldnames(model).'
       cells(n).(cell2mat(f)) = model.(cell2mat(f));
    end
    
    % choice probabilities
    [~,~,~,auc_detect] = perfcurve(tbl.resp,tbl.nspikes,1); 
    [~,~,~,auc_conf] = perfcurve(tbl.binconf,tbl.nspikes,1);
    [~,~,~,auc_conf_hit] = perfcurve(tbl.binconf(tbl.resp==1),tbl.nspikes(tbl.resp==1),1);
    [~,~,~,auc_conf_miss] = perfcurve(tbl.binconf(tbl.resp==0),tbl.nspikes(tbl.resp==0),0);
    
    cells(n).cp_detect = 2*auc_detect-1;
    cells(n).cp_conf = 2*auc_conf-1;
    cells(n).cp_conf_hit = 2*auc_conf_hit-1;
    cells(n).cp_conf_miss = 2*auc_conf_miss-1;

    % store raster
    cells(n).raster = [neuron.raster(:,id.idmissch),neuron.raster(:,id.idmisscl)*2, ...
        neuron.raster(:,id.idhitcl)*3 neuron.raster(:,id.idhitch)*4 ];
    
    % store firing rates
    cells(n).fhit = frate(:,id.idhit);
    cells(n).fhit_confhigh = frate(:,id.idhitch);
    cells(n).fhit_conflow = frate(:,id.idhitcl);

    cells(n).fmiss = frate(:,id.idmiss);
    cells(n).fmiss_confhigh = frate(:,id.idmissch);
    cells(n).fmiss_conflow = frate(:,id.idmisscl);

    % store average firing rates
    s = mean(mean(frate(selbaseline,:)));
    cells(n).avgfhit = mean(frate(:,id.idhit)./s,2);
    cells(n).avgfhit_confhigh = mean(frate(:,id.idhitch)./s,2);
    cells(n).avgfhit_conflow = mean(frate(:,id.idhitcl)./s,2);

    cells(n).avgfmiss = mean(frate(:,id.idmiss)./s,2);
    cells(n).avgfmiss_confhigh = mean(frate(:,id.idmissch)./s,2);
    cells(n).avgfmiss_conflow = mean(frate(:,id.idmisscl)./s,2);

    if docue
        % not in the paper
        neuron_cue = load(['../conf/neurons/' d(n).name(1:end-9) '_cue.mat']);
        id.idhit(id.on > 1) = 0;
        id.idmiss(id.on > 1) = 0;
        nspikes_cue = sum(neuron_cue.raster(3001:3500,:),1).';
        nspikes_cue_base = sum(neuron_cue.raster(2501:3000,:),1).';
        
        cells(n).delay_ismod = ranksum(nspikes_cue(id.idhit),nspikes_cue(id.idmiss));
        cells(n).delay_ishit = signtest(nspikes_cue(id.idhit),nspikes_cue_base(id.idhit));
        cells(n).delay_ismiss = signtest(nspikes_cue(id.idmiss),nspikes_cue_base(id.idmiss));
        
        cells(n).delay_fhit = neuron_cue.frate(:,id.idhit);
        cells(n).delay_fmiss = neuron_cue.frate(:,id.idmiss);
        cells(n).delay_fhit_confhigh = neuron_cue.frate(:,id.idhitch);
        cells(n).delay_fhit_conflow = neuron_cue.frate(:,id.idhitcl);
       
        s = mean(mean(neuron_cue.frate(2501:3000,:)));
        cells(n).delay_avgfhit = mean(neuron_cue.frate(:,id.idhit)./s,2);
        cells(n).delay_avgfhit_confhigh = mean(neuron_cue.frate(:,id.idhitch)./s,2);
        cells(n).delay_avgfhit_conflow = mean(neuron_cue.frate(:,id.idhitcl)./s,2);
        cells(n).delay_avgfmiss = mean(neuron_cue.frate(:,id.idhitcl)./s,2);

    end

end

fprintf('\n [ analysis ] DONE\n');
confsave = conf;
save('save/cells_exp2.mat','cells','confsave','-v7.3');