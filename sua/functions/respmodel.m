function [model] = respmodel(tbl,conf)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if conf.modelcomp == 0
    m = fitglm(tbl,'nspikes ~ resp','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    model.isdetect = m.Coefficients.pValue(2);
    model.coeffdetect = m.Coefficients.Estimate(2);
    model.model = 1;
elseif conf.modelcomp == 1
    m = fitglm(tbl,'nspikes ~ resp','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    if model.ismod < 0.05
        model.isdetect = m.Coefficients.pValue(2);
        model.coeffdetect = m.Coefficients.Estimate(2);
        model.model = 1;
    else
        model.isdetect = NaN;
        model.coeffdetect = NaN;
        model.model = 0;
    end
elseif conf.modelcomp == 2
 
    
    m0 = fitglm(tbl,'nspikes ~ 1','Distribution','poisson','Link','log');
    m1 = fitglm(tbl,'nspikes ~ resp','Distribution','poisson','Link','log');
    
    crit = [m0.ModelCriterion.(conf.modelcrit) m1.ModelCriterion.(conf.modelcrit) ...
        ];
    [~,bestmodel] = min(crit);
    
    tst = m1.devianceTest();
    model.ismod = tst.pValue(2);
    
    model.model = bestmodel-1;
    switch model.model
        
        case 1 % nspikes ~ resp
            model.isdetect = m1.Coefficients.pValue(2);
            model.coeffdetect = m1.Coefficients.Estimate(2);
        otherwise
            model.isdetect = NaN;
            model.coeffdetect = NaN;
    end
    
end

end

