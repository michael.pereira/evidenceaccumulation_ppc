function [rmat,bad] = realign(mat,irt,win)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
irt = round(irt);
[nt,ntr] = size(mat);
bad = false(1,ntr);
j2 = 0;
for j = 1:ntr
    if irt(j)+win(1) > 0 && irt(j)+win(2)<=nt
        j2 = j2+1;
        rmat(:,j2) = mat(irt(j)+(win(1):win(2)),j);
    else
        bad(j) = 1;
    end
end

