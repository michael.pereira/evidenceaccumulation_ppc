function [h,p,chi2] = mcnemar(tbl)
%MCNEMAR Summary of this function goes here
%   Detailed explanation goes here
b = tbl(1,2);
c = tbl(2,1);

chi2 = (abs(b-c)-1).^2./(b+c);
p = 1-chi2cdf(chi2,1);
h = p<0.05;
end

