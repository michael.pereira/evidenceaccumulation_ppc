function [model] = respconfmodel(tbl,conf)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
if conf.modelcomp == -1
    m = stepwiseglm(tbl,'nspikes ~ resp*conf','Distribution','poisson','Link','log');
%    tst = m.devianceTest();
    model.ismod = NaN;% tst.pValue(2);
    
    idresp = strcmp(m.CoefficientNames,'resp_1');
    idconf = strcmp(m.CoefficientNames,'conf');
    idint = strcmp(m.CoefficientNames,'resp_1:conf');
    
    model.isdetect = NaN;
    model.isconf = NaN;
    model.isboth = NaN;
    model.coeffdetect = NaN;
    model.coeffconf = NaN;
    model.coeffboth = NaN;
    model.model = 1;
    if any(idresp)
        model.isdetect = m.Coefficients.pValue(idresp);
        model.coeffdetect = m.Coefficients.Estimate(idresp);
    end
    if any(idconf)
        model.isconf = m.Coefficients.pValue(idconf);
        model.coeffconf = m.Coefficients.Estimate(idconf);
    end
    if any(idint)
        model.isboth = m.Coefficients.pValue(idint);
        model.coeffboth = m.Coefficients.Estimate(idint);
    end
elseif conf.modelcomp == 0
    m = fitglm(tbl,'nspikes ~ resp*conf','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    model.isdetect = m.Coefficients.pValue(2);
    model.isconf = m.Coefficients.pValue(3);
    model.isboth = m.Coefficients.pValue(4);
    model.coeffdetect = m.Coefficients.Estimate(2);
    model.coeffconf = m.Coefficients.Estimate(3);
    model.coeffboth = m.Coefficients.Estimate(4);
    model.model = 5;
elseif conf.modelcomp == 1
    tbl2 = tbl; tbl2.conf = ordinal(tbl.conf);
    m = fitglm(tbl2,'nspikes ~ resp*conf','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    model.isdetect = m.Coefficients.pValue(2);
    model.isconf = min(m.Coefficients.pValue(3:4)*2);
    model.isboth = min(m.Coefficients.pValue(5:6)*2);
    model.coeffdetect = m.Coefficients.Estimate(2);
    model.coeffconf = m.Coefficients.Estimate(3);
    model.coeffboth = m.Coefficients.Estimate(4);
    model.model = 5;
   
else
    
    m0 = fitglm(tbl,'nspikes ~ 1','Distribution','poisson','Link','log');
    m1 = fitglm(tbl,'nspikes ~ resp','Distribution','poisson','Link','log');
    m2 = fitglm(tbl,'nspikes ~ conf','Distribution','poisson','Link','log');
    m3 = fitglm(tbl,'nspikes ~ resp:conf','Distribution','poisson','Link','log');
    %m4 = fitglm(tbl,'nspikes ~ resp+conf','Distribution','poisson','Link','log');
    %m5 = fitglm(tbl,'nspikes ~ resp*conf','Distribution','poisson','Link','log');
    
    crit = [m0.ModelCriterion.(conf.modelcrit) m1.ModelCriterion.(conf.modelcrit) ...
        m2.ModelCriterion.(conf.modelcrit) m3.ModelCriterion.(conf.modelcrit) ...
       ];
   
   % m4.ModelCriterion.(conf.modelcrit) m5.ModelCriterion.(conf.modelcrit) ...
       % ];
    [~,bestmodel] = min(crit);
    
    tst = m2.devianceTest();
    model.ismod = tst.pValue(2);
    model.crit = crit;
    model.model = bestmodel-1;
    switch model.model
        case 5 % nspikes ~ resp*conf
            model.isdetect = m5.Coefficients.pValue(2);
            model.isconf = m5.Coefficients.pValue(3);
            model.isboth = m5.Coefficients.pValue(4);
            model.coeffdetect = m5.Coefficients.Estimate(2);
            model.coeffconf = m5.Coefficients.Estimate(3);
            model.coeffboth = m5.Coefficients.Estimate(4);
        case 4 % nspikes ~ resp+conf
            model.isdetect = m4.Coefficients.pValue(2);
            model.isconf = m4.Coefficients.pValue(3);
            model.isboth = NaN;
            model.coeffdetect = m4.Coefficients.Estimate(2);
            model.coeffconf = m4.Coefficients.Estimate(3);
            model.coeffboth = NaN;
        case 3 % nspikes ~ resp:conf
            model.isdetect = NaN;
            model.isconf = NaN;
            model.isboth = m3.Coefficients.pValue(2);
            model.coeffdetect = NaN;
            model.coeffconf = NaN;
            model.coeffboth = m3.Coefficients.Estimate(2);
        case 2 % nspikes ~ conf
            model.isdetect = NaN;
            model.isconf = m2.Coefficients.pValue(2);
            model.isboth = NaN;
            model.coeffdetect = NaN;
            model.coeffconf = m2.Coefficients.Estimate(2);
            model.coeffboth = NaN;
        case 1 % nspikes ~ resp
            model.isdetect = m1.Coefficients.pValue(2);
            model.isconf = NaN;
            model.isboth = NaN;
            model.coeffdetect = m1.Coefficients.Estimate(2);
            model.coeffconf = NaN;
            model.coeffboth = NaN;
        otherwise
            model.isdetect = NaN;
            model.isconf = NaN;
            model.isboth = NaN;
            model.coeffdetect = NaN;
            model.coeffconf = NaN;
            model.coeffboth = NaN;
    end
    
end

end

