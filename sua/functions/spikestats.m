function [stat] = spikestats(ch,clid,data,conf,spikes,production)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 6
    production = 0;
end
if nargin < 5
    spikefile = [conf.dir.output '/sort/'  num2str(conf.clust.usethr(ch)) '/A' num2str(ch) '_sorted_new.mat'];
    spikes = load(spikefile);
end
if nargin < 4
    config_paths;
end

stat = struct();

fs = 30e3;
dwn = 30;
fsr = fs/dwn;

nt = size(spikes.allSpikesCorrFree,2);
selneuron = ismember(spikes.assignedNegative,clid);
template = spikes.newSpikesNegative(selneuron,:);

% get the index of firing of this neuron
idx = ismember(spikes.assignedNegative,clid);
t = fs*(spikes.newTimestampsNegative(idx))*1e-6;
spiketime = (spikes.newTimestampsNegative(idx))*1e-6;

% put indexes in a sparse vector
x = zeros(1,ceil(max(t)/dwn));
x(ceil(t/dwn)) = 1;


    stat.p_plv = NaN;



% moving average filter to compute firing rate
win = round(1*fsr);
y = filter(1/win*ones(1,win),1,x);
% compensate introduced delay
y = y(round(win/2):end)*fsr;

timestamp = (1:length(y))./fsr;
keep = true(size(timestamp));
allrmsp = [];
for b = 1:size(conf.badseg.times,1)
   rm = find(timestamp >= conf.badseg.times(b,1)-1 & timestamp <= conf.badseg.times(b,2)+1);
   keep(rm) = false(1);
   rmsp = find(spiketime >= conf.badseg.times(b,1)-1 & spiketime <= conf.badseg.times(b,2)+1);
   allrmsp = [allrmsp rmsp];
end
t(allrmsp) = [];
template(allrmsp,:) = [];
%y(1:(650*fsr)) = [];

% ISI
dt = diff(t)./fs;

ntr = length(data.trig);
 
%    trraster = zeros(1+round(fsr*0.8),ntr);   
%     for tr=1:ntr
%         trraster(:,tr) = x(round(data.trig(tr)/dwn)+round(0*fsr:round(0.8*fsr)));
%     end
%     dt = diff(find(trraster(:)))./fsr;
stat.badisi = mean(dt<3e-3);
stat.cv = std(dt)./mean(dt);
stat.cv2 = mean(2*abs(dt(2:end)-dt(1:end-1))./(dt(2:end)+dt(1:end-1)));
stat.firingrate = mean(y(keep));
mtemp = mean(template,1);
stat.amplitude = -(min(mtemp) - mtemp(1));
stat.burstindex = mean(dt<10e-3);
%%
if ~production
    figure(100); clf; subplot(2,3,1); hold on;
else
    figure(210); clf; hold on;
end
% plot template
m = min(size(template,1),100);
stat.snr = abs(mean(template(:,95)))./spikes.stdEstimateOrig;
stat.peakvar = std(template(:,95))./abs(mean(template(:,95)));


if ~production
plot(linspace(-1,2,256),template(1:m,:),'c','LineWidth',1);
plot(linspace(-1,2,256),mean(template),'k','LineWidth',2);
xlabel('Time [ms]');
ylabel('Amplitude [{\mu}V]');
title(sprintf('Neuron %d',clid(1)))
yl = ylim();
text(0.05,yl(1)+diff(yl)/2,sprintf('SNR: %.1f',stat.snr));

else
    stdshade(template,[],'k',linspace(-1,2,256),'LineWidth',2);
    set(gca,'XTick',-1:1:2,'XTickLabel',{},'YTick',-1000:100:1000,'YTickLabel',{});
    plot([-1,0],100*[1 1],'k','LineWidth',2)
    plot([-0.7,-0.7],[-100,0],'k','LineWidth',2)
    set(gcf,'Position',[50,100,250,200])
    figure();
end


subplot(2,3,2); hold on;



% plot ISI histogram
hist(dt,0:0.01:1);
xlim([0,0.5]);


mu = 1/stat.firingrate;
%mo = mode(diff(t)/fs);

plot(mu*[1 1],ylim(),'r');
% plot(mo*[1 1],ylim(),'r--');
title(sprintf('FR %.2f Hz',stat.firingrate));



subplot(2,3,3); hold on;
% plot ISI histogram
xhist = 1:1000;
hist(1e3*dt,xhist);
xlim([0,10]);
%plot(1/mean(y)*[1 1],ylim(),'r');
xlabel('Inter-spike interval [ms]');
plot([3 3],ylim(),'g--');
yl = ylim();
title(sprintf('ISI<3 ms: %.2f %%',100*stat.badisi));




subplot(2,3,4);
[p,f] = pwelch(x,fsr,fsr/2,fsr,fsr);
plot(f(1:40),20*log10(p(1:40)),'k','LineWidth',2);
xlabel('Frequency [hz]');
ylabel('Power [dB]');
xlim([0,40])

subplot(2,3,5);
hist(template(:,95),100)
xlabel('Align [\mu{V}]');

subplot(2,3,6); hold on;
ysm = smooth(y,fsr*10);
ysm(~keep) = 0;

bisi = ysm*0;
bisi(round(t(dt<3e-3)/dwn)) =1 ;
badisi = smooth(bisi,fsr*10);
badisi = badisi./max(badisi)*5;

plot((1:length(ysm))./fsr,ysm);
plot((1:length(badisi))./fsr,badisi);

pause(0.1);

end

