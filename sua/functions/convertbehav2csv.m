function convertbehav2csv(matfile,csvfile,sujid)
if nargin<3
    sujid = 0;
end
data = load(matfile);

tr = [data.P.T.trial].';
block = [data.P.T.block].';
stimamp = [data.P.T.stimamp].';
stimonset = [data.P.T.stimonset].';
start = [data.P.T.StartSound].';
stop = [data.P.T.StopSound].';

suj = tr*0+sujid;

tbl = table(suj,tr,block,stimamp,stimonset,start,stop,...
    'VariableNames',{'sub','trial','block','stimamp','stimonset','StartSound','StopSound'});
writetable(tbl,csvfile);

end