function [fire,spikes,neuron,name] = getfeatures(d,conf)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n = 0;
skip = 0;
name = [];
for i=1:length(d) 
    neuron = load(['../conf/neurons/' d(i).name]);
    if (neuron.stat.fire>conf.classif.firethr) && (neuron.stat.sua >= conf.classif.onlysua)
        n = n+1;
        spikes(n,:,:) = neuron.raster((conf.classif.ignore+1):end,:);
        fire(n,:,:) = neuron.frate((conf.classif.ignore+1):end,:);
        name{n} = d(i).name;
    else
        skip = skip+1;
        fprintf(' [ analysis ] %d) skipping %s (%.2f fire, SUA=%d)\n',skip,d(i).name,neuron.stat.fire,neuron.stat.sua);
    end
end

if ~strcmp(conf.fratemethod,'orig')
    [fire] = fratefilt(spikes,conf);
else
    fprintf(' [ analysis ] keeping precomputed frame rate\n');
end

if conf.classif.dolog
    fire = log(fire);
end
end

