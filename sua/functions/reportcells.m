function [p] = reportcells(nresp,ntot,txt,p)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    p = NaN;
end
% p = 1-binocdf(nresp,ntot,alpha);
if p<1e-3
    fprintf(' [ result ] %d/%d cells for %s (%.2f %%, p = %d)\n',nresp,ntot,txt,100*nresp/ntot,p);
else
    fprintf(' [ result ] %d/%d cells for %s (%.2f %%, p = %.4f)\n',nresp,ntot,txt,100*nresp/ntot,p);
end



