%% CSC params
if storeCSC
FieldSelection(1) = 1; % Timestamps
FieldSelection(2) = 0; % Channel Numbers
FieldSelection(3) = 1; % Sample Frequency
FieldSelection(4) = 0; % Number of Valid Samples
FieldSelection(5) = 1; % Samples
FieldSelection(6) = 0;
ExtractHeader = 1;
ExtractMode = 1;

%%
for ch=1:nchans
    fprintf(' [stage 1] Saving channel A%d in ncs format\n',ch);
    DD = br_data_spikes(ch,:);

    N =  length(DD);
    t = (1:N) / br_fs * 1e6;
    %t = t + 1e10;
    
    I = 1:512:N;
    timestamps = t(I(1:end-1));
    NumRecs = length(timestamps);
    
    
    dataSamples = zeros(512,floor(N/512));
    for i = 1:floor(N/512)
        dataSamples(1:512,i) = DD((1:512)+(i-1)*512);
    end
    if ~exist([conf.dir.output '/sort'])
        mkdir([conf.dir.output '/sort']);
    end
    Mat2NlxCSC( [conf.dir.output '/sort/A' num2str(ch) '.Ncs'], 0, ExtractMode, 1, NumRecs, FieldSelection, timestamps, dataSamples );
    
end
end