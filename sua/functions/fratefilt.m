function [frate,L,win] = fratefilt(spikes,conf)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
L = NaN;
win = NaN;
if strcmp(conf.fratemethod,'cumsum')
    frate = cumsum(spikes,2);
elseif strcmp(conf.fratemethod,'expfilt')
    a = conf.classif.expfiltcoeff;
    frate = filter(1-a,[1,-a],spikes,0,2);
elseif strcmp(conf.fratemethod,'gauss')
     L = conf.fs.analysis/2+1;
    t = (-L:L)./conf.fs.analysis;
    win = normpdf(t,0,conf.gausswin);
    win = win./sum(win);
    frate = filter(win,1,spikes,[],2);
elseif strcmp(conf.fratemethod,'halfgauss')
    L = conf.fs.analysis/2+1;
    t = (-L:L)./conf.fs.analysis;
    win = normpdf(t,0,conf.gausswin);
    win = win((L+1):end);
    win = win./sum(win);
    fprintf(' [ analysis ] half gaussian L=%d, sigma=%.2f\n',L,conf.gausswin);
    frate = filter(win,1,spikes,[],2);    
elseif strcmp(conf.fratemethod,'boxcar')
    k = conf.classif.filtwin;
    win = ones(1,k);
    frate = filter(win,1,spikes,[],2);    
end
end

