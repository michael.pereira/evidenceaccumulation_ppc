function [model] = ampmodel(tbl,conf)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if conf.modelcomp == 0
    m = fitglm(tbl,'nspikes ~ amp','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    model.isamp = m.Coefficients.pValue(2);
    model.coeffamp = m.Coefficients.Estimate(2);
    model.model = 1;
        
elseif conf.modelcomp == 1
    m = fitglm(tbl,'nspikes ~ amp','Distribution','poisson','Link','log');
    tst = m.devianceTest();
    model.ismod = tst.pValue(2);
    if model.ismod < 0.05
        model.isamp = m.Coefficients.pValue(2);
        model.coeffamp = m.Coefficients.Estimate(2);
        model.model = 1;
    else
        model.isamp = NaN;
        model.coeffamp = NaN;
        model.model = 0;
    end
else
    m0 = fitglm(tbl,'nspikes ~ 1','Distribution','poisson','Link','log');
    m1 = fitglm(tbl,'nspikes ~ amp','Distribution','poisson','Link','log');
    
    crit = [m0.ModelCriterion.(conf.modelcrit) m1.ModelCriterion.(conf.modelcrit) ...
        ];
    [~,bestmodel] = min(crit);
    
    tst = m1.devianceTest();
    model.ismod = tst.pValue(2);
    
    model.model = bestmodel-1;
    switch model.model
        
        case 1 % nspikes ~ resp
            model.isamp = m1.Coefficients.pValue(2);
            model.coeffamp = m1.Coefficients.Estimate(2);
        otherwise
            model.isamp = NaN;
            model.coeffamp = NaN;
    end
    
end

end

