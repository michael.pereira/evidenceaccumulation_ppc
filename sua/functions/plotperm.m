function h = plotperm(rnddist,data,ntot,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    p = 0.05;
end
hold on;
x = 0:ntot;

nperm = length(rnddist);
[h,c] = hist(rnddist,x);
bar(c,h,'FaceColor',0.8*[1 1 1])
plot(data*[1 1],ylim(),'r','LineWidth',2);
theoretical = binopdf(x,ntot,p)*nperm;
plot(x,theoretical,'b--','LineWidth',2);
xlabel('#Neurons');
ylabel('Count');
set(gca,'XTick',0:50:200,'YTick',0:100:1000,'FontSize',16);

%set(gcf,'Position',[500,600,160,120])
end

