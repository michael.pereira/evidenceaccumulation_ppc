function [c,p] = reportconf(x1,x2,txt)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[c,p] = corr(x1,x2,'type','Spearman','rows','complete');
if p>1e-3
    fprintf(' [ result ] corr for %s: %.2f (p=%.4f)\n',txt,c,p);
else
    fprintf(' [ result ] corr for %s: %.2f (p=%d)\n',txt,c,p);
end
end

