function [fwin,n] = gausswin(sdev,L)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        L = 10*sdev;
    end
    n = -(L-1)/2:(L-1)/2;  
    fwin = exp(-1/2*(n/(sdev)).^2);
    %normalize
    fwin = fwin./sum(abs(fwin));
end

