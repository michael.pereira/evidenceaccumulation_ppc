function [id] = getid(behav,bad)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

keep = true(length(behav.amp),1);
keep(bad) = 0;

id.idall = keep; 
id.amp = behav.amp';
id.on = behav.onset.';
id.idstim = keep & id.amp>0;% id.idhit | id.idmiss;

%[q] = quantile(id.amp(id.idstim),[1/3,2/3]);
q = [0.875,1.4];
%q = [0.7,1.4];

id.idamp1 = keep & id.amp < q(1);
id.idamp2 = keep & id.amp >= q(1) & id.amp < q(2);
id.idamp3 = keep & id.amp >= q(2);

med = median(id.amp(id.idstim));

id.idamplow = keep & id.amp < med;
id.idamphigh = keep & id.amp >= med;

% for exp1 and exp2
if isfield(behav,'sdt')
    id.idhit = keep & strcmp(behav.sdt.','hit');
    id.idmiss = keep & strcmp(behav.sdt.','miss');
    id.idcrej = keep & strcmp(behav.sdt.','crej');
    
    % confidence for exp1
    if isfield(behav,'conf')
        id.conf = behav.conf.'==3;
        id.trueconf = behav.conf.';
        id.idhitcl = keep & strcmp(behav.sdt.','hit') & id.conf == 0;
        id.idhitch = keep & strcmp(behav.sdt.','hit') & id.conf == 1;
        id.idmisscl = keep & strcmp(behav.sdt.','miss') & id.conf == 0;
        id.idmissch = keep & strcmp(behav.sdt.','miss') & id.conf == 1;
        id.idcrejcl = keep & strcmp(behav.sdt.','crej') & id.conf == 0;
        id.idcrejch = keep & strcmp(behav.sdt.','crej') & id.conf == 1;
    end
    
    % rt for exp1
    if isfield(behav,'rt')
        id.rt = behav.rt.';
        
        [q] = quantile(id.rt(id.idhit),(1:2)./3);
        id.idhitrt1 = keep & strcmp(behav.sdt.','hit') & id.rt < q(1);
        id.idhitrt2 = keep & strcmp(behav.sdt.','hit') & id.rt >= q(1) & id.rt < q(2);
        id.idhitrt3 = keep & strcmp(behav.sdt.','hit') & id.rt >= q(2);
        
        
        [q] = quantile(id.rt(id.idhit),(1:4)./5);
        id.idhitrt1b = keep & strcmp(behav.sdt.','hit') & id.rt < q(1);
        id.idhitrt2b = keep & strcmp(behav.sdt.','hit') & id.rt >= q(1) & id.rt < q(2);
        id.idhitrt3b = keep & strcmp(behav.sdt.','hit') & id.rt >= q(2) & id.rt < q(3);
        id.idhitrt4b = keep & strcmp(behav.sdt.','hit') & id.rt >= q(3) & id.rt < q(4);
        id.idhitrt5b = keep & strcmp(behav.sdt.','hit') & id.rt >= q(4);

    end
    
end

end

