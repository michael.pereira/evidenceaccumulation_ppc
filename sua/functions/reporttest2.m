function [p,st,m1,ci1,m2,ci2] = reporttest2(x,y,txt,prec,bonf)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    txt = {'var1','var2'};
end
if nargin < 4
    prec = 2;
end
if nargin < 5
    bonf = 1;
end

fprintf('-------------------\n');
[m1,ci1] = describe(x,txt{1},prec);
[m2,ci2] = describe(y,txt{2},prec);
[~,p,~,st] = ttest2(x,y);
fprintf('ttest2: t(%d) = %.2f, p=%f\n',st.df,st.tstat,p*bonf);

end

