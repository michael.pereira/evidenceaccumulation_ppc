%========================================================
%
%   Decoding analyses
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   17 Dec 2020
%========================================================

% Need to run needs decode_exp2_prep.m and needs decode_exp2_rndperm.m before
%
clear
addpath ../conf/
addpath ../functions/

if ~exist('figs','dir')
    mkdir('figs');
end
short = 1;
if short == 1
    lw = 1;
    figsz = [100,400,150,120];
else
    lw = 2;
    figsz = [100,400,300,250];
end
ci = @(x) std(x)./sqrt(length(x));

conf.exp = 2;
conf.session = 1;
config_spikes;

frate = 0.1;
alpha = 0.05; % significance level

t = linspace(-0.5,1.5,2001);
t2 = linspace(-0.5,3.5,4001);
selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));

fprintf(' [ analysis ] loading decoding results\n');
load('save/decoder_exp2.mat','confclassif','tstim','norm','clas');

fprintf(' [ analysis ] loading random decoding results\n');
load('save/rnddecoder_exp2.mat','rndclas');

fprintf(' [ analysis ] loading stim-locked firing rate\n');
d = dir(['../conf/neurons/e2*_gauss' num2str(frate) '_stim.mat']);
[cumfeat,feat,neuron] = getfeatures(d,confclassif);
% normalize
cumfeat = bsxfun(@minus,cumfeat,norm);
%  get indices
id = getid(neuron.behav,conf.badtrials);

fprintf(' [ analysis ] computing decoder generalization\n');
decodeout = clas.output;

cumfeatsel = cumfeat(:,conf.selt,id.idstim);
nt = length(conf.selt);


%% Compute generalisation (decoder trained on one time point on another time point)
generalization = nan(size(cumfeatsel,3),nt,nt);
fprintf(' [ analysis ] computing decoder generalization\n');
% get unthresholded decoder output
fprintf('    ');
for xv = 1:clas.cv.NumTestSets
    idtest = clas.cv.test(xv);
    fprintf('\b\b\b\b[%2d]',xv);
    for k=1:nt
        w = clas.cl_xval{k,xv}.Coeffs(2,1).Linear;
        b = clas.cl_xval{k,xv}.Coeffs(2,1).Const;
        f = squeeze(cumfeatsel(:,:,idtest));
        % so dimension 2 is "validation" and dimension 3 is "training"
        generalization(idtest,:,k) = reshape(w.'*f(:,:)+b,[nt,sum(idtest)]).';
    end
end

% compute AUROC for all pairs of timepoints
fprintf(' [ analysis ] computing decoder generalization AUROC\n');
fprintf('     ');
genaroc = nan(nt,nt);
for k1 = 1:nt     
    fprintf('\b\b\b\b\b[%3d]',k1);
    for k2 = 1:nt
        [~,~,~,genaroc(k1,k2)] = perfcurve(id.idhit(id.idstim),generalization(:,k1,k2),1);
    end
end

figure(); 
imagesc(t(conf.selt),t(conf.selt),genaroc); 
axis xy square
hold on;
plot([0 0],ylim(),'w--');
plot(xlim(),[0 0],'w--');
plot(xlim(),ylim(),'w-');

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-0.5:0.5:1.5,'YTickLabel',{});
set(gcf,'Position',figsz)
caxis([0.5,0.8]);
colormap('hot');
%colorbar; 
print('figs/exp2_decode_generalization.eps','-depsc');
pause(0.1);

%% Stim-locked proxy (output of the classifier for each time point)
fprintf(' [ analysis ] plotting stim-locked proxy\n');

figure(); hold on;
cishade(decodeout(id.idhitcl(id.idstim)==1,:),[],conf.color.hit2,t(conf.selt),'LineWidth',lw);
cishade(decodeout(id.idhitch(id.idstim)==1,:),[],conf.color.hit3,t(conf.selt),'LineWidth',lw);
cishade(decodeout(id.idmiss(id.idstim)==1,:),[],conf.color.miss,t(conf.selt),'LineWidth',lw);

hhit = permtest(decodeout(id.idhitcl(id.idstim)==1,:),decodeout(id.idhitch(id.idstim)==1,:),conf.nperm);
hdetect = permtest(decodeout(id.idhitcl(id.idstim)==1,:),decodeout(id.idmiss(id.idstim)==1,:),conf.nperm);
hmiss = permtest(decodeout(id.idmissch(id.idstim)==1,:),decodeout(id.idmisscl(id.idstim)==1,:),conf.nperm);

xlim([-0.5,1.5])
y = ylim();
plot([0 0],ylim(),'k--');
ylim(y);
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{});
set(gcf,'Position',figsz)
print('figs/exp2_decode_proxy.eps','-depsc');
pause(0.1);

fprintf('     ');
fprintf(' [ analysis ] computing stim-locked true and  random performance\n');
nt = length(conf.selt);
aroc = nan(1,nt);
aroc1 = nan(1,nt);
aroc2 = nan(1,nt);
rndaroc = nan(nt,conf.nperm);
rndaroc1 = nan(nt,conf.nperm);
rndaroc2 = nan(nt,conf.nperm);

for k=1:nt
    fprintf('\b\b\b\b\b[%3d]',k);
    % for each time point, compute auroc
    [~,~,~,aroc(k)] = perfcurve(id.idhit(id.idstim),decodeout(:,k),1);
    [~,~,~,aroc1(k)] = perfcurve(id.idhit(id.idstim & id.trueconf<3),decodeout(id.trueconf(id.idstim)<3,k),1);
    [~,~,~,aroc2(k)] = perfcurve(id.idhit(id.idstim & id.trueconf==3),decodeout(id.trueconf(id.idstim)==3,k),1);
    
    for r=1:conf.nperm
        % same for every permutation
        [~,~,~,rndaroc(k,r)] = perfcurve(id.idhit(id.idstim),rndclas.output(:,k,r),1);
        [~,~,~,rndaroc1(k,r)] = perfcurve(id.idhit(id.idstim & id.trueconf<3),rndclas.output(id.trueconf(id.idstim)<3,k,r),1);
        [~,~,~,rndaroc2(k,r)] = perfcurve(id.idhit(id.idstim & id.trueconf==3),rndclas.output(id.trueconf(id.idstim)==3,k,r),1);
    
    end
end
save('save/exp2_aroc.mat','aroc','aroc1','aroc2','rndaroc','rndaroc1','rndaroc2');

%% compute statistical significance of performance for all conf/low conf/high conf


% compute clusters of AUROC > 0.6 
thr = 0.6;
cl = bwconncomp(aroc>thr);
cl1 = bwconncomp(aroc1>thr);
cl2 = bwconncomp(aroc2>thr);
% thr = 0.1;
% cl = bwconncomp(abs(aroc-0.5)>thr);
% cl1 = bwconncomp(abs(aroc1-0.5)>thr);
% cl2 = bwconncomp(abs(aroc2-0.5)>thr);

rndmaxcl = nan(1,size(rndaroc,2));
rndmaxcl1 = nan(1,size(rndaroc,2));
rndmaxcl2 = nan(1,size(rndaroc,2));

for i=1:size(rndaroc,2)
    % for all indices
    %rndcl =  bwconncomp(abs(rndaroc(:,i)-0.5)>thr);
    rndcl =  bwconncomp(rndaroc(:,i)>thr);
    m = max(cellfun(@length,rndcl.PixelIdxList));
    if isempty(m)
        rndmaxcl(i) = 0;
    else
        rndmaxcl(i) = m;
    end
    % for high conf
    %rndcl1 =  bwconncomp(abs(rndaroc1(:,i)-0.5)>thr);
    rndcl1 =  bwconncomp(rndaroc1(:,i)>thr);
    m1 = max(cellfun(@length,rndcl1.PixelIdxList));
    if isempty(m1)
        rndmaxcl1(i) = 0;
    else
        rndmaxcl1(i) = m1;
    end
    % for low conf
    %rndcl2 =  bwconncomp(abs(rndaroc2(:,i)-0.5)>thr);
    rndcl2 =  bwconncomp(rndaroc2(:,i)>thr);
    m2 = max(cellfun(@length,rndcl2.PixelIdxList));
    if isempty(m2)
        rndmaxcl2(i) = 0;
    else
        rndmaxcl2(i) = m2;
    end
end

sigperf = nan(size(aroc));
sigperf1 = nan(size(aroc1));
sigperf2 = nan(size(aroc2));

% non parametric significance level
q = quantile(rndmaxcl,1-alpha);
q1 = quantile(rndmaxcl1,1-alpha);
q2 = quantile(rndmaxcl2,1-alpha);

% make a vector with significant time points
for c = 1:length(cl.PixelIdxList)
    if length(cl.PixelIdxList{c}) > q
        sigperf(cl.PixelIdxList{c}) = 0.35;
    end
end
for c1 = 1:length(cl1.PixelIdxList)
    if length(cl1.PixelIdxList{c1}) > q1
        sigperf1(cl1.PixelIdxList{c1}) = 0.33;
    end
end
for c2 = 1:length(cl2.PixelIdxList)
    if length(cl2.PixelIdxList{c2}) > q2
        sigperf2(cl2.PixelIdxList{c2}) = 0.31;
    end
end
% plot
fprintf(' [ analysis ] plotting stim-locked results for DETECTION\n');

figure(); clf; hold on;
plot(t(confclassif.selt),aroc,'k-','LineWidth',lw,'Color',0.5*[1 1 1]);
plot(t(confclassif.selt),aroc1,'k-','LineWidth',lw,'Color',conf.color.hit2);
plot(t(confclassif.selt),aroc2,'k-','LineWidth',lw,'Color',conf.color.hit3);

plot(t(confclassif.selt),sigperf,'k-','LineWidth',2,'Color',0.5*[1 1 1]);
plot(t(confclassif.selt),sigperf1,'r-','LineWidth',2,'Color',conf.color.hit2);
plot(t(confclassif.selt),sigperf2,'g-','LineWidth',2,'Color',conf.color.hit3);


ylim([0.2,1])
plot(xlim(),[0.5,0.5],'k-');
plot([0 0],ylim(),'k--');
xlim([-0.5,1.5])
plot(xlim(),0.6*[1 1],'r--')
plot(xlim(),0.4*[1 1],'r--')

first = find(~isnan(sigperf),1,'first');
[vmax,ibest] = max(aroc);

fprintf('Decoding for all conf. -> first at @t = %.2f, max: %.2f @t = %.2f\n', t(confclassif.selt(first)),vmax,t(confclassif.selt(ibest)));

first1 = find(~isnan(sigperf1),1,'first');
[vmax1,ibest1] = max(aroc1);
fprintf('Decoding for low conf.-> first at @t = %.2f, max: %.2f @t = %.2f\n', t(confclassif.selt(first1)),vmax1,t(confclassif.selt(ibest1)));

first2 = find(~isnan(sigperf2),1,'first');
[vmax2,ibest2] = max(aroc2);
fprintf('Decoding for high conf.-> first at @t = %.2f, max: %.2f @t = %.2f\n', t(confclassif.selt(first2)),vmax2,t(confclassif.selt(ibest2)));

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{});
set(gcf,'Position',figsz)
print('figs/Fig2c.eps','-depsc');
pause(0.1);



%% Check whether best decoder output correlates with confidence
cl = decodeout(:,(ibest));

figure(); hold on;
m(1) = mean(cl(id.trueconf(id.idstim)==1 & id.idhit(id.idstim)));
m(2) = mean(cl(id.trueconf(id.idstim)==2 & id.idhit(id.idstim)));
m(3) = mean(cl(id.trueconf(id.idstim)==3 & id.idhit(id.idstim)));
ci95(1) = ci(cl(id.trueconf(id.idstim)==1 & id.idhit(id.idstim)));
ci95(2) = ci(cl(id.trueconf(id.idstim)==2 & id.idhit(id.idstim)));
ci95(3) = ci(cl(id.trueconf(id.idstim)==3 & id.idhit(id.idstim)));
errorbar(1:3,m,ci95,'o-','Color',conf.color.hit,'MarkerFaceColor',conf.color.hit);

m(1) = mean(cl(id.trueconf(id.idstim)==1 & id.idmiss(id.idstim)));
m(2) = mean(cl(id.trueconf(id.idstim)==2 & id.idmiss(id.idstim)));
m(3) = mean(cl(id.trueconf(id.idstim)==3 & id.idmiss(id.idstim)));
ci95(1) = ci(cl(id.trueconf(id.idstim)==1 & id.idmiss(id.idstim)));
ci95(2) = ci(cl(id.trueconf(id.idstim)==2 & id.idmiss(id.idstim)));
ci95(3) = ci(cl(id.trueconf(id.idstim)==3 & id.idmiss(id.idstim)));

errorbar(1:3,m,ci95,'o-','Color',conf.color.miss,'MarkerFaceColor',conf.color.miss);
xlim([0.5,3.5])
set(gca,'XTick',1:2,'YTick',60:20:100,'XTickLabel',{},'YTickLabel',{});

set(gcf,'Position',[50,500,150,150])
print('figs/exp1_decode_stimlockconf.eps','-depsc');

[corhit,pcorhit] = corr(cl(id.idhit(id.idstim)),id.trueconf(id.idhit & id.idstim),'type','Spearman');
[cormiss,pcormiss] = corr(cl(id.idmiss(id.idstim)),id.trueconf(id.idmiss & id.idstim),'type','Spearman');
fprintf(' [ analysis ] computing chance level for CONFIDENCE\n');
%figure();
%plot(cl(id.idhit(id.idstim))

rng('default');
rndcorhit = zeros(1,conf.nperm);
rndcormiss = zeros(1,conf.nperm);
fprintf('      ');
for i=1:conf.nperm
    fprintf('\b\b\b\b\b\b[%4d]',i);
    cl2 = cl(randperm(size(clas.pred,1)));
    rndcorhit(i) = corr(cl2(id.idhit(id.idstim)),id.trueconf(id.idhit(id.idstim)),'type','Spearman');
    rndcormiss(i) = corr(cl2(id.idmiss(id.idstim)),id.trueconf(id.idmiss(id.idstim)),'type','Spearman');
end

fprintf('------------------\n');
fprintf('Corr. conf for hit: R=%.2f, p=%.4f\n',corhit,mean(rndcorhit>corhit));
fprintf('Corr. conf for miss: R=%.2f, p=%.4f\n',cormiss,mean(rndcormiss<cormiss));
fprintf('------------------\n');
%reporttest2(cl(id.idhit(id.idstim) & id.trueconf(id.idstim)==1),cl(id.idmiss(id.idstim) & id.trueconf(id.idstim)==1),{'Hit (conf=1)','Miss (conf=1)'},2,1);
%reporttest2(cl(id.idhit(id.idstim) & id.trueconf(id.idstim)==2),cl(id.idmiss(id.idstim) & id.trueconf(id.idstim)==2),{'Hit (conf=2)','Miss (conf=2)'},2,1);
%reporttest2(cl(id.idhit(id.idstim) & id.trueconf(id.idstim)==3),cl(id.idmiss(id.idstim) & id.trueconf(id.idstim)==3),{'Hit (conf=3)','Miss (conf=3)'},2,1);
%fprintf('------------------\n');
%reporttest2(cl(id.idhit(id.idstim) & id.trueconf(id.idstim)==1),cl(id.idhit(id.idstim) & id.trueconf(id.idstim)==3),{'Conf=1 (hit)','Conf=3 (hit)'},2,1);
%dreporttest2(cl(id.idmiss(id.idstim) & id.trueconf(id.idstim)==1),cl(id.idmiss(id.idstim) & id.trueconf(id.idstim)==3),{'Conf=1 (miss)','Conf=3 (miss)'},2,1);

%% Compute cue-locked decoding

% get features
fprintf(' [ analysis ] processing cue-locked firing rate\n');
d = dir(['../conf/neurons/e2*_gauss' num2str(frate) '_cue.mat']);
%confclassif.classif.ignore = 800;
cumfeat_cue = getfeatures(d,confclassif);
cumfeat_cue = bsxfun(@minus,cumfeat_cue,norm);

%  ibest = i;
tbest = tstim(ibest);

fprintf('Choosing classifier at t=%.2f (perf: %.2f %%)\n',tbest,clas.acc(ibest));
cumfeatsel_cue = cumfeat_cue(:,:,id.idstim);
decodeout_cue = nan(size(cumfeatsel_cue,3),size(cumfeatsel_cue,2));

% compute classifier output in a x-val scheme
for xv = 1:clas.cv.NumTestSets
    idtest = clas.cv.test(xv);
    w = clas.cl_xval{ibest,xv}.Coeffs(2,1).Linear;
    b = clas.cl_xval{ibest,xv}.Coeffs(2,1).Const;
    for k=1:size(decodeout_cue,2)
        decodeout_cue(idtest,k) = w.'*squeeze(cumfeatsel_cue(:,k,idtest))+b;%predict(clas.cl_xval{ibest,xv},squeeze(cumfeat(:,k,idtest)).');
    end
end


%% Check whether max of decoded output time-locked to cue correlates with conf
% start only at a latency corresponding to the decoder's latency 
mint = 0.8;% ibest/100-0.5;
maxt = 3.0;
[maxstat,imax] = max(decodeout_cue(:,t2>mint & t2<maxt),[],2);

%maxstat = log(maxstat)
% test correlations
imax = 1e-3*(imax + mint);
[chit,phit] = corr(maxstat(id.idhit(id.idstim)),id.trueconf(id.idhit & id.idstim),'type','Spearman');
[cmiss,pmiss] = corr(maxstat(id.idmiss(id.idstim)),id.trueconf(id.idmiss & id.idstim),'type','Spearman');

[chit_on,phit_on] = corr(imax(id.idhit(id.idstim)),id.on(id.idhit & id.idstim),'type','Spearman');
[cmiss_on,pmiss_on] = corr(imax(id.idmiss(id.idstim)),id.on(id.idmiss & id.idstim),'type','Spearman');

[chit_amp,phit_amp] = corr(maxstat(id.idhit(id.idstim)),id.amp(id.idhit & id.idstim),'type','Spearman');
[cmiss_amp,pmiss_amp] = corr(maxstat(id.idmiss(id.idstim)),id.amp(id.idmiss & id.idstim),'type','Spearman');

% random permutation stats
rng('default');
rndchit = nan(1,conf.nperm);
rndcmiss = nan(1,conf.nperm);
rndchit_on = nan(1,conf.nperm);
rndcmiss_on = nan(1,conf.nperm);
rndchit_amp = nan(1,conf.nperm);
rndcmiss_amp = nan(1,conf.nperm);
for i=1:conf.nperm
    % get max value
    maxstat_hit = maxstat(id.idhit(id.idstim));
    maxstat_miss = maxstat(id.idmiss(id.idstim));
    % permute max value independantly for hits and misses
    maxstat_hit2 = maxstat_hit(randperm(length(maxstat_hit)));
    maxstat_miss2 = maxstat_miss(randperm(length(maxstat_miss)));
    % get nax latency
    imax_hit = imax(id.idhit(id.idstim));
    imax_miss = imax(id.idmiss(id.idstim));
    % permute max latency independantly for hits and misses
    imax_hit2 = imax_hit(randperm(length(imax_hit)));
    imax_miss2 = imax_miss(randperm(length(imax_miss)));
    
    % correlate permuted data
    rndchit(i) = corr(maxstat_hit2,id.trueconf(id.idhit & id.idstim),'type','Spearman');
    rndcmiss(i)= corr(maxstat_miss2,id.trueconf(id.idmiss & id.idstim),'type','Spearman');
  
    rndchit_on(i) = corr(imax_hit2,id.on(id.idhit & id.idstim),'type','Spearman');
    rndcmiss_on(i) = corr(imax_miss2,id.on(id.idmiss & id.idstim),'type','Spearman');
    
    rndchit_amp(i) = corr(maxstat_hit2,id.amp(id.idhit & id.idstim),'type','Spearman');
    rndcmiss_amp(i) = corr(maxstat_miss2,id.amp(id.idmiss & id.idstim),'type','Spearman');
end

fprintf('------------------\n');
fprintf('Corr. conf for hit (%.2f): R=%.2f, p=%.4f\n',tbest,chit,mean(rndchit > chit));
fprintf('Corr. conf for miss (%.2f): R=%.2f, p=%.4f\n',tbest,cmiss,mean(rndcmiss < cmiss));
fprintf('------------------\n');

fprintf('Corr. onset for hit (%.2f): R=%.2f, p=%.4f\n',tbest,chit_on,mean(rndchit_on > chit_on));
fprintf('Corr. onset for miss (%.2f): R=%.2f, p=%.4f\n',tbest,cmiss_on,mean(rndcmiss_on > cmiss_on));

fprintf('Corr. amp for hit (%.2f): R=%.2f, p=%.4f\n',tbest,chit_amp,mean(rndchit_amp > chit_amp));
fprintf('Corr. amp for miss (%.2f): R=%.2f, p=%.4f\n',tbest,cmiss_amp,mean(rndcmiss_amp > cmiss_amp));

%% Check the distribution of max latencies

figure(); hold on;
[k,x] = hist(mint+imax(id.idhit(id.idstim)),0:0.2:4);
plot(x,k,'LineWidth',2,'Color',conf.color.hit);
[k,x] = hist(mint+imax(id.idmiss(id.idstim)),0:0.2:4);
plot(x,k,'LineWidth',2,'Color',conf.color.miss);

%% plot max value per confidence value and response
figure();
hold on;
clear m confint

for i=1:3
    m(i) = mean(maxstat(id.idhit(id.idstim) & id.trueconf(id.idstim)==i));
    confint(i) = ci(maxstat(id.idhit(id.idstim) & id.trueconf(id.idstim)==i));
end
errorbar((1:3)-0.1,m,confint,'o-','Color',conf.color.hit,'MarkerFaceColor',conf.color.hit);

for i=1:3
    m(i) = mean(maxstat(id.idmiss(id.idstim) & id.trueconf(id.idstim)==i));
    confint(i) = ci(maxstat(id.idmiss(id.idstim) & id.trueconf(id.idstim)==i));
end
errorbar((1:3)-0.1,m,confint,'o-','Color',conf.color.miss,'MarkerFaceColor',conf.color.miss);


xlim([0.5,3.5])
set(gca,'XTick',1:2,'YTick',60:20:100,'XTickLabel',{},'YTickLabel',{});

set(gcf,'Position',[50,500,150,150])
print('figs//exp2_decode_cuelockconf.eps','-depsc');
%% plot decoder output time-locked to the cue 
figure();
cishade(decodeout_cue(id.idhit(id.idstim),:),[],conf.color.hit,t2,'LineWidth',2)
hold on;
cishade(decodeout_cue(id.idmiss(id.idstim),:),[],conf.color.miss,t2,'LineWidth',2)

plot([0,0],ylim(),'k--');
plot([2,2],ylim(),'k--');
plot([3,3],ylim(),'k--');
set(gca,'XTick',-0.5:0.5:4,'YTick',20:20:120,'XTickLabel',{},'YTickLabel',{});
set(gcf,'Position',[20,500,400,200])
print('figs/exp2_decode_cuelockpred.eps','-depsc');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %%
    figure();
    allons = id.on(id.idstim);
    ihit = id.idhit(id.idstim);
    imiss = id.idmiss(id.idstim);
    
    subplot(211);
    sel = find(ihit);
    [on,ion] = sort(allons(ihit));
    imagesc(t2,1:length(sel),decodeout_cue(sel(ion),:));
    hold on;
    plot(on,1:length(sel),'k');
    xlabel('Time from cue [s]');
    ylabel('Trials');
    title('hits');
    
     subplot(212);
    sel = find(imiss);
    [on,ion] = sort(allons(imiss));
    imagesc(t2,1:length(sel),decodeout_cue(sel(ion),:));
    hold on;
    plot(on,1:length(sel),'k');
    xlabel('Time from cue [s]');
    ylabel('Trials');
    title('misses');
    
    print('figs/exp2_checkcuelocked.eps','-depsc');

