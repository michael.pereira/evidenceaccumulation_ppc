%========================================================
%
%   Classification for decoding
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   17 Dec 2020
%========================================================
clear

addpath ../conf
addpath ../functions/
conf.exp = 2;
conf.session = 1;
config_spikes;

if ~exist('save','dir')
    mkdir('save');
end
frate = 0.1;
t = linspace(-0.5,1.5,2001);

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));

fprintf(' [ analysis ]  processing stim\n');

% load neuronal data
d = dir(['../conf/neurons/e2*_gauss' num2str(frate) '_stim.mat']);

% get features
[cumfeat,feat,neuron] = getfeatures(d,conf);

% normalize
norm = mean(cumfeat(:,selbaseline,:),2);
cumfeat = bsxfun(@minus,cumfeat,norm);

% get behavior
id = getid(neuron.behav,conf.badtrials);
label = id.idhit(id.idstim);
confidence = id.trueconf(id.idstim);

[nn,~,ntr] = size(cumfeat);

nt = length(conf.selt);

rng('default');
%%

% prealoc
clas = [];
clas.acc = nan(1,nt);
clas.pred = nan(length(label),nt);
clas.output = nan(length(label),nt);
clas.cv = cvpartition(length(label),'Kfold',conf.classif.nfoldext);

fprintf('          ');
for ti=1:nt
    %%
    
    % select features, exclude zero firing rate
    f = squeeze(cumfeat(:,conf.selt(ti),:));
    featsel = find(any(f.'~=0));
    f = (f(featsel,id.idstim)).';
    
    % for each cross-validation fold
    for xv=1:conf.classif.nfoldext
        % train decoder
        idtrain = clas.cv.training(xv);
        idtest = clas.cv.test(xv);
        clas.cl_xval{ti,xv} = fitcdiscr(f(idtrain,:),label(idtrain), ...
            'FillCoeffs','on','Prior','uniform','scoreTransform','logit', ...
            'Gamma',conf.classif.gamma,'Delta',conf.classif.delta,'DiscrimType',conf.classif.discrim, ...
            'HyperparameterOptimizationOptions',...
            struct('ShowPlots',0,'Verbose',0,'UseParallel',1,'MaxObjectiveEvaluations',30,...
            'KFold',conf.classif.nfoldint, 'AcquisitionFunctionName','expected-improvement-plus'), ...
            'OptimizeHyperparameters',conf.classif.optimize);
        % save weights and bias
        w = clas.cl_xval{ti,xv}.Coeffs(2,1).Linear;
        b = clas.cl_xval{ti,xv}.Coeffs(2,1).Const;
        
        % predict test data
        out = f(idtest,:)*w+b;
        pred = out > 0;
        clas.output(clas.cv.test(xv),ti) = out;
        clas.pred(clas.cv.test(xv),ti) = pred;
       
    end
    clas.acc(ti) = mean(clas.pred(:,ti) == label);

    fprintf('\b\b\b\b\b\b\b\b\b\b[%3d - %2.0f]',ti,100*clas.acc(ti));

end
fprintf('\n');
%%
confclassif = conf;
tstim = t(conf.selt);
fprintf(' [ analysis ] saving in %s\n','save/decoder_exp1.mat');
save('save/decoder_exp2.mat','confclassif','tstim','norm','clas');%,'reghit','regmiss');

%%