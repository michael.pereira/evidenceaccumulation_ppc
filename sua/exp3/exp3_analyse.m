%========================================================
%   Experiment 3 - responsive neurons
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script produces most analyses for experiment 3:
% number of responsive neurons, plotting SUA and average firing rates
%
% /!\ it requires running exp3_prep.m and exp3_rndperm.m which could take a
% while.
clear

config_spikes
doplot = 1;
t = linspace(-0.5,1.5,2001);

if ~exist('cells','var') || cells(1).exp ~= 3
    fprintf('Loading precomputed data\n');
    if exist('save/cells_exp3.mat','file')
        load('save/cells_exp3.mat');
    else
        error('Could not find save/cells_exp3.mat\nyou probably should run exp3_prep.m');
    end
end
if ~exist('rndcells','var') || rndcells(1).exp ~= 1
    fprintf('Loading precomputed shuffled data, might take a while\n');
    if exist('save/rndcells_exp3.mat','file')
        load('save/rndcells_exp3.mat');
    else
        error('Could not find save/rndcells_exp3.mat\nyou probably should run exp3_rndperm.m');
    end
end

%%
%% get relevant information

% ids
sua = [cells.sua]==1;
rndsua = repmat([cells.sua].'==1,[1,size(rndcells,2)]);

% p detection
amp = [cells.isamp];
rndamp = reshape([rndcells.isamp],size(rndcells,1),[]);

coef = [cells.coeffamp];
rndcoef = reshape([rndcells.coeffamp],size(rndcells,1),[]);

nsua = sum(sua);
fprintf('-----------------\n');
fprintf('----- count -----\n');
fprintf('-----------------\n');

%%
%% detection-selective
idamp = amp<0.05 & coef > 0 & sua;
rndidamp = rndamp<0.05 & rndcoef > 0 & rndsua;
namp = sum(idamp);
rndnamp = sum(rndidamp);
reportcells(namp,nsua,'amplitude',mean(rndnamp >= namp));
figure(301); clf; plotperm(rndnamp,namp,nsua,0.05); title('amplitude');
if doplot
    h = plot_exp3_gavg(cells(idamp),t,conf);
    saveas(h{1},['figs/Fig3a.png']);
    %fprintf('Press a key to continue>>\n'); pause();
end

%%
%plot_exp3(cells,'#419',t,conf)
%plot_exp3(cells,'#440',t,conf)
