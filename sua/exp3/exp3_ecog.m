%========================================================
%   Experiment 3 - ECoG grid analysis
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This script performs the analyses of the ECoG grid

clear

sel = 'trlfp_sm';
selstat = 'trlfp';

addpath ../conf
addpath ../functions/

conf.version = 3;
conf.session = 1;
config_spikes;
addpath(genpath(conf.dir.toolbox_ielvis));

fs = conf.ecog.fs;%/dwnsamp;

global globalFsDir;
globalFsDir=conf.dir.brainmodel;


for type = {'lfp','hga'}; % 'lfp' or 'hga' for high gamma

f1 = [conf.dir.preprocdir '/e3s1/ecog' sel '_e3s1.mat'];
fprintf('Loading %s\n',f1);
d1 = load(f1);
trials = d1.(sel);

f1stat = [conf.dir.preprocdir '/e3s1/ecog' selstat '_e3s1.mat'];
fprintf('Loading %s\n',f1stat);
d1stat = load(f1stat);
trialsstat = d1stat.(selstat);%sdt = d1.beh.sdt;

selid = 0.5*fs+(1:2*fs);
   
statid = 1.5*fs+(1:1*fs);
    
d = dir(['../conf/neurons/e3*_gauss0.1_stim.mat']);
neuron = load(['../conf/neurons/' d(1).name]);
amp = neuron.behav.amp;
uamp = unique(amp);

idamp1 = amp > 0 & amp <= 0.7;
idamp2 = amp > 0.7 & amp <= 1.225;
idamp3 = amp > 1.225;%& amp <= 0.7;

idamp1(conf.allbadtrials) = 0;
idamp2(conf.allbadtrials) = 0;
idamp3(conf.allbadtrials) = 0;

fprintf('amp1: %d, amp2: %d, amp3: %d\n',sum(idamp1),sum(idamp2),sum(idamp3));
%%

[ntr,nt,nchan] = size(trials);
t = linspace(-1,2,nt);
figure(conf.version*100); clf;
dif = zeros(1,nchan);
initrow = 21; row = initrow;
for ch=1:nchan
    subplot(6,4,row); hold on;
    
    %figure(); hold on;
    row = row - 4;
    if row < 1
        initrow = initrow+1;
        row = initrow;
    end

    amp1 = trials(idamp1,:,ch);
    amp2 = trials(idamp2,:,ch);
    amp3 = trials(idamp3,:,ch);
    
    avg = mean(trialsstat(amp>0,statid,ch),2);
    tbl = table(avg,amp(amp>0).','VariableNames',{'avg','amp'});
    if strcmp(type,'lfp')
        m = fitlm(tbl,'avg ~ amp');
    else
        m = fitglm(tbl,'avg ~ amp','Distribution','gamma','Link','log');
    end
    coeff(ch) = m.Coefficients.Estimate(2);
    pval(ch) = m.Coefficients.pValue(2);
    
   
    cishade(amp1,0.4,conf.color.amp1,t,'LineWidth',lw);
    cishade(amp2,0.2,conf.color.amp4,t,'LineWidth',lw);
    cishade(amp3,0.2,conf.color.amp7,t,'LineWidth',lw);
    
    %h = nan(size(hperm));% h(pfdr<0.05) = 0;
    %h(hperm==1) =  min(mean(miss,1))-20;
    %plot(t(selid),h,'k','LineWidth',2);
    xlim([-0.5,1.5]);
    % ylim([-120,100]);
    plot([0 0],ylim(),'k--');
    set(gca,'XTick',-0.5:0.5:1.5);
    title(sprintf('ch %d',ch));
    xlabel('Time [s]');
    ylabel('Amplitude [{\mu}V]');
    
    pause(0.1);
end
set(gcf,'Position',[100,100,600,600])
print(['figs/exp3_' type{1} '_allelec.eps'],'-depsc');

if 1
    %%
   
    coeff2 = coeff;
    elecNames=cell(nchan,1);
    for a=1:nchan
        elecNames{a}=sprintf('G%d',a);
    end
    
%    coeff2(pval > 0.05/24) = -39;
    col = hot(51);
    %colors = col(40+min(round(coeff2),40),:);
    colors = col(1+min(round(abs(coeff2)),50),:);
    
    colors(pval > 0.05/24,:) = 0;
    colors = [colors ; [76,168,41]./255];
    %elecNames(pval > 0.05) = [];
    elecNames{end+1} = 'Utah1';
    
    cfg = [];
    %cfg.surfType = 'pial';
    cfg.figId=102;
    cfg.elecNames=elecNames;
    cfg.pullOut=0;
    cfg.bidsDir = conf.dir.data;
    cfg.elecColors=colors;%[-coeff2.' ; 0];
    cfg.elecColorScale=[-40,40];
    cfg.elecCoord='LEPTO';
    cfg.elecCbar = 'n';
    cfg.showLabels='n';
    cfg.elecUnits='r';
    cfg.title='';
    cfg.view = 'l';
    cfg.elecSize=3;
    cfg.elecShape='sphere';
    %cfg.opaqueness = 0.7;


    
    figure();
    plotPialSurf('patient',cfg);
    view(-68,46);
    print(['figs/exp3_' type{1} '_pial.eps'],'-depsc');
    
end



end
