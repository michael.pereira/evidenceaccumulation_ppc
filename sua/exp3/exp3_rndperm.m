%========================================================
%   Experiment 3 - random permutations
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the null distributions of the number
% of responsive neurons 
clear

addpath ../conf
addpath ../functions/
conf.exp = 3;
conf.session = 1;
config_spikes;

conf.nperm = 1000;
frate = 0.1;

d = dir(['../conf/neurons/e3*_gauss' num2str(frate) '_stim.mat']);
t = linspace(-0.5,1.5,2001);

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));

neuron = load(['../conf/neurons/' d(1).name]);
id = getid(neuron.behav,conf.badtrials);


rng('default');
fprintf('     ');

for rnd = 1:conf.nperm
    fprintf('\b\b\b\b\b[%3d]',rnd);
    perm = randperm(sum(id.idstim));    
    
    for n=1:length(d)
        
        %%        
        neuron = load(['../conf/neurons/' d(n).name]);
        x = strsplit(d(n).name,'-');
        y = strsplit(x{2},'_');
        
        %frate = fratefilt(neuron.raster,conf);
        frate = neuron.frate;
        
        % get behav & ids
        id = getid(neuron.behav,conf.badtrials);
        perm = randperm(sum(id.idstim));  
        
        % stats
        rndcells(n,rnd).exp = 3;
        rndcells(n,rnd).id = y{1};
        rndcells(n,rnd).keep = sum(id.idall);
        rndcells(n,rnd).sua = neuron.stat.sua;
        
        nspikes = sum(neuron.raster(selstat,:),1).';
        
        tbl = table(nspikes(id.idstim & id.amp > 0),(id.amp(id.idstim & id.amp > 0)),'VariableNames',{'nspikes','amp'});
        %tbl.nspikes = tbl.nspikes(perm);
        
        % same model
        model = ampmodel(tbl,conf);
        for f = fieldnames(model).'
            rndcells(n,rnd).(cell2mat(f)) = model.(cell2mat(f));
        end
        
    end
    
    if mod(rnd,50) == 0
        save('save/rndcells_exp3.mat','rndcells');
    end
end

fprintf('\n [ analysis ] DONE\n');
rndconfsave = conf;
save('save/rndcells_exp3.mat','rndcells','rndconfsave');