%========================================================
%   Experiment 3 - responsive neurons
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the stats for each SUA and MUA and stores
% firing rates and rasters
clear

addpath ../conf
addpath ../functions/
conf.exp = 3;
conf.session = 1;
config_spikes;

frate = 0.1;

d = dir(['../conf/neurons/e3*_gauss' num2str(frate) '_stim.mat']);
t = linspace(-0.5,1.5,2001);

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));
fprintf('     ');
for n=1:length(d)
    
    %%
    fprintf('\b\b\b\b\b[%3d]',n);
    
    neuron = load(['../conf/neurons/' d(n).name]);
    x = strsplit(d(n).name,'-');
    y = strsplit(x{2},'_');
    
    %frate = fratefilt(neuron.raster,conf);
    frate = neuron.frate;
    
    %fprintf(' [%s]',id);
    % get behav & ids
    id = getid(neuron.behav,conf.badtrials);
    
    % stats
    cells(n).exp = 3;
    cells(n).id = y{1};
    cells(n).keep = sum(id.idall);
    cells(n).sua = neuron.stat.sua;
    cells(n).fire = neuron.stat.fire;
    cells(n).isi = neuron.stat.isi;
    cells(n).cv2 = neuron.stat.cv2;
    cells(n).bi = neuron.stat.bi;
    cells(n).snr = neuron.stat.snr;
    cells(n).spikeamp = neuron.stat.spikeamp;
    cells(n).peakvar = neuron.stat.peakvar;
    cells(n).ap = neuron.ap;
    cells(n).apstd = neuron.apstd;
    
    
    nspikes = sum(neuron.raster(selstat,:),1).';
    tbl = table(nspikes(id.idstim & id.amp > 0),(id.amp(id.idstim & id.amp > 0)),'VariableNames',{'nspikes','amp'});
    
     [~,imax(n)] = max(nspikes(id.idstim));
     
    % same model
    rng('default');
    model = ampmodel(tbl,conf);
    for f = fieldnames(model).'
       cells(n).(cell2mat(f)) = model.(cell2mat(f));
    end
    
    amps = unique(id.amp); amps(amps==0) = [];

        % store raster
    cells(n).raster = [neuron.raster(:,id.idamp1),neuron.raster(:,id.idamp2)*2, ...
        neuron.raster(:,id.idamp3)*3];
    cells(n).binraster = [neuron.raster(:,id.idamplow),neuron.raster(:,id.idamphigh)*2];    % store firing rates
   
   
    cells(n).famp1 = frate(:,id.idamp1);
    cells(n).famp2 = frate(:,id.idamp2);
    cells(n).famp3 = frate(:,id.idamp3);
    
    cells(n).famplow = frate(:,id.idamplow);
    cells(n).famphigh= frate(:,id.idamphigh);
    
     
    % store average firing rates
    mu = 0*mean(frate(selbaseline,:));
    s = mean(mean(frate(selbaseline,:)));
  
    
    cells(n).avgfamp1 = mean(bsxfun(@minus,frate(:,id.idamp1),mu(id.idamp1))./s,2);
    cells(n).avgfamp2 = mean(bsxfun(@minus,frate(:,id.idamp2),mu(id.idamp2))./s,2);
    cells(n).avgfamp3 = mean(bsxfun(@minus,frate(:,id.idamp3),mu(id.idamp3))./s,2);
    
    cells(n).avgfamplow = mean(bsxfun(@minus,frate(:,id.idamplow),mu(id.idamplow))./s,2);
    cells(n).avgfamphigh = mean(bsxfun(@minus,frate(:,id.idamphigh),mu(id.idamphigh))./s,2);
    
 
    
end

fprintf('\n [ analysis ] DONE\n');
confsave = conf;
save('save/cells_exp3.mat','cells','confsave');