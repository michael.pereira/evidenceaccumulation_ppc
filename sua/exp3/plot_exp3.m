%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function plot_exp3(cells,id,t,conf)
%plot_exp3 Helper function to plot data from single neurons
%   Inputs:
%       - cells: an array of structures computed by exp1_prep.m and stored
%       in save/cells_exp1.mat
%       - id: the index of the SUA/MUA for plotting
%       - t: time indices for the x-axis (see exp1_analyse.m)
%       - conf: the struct of configs from ../conf/config_spikes.m
%
short = 1;
if short == 1
lw = 1.5;
figsz = [100,400,150,120];
else
lw = 2;
figsz = [100,400,300,250];
end
%%
    if ~exist('figs/exp3','dir')
        mkdir('figs/exp3');
    end
    cell = cells(strcmp({cells.id},id));
    
    h = figure(311); clf; hold on;
    
    plottype = 3 ;
    
   if plottype==2
        cishade(cell.famplow.',[],conf.color.amp1,t,'LineWidth',lw);
        cishade(cell.famphigh.',[],conf.color.amp7,t,'LineWidth',lw);
   elseif plottype==3
        cishade(cell.famp1.',[],conf.color.amp1,t,'LineWidth',lw);
        cishade(cell.famp2.',[],conf.color.amp4,t,'LineWidth',lw);
        cishade(cell.famp3.',[],conf.color.amp7,t,'LineWidth',lw);
    end
    y = ylim();

    plot([0 0],ylim(),'k--');
    xlim([-0.5,1.5]);

    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
    set(gcf,'Position',figsz)
    print(['figs/exp3/exp3_' cell.id '_frate.eps'],'-depsc');

    %%
     
    h = figure(312); clf;
    hold on;
   
    col = [conf.color.amp1 ; conf.color.amp4 ; conf.color.amp7];%
    %col = cool(5);
    raster(cell.raster.',t,col,2)
    
    
    plot([0 0],ylim(),'k--');
    %title(sprintf('firing rate: %.1f',allfire(n)));
    xlim([-0.5,1.5]);
    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:20:100,'YTickLabel',{});
    set(gcf,'Position',figsz+[150,0,0,0])
    %axis off
    
    print(['figs/exp3/exp3_' cell.id '_raster.eps'],'-depsc');

    %%
    h = figure(313); clf; hold on;
    t2 = linspace(-1,2,256);
    plot(t2,cell.ap,'k','LineWidth',2)
    plot([t2 fliplr(t2)],[(cell.ap+cell.apstd) fliplr(cell.ap-cell.apstd)],'k','LineWidth',1)
    plot([1 1],[-100,-50],'k','LineWidth',2);
    plot([1,2],[-50,-50],'k','LineWidth',2);
    set(gca,'XTick',-1:2,'XTickLabel',{},'YTick',0:50:100,'YTickLabel',{});
    set(gcf,'Position',[400,700,150,100])
    axis off
    print(['figs/exp3/exp3_' cell.id '_ap.eps'],'-depsc');

    pause(0.1);
end

