%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function h = plot_exp3_gavg(cells,t,conf)
%plot_exp3_gavg plots for populations
%   
short = 1;
if short == 1
lw = 1.5;
figsz = [100,400,150,120];
else
lw = 2;
figsz = [100,400,300,250];
end
%%
    if ~exist('figs/exp3','dir')
        mkdir('figs/exp3');
    end
    
    h{1} = figure(351); clf; hold on;
    plottype = 3;
    col = cool(9).';
   
    if plottype==2
         cishade([cells.avgfamplow].',[],conf.color.amp1,t,'LineWidth',lw);
        cishade([cells.avgfamphigh].',[],conf.color.amp7,t,'LineWidth',lw);
    
    elseif plottype==3
        cishade([cells.avgfamp1].',[],conf.color.amp1,t,'LineWidth',lw);
        cishade([cells.avgfamp2].',[],conf.color.amp4,t,'LineWidth',lw);
        cishade([cells.avgfamp3].',[],conf.color.amp7,t,'LineWidth',lw);
    
    end
    y = ylim();

    plot([0 0],ylim(),'k--');
    xlim([-0.5,1.5])
    ylim(y);
    set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
    set(gcf,'Position',figsz)
    %print(['figs//exp3_gavg_frate.eps'],'-depsc');

   % save data
    low_ = mean([cells.avgfamp1],2).';
    mid_ = mean([cells.avgfamp2],2).';
    high_ = mean([cells.avgfamp3],2).';
    
    sel = t>= -0.5 & t < 1.5;
    tbl = table(t(sel).',low_(sel).',mid_(sel).',high_(sel).','VariableNames',{'time','low','mid','high'});
    
    writetable(tbl,'../../supdata/Fig.3a.xls');
    
    pause(0.1);
end

