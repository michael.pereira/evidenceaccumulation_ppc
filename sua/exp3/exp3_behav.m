%========================================================
%   Experiment 3 - behaviour
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script simply outputs the amplitudes of the stimuli used in Exp. 3
%%
addpath ../conf/
addpath ../functions/

conf = [];
conf.frate = 0.1;
conf.version = 3;
conf.session = 1;
config_spikes;


d = dir(['../conf/neurons/e3*_gauss' num2str(conf.frate) '_stim.mat']);

neuron = load(['../conf/neurons/' d(1).name]);

amp = neuron.behav.amp;

badtrials = false(1,length(idhit));
badtrials(conf.allbadtrials) = 1;

bad = badtrials;

idhit(bad) = 0;
idmiss(bad) = 0;
idcrej(bad) = 0;
idfa(bad) = 0;

%%
unique(amp)
