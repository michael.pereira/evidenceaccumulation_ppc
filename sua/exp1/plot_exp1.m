%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
 function h = plot_exp1(cells,id,t,conf)
%plot_exp1 Helper function to plot data from single neurons
%   Inputs:
%       - cells: an array of structures computed by exp1_prep.m and stored
%       in save/cells_exp1.mat
%       - id: the index of the SUA/MUA for plotting
%       - t: time indices for the x-axis (see exp1_analyse.m)
%       - conf: the struct of configs from ../conf/config_spikes.m
%
short = 1;
if short == 1
lw = 1;
figsz = [100,400,150,120];
else
lw = 2;
figsz = [100,400,300,250];
end
%%
if ~exist('figs/exp1','dir')
    mkdir('figs/exp1');
end
cell = cells(strcmp({cells.id},id));

fprintf('= cell %s n',cell.id);
fprintf('= SUA: %d\n',cell.sua);
fprintf('= SNR: %.1f\n',cell.snr);
fprintf('= ISI < 3 ms: %.2f %%\n',cell.isi*100);
fprintf('= Avg fire: %.1f\n',cell.fire);
fprintf('= CV2: %.2f\n',cell.cv2);
fprintf('= Bustiness: %.2f\n',cell.bi);

if ~cell.sua
    affix = 'mua_';
else
    affix = 'sua_';
end
h{1} = figure(211); clf; hold on;
rt1 = cell.fhit_rt1.';
rt2 = cell.fhit_rt2.';
rt3 = cell.fhit_rt3.';
%rt4 = cell.fhit_rt4.';

miss = cell.fmiss.';

l1 = 500+round(cell.avgrt1*conf.fs.analysis);
l2 = 500+round(cell.avgrt2*conf.fs.analysis);
l3 = 500+round(cell.avgrt3*conf.fs.analysis);
%l4 = 500+round(cell.avgrt4*conf.fs.analysis);

cishade(miss,[],conf.color.miss,t,'LineWidth',2);
%cishade(rt4(:,1:l4),[],conf.color.hit4,t(1:l4),'LineWidth',2);
cishade(rt3(:,1:l3),[],conf.color.hit3,t(1:l3),'LineWidth',2);
cishade(rt2(:,1:l2),[],conf.color.hit2,t(1:l2),'LineWidth',2);
cishade(rt1(:,1:l1),[],conf.color.hit1,t(1:l1),'LineWidth',2);
plot(xlim(),[0 0],'k');

ysave = ylim();
if ysave(2) <= 5
    yscale = -100:1:100;
else
    yscale = -100:5:100;
end
ylim([ysave(1),ysave(2)*1.1]);

plot([0 0],ylim(),'k--');

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',yscale,'YTickLabel',{});
set(gcf,'Position',[100,200,300,250])
%print(['figs/exp1/' affix 'exp1_' cell.id '_frate.eps'],'-depsc');
pause(0.1);

%%

h{2} = figure(212); clf;
hold on;
raster(cell.raster.',t,[conf.color.miss;conf.color.hit1;conf.color.hit2;conf.color.hit3;conf.color.hit4],2)
hold on;
plot(cell.raster_rt,1:length(cell.raster_rt),'k--','LineWidth',1.5);
plot([0 0],ylim(),'k--');
%title(sprintf('firing rate: %.1f',allfire(n)));

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:20:100,'YTickLabel',{});
set(gcf,'Position',[100,500,300,250])
%axis off

%print(['figs/exp1/exp1_' cell.id '_raster.eps'],'-depsc');
%saveas(h,['figs/exp1/' affix 'exp1_' cell.id '_raster.png']);
pause(0.1);

% save data
miss_ = mean(miss,1);
rt1_ = nan(size(miss_));
rt2_ = nan(size(miss_));
rt3_ = nan(size(miss_));

rt1_(1:l1) = mean(rt1(:,1:l1),1);
rt2_(1:l2) = mean(rt2(:,1:l2),1);
rt3_(1:l3) = mean(rt3(:,1:l3),1);

sel = t>= -0.5 & t < 1.5;
tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});

writetable(tbl,'../../supdata/Fig.1d_tmp.xls');
%%
t2 = linspace(-0.8,0.5,1301);
h{3} = figure(221); clf; hold on;
rt1 = cell.fhit_rt1_realign.';
rt2 = cell.fhit_rt2_realign.';
rt3 = cell.fhit_rt3_realign.';
%rt4 = cell.fhit_rt4_realign.';

l1 = 500+round(cell.avgrt1*conf.fs.analysis);
l2 = 500+round(cell.avgrt2*conf.fs.analysis);
l3 = 500+round(cell.avgrt3*conf.fs.analysis);
%l4 = 500+round(cell.avgrt4*conf.fs.analysis);

cishade(rt1,[],conf.color.hit1,t2,'LineWidth',2);
cishade(rt2,[],conf.color.hit2,t2,'LineWidth',2);
cishade(rt3,[],conf.color.hit3,t2,'LineWidth',2);
%cishade(rt4,[],conf.color.hit4,t2,'LineWidth',2);

plot([0 0],ylim(),'k--');
ylim([ysave(1),ysave(2)*1.1]);
xlim([-0.8,0.5])
set(gca,'XTick',-1:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
set(gcf,'Position',[400,200,120,250])
%print(['figs/exp1/' affix 'exp1_' cell.id '_frate_realign.eps'],'-depsc');
pause(0.1);


%%
h{4} = figure(222); clf;
hold on;
raster(cell.raster_align.',t2,[conf.color.hit1;conf.color.hit2;conf.color.hit3;conf.color.hit4],2)
hold on;
%plot(-cell.raster_align_rt,1:length(cell.raster_align_rt),'k--','LineWidth',1.5);
plot([0 0],ylim(),'k--');
%title(sprintf('firing rate: %.1f',allfire(n)));
xlim([-0.8,0.5])
set(gca,'XTick',-1:0.5:1.5,'XTickLabel',{},'YTick',0:20:100,'YTickLabel',{});
set(gcf,'Position',[400,500,120,70])
%axis off

%print(['figs/exp2/exp2_' cell.id '_raster_realign.eps'],'-depsc');
%saveas(h,['figs/exp1/' affix 'exp1_' cell.id '_raster_realign.png']);
pause(0.1);
%%
h{5} = figure(223); clf; hold on;
bins = -0.8:0.1:0.5;
psth = zeros(1,length(bins)-1);
for b=2:length(bins)
    psth(b-1) = sum(sum(cell.raster_align(t2>=bins(b-1) & t2<bins(b),:)));
end
bar(bins(1:end-1)+mode(diff(bins))./2,psth,'FaceColor',conf.color.hit);
plot([0 0],ylim(),'k--');

xlim([-0.8,0.5])
set(gca,'XTick',-1:0.5:1.5,'XTickLabel',{},'YTick',0:20:100,'YTickLabel',{});
set(gcf,'Position',[400,500,120,70])
%axis off

%print(['figs/exp1/' affix 'exp1_' cell.id '_psth_realign.eps'],'-depsc');
pause(0.1);

% save data
tbl = table((bins(1:end-1)+mode(diff(bins))./2).',psth.',...
    'VariableNames',{'RTbin','count'});

writetable(tbl,'../../supdata/Fig.1e_tmp.xls');
%%
h{6} = figure(240); clf; hold on;
t2 = linspace(-1,2,256);
plot(t2,cell.ap,'k','LineWidth',2)
plot([t2 fliplr(t2)],[(cell.ap+cell.apstd) fliplr(cell.ap-cell.apstd)],'k','LineWidth',1)
plot([1 1],[-100,-50],'k','LineWidth',2);
plot([1,2],[-50,-50],'k','LineWidth',2);
set(gca,'XTick',-1:2,'XTickLabel',{},'YTick',0:50:100,'YTickLabel',{});
set(gcf,'Position',[520,650,150,100])
axis off
%print(['figs/exp1/' affix 'exp1_' cell.id '_ap.eps'],'-depsc');

pause(0.1);
end

