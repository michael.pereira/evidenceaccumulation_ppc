%========================================================
%   Experiment 1 - responsive neurons
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script produces most analyses for experiment 1:
% number of responsive neurons, plotting SUA and average firing rates
%
% /!\ it requires running exp1_prep.m and exp1_rndperm.m which could take a
% while.

clear
addpath ../admin
addpath ../functions/

config_spikes
doplot = 1;
t = linspace(-0.5,1.5,2001);

if ~exist('cells','var') || cells(1).exp ~= 1
    fprintf('Loading precomputed data\n');
    if exist('save/cells_exp1.mat','file')
        load('save/cells_exp1.mat');
    else
        error('Could not find save/cells_exp1.mat\nyou probably should run exp1_prep.m');
    end
end
if ~exist('rndcells','var') %|| rndcells(1).exp ~= 1
    fprintf('Loading precomputed shuffled data, might take a while\n');
    if exist('save/rndcells_exp1.mat','file')
        load('save/rndcells_exp1.mat');
    else
        error('Could not find save/rndcells_exp1.mat\nyou probably should run exp1_rndperm.m');
    end
end
%% get relevant information

% ids
sua = [cells.sua]==1;
rndsua = repmat([cells.sua].'==1,[1,size(rndcells,2)]);

% p detection
detect = [cells.isdetect];
rnddetect = reshape([rndcells.isdetect],size(rndcells,1),[]);

% p corr. slope / RT
rt = [cells.isrt];
rndrt = reshape([rndcells.isrt],size(rndcells,1),[]);

% p diff. vs. baseline
hitsig = [cells.ishitsig];
rndhitsig = reshape([rndcells.ishitsig],size(rndcells,1),[]);

misssig = [cells.ismisssig];
rndmisssig = reshape([rndcells.ismisssig],size(rndcells,1),[]);

% choice probability
cp = [cells.cp_detect];
rndcp = reshape([rndcells.cp_detect],size(rndcells,1),[]);

% spike counts
frhit = [cells.spikes_hit];
rndfrhit = reshape([rndcells.spikes_hit],size(rndcells,1),[]);

frmiss = [cells.spikes_miss];
rndfrmiss = reshape([rndcells.spikes_miss],size(rndcells,1),[]);

frbase = [cells.spikes_base];
rndfrbase = reshape([rndcells.spikes_base],size(rndcells,1),[]);

%% selective neurons
figure(101); clf;
nsua = sum(sua);
fprintf('-----------------\n');
fprintf('----- count -----\n');
fprintf('-----------------\n');

%% detection-selective
iddetect = detect<0.05 & sua;
rndiddetect = rnddetect<0.05 & rndsua;
ndetect = sum(iddetect);
rndndetect = sum(rndiddetect);
reportcells(ndetect,nsua,'detection',mean(rndndetect >= ndetect));
figure(101); subplot(5,3,1); plotperm(rndndetect,ndetect,nsua,0.05); title('detect');
if doplot
    h = plot_exp1_gavg(cells(iddetect),t,conf,'iddetect',10);
    saveas(h{4},['figs/SFig2a.png']);
    %fprintf('Press a key to continue>>\n'); pause();
end
%% RT-selective
idrt = rt<0.05 & sua;
rndidrt = rndrt<0.05 & rndsua;
nrt = sum(idrt);
rndnrt = sum(rndidrt);
reportcells(nrt,nsua,'RT',mean(rndnrt >= nrt));
figure(101); subplot(5,3,2); plotperm(rndnrt,nrt,nsua,0.05); title('RT');

%% detection- and RT- selective (i.e. *evidence accumulation*)
iddetectandrt = rt<0.05 & sua;
rndiddetectandrt = rndrt<0.05  & rndsua;
ndetectandrt = sum(iddetectandrt(iddetect));
rndndetectandrt = sum(rndiddetectandrt(iddetect,:));
reportcells(ndetectandrt,ndetect,'detection + RT',mean(rndndetectandrt >= ndetectandrt));
figure(101); subplot(5,3,3); plotperm(rndndetectandrt,ndetectandrt,ndetect,0.05); title('detect + RT');
if doplot
    h = plot_exp1_gavg(cells(iddetectandrt & iddetect),t,conf,'iddetectandrt',20);
    saveas(h{4},['figs/Fig1f.png']);
    %fprintf('Press a key to continue>>\n'); pause();
end

%% Plot also MUA for SFig 3
if doplot
h = plot_exp1_gavg(cells(rt<0.05 & detect<0.05),t,conf,'mua',20);
saveas(h{4},['figs/SFig3a.png']);
saveas(h{2},['figs/SFig3b.png']);
saveas(h{3},['figs/SFig3c.png']);
end
    
fprintf('-----------------\n');
%% detection-selective and different from baseline for hits
idhit = hitsig<0.05 & sua;
rndidhit =  rndhitsig<0.05 & rndsua;
nhit = sum(idhit(iddetect));
rndnhit = sum(rndidhit(iddetect,:));
reportcells(nhit,ndetect,'detection + dif. from baseline for hits',mean(rndnhit >= nhit));
figure(101); subplot(5,3,4); plotperm(rndnhit,nhit,ndetect,0.05); title('detect + diff. baseline (hits)');

%% detection-selective and increase from baseline for hits
idhit_incr = hitsig<0.05 & (frhit > frbase) & sua;
rndidhit_incr = rndhitsig<0.05 & (rndfrhit > rndfrbase) & rndsua;
nhit_incr = sum(idhit_incr(iddetect));
rndnhit_incr = sum(rndidhit_incr(iddetect,:));
reportcells(nhit_incr,ndetect,'detection + incr. from baseline for hits',mean(rndnhit_incr >= nhit_incr));
figure(101); subplot(5,3,5); plotperm(rndnhit_incr,nhit_incr,ndetect,0.05*0.5); title('detect + incr. baseline (hits)');
if doplot
   h =  plot_exp1_gavg(cells(iddetect & idhit_incr),t,conf,'idhit_incr',30);
    saveas(h{4},['figs/SFig2b.png']);
end

%% detection-selective and different from baseline for miss
idmiss = misssig<0.05 & sua;
rndidmiss = rndmisssig<0.05 & rndsua;
nmiss = sum(idmiss(iddetect));
rndnmiss = sum(rndidmiss(iddetect,:));
reportcells(nmiss,ndetect,'detection + dif. from baseline for misses',mean(rndnmiss >= nmiss));
figure(101); subplot(5,3,7); plotperm(rndnmiss,nmiss,ndetect,0.05); title('detect + diff. baseline (misses)');

%% detection-selective and increase from baseline for miss
idmiss_incr = misssig<0.05 & (frmiss > frbase) & sua;
rndidmiss_incr = rndmisssig<0.05& (rndfrmiss > rndfrbase)  & rndsua;
nmiss_incr = sum(idmiss_incr(iddetect));
rndnmiss_incr = sum(rndidmiss_incr(iddetect,:));
reportcells(nmiss_incr,ndetect,'detection + incr. from baseline for misses',mean(rndnmiss_incr >= nmiss_incr));
figure(101); subplot(5,3,8); plotperm(rndnmiss_incr,nmiss_incr,ndetect,0.05*0.5); title('detect + incr. baseline (misses)');
if doplot
    h = plot_exp1_gavg(cells(iddetect & idmiss_incr),t,conf,'idmissincr',50);
    saveas(h{4},['figs/SFig2c.png']);    
end
%% detection-selective and decrease from baseline for miss
idmiss_decr = misssig<0.05 & (frmiss < frbase) & sua;
rndidmiss_decr = rndmisssig<0.05 & (rndfrmiss < rndfrbase)  & rndsua;
nmiss_decr = sum(idmiss_decr(iddetect));
rndnmiss_decr = sum(rndidmiss_decr(iddetect,:));
reportcells(nmiss_decr,ndetect,'detection + decr. from baseline for misses',mean(rndnmiss_decr >= nmiss_decr));
figure(101); subplot(5,3,9); plotperm(rndnmiss_decr,nmiss_decr,ndetect,0.05*0.5); title('detect + decr. baseline (misses)');


fprintf('-----------------\n');
%% detection-selective and increase from baseline for misses but not as much as hits
idmissincr1 = misssig<0.05 & idrt & (frmiss < frhit) & (frmiss > frbase) & sua;
rndidmissincr1 = rndmisssig<0.05 & rndidrt & (rndfrmiss < rndfrhit) & (rndfrmiss > rndfrbase) & rndsua;
nmissincr1 = sum(idmissincr1(iddetect));
rndnmissincr1 = sum(rndidmissincr1(iddetect,:));
reportcells(nmissincr1,ndetect,'detection + incr. from baseline for misses <  hits',mean(rndnmissincr1 >= nmissincr1));
figure(101); subplot(5,3,10); plotperm(rndnmissincr1,nmissincr1,ndetect,0.05*0.5*0.5); title('detect + incr. baseline (misses) < hits');

%% detection-selective and decrease from baseline for misses but not as much as hits
idmissdecr1 = misssig<0.05 & (frmiss > frhit) & (frmiss < frbase) & sua;
rndidmissdecr1 = rndmisssig<0.05 & (rndfrmiss > rndfrhit) & (rndfrmiss < rndfrbase)  & rndsua;
nmissdecr1 = sum(idmissdecr1(iddetect));
rndnmissdecr1 = sum(rndidmissdecr1(iddetect,:));
reportcells(nmissdecr1,ndetect,'detection + decr. from baseline for misses < decr. from hits',mean(rndnmissdecr1 >= nmissdecr1));
figure(101); subplot(5,3,11); plotperm(rndnmissdecr1,nmissdecr1,ndetect,0.05*0.5*0.5); title('detect + decr. baseline (misses) < hits');

%% detection-selective and increase from baseline for misses and more than hits
idmissincr2 = misssig<0.05 & (frmiss > frhit) & (frmiss > frbase) & sua;
rndidmissincr2 = rndmisssig<0.05 & (rndfrmiss > rndfrhit) & (rndfrmiss > rndfrbase)  & rndsua;
nmissincr2 = sum(idmissincr2(iddetect));
rndnmissincr2 = sum(rndidmissincr2(iddetect,:));
reportcells(nmissincr2,ndetect,'detection + incr. from baseline for misses > hits (miss-coding)',mean(rndnmissincr2 >= nmissincr2));
figure(101); subplot(5,3,12); plotperm(rndnmissincr2,nmissincr2,ndetect,0.05*0.5*0.5); title('detect + incr. baseline (misses) > hits');
if doplot
    h = plot_exp1_gavg(cells(iddetect & idmissincr2),t,conf,'idmissincr_gt_miss',60);
    saveas(h{4},['figs/SFig2d.png']);    
end
%% detection-selective and decrease from baseline for misses and more than hits
idmissdecr2 = misssig<0.05 & (frmiss < frhit) & (frmiss < frbase) & sua;
rndidmissdecr2 = rndmisssig<0.05 & (rndfrmiss < rndfrhit) & (rndfrmiss < rndfrbase)  & rndsua;
nmissdecr2 = sum(idmissdecr2(iddetect));
rndnmissdecr2 = sum(rndidmissdecr2(iddetect,:));
reportcells(nmissdecr2,ndetect,'detection + decr. from baseline for misses < hits',mean(rndnmissdecr2 >= nmissdecr2));
figure(101); subplot(5,3,13); plotperm(rndnmissdecr2,nmissdecr2,ndetect,0.05*0.5*0.5); title('detect + decr. baseline (misses) < hits');

%%
figure(101); set(gcf,'Position',[200,200,800,800])
%%


if doplot>=1

    %%
    h = plot_exp1(cells,'#26',t,conf);
    saveas(h{1},['figs/Fig1d_21.png']);  
    saveas(h{2},['figs/Fig1d_11.png']);    
    saveas(h{4},['figs/Fig1e_21.png']);    
    saveas(h{5},['figs/Fig1e_11.png']);    
    saveas(h{6},['figs/Fig1d_11_ap.png']);    

    h = plot_exp1(cells,'#160',t,conf);
    saveas(h{1},['figs/Fig1d_22.png']);  
    saveas(h{2},['figs/Fig1d_12.png']);    
    saveas(h{4},['figs/Fig1e_22.png']);    
    saveas(h{5},['figs/Fig1e_12.png']);    
    saveas(h{6},['figs/Fig1d_12_ap.png']);    
    
        %h = plot_exp1(cells,'#102',t,conf);

end
if doplot>=3
    
    % 19,203,258 is nice MUA
    plot_exp1(cells,'#19',t,conf)
    plot_exp1(cells,'#203',t,conf)
    plot_exp1(cells,'#258',t,conf)
end


%%
%%
if doplot>0
    %%
    letter = {'d','e'};
    figure(102); clf;
    col = conf.color.amp5;
    
    
    for ses = 1:2 %
        
        
        ss = ([cells.ses] == ses);
        ids = find([cells.isdetect] <0.05 & [cells.isrt]<0.05 & ss);
        
        
        fr = 0;
        for i=1:length(ids)
            fr_ = cells(ids(i)).fhit;
            fr = fr + fr_;
        end
        fr = fr./length(ids);
        
        clear slope
        for tr = 1:size(fr,2)
            m = round(500+cells(ids(1)).rt(tr)*1000);
            fr((m+1):end,tr) = NaN;
            y = fr(801:m,tr);
            l = length(y);
            b = regress(y,[1:l ; ones(size(1:l))].');
            slope(tr) = b(1);
        end
        
        
        [c,p] = corr(slope.',cells(ids(1)).rt,'type','Spearman');
        for r=1:1000
            perm = randperm(length(slope));
            crnd(r) = corr(slope(perm).',cells(ids(1)).rt,'type','Spearman');
        end
        fprintf('Corr RT and slope: R = %.2f, p = %.4f\n',c,mean(crnd < c));
        [~,irt] = sort(cells(ids(1)).rt);
        
        if 1
            %%
            h = figure; hold on;
            plot(cells(ids(1)).rt,slope,'ko','MarkerFaceColor',col);
            r = cells(ids(1)).rt;
            
            
            m = fitlm(r.',slope.');
            x = (0.3:0.1:1.3).';
            [y,yint] = m.predict(x);
            plot(x,y,'r','LineWidth',2);
            plot([x ; x(end:-1:1)],[yint(:,1) ; yint(end:-1:1,2)],'r','LineWidth',1);
            
            
            plot([0.3,1.3],[0,0],'k');
            set(gcf,'Position',[500+(ses==2)*100,1300,200,150]);
            axis([0.2,1.5,-0.02,0.06])
            set(gca,'XTick',0.5:0.5:1.5,'XTickLabel',{},'YTick',-0.02:0.02:0.06,'YTickLabel',{});
            saveas(h,['figs/SFig3' letter{ses} '_2.png']);
        end
        if 1
            %%
            h = figure;
            t = linspace(-0.5,1.5,2001);
            imagesc(t,1:size(fr,2),fr(:,irt).')
            hold on;
            plot([0 0],ylim(),'w--')
            plot([0.3 0.3],ylim(),'w-')
            
            if ses==1
                caxis([4,11])
            else
                caxis([5,14])
            end
            colormap('hot');
            set(gcf,'Position',[500+(ses==2)*100,1700,300,200]);
            set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',[]);
            %print(['figs/exp1/exp1_avgdrift_s' num2str(ses) '.eps'],'-depsc');
            saveas(h,['figs/SFig3' letter{ses} '_1.png']); 
            
            % save data
             tbl = table(cells(ids(1)).rt,slope.','VariableNames',{'RT','slope'});
            if ses==1
                writetable(tbl,'../../supdata/SFig.3d_inset.xls');
            elseif ses==2
                writetable(tbl,'../../supdata/SFig.3e_inset.xls');
            end
        end
    end
    %plot(rt,1:size(fr,2),'k');
end

%%
if doplot
    %%
    h = figure(103); hold on;
    crt = [cells.rtcorr];
    for i=1:length(crt)
        if sua(i) && ~isnan(detect(i))
            if detect(i)<0.05
                plot(crt(i),cp(i),'ko','MarkerFaceColor',conf.color.amp4);
            else
                plot(crt(i),cp(i),'ko','MarkerFaceColor',conf.color.amp1);
            end
            if rt(i)<0.05
                plot(crt(i),cp(i),'ro','MarkerSize',6);
            end
            if idmissincr2(i) && iddetect(i)
                plot(crt(i),cp(i),'ko','MarkerSize',6,'MarkerFaceColor',conf.color.miss);
            end
        end
    end
    plot([0 0],ylim(),'k');
    axis([-0.6,0.4,-0.6,0.6]);
    plot(xlim(),[0,0],'k');
    set(gca,'XTick',-0.6:0.2:0.6,'XTickLabel',{},'YTick',-0.6:0.2:0.6,'YTickLabel',{});
    set(gcf,'Position',[500,700,360,250])
    saveas(h,['figs/Fig1g.png']);
    
    % save data
    sel = sua & ~isnan(detect);
    tbl = table(crt(sel).',cp(sel).',detect(sel).'<0.05,rt(sel).'<0.05,idmissincr2(sel).' & iddetect(sel).',...
    'VariableNames',{'corrRT','CP','detect_sel','RT_sel','miss'});
    writetable(tbl,'../../supdata/Fig.1g.xls');
end
