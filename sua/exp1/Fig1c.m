%========================================================
%   ECOG ANALYSIS FOR EXPERIMENT 1
%   Generates Fig. 1c
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================
% this script plots behavior for experiment 1
conf = [];
conf.frate = 0.1;
conf.exp = 1;
conf.session = 1;
config_spikes;
prod = 0;
ci = @(x) std(x)./sqrt(length(x));

d = dir(['../conf/neurons/e1s1*_gauss' num2str(conf.frate) '_stim.mat']);
neuron1 = load(['../conf/neurons/' d(1).name]);

d = dir(['../conf/neurons/e1s2*_gauss' num2str(conf.frate) '_stim.mat']);
neuron2 = load(['../conf/neurons/' d(1).name]);

sdt = [neuron1.behav.sdt neuron2.behav.sdt]; 
rt = [neuron1.behav.rt neuron2.behav.rt]; 
onset = [neuron1.behav.onset neuron2.behav.onset]; 
amp = [neuron1.behav.amp neuron2.behav.amp]; 


idhit = strcmp(sdt,'hit');
idmiss = strcmp(sdt,'miss');
idfa = strcmp(sdt,'fa');

badtrials = false(1,length(sdt));
badtrials(conf.allbadtrials) = 1;

bad = badtrials;

idhit(bad) = 0;
idmiss(bad) = 0;
idfa(bad) = 0;

%%
fprintf('rate: %.1f%% hit | %d fa \n',...
    100*sum(idhit)./sum(idhit | idmiss),sum(idfa));

[h,p,~,st] = ttest2(amp(idhit),amp(idmiss));
fprintf('amplitude: %.3f hit | %.3f miss, t(%d)=%.2f, p=%.3f\n',...
    mean(amp(idhit)),mean(amp(idmiss)), ...
    st.df,st.tstat,p);

[h,p,~,st] = ttest2(onset(idhit),onset(idmiss));
fprintf('onset: %.2f hit | %.2f miss, t(%d)=%.2f, p=%.3f\n',...
    mean(onset(idhit)),mean(onset(idmiss)), ...
    st.df,st.tstat,p);

%%

h = figure(110); clf; hold on;
bar(1,sum(idhit),'FaceColor',conf.color.hit);
bar(2,sum(idmiss),'FaceColor',conf.color.miss);
if prod
    set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:50:200,'YTickLabel',{});
else
    set(gca,'XTick',1:2,'XTickLabel',{'hits','misses'},'YTick',0:50:200);    
end
set(gcf,'Position',[50,800,120,120])
print('figs/exp1_t1hist.eps','-depsc');

h = figure(111); clf; hold on;
bar(1,mean(amp(idhit)),'FaceColor',conf.color.hit);
bar(2,mean(amp(idmiss)),'FaceColor',conf.color.miss);
if prod
    set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:0.1:2,'YTickLabel',{});
else
    set(gca,'XTick',1:2,'XTickLabel',{'hits','misses'},'YTick',0:0.1:2);
end
set(gcf,'Position',[200,800,120,120])
print('figs/exp1_stimamp.eps','-depsc');

h = figure(112); clf; hold on;
[k,x] = hist(rt(idhit));%,'support','positive');
xlim([0,2])
m = mean(rt(idhit));
fprintf('Mean RT: %.3f ± %.3f\n',m,ci(rt(idhit)));
bar(x,k,'FaceColor',conf.color.hit);%,'k','LineWidth',2)
%plot(m*[1 1],ylim(),'k--','LineWidth',2);
[q] = quantile(rt(idhit),[1/3,2/3]);
plot(q(1)*[1 1],ylim(),'k--','LineWidth',2);
plot(q(2)*[1 1],ylim(),'k--','LineWidth',2);
if prod
    set(gca,'XTick',0:0.5:2,'XTickLabel',{},'YTick',0:10:30,'YTickLabel',{});
else
    set(gca,'XTick',0:0.5:2,'YTick',0:10:30);
end
set(gcf,'Position',[350,800,120,120])
print('figs/Fig1c.eps','-depsc');
tbl = table(x.',k.','VariableNames',{'RTbin','count'});
writetable(tbl,'../../supdata/Fig.1c.xls');

h = figure(113); clf; hold on;
[k,x] = hist(2+onset(idhit | idmiss));
m = mean(2+onset(idhit | idmiss));
fprintf('Mean ISI: %.3f ± %.3f\n',m,ci(2+onset(idhit | idmiss)));
bar(x,k,'FaceColor',[0.5 0.5 0.5]);%,'LineWidth',2)
plot(m*[1 1],ylim(),'k--','LineWidth',2);
set(gca,'XTick',0:5:20,'XTickLabel',{},'YTick',0:50:150,'YTickLabel',{});
xlim([0,20])
set(gcf,'Position',[500,800,120,120])
print('figs/exp1_isi.eps','-depsc');