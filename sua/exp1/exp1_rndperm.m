%========================================================
%   Experiment 1 - random permutations
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the null distributions of the number
% of responsive neurons 

clear

rng('default');
addpath ../conf
addpath ../functions/

conf.version = 1;
conf.session = 1;
config_spikes;
bad{conf.session} = conf.badtrials;
conf.session = 2;
config_spikes;
bad{conf.session} = conf.badtrials - 150;

doperm = 0;
dothr = 0;

frate = 0.1;
d = dir(['../conf/neurons/e1*_gauss' num2str(frate) '_stim.mat']);

t = linspace(-0.5,1.5,2001);
t0 = find(t > 0.3,1,'first');
t1 = find(t >= 1.5,1,'first');

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));

fprintf('     ');

for rnd = 1:conf.nperm
    fprintf('\b\b\b\b\b[%3d]',rnd);
    ch = 0;
    for n=1:length(d)
        
        
        %%
        %
        %   Load neuron data
        %   
        neuron = load(['../conf/neurons/' d(n).name]);
        [nt,ntr] = size(neuron.raster);
        
        % get behav
        id = getid(neuron.behav,bad{neuron.stat.session});
               
        % sum spikes in stats window
        nspikes = sum(neuron.raster(selstat,id.idstim),1).';
        nspikesbl = sum(neuron.raster(selbaseline,id.idstim),1).'./0.3;
        nsp_ = [nspikes; nspikesbl];
        
        frate = neuron.frate(:,id.idstim);
        
        % permute
        perm = randperm(length(nspikes));
        nspikes = nspikes(perm);
        frate = frate(:,perm);
        
        perm2 = randperm(length(nsp_));
        nsp_ = nsp_(perm2);
        nspikes_ = nsp_(1:length(nspikes));
        nspikesbl_ = nsp_(length(nspikes)+1:end);
        
        % rm non-stim
        id.idhit(~id.idstim) = [];
        id.idmiss(~id.idstim) = [];
        id.rt(~id.idstim) = [];
        id.idstim(~id.idstim) = [];

        ntr = length(id.rt);
        rndcells(n,rnd).exp = 1;
        % non-param stats
        rndcells(n,rnd).isdetect = ranksum(nspikes(id.idhit),nspikes(id.idmiss));
        rndcells(n,rnd).ishitsig = signtest(nspikes_(id.idhit),nspikesbl_(id.idhit));
        rndcells(n,rnd).ismisssig = signtest(nspikes_(id.idmiss),nspikesbl_(id.idmiss));
        
        rndcells(n,rnd).spikes_base = sum(nspikesbl_(id.idstim))/sum(id.idstim);
        rndcells(n,rnd).spikes_hit = sum(nspikes_(id.idhit))/sum(id.idhit);
        rndcells(n,rnd).spikes_miss = sum(nspikes_(id.idmiss))/sum(id.idmiss);
        % compute slope
        beta = nan(2,ntr);
        for trial = 1:ntr
            if id.idhit(trial)
                tsel = t0:(500+round(id.rt(trial)*1000));
            else
                tsel = t0:t1;
            end
            
            y = frate(tsel,trial);
            
            if all(y==0)
                b = [0 0];
            else
                x = (1:length(tsel));
                b = regress(y,[x ; ones(size(x))].');
            end
            beta(1,trial) = b(1);
            beta(2,trial) = b(2);
        end
        
        % correlate RT and slope
        [crtslope,prtslope] = corr(id.rt(id.idhit),beta(1,id.idhit).','rows','complete','type','Spearman','tail','left');
        rndcells(n,rnd).rtcorr = crtslope;
        rndcells(n,rnd).isrt = prtslope;
        rndcells(n,rnd).slope = mean(beta(1,id.idhit));
        rndcells(n,rnd).bias = mean(beta(2,id.idhit));
        
        % correlate RT and bias
        [crtbias,prtbias] = corr(id.rt(id.idhit),beta(2,id.idhit).','rows','complete','type','Spearman');
        rndcells(n,rnd).rtbias = crtbias;
        rndcells(n,rnd).isrtbias = prtbias;
        
        % choice probabilities
        [~,~,~,auc_detect] = perfcurve(id.idhit(id.idstim),nspikes(id.idstim),1);
        rndcells(n,rnd).cp_detect = 2*auc_detect-1;
        
       
    end
    if mod(rnd,100) == 0
        save('save/rndcells_exp1.mat','rndcells');
    end
end

fprintf('\n [ analysis ] DONE\n');
rndconfsave = conf;
save('save/rndcells_exp1.mat','rndcells','rndconfsave','-v7.3');