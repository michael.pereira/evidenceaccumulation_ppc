%========================================================
%   Experiment 1 - responsive neurons
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

% This script pre-computes the stats for each SUA and MUA and stores
% firing rates and rasters

clear;
addpath ../admin
addpath ../functions/


bad = cell(1,2);
conf.exp = 1;
conf.session = 1;
config_spikes;
bad{conf.session} = conf.badtrials;
conf.session = 2;
config_spikes;
bad{conf.session} = conf.badtrials - 150;

doperm = 0;
dothr = 0;

frate = 0.1;
d = dir(['../conf/neurons/e1*_gauss' num2str(frate) '_stim.mat']);
dthr = dir(['../conf/neurons/e1*_gauss' num2str(frate) '_thrstim.mat']);
dcue = dir(['../conf/neurons/e1*_gauss' num2str(frate) '_cue.mat']);

t = linspace(-0.5,1.5,2001);
t0 = find(t > 0.3,1,'first');
t1 = find(t >= 1.5,1,'first');

selstat = find(t>=conf.tstat(1) & t<conf.tstat(2));
selbaseline = find(t>=conf.tbaseline(1) & t<conf.tbaseline(2));

fprintf('     ');
for n=1:length(d)
    
    fprintf('\b\b\b\b\b[%3d]',n);
    
    %%
    
    neuron = load(['../conf/neurons/' d(n).name]);
    x = strsplit(d(n).name,'-');
    y = strsplit(x{2},'_');
    
    nspikes_ = sum(neuron.raster(selstat,:),1).';
    [~,imax(n)] = max(nspikes_);
    
    [nt,ntr] = size(neuron.raster);
    frate = neuron.frate;
    
    id = getid(neuron.behav,bad{neuron.stat.session});
    
    % realign data to RT
    irt = find(t>=0,1,'first')-1+round(id.rt*conf.fs.analysis);
    frate_align = realign(frate(:,id.idhit),irt(id.idhit),[-800,500]);
    [raster_align,rej] = realign(neuron.raster(:,id.idhit),irt(id.idhit),[-800,500]);
    idrt1_align = id.idhitrt1(id.idhit).'; idrt1_align(rej) = [];
    idrt2_align = id.idhitrt2(id.idhit).'; idrt2_align(rej) = [];
    idrt3_align = id.idhitrt3(id.idhit).'; idrt3_align(rej) = [];
    %idrt4_align = id.idhitrt4(id.idhit).'; idrt4_align(rej) = [];
    
    idrt1b_align = id.idhitrt1b(id.idhit).'; idrt1b_align(rej) = [];
    idrt2b_align = id.idhitrt2b(id.idhit).'; idrt2b_align(rej) = [];
    idrt3b_align = id.idhitrt3b(id.idhit).'; idrt3b_align(rej) = [];
    idrt4b_align = id.idhitrt4b(id.idhit).'; idrt4b_align(rej) = [];    
    idrt5b_align = id.idhitrt5b(id.idhit).'; idrt5b_align(rej) = [];    
    
    idhit = find(id.idhit); idhit(rej) = [];
    
    % stats
    cells(n).exp = 1;
    cells(n).ses = neuron.stat.session;
    cells(n).id = y{1};
    cells(n).keep = sum(id.idall);
    cells(n).sua = neuron.stat.sua;
    cells(n).fire = neuron.stat.fire;
    cells(n).isi = neuron.stat.isi;
    cells(n).cv2 = neuron.stat.cv2;
    cells(n).bi = neuron.stat.bi;
    cells(n).snr = neuron.stat.snr;
    cells(n).spikeamp = neuron.stat.spikeamp;
    cells(n).peakvar = neuron.stat.peakvar;
    cells(n).ap = neuron.ap;
    cells(n).apstd = neuron.apstd;
    
    % detection test
    nspikes = sum(neuron.raster(selstat,:),1).';
    nspikesbl = sum(neuron.raster(selbaseline,:),1).';
   % tbl = table(nspikes(id.idstim),nspikesbl(id.idstim),(id.idhit(id.idstim)),'VariableNames',{'nspikes','resp'});
    
    cells(n).isdetect = ranksum(nspikes(id.idhit),nspikes(id.idmiss));
    cells(n).ishitsig = signtest(nspikes(id.idhit)./1,nspikesbl(id.idhit)./0.3);
    cells(n).ismisssig = signtest(nspikes(id.idmiss)./1,nspikesbl(id.idmiss)./0.3);
    
    cells(n).spikes_base = sum(nspikesbl(id.idstim)./0.3)/sum(id.idstim);
    cells(n).spikes_base_hit = sum(nspikesbl(id.idhit)./0.3)/sum(id.idhit);
    cells(n).spikes_base_miss = sum(nspikesbl(id.idmiss)./0.3)/sum(id.idmiss);
    cells(n).spikes_hit = sum(nspikes(id.idhit)./1)/sum(id.idhit);
    cells(n).spikes_miss = sum(nspikes(id.idmiss)./1)/sum(id.idmiss);
    
    % compute slope
    beta = nan(2,ntr);
    for trial = 1:ntr
        if id.idhit(trial)
            tsel = t0:(500+round(id.rt(trial)*1000));
        else
            tsel = t0:t1;
        end
        
        y = frate(tsel,trial);
        
        if all(y==0)
            b = [0 0];
        else
            x = (1:length(tsel));
            b = regress(y,[x ; ones(size(x))].');
        end
        beta(1,trial) = b(1);
        beta(2,trial) = b(2);
        % =====
        
    end
    %%
    nhit = length(idhit);
    nbins = floor(id.rt(idhit) / 0.1);
    spkcnt = nan(1,sum(nbins));
    spktrial = nan(1,sum(nbins));
    spkbin = nan(1,sum(nbins));
    spkrt = nan(1,sum(nbins));
    sp = nan(nhit,max(nbins));
    x = 0;
    for trial = 1:nhit
         for bin = 1:nbins(trial)
            x = x+1;
            spkcnt(x) = sum(neuron.raster(500+(bin-1)*100 + (1:100),trial));
            spktrial(x) = trial;
            spkbin(x) = bin;
            spkrt(x) = id.rt(idhit(trial));
            sp(trial,bin) = spkcnt(x);
         end
    end
    tbl = table(spkcnt.',spktrial.',spkbin.',spkrt.','VariableNames',{'cnt','trial','bin','rt'});
    m = fitglme(tbl(tbl.bin>=2,:),'cnt ~ bin*rt','Distribution','Poisson','Link','log');
    cells(n).glmcoeff1 = m.Coefficients.pValue(2);
    cells(n).glmcoeff2 = m.Coefficients.pValue(3);
    cells(n).glmcoeff3 = m.Coefficients.pValue(4);
    
     m = fitglme(tbl(tbl.bin>=2,:),'cnt ~ bin:rt','Distribution','Poisson','Link','log');
    cells(n).glmcoeffb = m.Coefficients.pValue(2);
   
%%
    % correlate RT and slope
    [crtslope,prtslope] = corr(id.rt(id.idhit),beta(1,id.idhit).','rows','complete','type','Spearman','tail','left');    
    cells(n).rtcorr = crtslope;
    cells(n).isrt = prtslope;
    cells(n).slope = mean(beta(1,id.idhit));    
    cells(n).bias = mean(beta(2,id.idhit));
   
    % correlate RT and bias
    [crtbias,prtbias] = corr(id.rt(id.idhit),beta(2,id.idhit).','rows','complete','type','Spearman');
    cells(n).rtbias = crtbias;
    cells(n).isrtbias = prtbias;
    
    % choice probabilities
    [~,~,~,auc_detect] = perfcurve(id.idhit(id.idstim),nspikes(id.idstim),1);
    cells(n).cp_detect = 2*auc_detect-1;
    
    % store raster aligned to stim onset
    [srt1,isrt1] = sort(id.rt(id.idhitrt1)); id1 = find(id.idhitrt1);
    [srt2,isrt2] = sort(id.rt(id.idhitrt2)); id2 = find(id.idhitrt2);
    [srt3,isrt3] = sort(id.rt(id.idhitrt3)); id3 = find(id.idhitrt3);
    %[srt4,isrt4] = sort(id.rt(id.idhitrt4)); id4 = find(id.idhitrt4);
    cells(n).raster = [neuron.raster(:,id.idmiss), ...
        neuron.raster(:,id1(isrt1))*2, neuron.raster(:,id2(isrt2))*3 neuron.raster(:,id3(isrt3))*4];
    cells(n).raster_rt = [nan(sum(id.idmiss),1) ; srt1 ; srt2 ; srt3].';
    
    % store raster aligned to RT
    [srt1,isrt1] = sort(id.rt(idhit(idrt1_align))); id1 = find(idhit(idrt1_align));
    [srt2,isrt2] = sort(id.rt(idhit(idrt2_align))); id2 = find(idhit(idrt2_align));
    [srt3,isrt3] = sort(id.rt(idhit(idrt3_align))); id3 = find(idhit(idrt3_align));
    cells(n).raster_align = [ ...
        raster_align(:,id1(isrt1))*1, raster_align(:,id2(isrt2))*2, raster_align(:,id3(isrt3))*3];
    cells(n).raster_align_rt = [srt1 ; srt2 ; srt3].';
    
    % store median RT
    % 3 bins
    cells(n).avgrt1 = median(id.rt(id.idhitrt1));
    cells(n).avgrt2 = median(id.rt(id.idhitrt2));
    cells(n).avgrt3 = median(id.rt(id.idhitrt3));
    % 5 bins
    cells(n).avgrt1b = median(id.rt(id.idhitrt1b));
    cells(n).avgrt2b = median(id.rt(id.idhitrt2b));
    cells(n).avgrt3b = median(id.rt(id.idhitrt3b));
    cells(n).avgrt4b = median(id.rt(id.idhitrt4b));
    cells(n).avgrt5b = median(id.rt(id.idhitrt5b));
    
    % store firing rates
    cells(n).fhit = frate(:,id.idhit);
    cells(n).rt = id.rt(id.idhit);
    cells(n).fhit_rt1 = frate(:,id.idhitrt1);
    cells(n).fhit_rt2 = frate(:,id.idhitrt2);
    cells(n).fhit_rt3 = frate(:,id.idhitrt3);
    cells(n).fmiss = frate(:,id.idmiss);

    % store firing rates aligned to RT
    cells(n).fhit_rt1_realign = frate_align(:,idrt1_align);
    cells(n).fhit_rt2_realign = frate_align(:,idrt2_align);
    cells(n).fhit_rt3_realign = frate_align(:,idrt3_align);
    
    %
    %   Average firing rates, normalized to baseline
    %
    s = mean(mean(frate(selbaseline,:)));
    cells(n).avgfhit = mean(frate(:,id.idhit)./s,2);
    cells(n).avgfmiss = mean(frate(:,id.idmiss)./s,2);

    % 3 bins
    cells(n).avgfhit_rt1 = mean(frate(:,id.idhitrt1)./s,2);
    cells(n).avgfhit_rt2 = mean(frate(:,id.idhitrt2)./s,2);
    cells(n).avgfhit_rt3 = mean(frate(:,id.idhitrt3)./s,2);    
    cells(n).avgfhit_rt1_realign = mean(frate_align(:,idrt1_align)./s,2);
    cells(n).avgfhit_rt2_realign = mean(frate_align(:,idrt2_align)./s,2);
    cells(n).avgfhit_rt3_realign = mean(frate_align(:,idrt3_align)./s,2);

    % 5 bins
    cells(n).avgfhit_rt1b = mean(frate(:,id.idhitrt1b)./s,2);
    cells(n).avgfhit_rt2b = mean(frate(:,id.idhitrt2b)./s,2);
    cells(n).avgfhit_rt3b = mean(frate(:,id.idhitrt3b)./s,2);    
    cells(n).avgfhit_rt4b = mean(frate(:,id.idhitrt4b)./s,2);
    cells(n).avgfhit_rt5b = mean(frate(:,id.idhitrt5b)./s,2);
    
    cells(n).avgfhit_rt1b_realign = mean(frate_align(:,idrt1b_align)./s,2);
    cells(n).avgfhit_rt2b_realign = mean(frate_align(:,idrt2b_align)./s,2);
    cells(n).avgfhit_rt3b_realign = mean(frate_align(:,idrt3b_align)./s,2);
    cells(n).avgfhit_rt4b_realign = mean(frate_align(:,idrt4b_align)./s,2);
    cells(n).avgfhit_rt5b_realign = mean(frate_align(:,idrt5b_align)./s,2);
end


fprintf('\n [ analysis ] DONE\n');
confsave = conf;


save('save/cells_exp1.mat','cells','confsave','-v7.3');