%========================================================
%   ECOG ANALYSIS FOR EXPERIMENT 1
%   Generates Fig. 1h, Fig. 1i and Supplementary Fig. 3c
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================
clear
addpath ../conf/
addpath ../functions/
conf.exp = 1;
config_spikes;
addpath(genpath(conf.dir.toolbox_ielvis));

global globalFsDir

prod = 0;
for type = {'lfp','hga'}; % 'lfp' or 'hga' for high gamma

% select
if strcmp(type,'lfp')
    sel = 'trlfp_sm'; % load smoothed version
    selstat = 'trlfp'; % but do stats on the non-smoothed
elseif strcmp(type,'hga')
    sel = 'trhga';
    selstat = 'trhga';
else
    error('Unknown type');
end

fs = conf.ecog.fs;
selid = 0.5*fs+(1:2*fs);
statid = 1.5*fs+(1:1*fs);

%% load data
% first session
f1 = [conf.dir.preprocdir '/e1s1/ecog' sel '_e1s1.mat'];
fprintf('Loading %s\n',f1);
d1 = load(f1);
trials = d1.(sel);
f1stat = [conf.dir.preprocdir '/e1s1/ecog' selstat '_e1s1.mat'];
fprintf('Loading %s\n',f1stat);
d1stat = load(f1stat);
trialsstat = d1stat.(selstat);

d = dir(['../conf/neurons/e1s1*_gauss0.1_stim.mat']);
neuron = load(['../conf/neurons/' d(1).name]);
amp = neuron.behav.amp;
resp = strcmp(neuron.behav.sdt,'hit');
rt = neuron.behav.rt;

% second session
f2 = [conf.dir.preprocdir '/e1s2/ecog' sel '_e1s2.mat'];
fprintf('Loading %s\n',f2);
d2 = load(f2);
f2stat = [conf.dir.preprocdir '/e1s2/ecog' selstat '_e1s2.mat'];
fprintf('Loading %s\n',f2stat);
d2stat = load(f2stat);
d = dir(['../conf/neurons/e1s2*_gauss0.1_stim.mat']);
neuron = load(['../conf/neurons/' d(1).name]);
amp = [amp neuron.behav.amp];
resp = [resp strcmp(neuron.behav.sdt,'hit')];
rt = [rt neuron.behav.rt];
keep = true(size(rt));
keep(conf.allbadtrials) = 0;

% concatenate
trials = nan(size(d1.(sel),1)+size(d2.(sel),1),size(d1.(sel),2),size(d1.(sel),3));
trials(1:size(d1.(sel),1),:,:) = d1.(sel);
trials(size(d1.(sel),1)+(1:size(d2.(sel),1)),:,:) = d2.(sel);

trialsstat = nan(size(d1stat.(selstat),1)+size(d2stat.(selstat),1),size(d1stat.(selstat),2),size(d1stat.(selstat),3));
trialsstat(1:size(d1stat.(selstat),1),:,:) = d1stat.(selstat);
trialsstat(size(d1stat.(selstat),1)+(1:size(d2stat.(selstat),1)),:,:) = d2stat.(selstat);

fprintf('hits: %d, misses: %d\n',sum(resp & keep),sum(~resp & keep));

% define indices
rt_ = rt(resp==1 & keep);
qrt = quantile(rt_,[1/3,2/3]);
idrt1 = resp & keep & rt < qrt(1);
idrt2 = resp & keep & rt >= qrt(1) & rt < qrt(2);
idrt3 = resp & keep & rt >= qrt(2);
idmiss = resp==0 & keep;
idhit = resp==1 & keep;

%% PLOT ALL CHANNELS
[ntr,nt,nchan] = size(trials);
t = linspace(-1,2,nt);
h = figure(101); clf;
dif = zeros(1,nchan);
initrow = 21; row = initrow;
laton = nan(1,nchan);
latoff = nan(1,nchan);

for ch=1:nchan
    subplot(6,4,row); hold on;
    
    %figure(); hold on;
    row = row - 4;
    if row < 1
        initrow = initrow+1;
        row = initrow;
    end
    
    lw = 1.5;
    
    cishade(trials(idmiss,:,ch),0.2,conf.color.miss,t,'LineWidth',lw);
    
    cishade(trials(idrt1,:,ch),0.4,conf.color.hit1,t,'LineWidth',lw);
    cishade(trials(idrt2,:,ch),0.2,conf.color.hit2,t,'LineWidth',lw);
    cishade(trials(idrt3,:,ch),0.2,conf.color.hit3,t,'LineWidth',lw);
    plot(mean(rt(idrt1))*[1 1],ylim(),'--','Color',conf.color.hit1,'LineWidth',1);
    plot(mean(rt(idrt2))*[1 1],ylim(),'--','Color',conf.color.hit2,'LineWidth',1);
    plot(mean(rt(idrt3))*[1 1],ylim(),'--','Color',conf.color.hit3,'LineWidth',1);
    
    
    
    avg = mean(trialsstat(:,statid,ch),2);
    tbl = table(avg,resp.','VariableNames',{'avg','resp'});
    if strcmp(type,'lfp')
        m = fitlm(tbl,'avg ~ resp');
    else
        m = fitglm(tbl,'avg ~ resp','Distribution','gamma','Link','log');
    end
    coeff(ch) = m.Coefficients.Estimate(2);
    pval(ch) = m.Coefficients.pValue(2);
    y = ylim();
    
    xlim([-0.5,1.5]);
    plot([0 0],ylim(),'k--');
    set(gca,'XTick',-0.5:0.5:1.5);
    title(sprintf('ch %d',ch));
    xlabel('Time [s]');
    ylabel('Amplitude [{\mu}V]');
    
    pause(0.1);
end
set(gcf,'Position',[100,100,600,600])
saveas(h,['figs/exp1_ecog_' type{1} '.png']);

%% PLOT PIAL AND COEFFICIENTS
globalFsDir = conf.dir.brainmodel;

elecNames=cell(nchan,1);
for a=1:nchan
    elecNames{a}=sprintf('G%d',a);
end

col = hot(201);
if strcmp(type,'lfp')
    idx = min(round(abs(coeff)*10),150);
else
    idx = min(round(abs(coeff)*2000),150);
end
%idx = min(round(abs(coeffrt./3000)*10),150);
colors = col(idx,:);
colors(pval > 0.05/24,:) = 0;

colors = [colors ; [76,168,41]./255];
%elecNames(pval > 0.05) = [];
elecNames{end+1} = 'Utah1';

cfg = [];
%cfg.surfType = 'pial';
cfg.figId=102;
cfg.elecNames=elecNames;
cfg.pullOut=0;
cfg.bidsDir = conf.dir.data;
cfg.elecColors=colors;%[-coeff2.' ; 0];
cfg.elecColorScale=[-40,40];
cfg.elecCoord='LEPTO';
cfg.elecCbar = 'n';
cfg.showLabels='n';
cfg.elecUnits='r';
cfg.title='';
cfg.view = 'l';
cfg.elecSize=3;
cfg.elecShape='sphere';
%cfg.opaqueness = 0.7;

figure();
plotPialSurf('patient',cfg);
view(-68,46);
colorbar
colormap('hot')
caxis([0,20])
set(gcf,'Position',[100,100,800,800])
if strcmp(type,'lfp')
    print('figs/Fig1h.png','-dpng');
else
    print(['figs/exp1_ecogpial_' type{1} '.png'],'-dpng');
end

%% PLOT ELECTRODE NEXT TO UTAH ARRAY
alpha = [];
for ch = [14]
    figure(); hold on;
    
    lw = 1.5;
    
    cishade(trials(idmiss,:,ch),alpha,conf.color.miss,t,'LineWidth',lw);
    if 1
        rt1 = mean(rt(idrt1));
        rt2 = mean(rt(idrt2));
        rt3 = mean(rt(idrt3));
        
        l1 = find(t<rt1,1,'last');
        l2 = find(t<rt2,1,'last');
        l3 = find(t<rt3,1,'last');
        cishade(trials(idrt3,1:l3,ch),alpha,conf.color.hit3,t(1:l3),'LineWidth',lw);
        cishade(trials(idrt2,1:l2,ch),alpha,conf.color.hit2,t(1:l2),'LineWidth',lw);
        cishade(trials(idrt1,1:l1,ch),alpha,conf.color.hit1,t(1:l1),'LineWidth',lw);
        y = ylim();
    else
        cishade(trials(idhit,:,ch),alpha,conf.color.hit,t,'LineWidth',lw);
        
    end

    plot(xlim(),[0 0],'k-');
    
    %plot(t2(1:10:end),h,'k','LineWidth',2);
    xlim([-0.5,1.5]);
    ylim(y);
    % ylim([-120,100]);
    plot([0 0],ylim(),'k--');
    set(gca,'XTick',-0.5:0.5:1.5);
    %title(sprintf('ch %d',ch));
    if strcmp(type,'lfp')
        dy = 20;
    else
        dy = 0.1;
    end
    if ~prod
        xlabel('Time [s]');
        ylabel('Amplitude [{\mu}V]');
        set(gca,'XTick',-0.5:0.5:1.5,'YTick',-100:dy:100);
    else
        set(gca,'XTick',-0.5:0.5:1.5,'YTick',-100:dy:100,'XTickLabel',{},'YTickLabel',{});
    end
    set(gcf,'Position',[100,100,150,120])
    if strcmp(type,'lfp')
        print('figs/Fig1i.eps','-depsc');
        
        %  save data
        miss_ = mean(trials(idmiss,:,ch),1);
        rt1_ = nan(size(miss_));
        rt2_ = nan(size(miss_));
        rt3_ = nan(size(miss_));
        
        rt1_(1:l1) = mean(trials(idrt1,1:l1,ch),1);
        rt2_(1:l2) = mean(trials(idrt2,1:l2,ch),1);
        rt3_(1:l3) = mean(trials(idrt3,1:l3,ch),1);
        sel = t>= -0.5 & t < 1.5; 
        tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
        writetable(tbl,'../../supdata/Fig.1i.xls');
    else
        print('figs/SFig4.eps','-depsc');
    end
    pause(0.1);
end

end