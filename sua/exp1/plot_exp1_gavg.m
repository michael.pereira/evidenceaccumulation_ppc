%========================================================
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
 
function h = plot_exp1_gavg(cells,t,conf,suffix,plotid)
%plot_exp1_gavg 
%   
if nargin < 4
    suffix = '';
else
    suffix = ['_' suffix];
end
if nargin < 5
    plotid = 10;
end
short = 1;
if short == 1
    lw = 1.5;
    figsz = [100,100+plotid*10,150,120];
else
    lw = 2;
    figsz = [100,100+plotid*10,300,250];
end
%%
if ~exist('figs/exp1','dir')
    mkdir('figs/exp1');
end

%%
h{1} = figure(100+plotid+1); clf; hold on;
hit = [cells.avgfhit].';
miss = [cells.avgfmiss].';

cishade(miss,[],conf.color.miss,t,'LineWidth',lw);
cishade(hit,[],conf.color.hit,t,'LineWidth',lw);

yl = [0.2,3.5]; ylim(yl);
plot(xlim(),[1 1],'k-');

plot([0 0],yl,'k--');
xlim([-0.5,1.5])
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
set(gcf,'Position',figsz + [0,0,0,0])
print(['figs/exp1/exp1_gavg_frate_hitmiss' suffix '.eps'],'-depsc');

%%
h{2} = figure(100+plotid+2); clf; hold on;

rt1 = cells(1).avgrt1b;
rt2 = cells(1).avgrt2b;
rt3 = cells(1).avgrt3b;
rt4 = cells(1).avgrt4b;
rt5 = cells(1).avgrt5b;

l1 = find(t < rt1,1,'last');
l2 = find(t < rt2,1,'last');
l3 = find(t < rt3,1,'last');
l4 = find(t < rt4,1,'last');
l5 = find(t < rt5,1,'last');

fr1 = [cells.avgfhit_rt1b].';
fr2 = [cells.avgfhit_rt2b].';
fr3 = [cells.avgfhit_rt3b].';
fr4 = [cells.avgfhit_rt4b].';
fr5 = [cells.avgfhit_rt5b].';

frmiss = [cells.avgfmiss].';

lw2 = lw;
plot(t(l1:end),mean(fr1(:,l1:end),1),'--','Color',conf.color.hit1,'LineWidth',lw2);
plot(t(l2:end),mean(fr2(:,l2:end),1),'--','Color',conf.color.hit2,'LineWidth',lw2);
plot(t(l3:end),mean(fr3(:,l3:end),1),'--','Color','b','LineWidth',lw2);
plot(t(l4:end),mean(fr4(:,l4:end),1),'--','Color',conf.color.hit3,'LineWidth',lw2);
plot(t(l5:end),mean(fr5(:,l5:end),1),'--','Color',conf.color.hit4,'LineWidth',lw2);

cishade(frmiss,[],conf.color.miss,t,'LineWidth',lw);
cishade(fr1(:,1:l1),[],conf.color.hit1,t(1:l1),'LineWidth',lw);
cishade(fr2(:,1:l2),[],conf.color.hit2,t(1:l2),'LineWidth',lw);
cishade(fr3(:,1:l3),[],'b',t(1:l3),'LineWidth',lw);
cishade(fr4(:,1:l4),[],conf.color.hit3,t(1:l4),'LineWidth',lw);
cishade(fr5(:,1:l5),[],conf.color.hit4,t(1:l5),'LineWidth',lw);

yl = [0.2,3.5]; ylim(yl);

plot([0 0],yl,'k--');
xlim([-0.5,1.5])
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:0.5:30,'YTickLabel',{});
set(gcf,'Position',figsz + [200,0,0,0])
print(['figs/exp1/exp1_gavg_frate_q5' suffix '.eps'],'-depsc');

% save data
if strcmp(suffix,'_mua')
    miss_ = mean(frmiss,1);
    rt1_ = mean(fr1,1);
    rt2_ = mean(fr2,1);
    rt3_ = mean(fr3,1);
    rt4_ = mean(fr4,1);
    rt5_ = mean(fr5,1);
    
    sel = t>= -0.5 & t < 1.5;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',rt4_(sel).',rt5_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','rt4','rt5','miss'});
    writetable(tbl,'../../supdata/SFig.3b.xls');
end
%%

h{3} = figure(100+plotid+3); clf; hold on;

rt1 = cells(1).avgrt1b;
rt2 = cells(1).avgrt2b;
rt3 = cells(1).avgrt3b;
rt4 = cells(1).avgrt4b;
rt5 = cells(1).avgrt5b;

l1 = find(t < rt1,1,'last');
l2 = find(t < rt2,1,'last');
l3 = find(t < rt3,1,'last');
l4 = find(t < rt4,1,'last');
l5 = find(t < rt5,1,'last');

fr1 = [cells.avgfhit_rt1b_realign].';
fr2 = [cells.avgfhit_rt2b_realign].';
fr3 = [cells.avgfhit_rt3b_realign].';
fr4 = [cells.avgfhit_rt4b_realign].';
fr5 = [cells.avgfhit_rt5b_realign].';

t2 = (-800:500)./1000;
%cishade(rt1,[],conf.color.hit1,t2,'LineWidth',lw);
%cishade(rt2,[],conf.color.hit2,t2,'LineWidth',lw);
%cishade(rt3,[],conf.color.hit3,t2,'LineWidth',lw);

plot(t2,mean(fr1),'LineWidth',lw,'Color',conf.color.hit1);
plot(t2,mean(fr2),'LineWidth',lw,'Color',conf.color.hit2);
plot(t2,mean(fr3),'LineWidth',lw,'Color','b');
plot(t2,mean(fr4),'LineWidth',lw,'Color',conf.color.hit3);
plot(t2,mean(fr5),'LineWidth',lw,'Color',conf.color.hit4);

yl = [0.2,3.5]; ylim(yl);

plot([0 0],yl,'k--');
plot(xlim(),[0,0],'k');
xlim([-0.5,0.5])
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:0.5:30,'YTickLabel',{});
set(gcf,'Position',figsz+[400,0,0,0])
print(['figs/exp1/exp1_gavg_frate_realign_q5' suffix '.eps'],'-depsc');

% save data
if strcmp(suffix,'_mua')
    rt1_ = mean(fr1,1);
    rt2_ = mean(fr2,1);
    rt3_ = mean(fr3,1);
    rt4_ = mean(fr4,1);
    rt5_ = mean(fr5,1);
    
    sel = t2>= -0.5 & t2 < 0.5;
    tbl = table(t2(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',rt4_(sel).',rt5_(sel).','VariableNames',{'time','rt1','rt2','rt3','rt4','rt5'});
    writetable(tbl,'../../supdata/SFig.3c.xls');
end
%%
%%
h{4} = figure(100+plotid+4); clf; hold on;

rt1 = cells(1).avgrt1;
rt2 = cells(1).avgrt2;
rt3 = cells(1).avgrt3;

l1 = find(t < rt1,1,'last');
l2 = find(t < rt2,1,'last');
l3 = find(t < rt3,1,'last');

fr1 = [cells.avgfhit_rt1].';
fr2 = [cells.avgfhit_rt2].';
fr3 = [cells.avgfhit_rt3].';
frmiss = [cells.avgfmiss].';

lw2 = lw;
plot(t(l1:end),mean(fr1(:,l1:end),1),'--','Color',conf.color.hit1,'LineWidth',lw2);
plot(t(l2:end),mean(fr2(:,l2:end),1),'--','Color',conf.color.hit2,'LineWidth',lw2);
plot(t(l3:end),mean(fr3(:,l3:end),1),'--','Color',conf.color.hit3,'LineWidth',lw2);

cishade(frmiss,[],conf.color.miss,t,'LineWidth',lw);
cishade(fr1(:,1:l1),[],conf.color.hit1,t(1:l1),'LineWidth',lw);
cishade(fr2(:,1:l2),[],conf.color.hit2,t(1:l2),'LineWidth',lw);
cishade(fr3(:,1:l3),[],conf.color.hit3,t(1:l3),'LineWidth',lw);
yl = [0.2,3.5]; ylim(yl);

plot([0 0],yl,'k--');
plot(xlim(),[1,1],'k');
xlim([-0.5,1.5])
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:0.5:30,'YTickLabel',{});
set(gcf,'Position',figsz + [600,0,0,0])
print(['figs/exp1/exp1_gavg_frate_q3' suffix '.eps'],'-depsc');

% save data
miss_ = mean(frmiss,1);
rt1_ = mean(fr1,1);
rt2_ = mean(fr2,1);
rt3_ = mean(fr3,1);

sel = t>= -0.5 & t < 1.5;
tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
if strcmp(suffix,'_mua')
    writetable(tbl,'../../supdata/SFig.3a.xls');
elseif strcmp(suffix,'_iddetectandrt')
    rt1_(l1:end) = NaN;
    rt2_(l2:end) = NaN;
    rt3_(l3:end) = NaN;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});    
    writetable(tbl,'../../supdata/Fig.1f.xls');
elseif strcmp(suffix,'_iddetect')
    rt1_(l1:end) = NaN;
    rt2_(l2:end) = NaN;
    rt3_(l3:end) = NaN;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
    writetable(tbl,'../../supdata/SFig.2a.xls');
elseif strcmp(suffix,'_idhit_incr')
    rt1_(l1:end) = NaN;
    rt2_(l2:end) = NaN;
    rt3_(l3:end) = NaN;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
    writetable(tbl,'../../supdata/SFig.2b.xls');
elseif strcmp(suffix,'_idmissincr')
    rt1_(l1:end) = NaN;
    rt2_(l2:end) = NaN;
    rt3_(l3:end) = NaN;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
    writetable(tbl,'../../supdata/SFig.2c.xls');
elseif strcmp(suffix,'_idmissincr_gt_miss')
    rt1_(l1:end) = NaN;
    rt2_(l2:end) = NaN;
    rt3_(l3:end) = NaN;
    tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});
    writetable(tbl,'../../supdata/SFig.2d.xls');
    
end

%%

h{5} = figure(100+plotid+5); clf; hold on;

rt1 = cells(1).avgrt1;
rt2 = cells(1).avgrt2;
rt3 = cells(1).avgrt3;

l1 = find(t < rt1,1,'last');
l2 = find(t < rt2,1,'last');
l3 = find(t < rt3,1,'last');

fr1 = [cells.avgfhit_rt1_realign].';
fr2 = [cells.avgfhit_rt2_realign].';
fr3 = [cells.avgfhit_rt3_realign].';

t2 = (-800:500)./1000;
%cishade(rt1,[],conf.color.hit1,t2,'LineWidth',lw);
%cishade(rt2,[],conf.color.hit2,t2,'LineWidth',lw);
%cishade(rt3,[],conf.color.hit3,t2,'LineWidth',lw);

plot(t2,mean(fr1),'LineWidth',lw,'Color',conf.color.hit1);
plot(t2,mean(fr2),'LineWidth',lw,'Color',conf.color.hit2);
plot(t2,mean(fr3),'LineWidth',lw,'Color',conf.color.hit3);
yl = [0.2,3.5]; ylim(yl);

plot([0 0],yl,'k--');
xlim([-0.5,0.5])

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:0.5:30,'YTickLabel',{});
set(gcf,'Position',figsz + [800,0,0,0])
print(['figs/exp1/exp1_gavg_frate_realign_q3' suffix '.eps'],'-depsc');

% save data 
if strcmp(suffix,'_iddetectandrt')
    rt1_ = mean(fr1,1);
    rt2_ = mean(fr2,1);
    rt3_ = mean(fr3,1);
    
    
    sel = t2>= -0.5 & t2 < 0.5;
    tbl = table(t2(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).','VariableNames',{'time','rt1','rt2','rt3'});
    writetable(tbl,'../../supdata/Fig.1f_inset.xls');
end


pause(0.1);
end

