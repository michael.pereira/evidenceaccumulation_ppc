%========================================================
%   PREPROCESSING STEP 6
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This script just joins all the stats computed in STEP 5
addpath ../conf
addpath ../functions
plotmua = 0;
config_spikes;

cells1 = readtable('../conf/neurons/units_e1s1.csv');
cells1.exp = cells1.fire*0+1;
cells1.session = cells1.fire*0+1;

cells2 = readtable('../conf/neurons/units_e1s2.csv');
cells2.exp = cells2.fire*0+1;
cells2.session = cells2.fire*0+2;

cells3 = readtable('../conf/neurons/units_e2s1.csv');
cells3.exp = cells3.fire*0+2;
cells3.session = cells3.fire*0+1;

cells4 = readtable('../conf/neurons/units_e3s1.csv');
cells4.exp = cells4.fire*0+3;
cells4.session = cells4.fire*0+1;

cells = [cells1 ; cells2 ; cells3 ; cells4];
writetable(cells,'../conf/neurons/units.csv');


%%
fprintf(' [step 6] TOTAL: %d units, %d single\n',length(cells.sua),sum(cells.sua));
fprintf(' [step 6] Exp 1: %d units, %d single\n',length(cells1.sua)+length(cells2.sua),sum(cells1.sua)+sum(cells2.sua));
fprintf(' [step 6] Exp 2: %d units, %d single\n',length(cells3.sua),sum(cells3.sua));
fprintf(' [step 6] Exp 3: %d units, %d single\n',length(cells4.sua),sum(cells4.sua));

%%
histtype = 'histogram';

nneu = length(cells.id);
h = figure(201); clf;
subplot(331); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.fire(cells.sua==1,:),0:1:16,'support','positive');
    hmua = ksdensity(cells.fire,0:1:16,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.fire(cells.sua==1,:),0:1:16);
    hmua = hist(cells.fire,0:1:16);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2);
end
xlabel('Mean firing rate [spikes/s]'); ylabel('Count');
title(sprintf('FR: %.2f±%.2f Hz\n',mean(cells.fire(cells.sua==1)),1.96*std(cells.fire(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1a.xls');

subplot(332); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.spikeamp(cells.sua==1,:),0:20:400);
    hmua = ksdensity(cells.spikeamp,0:20:400);
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.spikeamp(cells.sua==1,:),0:20:400);
    hmua = hist(cells.spikeamp,0:20:400);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2);
end
xlabel('Spike amplitude'); ylabel('Count');
title(sprintf('Spike amplitude: %.2f±%.2f\n',mean(cells.spikeamp(cells.sua==1)),1.96*std(cells.spikeamp(cells.sua==1))./sqrt(sum(cells.sua==1))));
xlim([0,400])
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1b.xls');

subplot(333); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.snr(cells.sua==1,:),0:0.5:30,'support','positive');
    hmua = ksdensity(cells.snr,0:0.5:30,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.snr(cells.sua==1,:),0:1:30);
    hmua = hist(cells.snr,0:1:30);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2);
end

%hist(cells.snr,0:0.5:30);
%hold on;
%plot([3 3],ylim(),'r--');
xlabel('Peak SNR'); ylabel('Count');
title(sprintf('SNR: %.2f±%.2f\n',mean(cells.snr(cells.sua==1)),1.96*std(cells.snr(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1c.xls');

subplot(334); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.isi(cells.sua==1,:)*100,-0.25:0.2:20);
    hmua = ksdensity(cells.isi*100,-0.25:0.2:20);
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.isi(cells.sua==1,:)*100,0:0.2:20);
    hmua = hist(cells.isi*100,0:0.2:20);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2);
end
%hist(cells.isi*100,-0.25:0.2:3);
xlabel('% ISI < 3ms'); ylabel('Count');
title(sprintf('ISI: %.2f±%.2f %%\n',mean(cells.isi(cells.sua==1)*100),1.96*std(cells.isi(cells.sua==1)*100)./sqrt(sum(cells.sua==1))));
xlim([-0.2,3.4])
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1d.xls');

subplot(335); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.d(cells.sua==1,:),0:0.5:70,'support','positive');
    hmua = ksdensity(cells.d,0:0.5:70,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.d(cells.sua==1 & ~isnan(cells.d),:),0:2:70);
    hmua = hist(cells.d(~isnan(cells.d),:),0:2:70);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2)
end
%hist((cells.d),0:0.5:70);
hold on
%plot([4 4],ylim(),'r--');

xlabel('Proj. dist.'); ylabel('Count');
title(sprintf('projD: %.2f±%.2f\n',nanmean(cells.d(cells.sua==1)),1.96*nanstd(cells.d(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1e.xls');


subplot(336); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.cv2(cells.sua==1,:),0:0.1:2,'support','positive');
    hmua = ksdensity(cells.cv2,0:0.1:2,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.cv2(cells.sua==1,:),0:0.1:2);
    hmua = hist(cells.cv2,0:0.1:2);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
plot(c,hmua,'r','LineWidth',2);
end
%hist(cells.cv2,0:0.1:2);
xlabel('CV2'); ylabel('Count');
title(sprintf('CV2: %.2f±%.2f\n',mean(cells.cv2(cells.sua==1)),1.96*std(cells.cv2(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1f.xls');

subplot(337); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.bi(cells.sua==1,:),0:0.05:0.5,'support','positive');
    hmua = ksdensity(cells.bi,0:0.05:0.5,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.bi(cells.sua==1,:),0:0.05:0.5);
    hmua = hist(cells.bi,0:0.05:0.5);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
if plotmua
    plot(c,hmua,'r','LineWidth',2);
end
%hist(cells.cv2,0:0.1:2);
xlabel('BI'); ylabel('Count');
title(sprintf('Burst index: %.2f±%.2f\n',mean(cells.bi(cells.sua==1)),1.96*std(cells.bi(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1g.xls');

subplot(338); hold on;
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(cells.peakvar(cells.sua==1,:),0:0.05:0.5,'support','positive');
    hmua = ksdensity(cells.peakvar,0:0.05:0.5,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(cells.peakvar(cells.sua==1,:),0:0.05:0.5);
    hmua = hist(cells.peakvar,0:0.05:0.5);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
%plot(c,hmua,'r','LineWidth',2);
%hist(cells.cv2,0:0.1:2);
xlabel('Peak variability'); ylabel('Count');
title(sprintf('Peak variability: %.2f±%.2f %%\n',mean(cells.peakvar(cells.sua==1)),1.96*std(cells.peakvar(cells.sua==1))./sqrt(sum(cells.sua==1))));
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1h.xls');

subplot(339); hold on;
for ch = 1:96
    nneu1(ch) = sum(cells.chan==ch & cells.sua & cells.exp==1 & cells.session==1);
    nneu2(ch) = sum(cells.chan==ch & cells.sua & cells.exp==1 & cells.session==2);
    nneu3(ch) = sum(cells.chan==ch & cells.sua & cells.exp==2 & cells.session==1);
    nneu4(ch) = sum(cells.chan==ch & cells.sua & cells.exp==3 & cells.session==1);  
end
nneu = [nneu1 nneu2 nneu3 nneu4];
if strcmp(histtype,'density')
    [hsua,c] = ksdensity(nneu,0:0.05:0.5,'support','positive');
elseif strcmp(histtype,'histogram')
    [hsua,c] = hist(nneu,0:1:10);
end
bar(c,hsua,'FaceColor',0.5*[1 1 1]);
%plot(c,hmua,'r','LineWidth',2);
%hist(cells.cv2,0:0.1:2);
xlabel('# neurons / electrode'); ylabel('Count');
title(sprintf('#neurons per electrode: %.2f±%.2f %%\n',mean(nneu),1.96*std(nneu)./sqrt(length(nneu))));
xlim([-0.5,4.5]);
set(gca,'Xtick',0:4);
% save data
tbl = table(c.',hsua.','VariableNames',{'bins','count'});
writetable(tbl,'../../supdata/SFig.1i.xls');

saveas(h,['figs/SFig1.png']);
