%========================================================
%   PREPROCESSING STEP 1
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% Here, we load the SUA and ECoG datafiles, search for stimulus
% onset using a match filter on the auxillary channel recording
% a copy of the audio signal driving the vibrotactile device

clear
addpath ../conf
storeCSC = 0; % if set to zero, will not store MAT files with the blackrock data
config_spikes;

addpath(conf.dir.toolbox_fieldtrip);
ft_defaults

addpath(conf.dir.toolbox_blackrock);
%addpath(conf.dir.toolbox_maio);
addpath([conf.dir.toolbox_csc '/binaries']);
addpath(genpath(conf.dir.toolbox_osort));


block = 0;
% load physio

fprintf(' [step 1] Loading MicroMed file %s\n',conf.dir.physiofile);

% micromed data
mm_hdr = ft_read_header(conf.dir.physiofile);
mm_event = ft_read_event(conf.dir.physiofile);
mm_data = ft_read_data(conf.dir.physiofile);
mm_trig = [mm_event.value];
mm_lat = [mm_event.sample];
mm_lat = mm_lat(mm_trig==255);
mm_trig = mm_trig(mm_trig==255);
mm_fs = mm_hdr.Fs;

% blackrock trigger data
fprintf(' [step 1] Loading BlackRock file %s\n',conf.dir.spikesfile);
d = dir([conf.dir.spikesfile '/*.nev']);
blackrock_file = [conf.dir.spikesfile '/' d(1).name];
br_trigdata = openNEV(blackrock_file);
br_lat = double(br_trigdata.Data.SerialDigitalIO.TimeStamp);
br_fs = double(br_trigdata.MetaTags.SampleRes);
br_valid = double(br_trigdata.MetaTags.DataDuration);

br_date = br_trigdata.MetaTags.DateTime;
br_comment = br_trigdata.MetaTags.Comment;

%convertNSx_toMat([blackrock_file(1:end-4) '.ns6'], 1:96, conf.dir.output, 1, '.mat');

% blackrock spike data
br_data_ = openNSx([blackrock_file(1:end-4) '.ns6'],'uV');

MetaTags_ = br_data_.MetaTags;
RawData_ = br_data_.MetaTags;
ElectrodesInfo_ = br_data_.ElectrodesInfo;

    
nchans = (br_data_.MetaTags.ChannelCount)-1; % ignore last channel
fprintf(' [step 1] found %d channels at %d Hz\n',nchans,br_fs);

% if more than one recording is stored in Blackrock format, the 
% data are in cells
if size(br_data_.Data,2)==2
    br_data_spikes = br_data_.Data{1}(1:nchans,:); %
    stim = br_data_.Data{1}(nchans+1,:);
else
    br_data_spikes = br_data_.Data(1:nchans,:); %
    stim = br_data_.Data(nchans+1,:);
end

%%
% find stim in blackrock aux channel by template matching
freqstim = 220;
wl = round(0.1*br_fs/2);
t = linspace(0,0.1,wl*2+1);
template = sin(2*pi*t*freqstim);
match = filter(template,1,stim);
clear br_data_;
match(1:wl*2) = [];
st = std(match(1:br_fs));
mu = mean(match(1:br_fs));
match = (match - mu)./st;
[pk1,br_audio_trig] = findpeaks(match,'MinPeakHeight',5,'MinPeakDistance',br_fs);


mm_mint = conf.tmin.physio*mm_fs;
br_mint = conf.tmin.spikes*br_fs;
mm_mintrig = find(mm_lat >= mm_mint,1,'first');
br_mintrig = find(br_lat >= br_mint,1,'first');


figure(100);clf;
subplot(211); hold on;
stem(mm_lat./mm_fs,mm_lat*0+1,'bo')
plot(mm_mint./mm_fs*[1 1],ylim(),'k--');
subplot(212); hold on;
stem(br_lat./br_fs,br_lat*0+1,'ro')
plot(br_mint./br_fs*[1 1],ylim(),'k--');
saveas(['figs/step1_' conf.sessid '_triggers.png']);

%keyboard();
%%


% load behav
behav = readtable(conf.dir.behavfile,'FileType','text');

fprintf(' [step 1] Loading behav file %s\n',conf.dir.behavfile);


t = ([behav.StartSound] - behav.StartSound(1));

% remove the latency of the first trigger
mm_lat2 = mm_lat - mm_lat(mm_mintrig);
br_lat2 = br_lat - br_lat(br_mintrig);

mm_trig = zeros(1,length(t));
br_trig = zeros(1,length(t));

for i=1:length(t)
    %%
    
    mm_next = find(mm_lat2 >= t(i)*mm_fs);
    mm_maxnxt = min(4,length(mm_next));
    mm_delays = mm_lat2(mm_next(1:mm_maxnxt)) - t(i)*mm_fs;
    mm_ours = find(mm_delays < mm_fs*0.2,1,'first');
    mm_trig(i) = mm_lat2(mm_next(mm_ours))+mm_lat(mm_mintrig);
    
    br_next = find(br_lat2 >= t(i)*br_fs);
    br_maxnxt = min(4,length(br_next));
    br_delays = br_lat2(br_next(1:br_maxnxt)) - t(i)*br_fs;
    br_ours = find(br_delays < br_fs*0.2,1,'first');
    br_trig(i) = br_lat2(br_next(br_ours))+br_lat(br_mintrig);
    
    trial.amp(i) = behav.stimamp(i);
    %P.T(i).RT
    if conf.exp==1
        
        if strcmp(behav.key{i},'none')
            trial.resp(i) = 0;
        else
            trial.resp(i) = 1;
        end
    end
    if any(strcmp(behav.Properties.VariableNames,'RT'))
        trial.rt(i) = behav.RT(i);
    end
    if any(strcmp(behav.Properties.VariableNames,'RT1'))
        trial.rt1(i) = behav.RT1(i);
    end
    if any(strcmp(behav.Properties.VariableNames,'RT2'))
        trial.rt2(i) = behav.RT2(i);
    end
    if any(strcmp(behav.Properties.VariableNames,'key2'))
        trial.conf(i) = behav.key2(i);
    end
    if any(strcmp(behav.Properties.VariableNames,'conf'))
        trial.conf(i) = behav.conf(i);
    end
    if any(strcmp(behav.Properties.VariableNames,'SDT'))
        trial.sdt{i} = behav.SDT{i};
    end
    trial.stimamp(i) = behav.stimamp(i);
    trial.stimon(i) = behav.stimonset(i);
    
    
end
%%
mm_diff = diff(mm_trig)./mm_fs;
br_diff = diff(br_trig)./br_fs;
fprintf(' [step 1] Micromed trigger correlate with Blackrock at c=%.2f\n',corr(mm_diff',br_diff'));

dt_behav = diff([behav.StartSound].');
dt_trig = diff(br_trig)./br_fs;
c = corr(dt_behav.', dt_trig.')
fprintf(' [step 1] correlation with behavior: %.3f\n',c);

%realign serial triggers and audio triggers
idtrig = [];
for trig = 1:length(br_trig)
    [dist,idx] = min(abs(br_trig(trig) - br_audio_trig));
    if dist < 0.2*br_fs
        fprintf(' [step 1] - %d) Found audio trigger: diff: %.3f ms at %d / %d , replacing\n', ...
            trig,(br_trig(trig)-br_audio_trig(idx))./br_fs*1e3,...
            br_trig(trig),br_audio_trig(idx));
        br_trig(trig) = br_audio_trig(idx);
        idtrig = [idtrig idx];
    end
    checkstim(:,trig) = stim(br_trig(trig)+(-15e3:15e3));
    
end

% TRIGGERS FROM BLACKROCK
figure(101);
subplot(211);
stem(mm_trig./mm_fs,mm_trig*0+1.1,'c+')
tgrnd = ([behav.StartSound]-behav.StartSound(1)).*mm_fs+mm_lat(mm_mintrig);
stem(tgrnd./mm_fs,tgrnd*0+1.2,'g')
subplot(212);
stem(br_trig./br_fs,br_trig*0+1.1,'m+')
tgrnd = ([behav.StartSound]-behav.StartSound(1)).*br_fs+double(br_lat(br_mintrig));
stem(tgrnd./br_fs,tgrnd*0+1.2,'g')
saveas(['figs/step1_' conf.sessid '_triggersmatch.png']);

pause(0.1);



%% save files

% save ECoG data
ecog = struct();
ecog.grid = mm_data(1:24,:);
ecog.strip1 = mm_data(25:28,:);
ecog.strip2 = mm_data(29:32,:);

file_data = [conf.dir.output filesep 'ecog_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 1] Saving ECoG data %s\n',file_data);
save(file_data,'ecog');

% sanity check: store the audio channel to check triggers
file_stim = [conf.dir.output filesep 'stim_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 1] Saving stim file %s\n',file_stim);
save(file_stim,'stim');

% save behav with event latencies
event = struct();
event.behav = trial;
event.latency_spikes = br_trig;
event.latency_ecog = mm_trig;

file_events = [conf.dir.output filesep 'events_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 1] Saving events %s\n',file_events);
save(file_events,'event');

% save blackrock header
header = br_trigdata.MetaTags;
elecs = br_trigdata.ElectrodesInfo;
file_header = [conf.dir.output filesep 'blackrockheader_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 1] Saving header %s\n',file_header);
save(file_header,'header','elecs');

% save micromed header
header = mm_hdr;
file_header2 = [conf.dir.output filesep 'micromedheader_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 1] Saving header %s\n',file_header2);
save(file_header2,'header');

if 1
    fprintf(' [step 1] Saving 96 Utah channels, takes time (!) \n');

    fprintf('    ');
for ch=1:96
    fprintf('\b\b\b\b[%2.d]',ch);
    data = br_data_spikes(ch,:);
    MetaTags = MetaTags_;
    RawData = RawData_;
    ElectrodesInfo = ElectrodesInfo_(ch);
    
    save( [conf.dir.output '/sort/A' num2str(ch) '.mat'], 'data','MetaTags', 'RawData', 'ElectrodesInfo', '-v7.3');   %v7.3 so file supports partial loading (speedup)
end
else
    warning('NOT SAVING BLACKROCK DATA');
end
%%


