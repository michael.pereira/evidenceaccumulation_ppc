%========================================================
%   PREPROCESSING STEP 2
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%   adapted from code given by Prof. Shuo Wang.
%========================================================

% This is a wrapper to Osort.

%% General parameters
addpath ../conf
addpath ../functions/
config_spikes

addpath(genpath(conf.dir.toolbox_osort));

paths=[];
paths.basePath=[conf.dir.output '/sort/'];
paths.pathOut=[paths.basePath];
paths.pathRaw=[paths.basePath];
paths.pathFigs=[conf.dir.output '/sortfigs/'];
paths.patientID='utah'; % label in plots
paths.timestampspath = paths.basePath;

%% sorting params

filesToProcess=1:length(dir([paths.basePath '/*.mat']));  %which files to detect/sort
noiseChannels=[ ];
groundChannels=[ ];
doGroundNormalization=0;
normalizeOnly=[]; %which channels to use for normalization

paramsIn=[];
paramsIn.defaultAlignMethod=2;  %only used if peak finding method is "findPeak". 1=max, 2=min, 3=mixed

%% global settings

paramsIn.rawFileVersion=7;
paramsIn.samplingFreq=30000; %only used if rawFileVersion==3

%which tasks to execute
paramsIn.tillBlocks = 999;  %how many blocks to process (each ~20s). 0=no limit.
paramsIn.doDetection = 1;
paramsIn.doSorting = 1;
paramsIn.doFigures = 1;
paramsIn.noProjectionTest = 1;
paramsIn.doRawGraphs = 1;
paramsIn.doGroundNormalization=doGroundNormalization;
paramsIn.minNrSpikes=50; %min nr spikes assigned to a cluster for it to be valid
paramsIn.displayFigures = 0 ;  %1 yes (keep open), 0 no (export and close immediately); for production use, use 0

%params
paramsIn.blockNrRawFig=[ 1:10 20:30 60:70];
paramsIn.outputFormat='png';
paramsIn.thresholdMethod=1; %1=approx, 2=exact
paramsIn.prewhiten=0; %0=no, 1=yes,whiten raw signal (dont)

%%

paramsIn.peakAlignMethod=1; %1 find Peak, 2 none, 3 peak of power, 4 MTEO peak

%for power detection method
paramsIn.detectionMethod=3; %1 power, 2 T pos, 3 T min, 3 T abs, 4 wavelet
dp.kernelSize=18;
paramsIn.detectionParams=dp;

paramsIn.rawFilePrefix='A';
paramsIn.processedFilePrefix='A';

for thr = 1:length(conf.clust.detect_threshold)
    %%
    thres = [repmat(conf.clust.detect_threshold(thr), 1, length(filesToProcess))];
    
    if exist('groundChannels') && ~doGroundNormalization
        filesToProcess=setdiff(filesToProcess, groundChannels);
    end
    
    filesAlignMax=[ ];
    filesAlignMin=[ ];
    
    [normalizationChannels,paramsIn] = StandaloneGUI_prepare(noiseChannels,doGroundNormalization,paramsIn,filesToProcess,filesAlignMax, filesAlignMin);
    %addpath(genpath('code/OSort/code/osortTextUI_DBS/'))
    
    fprintf(' [step 2] Sorting for v%d s%d (thr=%.1f)\n',conf.version,conf.session,thres(1));
    pause(3);
    
    StandaloneGUI(paths, filesToProcess, thres, normalizationChannels, paramsIn);
end