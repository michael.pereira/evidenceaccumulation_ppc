%========================================================
%   PREPROCESSING STEP 7
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This script computes firing rates with a sliding window, time-locked to
% the initial cue or to the stimulus
addpath ../conf
addpath ../functions/

config_spikes;

type = {'gauss'};%,'halfgauss'};


exp = [1 1 2 3];
ses = [1 2 1 1];
beh = cell(3,2);
evt = cell(3,2);
for rep=1:length(exp)
    sessid = ['e' num2str(exp(rep)) 's' num2str(ses(rep))];
    file_evts = [conf.dir.preprocdir '/' sessid '/events_'  sessid '.mat'];
    file_beh = [conf.dir.data '/derivatives/sub-patient/behav/sub-patient_ses-0' num2str(rep) '_exp-0' num2str(exp(rep)) '_beh.tsv'];
    fprintf(' [stage 7] loading events %s\n',file_evts);
    evt{exp(rep),ses(rep)} = load(file_evts,'event');
    fprintf(' [stage 7] loading behavior %s\n',file_evts);    
    beh{exp(rep),ses(rep)} = readtable(file_beh,'FileType','text');
end

for type_ = 1:length(type)
    
    conf.fratemethod = type{type_};
    
    cells = readtable('../conf/neurons/units.csv');
    %% General parameters
    
    
    allfrate = [];
    allfrate_z = [];
    
    allraster = [];
    allresp = [];
    allconf = [];
    allamp = [];
    allamp_z = [];
    allwin1 = [];
    allwin2 = [];
    alltarget ={};
    allunit ={};
    
    avghit = [];
    avgmiss = [];
    avgall = [];
    avgtarget = {};
    
    nunit = 0;
    %%
    for i=1:size(cells,1)
        %% prepare data
        
        % load data 
        spikefile = sprintf('%s/e%ds%d/sort/%d/A%d_sorted_new.mat', ...
            conf.dir.preprocdir,cells.exp(i),cells.session(i),cells.thr(i),cells.chan(i));
        spikes = load(spikefile);

        unitid = sprintf('e%ds%d-#%d',cells.exp(i),cells.session(i),i);
        sessid = sprintf('e%ds%d',cells.exp(i),cells.session(i));
        events = evt{cells.exp(i),cells.session(i)}.event;
        behav = beh{cells.exp(i),cells.session(i)};
        fprintf(' [stage 7] firing rate for %s (chan %d) in %s: ',unitid,cells.chan(i),spikefile);
        
        % cluster match in channels
        clust = conf.clust.(sessid)(cells.chan(i)).clust;
        for c=1:length(clust)
            if any(clust{c}==cells.idosort(i))
                fprintf('[cell%d]',c);
                cl = c;
            end
        end
        fprintf('\n');
        clid = clust{cl};
        
        % get the index of firing of this neuron
        idx = ismember(spikes.assignedNegative,clid);
        
        % action potential
        ap = mean(spikes.newSpikesNegative(idx,:));
        apstd = std(spikes.newSpikesNegative(idx,:));
        
        fs = conf.fs.spikes;
        
        %% compute firing rate
        
        % get spike onsets
        t = fs*(spikes.newTimestampsNegative(idx))*1e-6;
         
        % compute every 1ms: so divide 30000 Hz by 30
        fsdiv = 30;
        fsr = fs/fsdiv;
        
        % allocate a vector for firing rates (a little longer than the last
        % event)
        x = zeros(1,ceil(fs*(max(events.latency_spikes)./fs+10)/fsdiv));
        % put a dirac every spike
        x(ceil(t/fsdiv)) = 1;
        
        % compute firing rate
        [y,L] = fratefilt(x,conf);
        
        % compensate for filter delay, only if using a full Gaussian window
        if strcmp(conf.fratemethod,'halfgauss')
            y = y*fsr;
        else
            y = y((L+1):end)*fsr;
        end
        
        %time = linspace(0,length(y)/fsr,length(y));
        
        % loop through epochs for stim and cue onsets
        trig.stimonset = events.latency_spikes./fs;
        trig.cueonset = trig.stimonset - events.behav.stimon;
       
        ntr = length(trig.stimonset);
        win = -0.5*fsr:1.5*fsr;
        trfrate_stim = nan(length(win),ntr);
        trraster_stim = nan(length(win),ntr);
        for tr=1:ntr
            if ~isnan(trig.stimonset(tr))
                trfrate_stim(:,tr) = y(round(trig.stimonset(tr)*fsr+win));
                trraster_stim(:,tr) = x(round(trig.stimonset(tr)*fsr+win));
            end
        end
        
        ntr = length(trig.cueonset);
        win = -0.5*fsr:3.5*fsr;
        trfrate_cue = nan(length(win),ntr);
        trraster_cue = nan(length(win),ntr);
        for tr=1:ntr
            if ~isnan(trig.cueonset(tr))
                trfrate_cue(:,tr) = y(round(trig.cueonset(tr)*fsr+win));
                trraster_cue(:,tr) = x(round(trig.cueonset(tr)*fsr+win));
            end
        end
        
        % prepare behavior: make it identical between experiments
        % i.e. put zeros when irrelevant
        amp = [behav.stimamp];
        onset = [behav.stimonset];
       
        if any(strcmp(behav.Properties.VariableNames,'SDT'))
            sdt = [behav.SDT];
            amp(strcmp(sdt,'fa')) = 0;
            amp(strcmp(sdt,'crej')) = 0;
            onset(strcmp(sdt,'fa')) = 0;
            onset(strcmp(sdt,'crej')) = 0;
        else
            sdt = [];
        end
        if any(strcmp(behav.Properties.VariableNames,'key2'))
            confidence = [behav.key2];
        else
            confidence = onset*0;
        end
        if any(strcmp(behav.Properties.VariableNames,'RT1'))
            rt = [behav.RT1];
        elseif any(strcmp(behav.Properties.VariableNames,'RT'))
            rt = [behav.RT];
        else
            rt = onset*0;
        end
        behav = struct();
        behav.sdt = sdt.';
        behav.conf = confidence.';
        behav.amp = amp.';
        behav.onset = onset.';
        behav.rt = rt.';
        
        stat = cells(i,:);
        
        % save stim-locked
        frate = trfrate_stim;
        raster = trraster_stim;
        savefile = ['../conf/neurons/' unitid '_' conf.fratemethod num2str(conf.gausswin) '_stim.mat'];
        fprintf(' [stage 7] saving %s\n',savefile);
        save(savefile,'frate','raster','behav','stat','ap','apstd');
        
        % save cue-locked
        frate = trfrate_cue;
        raster = trraster_cue;
        savefile = ['../conf/neurons/' unitid '_' conf.fratemethod num2str(conf.gausswin) '_cue.mat'];
        fprintf(' [stage 7] saving %s\n',savefile);
        save(savefile,'frate','raster','behav','stat','ap','apstd');
        
        
        
    end
end