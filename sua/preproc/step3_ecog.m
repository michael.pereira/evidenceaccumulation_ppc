%========================================================
%   PREPROCESSING STEP 3
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% Here, we rereference ECoG data to a common average and filter 
% the data

%%
addpath ../conf
addpath ../functions/
config_spikes;

dofreq = 1; % set to 0 to skip high gamma computations

%% LOAD DATA

% load ephys
file_data = [conf.dir.output filesep 'ecog_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 3] loading ECoG data %s\n',file_data);
load(file_data,'ecog');

% load events
file_evts = [conf.dir.output filesep 'events_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 3] loading events %s\n',file_evts);
load(file_evts,'event');

%% FILTERING
[nchan,n] = size(ecog.grid);
nhgafilt = length(conf.ecog.bbp_hg);

% allocate
lfp = zeros(n,nchan);
lfp_sm = zeros(n,nchan);
hga = zeros(n,nchan);
hga_sm = zeros(n,nchan);

% CAR
fprintf(' [step 3] common average rereferencing\n');
ecog.grid = bsxfun(@minus,ecog.grid,mean(ecog.grid,1));

% loop through channels and filter
fprintf(' [step 3] filtering: ');
fl = round(0.1*conf.ecog.fs)*2+1;
for ch=1:nchan
    fprintf('[%d]',ch);
    % LFP 
    lfp(:,ch) = filtfilt(conf.ecog.blp1,conf.ecog.alp1,ecog.grid(ch,:));
    
    % Savitzky-Golay smoothing
    lfp_sm(:,ch) = sgolayfilt(lfp(:,ch),2,fl);
    
    if dofreq
        % loop through HGA sub-bands and normalize by average magnitude
        notched = filtfilt(conf.ecog.bn2,conf.ecog.an2,ecog.grid(ch,:));
        for f = 1:nhgafilt
            hga_ = abs(hilbert(filtfilt(conf.ecog.bbp_hg{f},conf.ecog.abp_hg{f},notched)));
            mu = mean(hga_);
            hga(:,ch) = hga(:,ch) + (1/nhgafilt)*(hga_.'./mu);
        end
        % Savitzky-Golay smoothing
        hga_sm(:,ch) = sgolayfilt(hga(:,ch),2,fl);
    end
end
fprintf('\n');

%% EPOCHING
nevt = length(event.latency_ecog);
win = [-1,2];
winidx = round(win(1)*conf.ecog.fs:win(2)*conf.ecog.fs);

ns = length(winidx);

fprintf(' [step 3] epoching\n');
% allocate
trlfp = nan(nevt,ns,nchan);
trlfp_sm = nan(nevt,ns,nchan);
trhga = nan(nevt,ns,nchan);
trhga_sm = nan(nevt,ns,nchan);

% loop through events
for e = 1:nevt
    idx = event.latency_ecog(e);
    trlfp(e,:,:) = lfp(idx+winidx,:);
    trlfp_sm(e,:,:) = lfp_sm(idx+winidx,:);
    
    if dofreq
        trhga(e,:,:) = hga(idx+winidx,:);
        trhga_sm(e,:,:) = hga_sm(idx+winidx,:);
    end
end

%% SAVE DATA
beh = event.behav;

file_lfp = [conf.dir.output '/' 'ecogtrlfp_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 3] saving trials (lfp) %s\n',file_lfp);
save(file_lfp,'trlfp','beh');

file_lfp_sm = [conf.dir.output '/' 'ecogtrlfp_sm_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
fprintf(' [step 3] saving trials (lfp smooth) %s\n',file_lfp_sm);
save(file_lfp_sm,'trlfp_sm','beh');

if dofreq
    file_hga = [conf.dir.output '/' 'ecogtrhga_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
    fprintf(' [step 3] saving trials (hga) %s\n',file_hga);
    save(file_hga,'trhga','beh');
    
    file_hga_sm = [conf.dir.output '/' 'ecogtrhga_sm_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
    fprintf(' [step 3] saving trials (hga_sm) %s\n',file_hga_sm);
    save(file_hga_sm,'trhga_sm','beh');
end
