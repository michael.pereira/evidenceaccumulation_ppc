
%========================================================
%   PREPROCESSING STEP 4
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% In this step, we loop through the ECoG epochs to find bad trials
addpath ../conf
addpath ../functions/
config_spikes;
addpath(genpath(conf.dir.toolbox_ielvis));


global globalFsDir;
globalFsDir=conf.dir.brainmodel;

f1 = [conf.dir.output '/ecogtrlfp_' conf.sessid '.mat'];
fprintf('Loading %s\n',f1);
d1 = load(f1);
trlfp = d1.trlfp;
f1 = [conf.dir.output '/ecogtrhga_' conf.sessid '.mat'];
fprintf('Loading %s\n',f1);
d1 = load(f1);
trhga = d1.trhga;
[ntr1,nt,nchan] = size(d1.trhga);


%%
for tr = 1:size(trlfp,1)
    
    selch = [8,14];
    t = linspace(-1,2,nt);
    
    % tr = tr+1;
    figure(101); clf;
    subplot(211);
    hold on;
    sig = squeeze(trlfp(tr,:,:));
    a = bsxfun(@plus,sig,(1:24)*400);
    plot(t,a,'b');
    plot(t,a(:,selch),'r');
    
    plot(-0.3*[1 1],ylim,'r--');
    plot(1.5*[1 1],ylim,'r--');
    
    title(sprintf('trial %d (lfp)',tr));
    
    subplot(212);
    hold on;
    sig = squeeze(trhga(tr,:,:));
    a = bsxfun(@plus,sig,(1:24));
    plot(t,a,'b');
    plot(t,a(:,selch),'r');
    
    plot(-0.5*[1 1],ylim,'r--');
    plot(1.5*[1 1],ylim,'r--');
    
    title(sprintf('trial %d (hga)',tr));
    ylim([0,30])
    pause();
end