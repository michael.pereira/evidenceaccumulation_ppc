%========================================================
%   PREPROCESSING STEP 5
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% Compute statistics on curated putative single neurons to 
% check sorting
addpath ../conf
addpath ../functions/
exp = [1 1 2 3];
ses = [1 2 1 1];
if ~exist('../conf/neurons','dir')
    system('mkdir -p ../conf/neurons');
end

nunit = 0;
% for all experiments / sessions 
for rep=1:length(exp)
    conf.exp = exp(rep);
    conf.session = ses(rep);
    config_spikes;
    
    if ~exist(['figs/' conf.sessid],'dir')
        system(['mkdir -p figs/' conf.sessid]);
    end

    evtfile = [conf.dir.output '/events_e' num2str(conf.exp) 's' num2str(conf.session) '.mat'];
    fprintf(' [step5] Loading event file: %s\n',evtfile);
    
    preload = 1;
    d = load(evtfile);
    nses = length(d.event.behav.amp);
    data = [];
    data.trig = d.event.latency_spikes;
    data.behav = d.event.behav;
    neu_sua = [];
    neu_id = {};
    neu_idosort = [];
    neu_ch = [];
    neu_fr = [];
    neu_badisi = [];
    neu_snr = [];
    neu_peakvar = [];
    neu_projd = [];
    neu_thr = [];
    neu_cv2 = [];
    neu_spikeamp = [];
    neu_bi = [];
    type = {'mua','sua'};
    nmerge = [];
    addpath([conf.dir.toolbox_osort '/code/sortingNew/projectionTest/']);
    
    
    %% for each channel
    for ch = 1:96
        if isempty(conf.clust.(conf.sessid)(ch).clust)
            continue
        end
        %%
        % load OSort output
        spikefile = [conf.dir.output '/sort/'  num2str(conf.clust.(conf.sessid)(ch).thr) '/A' num2str(ch) '_sorted_new.mat'];
        spikes = load(spikefile);
        
        nneu = length(conf.clust.(conf.sessid)(ch).clust);
        % for each cluster
        for neu = 1:nneu
            nunit = nunit + 1;
            nmerge = [nmerge length(conf.clust.(conf.sessid)(ch).clust{neu})];
            idosort = conf.clust.(conf.sessid)(ch).clust{neu};
            sua = conf.clust.(conf.sessid)(ch).single(neu);
            id = sprintf('e%ds%d#%03.f',conf.exp,conf.session,nunit);
            % compute stats
            stats = spikestats(ch,idosort,data,conf,spikes);
            subplot(2,3,1); title([type{sua+1} ' ' id]);
            % save result
            saveas(gcf,['figs/' conf.sessid '/' type{sua+1} '_ch-' num2str(ch) '_id-' id '.png']);
            
            fprintf('%s) Channel %d, unit %d (%s) - isi<3ms: %.2f - CV: %.2f ',id,ch,idosort(1),type{sua+1},stats.badisi*100,stats.cv2);
            if nmerge(end)>1
                fprintf(' (merged)\n');
            else
                fprintf('\n');
            end
            
            % compute projection 
            alldist{neu} = [];
            for n2 = 1:(neu-1)
                idx1 = ismember(spikes.assignedNegative,idosort);
                idx2 = ismember(spikes.assignedNegative,conf.clust.(conf.sessid)(ch).clust{n2});
                
                
                decor1 = spikes.allSpikesCorrFree(idx1,:);
                decor2 = spikes.allSpikesCorrFree(idx2,:);
                
                [m1,m2, residuals1, residuals2, overlap,d ] = ...
                    projectionTest( decor1, decor2);
                alldist{neu} = [alldist{neu} d];
                
            end
            % concatenate
            neu_id = [neu_id id];
            neu_sua = [neu_sua sua];
            neu_idosort = [neu_idosort idosort(1)];
            neu_ch = [neu_ch ch];
            neu_fr = [neu_fr stats.firingrate];
            neu_badisi = [neu_badisi stats.badisi];
            neu_snr = [neu_snr stats.snr];
            neu_peakvar = [neu_peakvar stats.peakvar];
            
            neu_projd = [neu_projd mean(alldist{neu})];
            neu_thr = [neu_thr  conf.clust.(conf.sessid)(1).thr];
            neu_cv2 = [neu_cv2 stats.cv2];
            neu_spikeamp = [neu_spikeamp stats.amplitude];
            neu_bi = [neu_bi stats.burstindex];
            
        end
    end
    %% save cells in csv file
    cells = table(neu_id.',neu_sua.',neu_idosort.',neu_ch.',neu_thr.',neu_fr.',...
        neu_badisi.',neu_snr.',neu_peakvar.',neu_projd.',...
        neu_cv2.',neu_spikeamp.',neu_bi.',nmerge.',...
        'VariableNames',{'id','sua','idosort','chan','thr','fire','isi','snr','peakvar','d','cv2','spikeamp','bi','nmerge'});
    writetable(cells,['../conf/neurons/units_e' num2str(conf.exp) 's' num2str(conf.session) '.csv']);
    
    %%
    checksua = find((cells.isi > 0.03 | cells.peakvar > 0.3) & cells.sua==1);
    checkmua = find((cells.isi < 0.03 &  cells.peakvar < 0.3) & cells.sua==0);
    checkfr = find(cells.fire < 0.5);
    checksnr = find(cells.snr < 5);
    checkd = find(cells.d < 10);
    
    fprintf('-------------------\n');
    for n = 1:length(checksua)
        fprintf('!SUA: chan %d, osortid %d: isi=%.2f%%, frate=%.2f, snr=%.2f, pkvar=%.2f\n',...
            cells.chan(checksua(n)), cells.idosort(checksua(n)), 100*cells.isi(checksua(n)), cells.fire(checksua(n)),cells.snr(checksua(n)),cells.peakvar(checksua(n)));
    end
    fprintf('-------------------\n');
    for n = 1:length(checkmua)
        fprintf('!MUA: chan %d, osortid %d: isi=%.2f%%, frate=%.2f, snr=%.2f, pkvar=%.2f\n',...
            cells.chan(checkmua(n)), cells.idosort(checkmua(n)), 100*cells.isi(checkmua(n)), cells.fire(checkmua(n)),cells.snr(checkmua(n)),cells.peakvar(checkmua(n)));
    end
    fprintf('-------------------\n');
    for n = 1:length(checkfr)
        fprintf('!FR: chan %d (%s), osortid %d: isi=%.2f%%, frate=%.2f, snr=%.2f, pkvar=%.2f\n',...
            cells.chan(checkfr(n)), type{cells.sua(checkfr(n))+1}, cells.idosort(checkfr(n)), 100*cells.isi(checkfr(n)), cells.fire(checkfr(n)),cells.snr(checkfr(n)),cells.peakvar(checkfr(n)));
    end
    fprintf('-------------------\n');
    for n = 1:length(checksnr)
        fprintf('!SNR: chan %d (%s), osortid %d: isi=%.2f%%, frate=%.2f, snr=%.2f, pkvar=%.2f\n',...
            cells.chan(checksnr(n)), type{cells.sua(checksnr(n))+1}, cells.idosort(checksnr(n)), 100*cells.isi(checksnr(n)), cells.fire(checksnr(n)),cells.snr(checksnr(n)),cells.peakvar(checksnr(n)));
    end
    fprintf('-------------------\n');
    for n = 1:length(checkd)
        fprintf('!PROJD: chan %d: %.1f \n',...
            cells.chan(checkd(n)), cells.d(checkd(n)));
    end
    fprintf('-------------------\n');
    
    %%
    h2 = figure(rep*100+201); clf; hold on;
    
    cols = colormap(cool(15));
    for c = 1:length(cells.sua)
        isnr = min(round(cells.snr(c)),15);
        col = cols(isnr,:);
        if(cells.sua(c)==1)
            plot(cells.isi(c),cells.fire(c),'o','MarkerEdgeColor','g','LineWidth',0.5,'MarkerFaceColor',col);
        else
            plot(cells.isi(c),cells.fire(c),'o','MarkerEdgeColor','r','LineWidth',0.5,'MarkerFaceColor',col);
        end
        txt = ['ch' num2str(cells.chan(c)) '-' num2str(cells.idosort(c))];
        text(cells.isi(c),cells.fire(c),txt)
    end
    ylabel('Firing rate');
    xlabel('ISI');
    t = 0:0.01:0.1;
    plot(t,250*(t-0.01),'r')
    for x = 0.00:0.002:0.03
        plot([x x],ylim(),'k--');
    end
    plot([0.03 0.03],ylim(),'k-');
    %plot([0.04,0.04],ylim(),'k--');
    ylim([0,25])
    %%
    histtype = 'histogram';
    
    nneu = length(cells.id);
    h = figure(rep*100+10); clf;
    subplot(331); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.fire(cells.sua==1,:),0:1:16,'support','positive');
        hmua = ksdensity(cells.fire(cells.sua==0,:),0:1:16,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.fire(cells.sua==1,:),0:1:16);
        hmua = hist(cells.fire(cells.sua==0,:),0:1:16);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    
    xlabel('Mean firing rate [spikes/s]');
    title(sprintf('FR: %.2f±%.2f %%\n',mean(cells.fire(cells.sua==1,:)),1.96*std(cells.fire(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    
    subplot(332); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.isi(cells.sua==1,:)*100,-0.25:0.1:8);
        hmua = ksdensity(cells.isi(cells.sua==0,:)*100,-0.25:0.1:8);
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.isi(cells.sua==1,:)*100,-0.25:0.2:3);
        hmua = hist(cells.isi(cells.sua==0,:)*100,-0.25:0.2:3);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist(cells.isi*100,-0.25:0.2:3);
    xlabel('% ISI < 3ms');
    title(sprintf('ISI: %.2f±%.2f %%\n',mean(cells.isi(cells.sua==1,:)*100),1.96*std(cells.isi(cells.sua==1,:)*100)./sqrt(sum(cells.sua==1))));
    xlim([-0.2,3])
    
    
    subplot(333); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.d(cells.sua==1,:),0:0.5:70,'support','positive');
        hmua = ksdensity(cells.d(cells.sua==0,:),0:0.5:70,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.d(cells.sua==1 & ~isnan(cells.d),:),0:1:70);
        hmua = hist(cells.d(cells.sua==0 & ~isnan(cells.d),:),0:1:70);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist((cells.d),0:0.5:70);
    hold on
    plot([4 4],ylim(),'r--');
    
    xlabel('Proj. dist.');
    title(sprintf('projD: %.2f±%.2f %%\n',nanmean(cells.d(cells.sua==1,:)),1.96*nanstd(cells.d(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    
    subplot(334); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.snr(cells.sua==1,:),0:0.5:30,'support','positive');
        hmua = ksdensity(cells.snr(cells.sua==0,:),0:0.5:30,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.snr(cells.sua==1,:),0:1:30);
        hmua = hist(cells.snr(cells.sua==0,:),0:1:30);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist(cells.snr,0:0.5:30);
    hold on;
    plot([3 3],ylim(),'r--');
    xlabel('Peak SNR');
    title(sprintf('SNR: %.2f±%.2f %%\n',mean(cells.snr(cells.sua==1,:)),1.96*std(cells.snr(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    
    subplot(335); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.cv2(cells.sua==1,:),0:0.1:2,'support','positive');
        hmua = ksdensity(cells.cv2(cells.sua==0,:),0:0.1:2,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.cv2(cells.sua==1,:),0:0.1:2);
        hmua = hist(cells.cv2(cells.sua==0,:),0:0.1:2);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist(cells.cv2,0:0.1:2);
    xlabel('CV2');
    title(sprintf('CV2: %.2f±%.2f %%\n',mean(cells.cv2(cells.sua==1,:)),1.96*std(cells.cv2(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    
    subplot(336); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.bi(cells.sua==1,:),0:0.05:0.5,'support','positive');
        hmua = ksdensity(cells.bi(cells.sua==0,:),0:0.05:0.5,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.bi(cells.sua==1,:),0:0.05:0.5);
        hmua = hist(cells.bi(cells.sua==0,:),0:0.05:0.5);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist(cells.cv2,0:0.1:2);
    xlabel('BI');
    title(sprintf('Burst index: %.2f±%.2f %%\n',mean(cells.bi(cells.sua==1,:)),1.96*std(cells.bi(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    
    subplot(337); hold on;
    if strcmp(histtype,'density')
        [hsua,c] = ksdensity(cells.peakvar(cells.sua==1,:),0:0.05:0.5,'support','positive');
        hmua = ksdensity(cells.peakvar(cells.sua==0,:),0:0.05:0.5,'support','positive');
    elseif strcmp(histtype,'histogram')
        [hsua,c] = hist(cells.peakvar(cells.sua==1,:),0:0.05:0.5);
        hmua = hist(cells.peakvar(cells.sua==0,:),0:0.05:0.5);
    end
    bar(c,hsua,'FaceColor',0.5*[1 1 1]);
    %plot(c,hmua,'r','LineWidth',2);
    %hist(cells.cv2,0:0.1:2);
    xlabel('Peak variability');
    title(sprintf('Peak variability: %.2f±%.2f %%\n',mean(cells.peakvar(cells.sua==1,:)),1.96*std(cells.peakvar(cells.sua==1,:))./sqrt(sum(cells.sua==1))));
    saveas(h,['figs/step5_units_e' num2str(conf.exp) 's' num2str(conf.session) '_quality.png']);
    
    %subplot(338); hold on;
    %plot(c,1,'b','LineWidth',2);
    %%plot(c,1,'r','LineWidth',2);
    %legend({'SUA','MUA'});
end