%========================================================
%   ALL PREPROCESSING STEPS
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% run this to (hopefully) preproc all data

clear
%% first preproc each Experiment / session individually

% load SUA and ECoG data, compute stimulus onset, 
% merge with behaviour, save
conf.exp = 1; conf.session = 1; step1_aligntriggers;
conf.exp = 1; conf.session = 2; step1_aligntriggers;
conf.exp = 2; conf.session = 1; step1_aligntriggers;
conf.exp = 3; conf.session = 1; step1_aligntriggers;

% sort spikes using OSort
conf.exp = 1; conf.session = 1; step2_spikesorting_osort;
conf.exp = 1; conf.session = 2; step2_spikesorting_osort;
conf.exp = 2; conf.session = 1; step2_spikesorting_osort;
conf.exp = 3; conf.session = 1; step2_spikesorting_osort;

% re-ref and filter ECoG data
conf.exp = 1; conf.session = 1; step3_ecog;
conf.exp = 1; conf.session = 2; step3_ecog;
conf.exp = 2; conf.session = 1; step3_ecog;
conf.exp = 3; conf.session = 1; step3_ecog;


%% It is then recommended to check the ECoG data to find bad trials
conf.exp = 1; conf.session = 1; step4_checkecog;
conf.exp = 1; conf.session = 2; step4_checkecog;
conf.exp = 2; conf.session = 1; step4_checkecog;
conf.exp = 3; conf.session = 1; step4_checkecog;

%% Then all process all data together

% compute stats for single units
step5_neuronstats;

% join everything
step6_neuronstatsjoin;

% compute firing rate for cue-locked data
step7_cue_frate;

% compute firing rate for stimulus-locked data
step8_stim_frate;