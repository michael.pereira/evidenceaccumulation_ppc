function main_experiment(subnum,initialamp)
% in v2, we present trials with perithreshold stim + 25% of catch
% subject is instructed to respond after 2s (yes/no) and then provide confidence
clearvars -except subnum initialamp

global handle
global P

% close port
try
    IOPort('Close', handle);
catch
end

commandwindow;
addpath('./functions');

try PsychPortAudio('Close')
catch
    
end


%% load stim configuration
P = P_config_v2(subnum);

if strcmp(P.Triggers,'serial')
    %_______________________________
    % Open SerialPort
    for i=1:3
        try
            [handle, errmsg] = IOPort('OpenSerialPort', P.TriggerPort,sprintf('BaudRate=%d',P.BaudRate));
            break
        catch
            fprintf('Failed to open serial port, retrying...\n');
            pause(5);
        end
    end
end

%% specific configuration for main experiment
P.initialamp    = initialamp;         % first threshold estimation based on threshestimator.m
P.stepsize      = initialamp*0.02;    % stepsize for staircase in % intensity of P.basestim
P.block_number  = 3;                 % total number of blocks
P.trial_number  = 50;                 % total number of trials per block
P.lambda        = nan;                % in V2 stim onset is uniform random between 0 and 1.9s
P.ITI           = 0.5;
P.rand          = 2;                  % width of uniform distribution for stim onset

stimamp = P.initialamp;
stepsize = P.stepsize;


%% start experiment
for b = 1:P.block_number
    P.T = []; % reinitialize trial structure
    P.stimvec = Shuffle(repmat([0 1 1 1 1],1,P.trial_number/5));
    fprintf('\n-----------------------\n');
    fprintf(['Now running block ' num2str(b) '\n']);
    change_stimamp = '';
    filename = ['main_exp_suj' num2str(P.subnum) '_block' num2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat']
    
    %% send trigger beginning of block
    %     SendTrigger(P.TRIGGER_BLOCK,P.Triggers)
    
    for i = 1:P.trial_number
        PsychHID('KbQueueStart',P.index);

        if i == 1
            WaitSecs(2);
        end
        fprintf('\n-----------------------\n');
        fprintf(['Now running trial ' num2str(i) '\n']);
        
        P.T(i).sub = subnum;          % subject number
        P.T(i).trial = i;             % trial number
        P.T(i).block = b;             % block number
        P.T(i).stimamp = stimamp;     % stim amplitude for trial>1
        P.T(i).stepsize = stepsize;   % step size
        P.T(i).badfirstpress = 0;     % check what keys were pressed during stim
        P.T(i).badfirstrelease = 0;   % check what keys were released during stim
        P.T(i).starttrial = GetSecs;
        P.T(i).stimvec = P.stimvec(i);

        % avoid null stim
        if P.T(i).stimamp < 0
            P.T(i).stimamp = stepsize;
        end
        
        %% update current stim
        P.T(i).stimonset = P.rand*rand(1); % stim onset
        PsychPortAudio('fillBuffer',P.pahandle,P.basestim.*P.T(i).stimamp.*P.T(i).stimvec);
        WaitSecs(P.T(i).stimonset);
        
        %% send trigger for stim onset
        SendTrigger(P.TRIGGER_STIMSTART,P.Triggers)
        P.T(i).StartSound = PsychPortAudio('Start',P.pahandle,1,[],1);
        P.T(i).StopSound = PsychPortAudio('Stop',P.pahandle,1);
        %         SendTrigger(P.TRIGGER_STIMEND,P.Triggers)
        
        % give time for subjects to respond
        P.T(i).waitresp = P.waitstim + (P.rand-P.T(i).stimonset);
        WaitSecs(P.T(i).waitresp);
        fprintf('\n RESP!\n\n')
%         PsychPortAudio('FillBuffer',P.pahandle,P.respstim);
        P.T(i).StartResp = GetSecs;% PsychPortAudio('Start',P.pahandle,1,[],1);
        P.T(i).StopResp = GetSecs;%PsychPortAudio('Stop',P.pahandle,1);
        
        %% check bad key presses/releases during key press
        [badpressDown, badfirstPress, badfirstRelease] = PsychHID('KbQueueCheck', P.index);
        PsychHID('KbQueueFlush', P.index);
        PsychHID('KbQueueStop', P.index);

        P.T(i).badfirstpress = badfirstPress;	P.T(i).badfirstrelease = badfirstRelease;  
  
        
        %% type 1 answer
        keys=zeros(256,1);keys(KbName({'1','2','a','z'}))=1;   %  'end', 'down arrow', 'page down'
        RestrictKeysForKbCheck(find(keys));
        
        [secs1, keyCode1, deltaSecs1] = KbWait([],2);
        
        P.T(i).firstPress = secs1;
        P.T(i).key1 = KbName(keyCode1);
        P.T(i).RT1 = secs1 - P.T(i).StartResp; % RT
        
        if or(strcmp(P.T(i).key1, '2'), strcmp(P.T(i).key1, 'z')) & P.T(i).stimvec==1
            P.T(i).SDT = 'hit';
            stimamp =  P.T(i).stimamp - stepsize;
        elseif or(strcmp(P.T(i).key1, '2'), strcmp(P.T(i).key1, 'z')) & P.T(i).stimvec==0
            P.T(i).SDT = 'fa';
            stimamp =  P.T(i).stimamp;
        elseif or(strcmp(P.T(i).key1, '1'), strcmp(P.T(i).key1, 'a')) & P.T(i).stimvec==1
            P.T(i).SDT = 'miss';
            stimamp =  P.T(i).stimamp + stepsize;
        elseif or(strcmp(P.T(i).key1, '1'), strcmp(P.T(i).key1, 'a')) & P.T(i).stimvec==0
            P.T(i).SDT = 'crej';
            stimamp =  P.T(i).stimamp;
        else
            P.T(i).SDT = '??';
            stimamp =  P.T(i).stimamp;
            warning('Something''s funky here!')
        end
        
        %% type 2 answer
        RestrictKeysForKbCheck();
        keys=zeros(256,1); keys(KbName({'1','2','3','a','z','e', '*','SPACE'}))=1;
        RestrictKeysForKbCheck(find(keys));
        [secs2, keyCode2, deltaSecs2] = KbWait([],2);
        
        P.T(i).secondPress = secs2;
        P.T(i).key2 = KbName(keyCode2);
        P.T(i).RT2 = P.T(i).secondPress - P.T(i).firstPress; % RT2
        
        if strcmp(KbName(keyCode2), '*')
            SendTrigger(P.TRIGGER_PAUSE,P.Triggers)
            % take a break
            disp('Taking a break ...')
            while ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
                change_stimamp = input('New stimamp for next trial? (y or n) \n');
            end
            
            if change_stimamp == 'y'
                stimamp = input('What is the new stimamp?\n');
                if isnumeric(stimamp)
                    fprintf('stimamp set to %.2f \n', stimamp)
                else
                    while ~ isnumeric(stimamp)
                        fprintf('Sorry, numerical value expected \n')
                        stimamp = input('Please enter the new stimamp: \n');
                    end
                end
            end
            change_stimamp = ' ';
            disp ('Press any key when you are ready!')
            pause;
            continue;
        end
        
        
        fprintf('%s! \n yes = %s, onset = %.3f, RT1 = %.3f, conf = %s, RT2 = %.3f,Next intensity= %.3f Next stepsize= %.3f\n\n\n',P.T(i).SDT,P.T(i).key1,P.T(i).stimonset,P.T(i).RT1,P.T(i).key2, P.T(i).RT2, stimamp, stepsize);
        save([P.savePath filesep filename], 'P');
%         PsychPortAudio('DeleteBuffer',P.pahandle);
    end
    WaitSecs(P.ITI);
    if b < P.block_number
        disp('***Block done! Time for a break!*** \n')
        if ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
            change_stimamp = input('New stimamp for next block? (y or n) \n');
            disp(' ')
        end
        if change_stimamp == 'y'
            stimamp = input('What is the new stimamp?\n');
            if isnumeric(stimamp)
                fprintf('stimamp set to %.2f \n', stimamp)
            else
                while ~ isnumeric(stimamp)
                    fprintf('Sorry, numerical value expected \n')
                    stimamp = input('Please enter the new stimamp: \n');
                end
            end
        end
        disp ('Press any key when you are ready!')
        pause;
    end
end

% close port
if strcmp(P.Triggers,'serial') && handle > -1
    IOPort('Close', handle);
end
end
