function threshold_estimator(subnum, type)

clearvars -except subnum type

global handle
global P
commandwindow;
try
    IOPort('Close', handle);
catch
end
addpath('./functions');

try PsychPortAudio('Close')
catch
end

%% load stim configuration
P = P_config_v2(subnum);

if strcmp(P.Triggers,'serial')
    %_______________________________
    % Open SerialPort
    for i=1:3
        try
            [handle, errmsg] = IOPort('OpenSerialPort', P.TriggerPort,sprintf('BaudRate=%d',P.BaudRate));
            break
        catch
            fprintf('Failed to open serial port, retrying...\n');
            pause(5);
        end
    end
end

%% specific configuration for threshold estimator
P.lambda            = 0.5;              % specific lambda for threshold estimator
P.stim_vector       = 0.1:.02:2.5;    % Generate vector to modulate base stim 
if strcmp(type, 'down'), P.stim_vector = fliplr(P.stim_vector); end
filename = ['ThresholdEstimator_suj' int2str(subnum) '_' type '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];

%% start experiment
P.StartExperiment = tic;
saveTimes = 1; P.WhenItStops=0;
        PsychHID('KbQueueStart',P.index);

for  i=1:length(P.stim_vector)
    
    fprintf('\n-----------------------\n');
    fprintf(['Now running trial: ' num2str(i)]);
    P.T(i).sub = subnum; %save subnum
    P.T(i).trial = i;
    P.T(i).stimamp = P.stim_vector(i);
    
    %% stimulation
    P.T(i).stimonset = exprnd(P.lambda);
    PsychPortAudio('FillBuffer',P.pahandle,P.basestim*P.stim_vector(i));  %%just chanege the P.T(i).stimamp
    WaitSecs(P.T(i).stimonset);
    %% send trigger for stim onset
    SendTrigger(P.TRIGGER_STIMSTART,P.Triggers)
    P.T(i).StartSound = PsychPortAudio('Start',P.pahandle,1,[],1); %GetSecs+P.T(i).stimonset
    P.T(i).StopSound = PsychPortAudio('Stop',P.pahandle,1);
%     SendTrigger(P.TRIGGER_STIMEND,P.Triggers)
    
    disp(P.stim_vector(i))
    
   
    %% give time for subjects to respond
    WaitSecs(P.waitstim);
    
    [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);
    
    switch type
        case 'up'
            %Write into result file
            if keyIsDown == 1
                fprintf ('stim detected %d', saveTimes);
                
                P.T(i).key = KbName(find(firstPress,1)); % Key pressed
                P.T(i).RT = firstPress(find(firstPress,1)) - P.T(i).StartSound; % RT
                P.T(i).seq = 1;
                if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                    SendTrigger(P.TRIGGER_HIT1,P.Triggers)
                elseif or(strcmp(P.T(i).key, '2'), strcmp(P.T(i).key, 'z')) % medium
                    SendTrigger(P.TRIGGER_HIT2,P.Triggers)
                elseif or(strcmp(P.T(i).key,--------------------------------- '3'), strcmp(P.T(i).key, 'e')) % strong
                    SendTrigger(P.TRIGGER_HIT3,P.Triggers)
                end
                
                if saveTimes >= 3
                    if P.T(i).seq & P.T(i - 1).seq & P.T(i - 2).seq
                        P.WhenItStops = P.stim_vector(i-2); % the last stimulus felt
                        break
                    end
                end
                saveTimes = saveTimes + 1;
            elseif keyIsDown == 0
                P.T(i).seq = 0;
                P.T(i).key = 'none'; % Key pressed
                P.T(i).RT = 0; % RT
%                 SendTrigger(P.TRIGGER_MISS,P.Triggers)
            end
        case 'down'
            if keyIsDown == 0
                fprintf ('no keypress detected');

%                 SendTrigger(P.TRIGGER_MISS,P.Triggers)
                P.T(i).key = 'none'; % Key pressed
                P.T(i).RT = 0; % RT
                P.T(i).seq =1;
                
            elseif keyIsDown == 1
                fprintf ('stim missed %d', saveTimes)

                P.T(i).key = KbName(find(firstPress,1)); % Key pressed
                P.T(i).RT = firstPress(find(firstPress,1)) - P.T(i).StartSound; % RT
                P.T(i).seq = 0;
                if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                    SendTrigger(P.TRIGGER_HIT1,P.Triggers)
                elseif or(strcmp(P.T(i).key, '2'), strcmp(P.T(i).key, 'z')) % medium
                    SendTrigger(P.TRIGGER_HIT2,P.Triggers)
                elseif or(strcmp(P.T(i).key, '3'), strcmp(P.T(i).key, 'e')) % strong
                    SendTrigger(P.TRIGGER_HIT3,P.Triggers)
                end
                 if saveTimes >= 3
                        P.WhenItStops = P.stim_vector(i-2); % the last stimulus felt
                        break
                end
                saveTimes = saveTimes + 1;
            end
    end
    PsychHID('KbQueueFlush', P.index);
end

save([P.savePath filesep filename], 'P');
fprintf('\n End of the Experiment\n');
fprintf(' Threshold: %.3f\n',P.WhenItStops);
WaitSecs(2);

PsychPortAudio('Close');

% close port
if strcmp(P.Triggers,'serial') && handle > -1
    IOPort('Close', handle);
end

end
