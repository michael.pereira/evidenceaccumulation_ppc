function main_experiment(subnum,initialamp)

clearvars -except subnum initialamp

global handle
global P

% close port
try
    IOPort('Close', handle);
catch
end

commandwindow;
addpath('./functions');

try PsychPortAudio('Close')
catch
    
end


%% load stim configuration
P = P_config(subnum);

if strcmp(P.Triggers,'serial')
    %_______________________________
    % Open SerialPort
    for i=1:3
        try
            [handle, errmsg] = IOPort('OpenSerialPort', P.TriggerPort,sprintf('BaudRate=%d',P.BaudRate));
            break
        catch
            fprintf('Failed to open serial port, retrying...\n');
            pause(5);
        end
    end
end

%% specific configuration for main experiment
P.initialamp    = initialamp;         % first threshold estimation based on threshestimator.m
P.stepsize      = initialamp*0.02;    % stepsize for staircase in % intensity of P.basestim
P.block_number  = 10;                  % total number of blocks
P.trial_number  = 50;                % total number of trials per block
P.lambda        = 2;                  % specific lambda for main experiment

stimamp = P.initialamp;
stepsize = P.stepsize;

%% start experiment
for b = 1:P.block_number
    P.T = []; % reinitialize trial structure
    fprintf('\n-----------------------\n');
    fprintf(['Now running block ' num2str(b) '\n']);
    change_stimamp = '';
    filename = ['main_exp_suj' num2str(P.subnum) '_block' num2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat']
    
    %% send trigger beginning of block
%     SendTrigger(P.TRIGGER_BLOCK,P.Triggers)
    
    for i = 1:P.trial_number
        if i == 1
            WaitSecs(2);
        end
        
        
        fprintf('\n-----------------------\n');
        fprintf(['Now running trial ' num2str(i) '\n']);
        
        P.T(i).sub = subnum;          % subject number
        P.T(i).trial = i;             % trial number
        P.T(i).block = b;             % block number
        P.T(i).stimamp = stimamp;     % stim amplitude for trial>1
        P.T(i).stepsize = stepsize;   % step size
        
        % avoid null stim
        if P.T(i).stimamp < 0
            P.T(i).stimamp = stepsize;
        end
        
        %% update current stim
        P.T(i).stimonset = exprnd(P.lambda); % stim onset
        PsychPortAudio('FillBuffer',P.pahandle,P.basestim.*P.T(i).stimamp);
        WaitSecs(P.T(i).stimonset);
        %% send trigger for stim onset
        SendTrigger(P.TRIGGER_STIMSTART,P.Triggers)
        P.T(i).StartSound = PsychPortAudio('Start',P.pahandle,1,[],1); %GetSecs+P.T(i).stimonset
        P.T(i).StopSound = PsychPortAudio('Stop',P.pahandle,1);
%         SendTrigger(P.TRIGGER_STIMEND,P.Triggers)
        
        % give time for subjects to respond
        WaitSecs(P.waitstim);
        
        %% process answer
        [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);
        P.T(i).firstPress=firstPress;
        P.T(i).firstRelease=firstRelease;
        
        if keyIsDown
            if strcmp(KbName(find(firstPress,1)), '*')
                SendTrigger(P.TRIGGER_PAUSE,P.Triggers)
                % take a break
                disp('Taking a break ...')
                P.T(i).key = KbName(find(firstPress,1));
                P.T(i).SDT = 'break';
                P.T(i).RT = 0;
                while ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
                    change_stimamp = input('New stimamp for next trial? (y or n) \n');
                end
                
                if change_stimamp == 'y'
                    stimamp = input('What is the new stimamp?\n');
                    if isnumeric(stimamp)
                        fprintf('stimamp set to %.2f \n', stimamp)
                    else
                        while ~ isnumeric(stimamp)
                            fprintf('Sorry, numerical value expected \n')
                            stimamp = input('Please enter the new stimamp: \n');
                        end
                    end
                end
                change_stimamp = ' ';
                disp ('Press any key when you are ready!')
                pause;
                continue;
            else
                P.T(i).key = KbName(find(firstPress,1)); % Key pressed
                P.T(i).RT = firstPress(find(firstPress,1)) - P.T(i).StartSound; % RT
                if P.T(i).RT > 0 % hit
                    P.T(i).SDT = 'hit';
                    % change stepsize
                    if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                        stepsize = P.stepsize;
                        SendTrigger(P.TRIGGER_HIT1,P.Triggers)
                        
                    elseif or(strcmp(P.T(i).key, '2'), strcmp(P.T(i).key, 'z')) % medium
                        stepsize = P.stepsize * 1.5;
                        SendTrigger(P.TRIGGER_HIT2,P.Triggers)
                        
                    elseif or(strcmp(P.T(i).key, '3'), strcmp(P.T(i).key, 'e')) % strong
                        stepsize = P.stepsize * 2;
                        SendTrigger(P.TRIGGER_HIT3,P.Triggers)
                        
                    end
                    stimamp =  P.T(i).stimamp - stepsize;
                elseif P.T(i).RT < 0 % fa
                    stimamp =  P.T(i).stimamp;
                    P.T(i).SDT = 'fa';
                    if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                        SendTrigger(P.TRIGGER_FA1,P.Triggers)
                    elseif or(strcmp(P.T(i).key, '2'), strcmp(P.T(i).key, 'z')) % medium
                        SendTrigger(P.TRIGGER_FA2,P.Triggers)
                    elseif or(strcmp(P.T(i).key, '3'), strcmp(P.T(i).key, 'e')) % strong
                        SendTrigger(P.TRIGGER_FA3,P.Triggers)
                    end
                end
                WaitSecs(P.waitresp);
            end
        else % miss
            P.T(i).key = 'none';
            P.T(i).RT = 0;
            P.T(i).SDT = 'miss';
            stepsize = P.stepsize;
%             SendTrigger(P.TRIGGER_MISS,P.Triggers)
            stimamp =  P.T(i).stimamp + stepsize;
        end
        
        
        fprintf('%s! \nKey = %s, onset = %.3f, RT = %.3f, Next intensity= %.3f Next stepsize= %.3f\n\n\n',P.T(i).SDT,P.T(i).key,P.T(i).stimonset,P.T(i).RT, stimamp, stepsize);
        save([P.savePath filesep filename], 'P');
        PsychHID('KbQueueFlush', P.index);
    end
    if b < P.block_number
        disp('***Block done! Time for a break!*** \n')
        if ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
            change_stimamp = input('New stimamp for next block? (y or n) \n');
            disp(' ')
        end
        if change_stimamp == 'y'
            stimamp = input('What is the new stimamp?\n');
            if isnumeric(stimamp)
                fprintf('stimamp set to %.2f \n', stimamp)
            else
                while ~ isnumeric(stimamp)
                    fprintf('Sorry, numerical value expected \n')
                    stimamp = input('Please enter the new stimamp: \n');
                end
            end
        end
        disp ('Press any key when you are ready!')
        pause;
        PsychHID('KbQueueFlush', P.index);
    end
end

PsychHID('KbQueueStop', P.index);

% close port
if strcmp(P.Triggers,'serial') && handle > -1
    IOPort('Close', handle);
end
end
