function P = P_config(subnum)

P.subnum = subnum;

%% keyboard stuff
KbName('UnifyKeyNames');
P.index = [0]; % 13 use GetKeyboardIndices!!!! 
PsychHID('KbQueueCreate', P.index);%keys

%% base stim
P.samplingfreq  = 48000;    % sampling frequency (change to 48000 for linux)
P.stimfreq      = 220;      % frequeny for tactile stimulator/Haptuator
P.respfreq      = 440;      % cue to respond
P.stimdur       = 0.1;      % duration for tacti3le stimulator/Haptuator
P.dimsound      = 0.01;     % dim the sound intensity in case vibrator is plugged in
P.dimresp       = 1;
P.basestim      = repmat(MakeBeep(P.stimfreq,P.stimdur,P.samplingfreq).*P.dimsound,1,1);
P.basestim      = [P.basestim;P.basestim.*0];
P.respstim      = repmat(MakeBeep(P.respfreq,P.stimdur,P.samplingfreq).*P.dimresp,1,1);
P.respstim      = [P.respstim;P.respstim.*0];


%% temporal property
P.waitstim      = 1;        % allows 2s for subjects to press after stimonset

%% savePath
P.savePath = [pwd filesep 'data' filesep num2str(P.subnum)];
if ~isdir(P.savePath), mkdir(P.savePath); end

%% Initialize Sound driver for tactile stimulation
InitializePsychSound(1);
P.pahandle = PsychPortAudio('Open', [], [1], 0,P.samplingfreq,2);


%% triggers
config_io
P.Address = hex2dec('E020');

P.dosendtriggers = 1;
P.trigonset = 100;
P.trighit = 101;
P.trigmiss = 102;
P.trigfa = 103;
P.trigcrej = 104;
P.trigvas = 200;
P.trigresp2 = 201;


end