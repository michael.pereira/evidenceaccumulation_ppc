
function SendTrigger(trigger,triggertype)
t = timer;
t.StartDelay = 0;
if strcmp(triggertype,'serial')
    t.TimerFcn = {@TriggerTimer,trigger};
else
    t.TimerFcn = {@TriggerTimerDummy,trigger};
end
t.ExecutionMode = 'singleShot';
start(t);
end