% function [P] = vertical_vas(P, i, screenRect, mouseId)
function [P] = vertical_vas(P, i)
global win
global midY midX

VASrange    = [midY-150, midY+150];

% Hide cursor in screen
HideCursor();

% siarty = VASrange(1) + (VASrange(2) - VASrange(1)) * rand; % for random start
starty = (VASrange(1) + VASrange(2)) /2; % for random start
initialy = round(starty);

P.T(i).mousePosOrig = initialy ; %(initialy - VASrange(1))/range(VASrange);

SetMouse(0, initialy, win);

%Intialize
P.T(i).onset = GetSecs;
counter = 1; 
mousetrail_y = [];
movementBegun   = 0;
mouseButtons    = 0;

tic;
while 1
    
    [mouseX, mouseY, mouseButtons] = GetMouse(win);
    
    %update display
    draw_vertical_vas(VASrange);     %draw the scale
    
    % set spatial limits to the scale
    if mouseY > VASrange(2)
        mouseY = VASrange(2);
        SetMouse(mouseX,mouseY,win);
        
    elseif mouseY < VASrange(1)
        mouseY = VASrange(1);
        SetMouse(mouseX,mouseY,win);
    end
    
    
    Screen('TextSize', win, 80);    %draw the cursor
    DrawFormattedText(win, '.', midX-13, mouseY, [20 230 0]);
    
    conftmp=(VASrange(2) - mouseY)/range(VASrange);
    Screen('TextSize', win, 32); 
    DrawFormattedText(win, sprintf('%d%%',round(conftmp*100)), midX-115, midY+10, [255 255 255]);
    
    Screen('Flip',win); % display
    
    % detects the first movement made by the mouse
    if mouseY ~= initialy && ~movementBegun
        P.T(i).RTmouseVAS = GetSecs - P.T(i).onset;
        movementBegun = 1;
    end
    
    mousetrail_y(counter)   = mouseY; % save Y trajectory
    moveTime(counter)       = GetSecs - P.T(i).onset;
    counter                 = counter + 1;
    
    %% detect clicks
    if mouseButtons(1)>0 % left click
        if P.dosendtriggers,outp(P.Address,P.trigresp2); WaitSecs(0.002);outp(P.Address, 0);end
        P.T(i).confidence        = conftmp;
        P.T(i).conf_absolute     = mouseY;
        P.T(i).RTconf             = GetSecs - P.T(i).onset;
        disp('Click')
        break
    end
%     elseif any(mouseButtons(2:length(mouseButtons))>0) % cancel trial if right click
%         P.T.confidence(i)        = 999;
%         P.T.conf_absolute(i)     = mouseY;
%         P.T.movRT(i)             = GetSecs - P.T(i).onset;
%         break
%     end
    
    
end

% % what happens in case of a click?
% if mouseButtons(1)
%     %internal to external extremes
%     P.T.confidence(i)      = conftmp; % save confidence
%     P.T.conf_absolute(i)   = mouseY; % save last Y position
%     P.T.movRT(i)           = GetSecs - P.T.onset(i);
% 
%     draw_vertical_vas(VASrange);
%     Screen('Flip',win);
%     WaitSecs(.5);
%     
% else
%     P.T.confidence(i)    = NaN;
%     P.T.conf_absolute(i) = NaN;
%     P.T.movRT(i)         = NaN;
% end
% 
% if ~movementBegun
%     P.T.firstRT(i)      = NaN;
% end

% P.T(i).VAS.mousetrail_y = mousetrail_y;
% P.T(i).VAS.moveTime     = moveTime;
Screen('Flip',win);

end
