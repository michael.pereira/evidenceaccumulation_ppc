function main_experiment

subnum = 302;
initialamp = 0.35;
clearvars -except subnum initialamp

global handle
global P

% close port
try
    IOPort('Close', handle);
catch
end

commandwindow;
addpath('./functions');

try PsychPortAudio('Close')
catch
end


%% load stim configuration
P = P_config(subnum);

if strcmp(P.Triggers,'serial')
    %_______________________________
    % Open SerialPort
    for i=1:3
        try
            [handle, errmsg] = IOPort('OpenSerialPort', P.TriggerPort,sprintf('BaudRate=%d',P.BaudRate));
            break
        catch
            fprintf('Failed to open serial port, retrying...\n');
            pause(5);
        end
    end
end

%% specific configuration for main experiment
P.initialamp    = initialamp;                   % first threshold estimation based on threshestimator.m
P.stimamps      = [0 0.5:0.5:5].*P.initialamp;  % possible intensities
P.nrep          = 20;    %20                    % number of repetitions
P.trial_number  = length(P.stimamps)*P.nrep;    % total number of trials per block
P.block_number  = 100;                          % total number of blocks
P.lambda        = 2;                            % specific lambda for main experiment

P.rest1         = 5*60;                         % first phase of rest
P.rest2         = 10*60;                        % second phase of rest

%% start experiment
for b = 1:P.block_number
    P.T = []; % reinitialize trial structure
    P.stimamp      = Shuffle(repmat(P.stimamps,1,P.nrep));
    filename = ['main_exp_suj' num2str(P.subnum) '_block' num2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat']
    P.startblock = GetSecs;
    for i = 1:P.trial_number
        P.T(i).sub = subnum;          % subject number
        P.T(i).trial = i;             % trial number
        P.T(i).block = b;             % block number
        P.T(i).stimamp = P.stimamp(i); % stim amplitude for trial>1
        
        %% update current stim
        P.T(i).stimonset = exprnd(P.lambda); % stim onset
        PsychPortAudio('FillBuffer',P.pahandle,P.basestim.*P.T(i).stimamp);
        WaitSecs(P.T(i).stimonset);
        
        %% send trigger for stim onset
        SendTrigger(P.TRIGGER_STIMSTART,P.Triggers)
        P.T(i).StartSound = PsychPortAudio('Start',P.pahandle,1,[],1); %GetSecs+P.T(i).stimonset
        [~, ~, ~, P.T(i).StopSound] = PsychPortAudio('Stop',P.pahandle,1);
        WaitSecs(P.waitstim);
        
        %% process answer
        [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);
        P.T(i).firstPress=firstPress;
        P.T(i).firstRelease=firstRelease;
        
        
        if keyIsDown
            % take a break
            disp('Taking a break ...')
            disp ('Press any key when you are ready!')
            pause;
            PsychHID('KbQueueFlush', P.index);
            continue;
        end
        
        
        fprintf('Block %d, trial %d/%d: onset = %.3f, intensity = %.3f\n\n\n',b,i,P.trial_number,P.T(i).stimonset, P.T(i).stimamp);
        save([P.savePath filesep filename], 'P');
        PsychHID('KbQueueFlush', P.index);
    end
    P.endblock = GetSecs;
	save([P.savePath filesep filename], 'P');
    
    %% REST PHASE
    fprintf('\n\n ** NOW 5 mn REST ** \n\n');
    startrest1 = tic; 
    while toc(startrest1)<P.rest1
         %% process answer
        [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);       
        if keyIsDown
            WaitSecs(0.2);
            % take a break
            disp('Taking a break ...')
            disp ('Press any key when you are ready!')
            pause;
                        disp('Resume...')
            PsychHID('KbQueueFlush', P.index);
            continue;
        end
    end
%     WaitSecs(P.rest1);
    
    %% STIM PHASE
    S.T         = [];                       % reinitialize trial structure
    S.block     = b;                        % block number 
    S.subnum    = subnum;                   % subject ID
    S.stimfreq  = 0.75;                     % stim frequency
    S.stimamp   = 2.5*P.initialamp;           % stim amplitude
    S.SOA       = (1/S.stimfreq)-P.stimdur; % stim SOA
    S.ntrials   = round(5*60*S.stimfreq);   % number of trials
    
    S.startblock= GetSecs;
    s_filename  = ['sleep_suj' num2str(S.subnum) '_block' num2str(b) '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat']
    
    for is = 1:S.ntrials
        S.T(is).trial = is;
        PsychPortAudio('FillBuffer',P.pahandle,P.basestim.*S.stimamp);
        WaitSecs(S.SOA);
        %% send trigger for stim onset
        SendTrigger(P.TRIGGER_STIMSTART,P.Triggers)
        S.T(is).StartSound = PsychPortAudio('Start',P.pahandle,1,[],1);
        [~, ~, ~, S.T(is).StopSound] = PsychPortAudio('Stop',P.pahandle,1);
        fprintf('Sleep: Block %d: trial %d/%d\n\n\n',b,is,S.ntrials);
        
        %% process answer
        [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);       
        if keyIsDown
            % take a break
            disp('Taking a break ...')
            disp ('Press any key when you are ready!')
            pause;
                        disp('Resume...')
            PsychHID('KbQueueFlush', P.index);
            continue;
        end
        
    end
    S.endblock = GetSecs;
    
    save([P.savePath filesep s_filename], 'S');
    
    %% REST PHASE
    fprintf('\n\n ** NOW 10 mn REST ** \n\n');
%     WaitSecs(P.rest2);
    startrest2 = tic; 
    while toc(startrest2)<P.rest2
         %% process answer
        [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);       
        if keyIsDown
            % take a break
            disp('Taking a break ...')
            disp ('Press any key when you are ready!')
            pause;
            PsychHID('KbQueueFlush', P.index);
            continue;
        end
    end

end

PsychHID('KbQueueStop', P.index);

% close port
if strcmp(P.Triggers,'serial') && handle > -1
    IOPort('Close', handle);
end
end
