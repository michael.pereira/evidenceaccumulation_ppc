%========================================================
%   Spike sorting Experiment 2 - session 1
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This file contains a structure array (one item per channel)
% containing "thr": the osort threshold in standard deviations
% (must be precomputed in osort, see README), a cell array of 
% cluster IDs and a corresponding array indicating whether we 
% consider the cluster as single- or multi-unit
%
% for example, if for channel 1 with a threshold of 5, we 
% considered clusters 802 and 803 as one single unit, and 
% cluster 1023 as a multiunit, the sort structure would be the 
% following: 
%   sort(1).thr = 5;
%   sort(1).clust = {[802,803],1023};
%   sort(1).single = [1, 0];

function sort = sort_e2s1()

sort(1).thr = 5;
sort(1).clust = {6101,6140,6152};
sort(1).single = [1 1 1];
sort(2).thr = NaN;
sort(2).clust = {};
sort(2).single = [];
sort(3).thr = 5;
sort(3).clust = {278};
sort(3).single = [1];
sort(4).thr = 5;
sort(4).clust = { 5270,5590,5648,5652};
sort(4).single = [1,1,0,1];
sort(5).thr = 5;
sort(5).clust = {836,840};
sort(5).single = [1,0];
sort(6).thr = 5;
sort(6).clust = {[1262],1414}; % 1333 with 1262?
sort(6).single = [1,1];
sort(7).thr = NaN; 
sort(7).clust = {};
sort(7).single = [];
sort(8).thr = 5;
sort(8).clust = {1073,1082};
sort(8).single = [1,1];
sort(9).thr = NaN ;
sort(9).clust = {};
sort(9).single = [];
sort(10).thr = NaN;
sort(10).clust = {};
sort(10).single = [];
%
sort(11).thr = 5;
sort(11).clust = {3086,3189};
sort(11).single = [0,1];
sort(12).thr = NaN;
sort(12).clust = {};
sort(12).single = [];
sort(13).thr = 5;
sort(13).clust = {149};
sort(13).single = [1];
sort(14).thr = 5;
sort(14).clust = {1910};
sort(14).single = [0];
sort(15).thr = 5;
sort(15).clust = {4150,4818,4632,[4595,4792]};
sort(15).single = [1,1,1,1];
sort(16).thr = NaN;
sort(16).clust = {};
sort(16).single = [];
sort(17).thr = NaN;
sort(17).clust = {};
sort(17).single = [];
sort(18).thr = 5;
sort(18).clust = {2161,2187,2250};
sort(18).single = [1,0,1];
sort(19).thr = 5;
sort(19).clust = {[4269,4245,4232,4210,4165]};
sort(19).single = [0];
sort(20).thr = NaN;
sort(20).clust = {};
sort(20).single = [];
%
sort(21).thr = 5;
sort(21).clust = {162};
sort(21).single = [0];
sort(22).thr = 5;
sort(22).clust = {6581,[6578,6670]};
sort(22).single = [1,0];
sort(23).thr = 5;
sort(23).clust = {2882,2918};
sort(23).single = [0,0];
sort(24).thr = 5;
sort(24).clust = {5140,5244,5295};
sort(24).single = [0,0,0];
sort(25).thr = 5;
sort(25).clust = {290};
sort(25).single = [1];
sort(26).thr = 5;
sort(26).clust = {1095};
sort(26).single = [0];
sort(27).thr = NaN;
sort(27).clust = {};
sort(27).single = [];
sort(28).thr = 5;
sort(28).clust = {5946};
sort(28).single = [0];
sort(29).thr = 5;
sort(29).clust = {[5522,5655]};
sort(29).single = [0];
sort(30).thr = 5;
sort(30).clust = {3580,3579,3499,[1501,1633,2933,2992,2604,1849],[3378,3408],3558};%,,2604,1849,,1633,3408,3378]}; % 3580 low SNR
sort(30).single = [1,1,0,1,0,1];
% 
sort(31).thr = 5;
sort(31).clust = {4928,[5093,4655],5128,5173,5181};
sort(31).single = [1,1,1,0,1];
sort(32).thr = 5;
sort(32).clust = {4292,4320,4324,4335};
sort(32).single = [0,0,1,1];
sort(33).thr = NaN;
sort(33).clust = {};
sort(33).single = [];
sort(34).thr = NaN;
sort(34).clust = {};
sort(34).single = [];
sort(35).thr = NaN;
sort(35).clust = {};
sort(35).single = [];
sort(36).thr = NaN;
sort(36).clust = {};
sort(36).single = [];
sort(37).thr = 5;
sort(37).clust = {[240,245]};
sort(37).single = [1];
sort(38).thr = 5;
sort(38).clust = {632,657};
sort(38).single = [1,1];
sort(39).thr = 5;
sort(39).clust = {[3498,3650,3536],[3641,3669,3670]};
sort(39).single = [1,1];
sort(40).thr = NaN;
sort(40).clust = {};
sort(40).single = [];
%
sort(41).thr = NaN;
sort(41).clust = {};
sort(41).single = [];
sort(42).thr = NaN;
sort(42).clust = {};
sort(42).single = [];
sort(43).thr = 5;
sort(43).clust = {[656,699,700,762]};
sort(43).single = [1];
sort(44).thr = 5;
sort(44).clust = {[2383,2444,2464],2434,2439};
sort(44).single = [1,1,1];
sort(45).thr = 5;
sort(45).clust = {[428,434,461,405]};
sort(45).single = [1];
sort(46).thr = NaN;
sort(46).clust = {};
sort(46).single = [];
sort(47).thr = NaN;
sort(47).clust = {};
sort(47).single = [];
sort(48).thr = 5;
sort(48).clust = {[274,282]};
sort(48).single = [1];
sort(49).thr = NaN;
sort(49).clust = {};
sort(49).single = [];
sort(50).thr = NaN;
sort(50).clust = {};
sort(50).single = [];
%
sort(51).thr = 5;
sort(51).clust = {[643 584,660]};
sort(51).single = [1];
sort(52).thr = NaN;
sort(52).clust = {};
sort(52).single = [];
sort(53).thr = NaN;
sort(53).clust = {};
sort(53).single = [];
sort(54).thr = NaN;
sort(54).clust = {};
sort(54).single = [];
sort(55).thr = NaN;
sort(55).clust = {};
sort(55).single = [];
sort(56).thr = NaN;
sort(56).clust = {};
sort(56).single = [];
sort(57).thr = 5;
sort(57).clust = {[2029,2102]};
sort(57).single = [1];
sort(58).thr = NaN;
sort(58).clust = {};
sort(58).single = [];
sort(59).thr = 5;
sort(59).clust = {5946,6196,6205};
sort(59).single = [1 1 1];
sort(60).thr = 5;
sort(60).clust = {7011,7831,[7747,7702,4631],7833};
sort(60).single = [1,0,1,1];
%
sort(61).thr = NaN;
sort(61).clust = {};
sort(61).single = [];
sort(62).thr = NaN;
sort(62).clust = {};
sort(62).single = [];
sort(63).thr = 5;
sort(63).clust = {1615,1701};
sort(63).single = [1,0];
sort(64).thr = 5;
sort(64).clust = {[594,588]};
sort(64).single = [0];
sort(65).thr = 5;
sort(65).clust = {148};
sort(65).single = [1];
sort(66).thr = NaN;
sort(66).clust = {};
sort(66).single = [];
sort(67).thr = 5;
sort(67).clust = {339};
sort(67).single = [1];
sort(68).thr = 5;
sort(68).clust = {2351,2367,2676};
sort(68).single = [1,1,1]; % 3
sort(69).thr = 5;
sort(69).clust = {4140,4494,[4565,4599]};
sort(69).single = [1,1,1];
sort(70).thr = 5;
sort(70).clust = {[251,453]};
sort(70).single = [1];
%
sort(71).thr = 5;
sort(71).clust = {3744,3790,3813};
sort(71).single = [1,1,1];
sort(72).thr = 5;
sort(72).clust = {[225,244,248,254]};
sort(72).single = [1];
sort(73).thr = 5;
sort(73).clust = {[1304,1318]};
sort(73).single = [1];
sort(74).thr = 5;
sort(74).clust = {[1110,739,1144]};
sort(74).single = [1]; %1
sort(75).thr = 5;
sort(75).clust = {[753,822,824]};
sort(75).single = [0];
sort(76).thr = 5;
sort(76).clust = {[2221,2599],2537};
sort(76).single = [1,1];
sort(77).thr = 5;
sort(77).clust = {[902,1060]};
sort(77).single = [0];
sort(78).thr = 5;
sort(78).clust = {184};
sort(78).single = [1];
sort(79).thr = 5;
sort(79).clust = {[2752,2784,2861,2892],2895};
sort(79).single = [1,1];
sort(80).thr = NaN;
sort(80).clust = {};
sort(80).single = [];
%
sort(81).thr = 5;
sort(81).clust = {[124,1326,1724,1771],1739,1804}; % 1804 low snr
sort(81).single = [0,0,0];
sort(82).thr = NaN;
sort(82).clust = {}; % difference in firing rate over time
sort(82).single = [];
sort(83).thr = 5;
sort(83).clust = {163};
sort(83).single = [1];
sort(84).thr = 5;
sort(84).clust = {[3782,3962],[3976,3959]};
sort(84).single = [1,1];
sort(85).thr = NaN;
sort(85).clust = {};
sort(85).single = [];
sort(86).thr = NaN;
sort(86).clust = {};
sort(86).single = [];
sort(87).thr = 5;
sort(87).clust = {[587,593]};
sort(87).single = [1];
sort(88).thr = 5;
sort(88).clust = {110};
sort(88).single = [0];
sort(89).thr = 5;
sort(89).clust = {[3853,3878]};
sort(89).single = [0];
sort(90).thr = 5;
sort(90).clust = {[736,762],760};
sort(90).single = [1,1];
%
sort(91).thr = 5;
sort(91).clust = {[3831,3933,3943,4015,4027]};
sort(91).single = [0];
sort(92).thr = 5;
sort(92).clust = {1447,1588};
sort(92).single = [0,1];
sort(93).thr = 5; 
sort(93).clust = {4904,5023,5133,5138};
sort(93).single = [0,1,1,1];
sort(94).thr = 5;
sort(94).clust = {6743,[6684,6616]};
sort(94).single = [1,0];
sort(95).thr = 5;
sort(95).clust = {[4043,4372],4441,4571};
sort(95).single = [0,1,1]; % 2
sort(96).thr = 5;
sort(96).clust = {[4195,4063]};
sort(96).single = [1];
