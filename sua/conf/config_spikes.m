%========================================================
%   Spike sorting Experiment 1 - session 1
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   7 Dec 2020
%========================================================

% This file contains the configurations for the single neuron and
% ecog analyses

if ~exist('conf','var')
    conf = struct();
else
    if ~isstruct(conf)
        warning('conf variable is not a struct, will likely fail');
    end
end

user = 'michael';

if ~isfield(conf,'exp')
    conf.exp = 1;    
    fprintf(' [config] Setting version=%d\n',conf.exp);
end
if ~isfield(conf,'session')
    conf.session = 1;
    fprintf(' [config] Setting session=%d\n',conf.session);
end

conf.sessid = sprintf('e%ds%d',conf.exp,conf.session);

switch user
    case 'michael'
        conf.dir.toolbox_base = '~/Toolboxes/';
        % dir containing BIDS dataset
        conf.dir.data = '/Volumes/Data/data/BIDS/perithr_ppc/';
        % dir containing proprietary data we cannot share without a research 
        % agreement
        conf.dir.propdata = '/Volumes/Data/data/perithr-hug/';
        % dir for preprocessed data
        conf.dir.preprocdir = '/Volumes/Data/data/perithr-hug/preproc2/';
        if ~exist(conf.dir.preprocdir,'dir')
            fprintf(' [config] attempting to create %s\n',conf.dir.preprocdir);
            mkdir(conf.dir.preprocdir);
        end
end
conf.dir.toolbox_fieldtrip = ['/Users/meaperei/Dropbox/code/fieldtrip'];

% path to iELVis toolbox (http://ielvis.pbworks.com)
conf.dir.toolbox_ielvis = [conf.dir.toolbox_base '/iElVis/'];
conf.dir.toolbox_maio = [conf.dir.toolbox_base '/mdaio/'];

conf.dir.toolbox_json = [conf.dir.toolbox_base '/jsonlab/'];
conf.dir.toolbox_csc = [conf.dir.toolbox_base '/nlximport'];
conf.dir.toolbox_circ = [conf.dir.toolbox_base '/circstat'];
conf.dir.toolbox_osort = [conf.dir.toolbox_base '/osort_mod'];
% path to the NPMK toolbox from blackrock micro
% (https://www.blackrockmicro.com/neuroscience-research-products/ephys-analysis-software/matlab-development-kits/)
conf.dir.toolbox_blackrock = [conf.dir.toolbox_base '/NPMK/'];

conf.dir.brainmodel = [conf.dir.data '/derivatives/brainmodel/'];

conf.tmin.physio = 0;
conf.tmin.spikes = 0;

if conf.exp == 1 && conf.session == 1
    fprintf(' [ config ] e1s1\n');
    conf.dir.spikesfile = [conf.dir.propdata '/ephys/v1/20180320-105734'];
    conf.dir.physiofile = [conf.dir.data '/sub-patient/ieeg/sub-patient_ses-01_exp-01_ieeg.TRC'];
    conf.dir.behavfile = [conf.dir.data '/sub-patient/beh/sub-patient_ses-01_exp-01_beh.tsv'];

    % time at which to start analysing data
    conf.tmin.physio = 500;
    conf.tmin.spikes = 500;
    
    % time segments to remove
    conf.badseg.times = [172.7 172.75; 190.17 190.21 ; 347.51 347.525 ; ...
        616.67 616.68 ; 658.44 658.47 ; 722.2 722.24 ; 1375.5 1375.515 ; ...
        1416.95 1416.975 ; 1489.605 1489.65 ; 1521.825 1521.85];
    % id of bad trials
    conf.badtrials = [9];
    % copy of badtrials for both sessions (useful when merging sessions 1
    % and 2)
    conf.allbadtrials =  [9 151,182,183,194,200,214,225,242:255,268,272,291];
    
elseif conf.exp == 1 && conf.session == 2
    fprintf(' [ config ] e1s2\n');
    conf.dir.spikesfile = [conf.dir.propdata '/ephys/exp1/20180320-140316'];
    conf.dir.physiofile = [conf.dir.data '/derivatives/sub-patient/ieeg/sub-patient_ses-02_exp-01_ieeg.TRC'];
    conf.dir.behavfile = [conf.dir.data '/derivatives/sub-patient/beh/sub-patient_ses-02_exp-01_beh.tsv'];
    
    % time at which to start analysing data
    conf.tmin.physio = 700;
    conf.tmin.spikes = 700;
    
    % time segments to remove
    conf.badseg.times = [0.35 0.55 ; 23.68 23.73 ; 82.905 82.925 ; 102.07 102.1 ; ...
        137.4 137.6 ; 252.15 252.165 ; 386.46 386.51 ; 516.35 516.45 ; ...
        516.55 516.65 ; 518.1 518.2 ; 520.4 520.5 ; 520.7 520.9 ; ...
        719.8 719.85 ; 811.9 812 ; 923.9 924 ; 1058.85 1059 ; 1128.9 1129 ; ...
        1125.9 1226 ; 1413 1413.1 ; 1551.2 1551.4 ];
    % id of bad trials
    conf.badtrials = [151,182,183,194,200,214,225,242:255,268,272,291];
    conf.allbadtrials =  [9 151,182,183,194,200,214,225,242:255,268,272,291];
    
elseif conf.exp == 2 && conf.session == 1
    fprintf(' [config] e2s1\n');
    conf.dir.spikesfile = [conf.dir.propdata '/ephys/exp2/20180322-124033'];
    conf.dir.physiofile = [conf.dir.data '/derivatives/sub-patient/ieeg/sub-patient_ses-03_exp-02_ieeg.TRC'];
    conf.dir.behavfile = [conf.dir.data '/derivatives/sub-patient/beh/sub-patient_ses-03_exp-02_beh.tsv'];

    % time at which to start analysing data
    conf.tmin.physio = 640;
    conf.tmin.spikes = 640;
    
    % time segments to remove
    conf.badseg.times = [206.2 206.3 ; 411 411.1 ; 421.6 421.8 ; ...
        422.4 422.6 ; 960.2 960.4 ; 1104.3 1104.5 ; 1161.2 1161.4 ; ...
        1499.2 1499.3 ; 1618.9 1619.1 ];
    % id of bad trials
    conf.badtrials = [2:10,13,15,20,28,37,43,50,122,134,142];     
    conf.allbadtrials = conf.badtrials;

elseif conf.exp == 3 && conf.session == 1
    fprintf(' [ config ] e3s1\n');
    conf.dir.spikesfile = [conf.dir.propdata '/ephys/exp3/20180325-091116'];
    conf.dir.physiofile = [conf.dir.data '/derivatives/sub-patient/ieeg/sub-patient_ses-04_exp-03_ieeg.TRC'];
    conf.dir.behavfile = [conf.dir.data '/derivatives/sub-patient/beh/sub-patient_ses-04_exp-03_beh.tsv'];

    % time at which to start analysing data
    conf.tmin.physio = 60;
    conf.tmin.spikes = 60;
    
    % time segments to remove
    conf.badseg.times = [141.6 141.8 ; 279 282 ; 408 412.5 ; 470 471.2 ; ...
        489.8 490.2 ; 490 855 ; 1343.4 1343.6 ];
    % id of bad trials
    conf.badtrials = [34,73,79,97,99,35,50,51,57,65,85,105,125,172,201];
    conf.allbadtrials = conf.badtrials;
end

conf.dir.output = [conf.dir.preprocdir '/' conf.sessid '/'];

% sampling frequencies
conf.fs.spikes = 30e3; %
conf.fs.lfp = 500;
conf.fs.analysis = 1000;

%% Osort clustering parameters
conf.clust.samplerate = 30000;
conf.clust.detect_threshold = [5];

conf.fratemethod = 'orig';
conf.gausswin = 0.1; % standard deviation of the gaussian window to compute framerate
conf.modelcomp = 0; % do model comparison 
conf.modelcrit = 'AIC';
conf.nperm = 1000; % number of permutation for the stats

% parameters for decoding
conf.classif.ignore = 0; 
conf.classif.firethr = 0; % use only units with a higher firing rate
conf.classif.onlysua = 1; % use only single unit
conf.classif.gamma = 0.8; % gamma hyperparameter for regularization
conf.classif.delta = 0; % delta hyperparameter for regularization
conf.classif.dolog = 0; % apply log
conf.classif.discrim = 'linear'; % discriminant type (see fitcdiscr)
conf.classif.nfoldint = 0; % number of internal cross-validation folds to optimize parameters
conf.classif.nfoldext = 10; % number of external cross-validation folds
conf.classif.optimize = 'None'; % see OptimizeHyperparameters in fitcdiscr

% time subsampling for decoding
conf.selt = 1:10:2001;
% 300 ms baseline 
conf.tbaseline = [-0.3,0];
% 0.5 - 1.5 window for stats
conf.tstat = [0.5,1.5];

% link to the (curated) sorted data
conf.clust.e1s1 = sort_e1s1();
conf.clust.e1s2 = sort_e1s2();
conf.clust.e2s1 = sort_e2s1();
conf.clust.e3s1 = sort_e3s1();

%% SPIKE ANALYSIS
conf.spikes.fratewin = 0.3;

%% ECOG ANALYSIS
conf.ecog.fs = 2048;
conf.ecog.nfir = 512;
conf.ecog.nfirbig = 2048;

% Notches
wo = 50/(conf.ecog.fs/2);  
bw = 2/(conf.ecog.fs/2);
[conf.ecog.bn1,conf.ecog.an1] = iirnotch(wo,bw);

wo = 100/(conf.ecog.fs/2);  
bw = 1/(conf.ecog.fs/2);
[conf.ecog.bn2,conf.ecog.an2] = iirnotch(wo,bw);

wo = 150/(conf.ecog.fs/2);  
bw = 1/(conf.ecog.fs/2);
[conf.ecog.bn3,conf.ecog.an3] = iirnotch(wo,bw);

% low pass filter
wl = [40]./(conf.ecog.fs/2);  
[conf.ecog.blp1,conf.ecog.alp1] = fir1(conf.ecog.nfir,wl,'low');

% high pass filter
wh = [0.5]./(conf.ecog.fs/2);  
[conf.ecog.bhp,conf.ecog.ahp] = fir1(conf.ecog.nfir,wh,'high');

% HGA filters
freq = 50:10:180;
nfr = length(freq);
for fr=1:nfr
    wh = [freq(fr),freq(fr)+mode(diff(freq))]./(conf.ecog.fs/2);  
    [conf.ecog.bbp_hg{fr},conf.ecog.abp_hg{fr}] = fir1(512,wh,'bandpass');    
end

%% Colors
conf.color.hit = [83,169,212]./255;
conf.color.miss = [186,39,50]./255;
conf.color.crej = [149,189,20]./255;
conf.color.fa = [0 0 0]./255;
conf.color.hit1 = [162,202,211]./255;
conf.color.hit2 = [83,169,212]./255;
conf.color.hit3 = [2,61,79]./255;
conf.color.hit4 = [0,0,0]./255;

conf.color.miss1 = conf.color.miss;
conf.color.miss2 = [187,106,163]./255;
conf.color.crej1 = conf.color.crej;
conf.color.crej2 = [244,220,118]./255;

conf.color.binamp1 = [134,123,180]./255;
conf.color.binamp2 = [186,131,180]./255;
conf.color.binamp3 = [104,48,134]./255;

conf.color.amp1 = [74,99,30]./255;
conf.color.amp2 = [131,176,68]./255;
conf.color.amp3 = [187,251,105]./255;
conf.color.amp4 = [196,229,100]./255;
conf.color.amp5 = [217,203,104]./255;
conf.color.amp6 = [216,175,82]./255;
conf.color.amp7 = [214,146,66]./255;