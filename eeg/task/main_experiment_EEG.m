function main_experiment_EEG(subnum,initialamp)
% in v2, we present trials with perithreshold stim + 20% of catch (no stim)
% subject is instructed to respond after 2s (yes/no) and then provide confidence
% script has to be ran with sudo to be able to send triggers
% based on main_experiment_v2
clearvars -except subnum initialamp

global handle
global P

commandwindow;
addpath('./functions');

try PsychPortAudio('Close')
catch
    
end

%% load stim configuration
P = P_config_EEG(subnum); % config file found in ./functions
if strcmp(P.triggertype,'parallel')
    addpath('/home/eeg/Documents/perithreshold_experiment/ppdev-mex')
    ppdev_mex('CloseAll');
    ppdev_mex('Open', 1);
end



%% specific configuration for main experiment
P.initialamp    = initialamp;         % first threshold estimation based on threshestimator.m
P.stepsize      = initialamp*0.02;    % stepsize for staircase in % intensity of P.basestim
P.block_number  = 10;                 % total number of blocks
P.trial_number  = 50;                 % total number of trials per block (must be divisible by 5)
P.lambda        = nan;                % in V2 stim onset is uniform random between 0 and 1.9s
P.ITI           = 0.5;                % inter-trial interval
P.rand          = 2;                  % width of uniform distribution for stim onset

stimamp = P.initialamp;               % initialise stimulus amplitude
stepsize = P.stepsize;                % initialise stepsize

%% fixation cross and VAS
global win
global midY midX

% Switch to command window
commandwindow;

% % Hide cursor in screen
% HideCursor();

% Get the screen numbers
screens = Screen('Screens');

% Draw to the external screen if avaliable
screenNumber = max(screens);

% Define colors
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
gray = GrayIndex(screenNumber);
green = [0 130 0];

% Open an on screen window
[win, windowRect] = PsychImaging('OpenWindow', screenNumber, black, [0 0 1920 1200]); %full screen size
% [win, windowRect] = PsychImaging('OpenWindow', screenNumber, black, [0 0 1200 700]);

% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', win);

% Set up alpha-blending for smooth (anti-a liased) lines
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% Setup the text type for the window
Screen('TextFont', win, 'Arial');
Screen('TextSize', win, 36);

% Get the centre coordinate of the window
[midX, midY] = RectCenter(windowRect);

% Here we set the size of the arms of our fixation cross
fixCrossDimPix = 60;

% Now we set the coordinates (these are all relative to zero we will let
% the drawing routine center the cross in the center of our monitor for us)
xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
allCoords = [xCoords; yCoords];

% Set the line width for our fixation cross
lineWidthPix = 6;

DrawFormattedText(win, 'Press any key when you are ready!', ...
    'center', midY-90, white);

Screen('Flip',win)
RestrictKeysForKbCheck([]);
KbWait([]);

%% start experiment
SendTrigger(P.triggertype,P.start_mainexp);
for b = 1:P.block_number
    SendTrigger(P.triggertype,P.trigblock)
    
    P.T = []; % reinitialize trial structure
    % logical: 0 absence/ 1 presence of stimulus
    P.stimvec = Shuffle(repmat([0 1 1 1 1],1,P.trial_number/5));
    fprintf('\n-----------------------\n');
    fprintf(['Now running block ' num2str(b) '\n']);
    change_stimamp = '';
    filename = ['main_exp_suj' num2str(P.subnum) '_block' num2str(b) '_' ...
        [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat']
    
    
    for i = 1:P.trial_number
        PsychHID('KbQueueStart',P.index);
        Screen('DrawLines', win, allCoords,...
            lineWidthPix, gray, [midX midY-90], 2);   % draw gray fixation cross
        Screen('Flip', win);
        
        if i == 1
            WaitSecs(2);  % wait 2s before experiment begins
        end
        fprintf('\n-----------------------\n');
        fprintf(['Now running trial ' num2str(i) ', block ' num2str(b) '\n']);
        
        P.T(i).sub = subnum;            % subject number
        P.T(i).trial = i;               % trial number
        P.T(i).block = b;               % block number
        P.T(i).stimamp = stimamp;       % stim amplitude for trial > 1
        P.T(i).stepsize = stepsize;     % step size
        P.T(i).earlyfirstpress = 0;     % check what keys were pressed during stim
        P.T(i).earlyfirstrelease = 0;   % check what keys were released during stim
        P.T(i).starttrial = GetSecs;    % get time when trial begins
        P.T(i).stimvec = P.stimvec(i);  % presence/ absence of stimulus
        
        % avoid null stim
        if P.T(i).stimamp < 0
            P.T(i).stimamp = stepsize;
        end
        
        %% update current stim
        P.T(i).stimonset = P.rand*rand(1); % stim onset
        Screen('DrawLines', win, allCoords,...
            lineWidthPix, green, [midX midY-90], 2);   % draw green fixation cross
        Screen('Flip', win);
        PsychPortAudio('fillBuffer',P.pahandle,P.basestim.*P.T(i).stimamp.*P.T(i).stimvec); % send stim
        WaitSecs(P.T(i).stimonset);
        SendTrigger(P.triggertype,P.trigonset)
        
        P.T(i).startSound = PsychPortAudio('Start',P.pahandle,1,[],1);
        [~,~,~,P.T(i).stopSound] = PsychPortAudio('Stop',P.pahandle,1);
        
        % give time for subjects to respond
        P.T(i).waitresp = P.waitstim + (P.rand-P.T(i).stimonset);
        WaitSecs(P.T(i).waitresp);
        
        fprintf('\n RESP!\n\n')
        DrawFormattedText(win, 'Did you feel a stimulus?', ...
            'center',midY-90, white);
        Screen('Flip',win);
        P.T(i).startResp = GetSecs;% PsychPortAudio('Start',P.pahandle,1,[],1);
        
        %% check for key presses/releases before beep
        [earlypressDown, earlyfirstPress, earlyfirstRelease] = PsychHID('KbQueueCheck', P.index);
        PsychHID('KbQueueFlush', P.index);
        PsychHID('KbQueueStop', P.index);
        
        P.T(i).earlyfirstpress = earlyfirstPress;
        P.T(i).earlyfirstrelease = earlyfirstRelease;
        
        
        %% type 1 answer
        keys=zeros(256,1);
        keys(KbName({'4','6','a','z','*' ,'Return'}))=1;
        RestrictKeysForKbCheck(find(keys));
        
        [secs1, keyCode1, deltaSecs1] = KbWait([],2);
        
        P.T(i).firstPress = secs1;
        P.T(i).key1 = KbName(keyCode1);
        P.T(i).rt1 = secs1 - P.T(i).startResp; % RT
        
        if or(strcmp(P.T(i).key1, '6'), strcmp(P.T(i).key1, 'z')) ...
                & P.T(i).stimvec==1
            P.T(i).sdt = 'hit';
            % decrease stim amplitude for next trial when hit
            stimamp =  P.T(i).stimamp - stepsize;
            
            SendTrigger(P.triggertype,P.trighit)
            
        elseif or(strcmp(P.T(i).key1, '6'), strcmp(P.T(i).key1, 'z')) ...
                & P.T(i).stimvec==0
            P.T(i).sdt = 'fa';
            % stim amplitude unchanged for next trial when fa
            stimamp =  P.T(i).stimamp;
            SendTrigger(P.triggertype,P.trigfa)
            
        elseif or(strcmp(P.T(i).key1, '4'), strcmp(P.T(i).key1, 'a')) ...
                & P.T(i).stimvec==1
            P.T(i).sdt = 'miss';
            % increase stim amplitude for next trial when miss
            stimamp =  P.T(i).stimamp + stepsize;
            SendTrigger(P.triggertype,P.trigmiss)
            
            
        elseif or(strcmp(P.T(i).key1, '4'), strcmp(P.T(i).key1, 'a')) ...
                & P.T(i).stimvec==0
            P.T(i).sdt = 'crej';
            % stim amplitude unchanged for next trial when crej
            stimamp =  P.T(i).stimamp;
            SendTrigger(P.triggertype,P.trigcrej)
            
        elseif strcmp(P.T(i).key1, '*')
            % take a break
            disp('Taking a break ...');
            P.T(i).sdt = 'break';
            % possibility to change stimulus amplitude
            while ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
                change_stimamp = input('New stimamp for next trial? (''y'' or ''n'') \n');
            end
            
            if change_stimamp == 'y'
                stimamp = input('What is the new stimamp?\n');
                if isnumeric(stimamp)
                    fprintf('stimamp set to %.2f \n', stimamp);
                else
                    while ~ isnumeric(stimamp)
                        fprintf('Sorry, numerical value expected \n');
                        stimamp = input('Please enter the new stimamp: \n');
                    end
                end
            end
            change_stimamp = '';
            disp ('Press any key when you are ready!!');
            DrawFormattedText(win, 'Press any key when you are ready!', ...
                'center',midY-90, white);
            Screen('Flip',win);
           
            pause;
            continue;
        else
            P.T(i).sdt = '??';
            stimamp =  P.T(i).stimamp;
            warning('Something''s funky here!')
        end
        
        %% Continuous type2 answer
        if ~secs1 == 0
            disp('first response done');
            WaitSecs(0.5);
            
            SendTrigger(P.triggertype,P.trigvas)
            
            % show Visual Analog Scale
            P = vertical_vas_kb(P, i);
        end
        
        %% Save and show gray cross
        fprintf('%s! \n answer = %s, onset = %.3f, rt1 = %.3f, conf = %.3f, rt_conf = %.3f, Next intensity= %.3f, Next stepsize= %.3f\n\n\n', ...
            P.T(i).sdt,P.T(i).key1,P.T(i).stimonset,P.T(i).rt1,P.T(i).confidence,P.T(i).rt_conf, stimamp, stepsize);
        
        save([P.savePath filesep filename], 'P');
        %         PsychPortAudio('DeleteBuffer',P.pahandle);
        Screen('DrawLines', win, allCoords,...
            lineWidthPix, gray, [midX midY-90], 2);   % draw gray fixation cross
        Screen('Flip', win);
        
        WaitSecs(P.ITI);
    end
    
    
    if b < P.block_number
        fprintf('***Block %d done! Time for a break!*** \n', b);
        DrawFormattedText(win, 'Block done! Time for a break!', ...
            'center', midY-90, white); 
        Screen('Flip',win);
        disp(change_stimamp);
        while ~or(strcmp(change_stimamp, 'y'), strcmp(change_stimamp, 'n'))
            change_stimamp = input('New stimamp for next block? (y or n) \n');
            disp(change_stimamp);
        end
        if change_stimamp == 'y'
            stimamp = input('What is the new stimamp?\n');
            if isnumeric(stimamp)
                fprintf('stimamp set to %.2f \n', stimamp);
            else
                while ~ isnumeric(stimamp)
                    fprintf('Sorry, numerical value expected \n');
                    stimamp = input('Please enter the new stimamp: \n');
                end
            end
        end
        change_stimamp = '';
        disp ('Press any key when you are ready!!');
        DrawFormattedText(win, 'Press any key when you are ready!', ...
            'center',midY-90, white);
        Screen('Flip',win);
        pause;
    end
end

DrawFormattedText(win, 'End of Experiment', ...
    'center', midY-90, white);
Screen('Flip',win);
fprintf('\n End of the Experiment\n');
WaitSecs(3);
sca
ppdev_mex('Close', 1);
PsychPortAudio('Close');
end
