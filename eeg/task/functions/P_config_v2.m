function P = P_config(subnum)

P.subnum = subnum;

%% keyboard stuff
KbName('UnifyKeyNames');
P.index = [8]; % 13 use [keyboardIndices, productNames, allInfos] = GetKeyboardIndices!!!! 
PsychHID('KbQueueCreate', P.index);%keys

%% base stim
P.samplingfreq  = 48000;    % sampling frequency (change to 48000 for linux)
P.stimfreq      = 220;      % frequeny for tactile stimulator/Haptuator
P.respfreq      = 440;      % cue to respond
P.stimdur       = 0.1;      % duration for tacti3le stimulator/Haptuator
P.dimsound      = 0.01;     % dim the sound intensity in case vibrator is plugged in
P.dimresp       = 1;
P.basestim      = repmat(MakeBeep(P.stimfreq,P.stimdur,P.samplingfreq).*P.dimsound,1,1);
P.basestim      = [P.basestim;P.basestim.*0];
P.respstim      = repmat(MakeBeep(P.respfreq,P.stimdur,P.samplingfreq).*P.dimresp,1,1);
P.respstim      = [P.respstim;P.respstim.*0];


%% temporal property
P.waitstim      = 1;        % allows 2s for subjects to press after stimonset

%% savePath
P.savePath = [pwd filesep 'data' filesep num2str(P.subnum)];
if ~isdir(P.savePath), mkdir(P.savePath); end

%% Initialize Sound driver for tactile stimulation
InitializePsychSound(1);
P.pahandle = PsychPortAudio('Open', [], [1], 0,P.samplingfreq,2);

       
%% Triggers
P.Triggers      = ''; % Whether to use serial triggers
P.TriggerDuty   = 0.01;   % The length of a trigger pules
P.TriggerPort   = '/dev/ttyUSB0'; % The serial port to use ttyACM0
P.BaudRate      = 115200; % The baud rate (typically 115200, otherwise too slow)

%% Trigger values
P.TRIGGER_BLOCK = 2;
P.TRIGGER_STIMSTART= 4;
P.TRIGGER_STIMEND= 8;
P.TRIGGER_HIT1 = 16;
P.TRIGGER_HIT2 = 32;
P.TRIGGER_HIT3 = 64;
P.TRIGGER_MISS = 128;
P.TRIGGER_FA1 = 8;
P.TRIGGER_FA2 = 9;
P.TRIGGER_FA3 = 10;
P.TRIGGER_PAUSE = 11;
end