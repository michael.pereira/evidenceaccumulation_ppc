% draw the VAS in PTB
function drawVAS_ptb_TOJ(VASrange)

global win
global midX midY
global mid_scale

lineWidth = 2;
yup       = 10; % coefficient to change length of horizontal lines
VASlength = range(VASrange);
mid_scale = midY - 90;

% Hide cursor in screen
HideCursor();

stringTop  = 'Certainly right';
stringBottom = 'Certainly wrong';
stringMiddle = 'Just guessing';

%     vertLines_y = midY + VASlength/2 * ...
%         [-1.1 -1.1 -1 -1 -.75 -.75 -.5 -.5 -.25 -.25 0 0 .25 .25 .5 .5 .75 .75];
vertLines_y = mid_scale + VASlength/2 * [-1.06 -1.06 -0.04 -0.04 1.01 1.01];
% positive numbers are at the bottom of the scale
vertLines_x = midX + yup * repmat([1 -1], 1, length(unique(vertLines_y)));
middleVerticals = find(vertLines_y == mid_scale + VASlength/2*-0.04);
vertLines_x(middleVerticals(1)) = midX + yup * 2; % make middle line a bit longer
vertLines_x(middleVerticals(2)) = midX + yup * -2;  

horizLine_y = [VASrange(1)-7, VASrange(2)];
horizLine_x = midX + [0 0];   %this is a little bit ridiculous but will depend on the size of the * used as a cursor

% Draw scale
% draw horizontal lines of scale
Screen('DrawLines', win, [vertLines_x; vertLines_y], lineWidth);
% draw the backbone of the scale
Screen('DrawLines', win, [horizLine_x; horizLine_y], lineWidth);


% Write text next to scale
Screen('TextSize', win, 32); 
DrawFormattedText(win, stringTop,  midX+30, VASrange(1), [0 130 0]); % green
DrawFormattedText(win, stringMiddle,  midX+30, mid_scale+8, [255 255 255]); % yellow: [20 230 0] 
DrawFormattedText(win, stringBottom, midX+30, VASrange(2), [255 0 0]); % red

end

