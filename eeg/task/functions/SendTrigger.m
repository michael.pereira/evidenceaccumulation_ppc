
function SendTrigger(triggertype,trig)
t = timer;
t.StartDelay = 0;
if strcmp(triggertype,'parallel')
    t.TimerFcn = {@TriggerTimer_parallel,trig};
else
    t.TimerFcn = {@TriggerTimer_parallel_dummy,trig};
end
t.ExecutionMode = 'singleShot';
start(t);
end