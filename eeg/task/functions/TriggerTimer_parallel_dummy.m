%% send triggers
function sendtrigger(mTimer,mEvent,trig)
date = datestr(mEvent.Data.time,'dd-mmm-yyyy HH:MM:SS.FFF');
fprintf('\n ** %s: fake trigger %d \n',date,trig);
stop(mTimer);
end