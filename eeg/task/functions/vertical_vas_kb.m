function [P] = vertical_vas_kb(P, i)

global win
global midY midX
global mid_scale

% VASrange    = [midY-150, midY+150];
VASrange    = [midY-240, midY+60];
start_y = (VASrange(1) + VASrange(2)) /2; % initialize at 50% confidence
initial_y = round(start_y);

P.T(i).mousePosOrig = initial_y ;

% Hide cursor in screen
HideCursor();

%Intialize
speed = 3;             % number of pixels moved per key press
P.T(i).onset = GetSecs;
current_y = initial_y; % initialize current y value with initial y value
counter = 1;
trail_y = [];
movementBegun   = 0;

tic;
while 1
    keys=zeros(256,1);
    keys(KbName({'8','5', 'Return'}))=1;
    RestrictKeysForKbCheck(find(keys));
    
    [keyIsDown, seconds, keyCode] = KbCheck;
    keypressed = KbName(keyCode);
    
    if strcmp(keypressed, '8')      % move up
        current_y=current_y-speed;
    elseif strcmp(keypressed, '5')  % move down
        current_y=current_y+speed;
    end
    
    %update display
    draw_vertical_vas(VASrange);     %draw the scale
    
    % set spatial limits to the scale
    if current_y > VASrange(2)
        current_y = VASrange(2);
    elseif current_y < VASrange(1)
        current_y = VASrange(1);
    end
    
    
    Screen('TextSize', win, 80);    %draw the cursor
    DrawFormattedText(win, '.', midX-11, current_y, [20 230 0]);
    
    conftmp=(VASrange(2) - current_y)/range(VASrange);
    Screen('TextSize', win, 32);
    DrawFormattedText(win, sprintf('%d%%',round(conftmp*100)), ...
        midX-115, mid_scale+10, [255 255 255]);
    
    Screen('Flip',win); % display
    
    trail_y(counter)        = current_y; % save Y trajectory
    moveTime(counter)       = GetSecs - P.T(i).onset;
    counter                 = counter + 1;
    
    %% save responses
    if strcmp(keypressed, 'Return') % Enter to validate choice
        SendTrigger(P.triggertype,P.trigresp2)
        
        P.T(i).confidence        = conftmp;
        P.T(i).conf_absolute     = current_y;
        P.T(i).rt_conf           = GetSecs - P.T(i).onset;
        P.T(i).moveTime          = moveTime;
        P.T(i).trail_y           = trail_y;
        
        disp('second response done')
        break
    end
    

end

Screen('Flip',win);

end
