function threshold_estimator_EEG(subnum, type)
%  based on threshold_estimator_v2
clearvars -except subnum type

global handle
global P
commandwindow;
try
    IOPort('Close', handle);
catch
end
addpath('./functions');

try PsychPortAudio('Close')
catch
end

%% load stim configuration
P = P_config_EEG(subnum); % config file found in ./functions
if strcmp(P.triggertype,'parallel')
    addpath('/home/eeg/Documents/perithreshold_experiment/ppdev-mex')
    ppdev_mex('CloseAll');
    ppdev_mex('Open', 1);
end



%% specific configuration for threshold estimator
P.stim_vector       = 0.1:0.02:2.5;     % Generate vector to modulate base stim
P.rand              = 2;                % width of uniform distribution for stim onset
if strcmp(type, 'down'), P.stim_vector = fliplr(P.stim_vector); end
filename = ['ThresholdEstimator_suj' int2str(subnum) '_' type '_' [datestr(now,'yy.mm.dd') '_' datestr(now,'hh.MM')] '.mat'];

%% screen settings
global win
global midY midX

% Switch to command window
commandwindow;

% Get the screen numbers
screens = Screen('Screens');

% Draw to the external screen if avaliable
screenNumber = max(screens);

% Define colors
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
green = [0 130 0];

% Open an on screen window
[win, windowRect] = PsychImaging('OpenWindow', screenNumber, black, [0 0 1920 1200]); %
% [win, windowRect] = PsychImaging('OpenWindow', screenNumber, black, [0 0 1200 700]);

% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', win);

% Set up alpha-blending for smooth (anti-a liased) lines
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% Setup the text type for the window
Screen('TextFont', win, 'Arial');
Screen('TextSize', win, 36);

% Get the centre coordinate of the window
[midX, midY] = RectCenter(windowRect);

% Here we set the size of the arms of our fixation cross
fixCrossDimPix = 60;

% Now we set the coordinates (these are all relative to zero we will let
% the drawing routine center the cross in the center of our monitor for us)
xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
allCoords = [xCoords; yCoords];

% Set the line width for our fixation cross
lineWidthPix = 6;

DrawFormattedText(win, 'Press any key when you are ready!', ...
    'center', midY-70, white);
Screen('Flip',win);
RestrictKeysForKbCheck([]);
KbWait([]);

%% start experiment
P.startExperiment = tic;
saveTimes = 1;
P.thresholdEst=0;
RestrictKeysForKbCheck();
PsychHID('KbQueueStart',P.index);

SendTrigger(P.triggertype,P.start_TE);
for  i=1:length(P.stim_vector)
    
    PsychHID('KbQueueStart',P.index);
    Screen('DrawLines', win, allCoords,...
        lineWidthPix, green, [midX midY-70], 2);   % draw green fixation cross
    Screen('Flip', win);
    
    if i == 1
        WaitSecs(2);  % wait 2s before experiment begins
    end
    
    fprintf('\n-----------------------\n');
    fprintf(['Now running trial: ' num2str(i)]);
    P.T(i).sub = subnum;                    % save subnum 
    
    P.T(i).trial = i;                       % save trial number
    P.T(i).stimamp = P.stim_vector(i);      % save stimulus amplitude
    
    %% stimulation and send trigger for stim onset
    P.T(i).stimonset = P.rand*rand(1); % stim onset
    PsychPortAudio('fillBuffer',P.pahandle,P.basestim*P.stim_vector(i)); % prepare stim
    WaitSecs(P.T(i).stimonset);
    SendTrigger(P.triggertype,P.trigonset_TE)
    
    P.T(i).startSound = PsychPortAudio('Start',P.pahandle,1,[],1);  % start stim
    [~,~,~,P.T(i).stopSound] = PsychPortAudio('Stop',P.pahandle,1); % stop stim
    
    disp(P.stim_vector(i))
    
    
    %% give time for subjects to respond
    P.T(i).waitresp = P.waitstim + (P.rand-P.T(i).stimonset);
    WaitSecs(P.T(i).waitresp);
    
    
    [keyIsDown, firstPress, firstRelease] = PsychHID('KbQueueCheck', P.index);
    
    switch type
        case 'up'
            %Write into result file
            if keyIsDown == 1
                fprintf ('stim detected %d', saveTimes);             
                P.T(i).key = KbName(find(firstPress,1));      % Key pressed
                P.T(i).RT = firstPress(find(firstPress,1)) - P.T(i).startSound; % RT 
                if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                    P.T(i).seq = 1;    % to log consecutive trials
                    SendTrigger(P.triggertype,P.trighit_TE)
                end
                
                if saveTimes >= 3
                    % quit when three consecutive stimuli were felt
                    if P.T(i).seq & P.T(i - 1).seq & P.T(i - 2).seq
                        P.thresholdEst = P.stim_vector(i-2); % the last stimulus felt
                        break
                    end
                end
                saveTimes = saveTimes + 1;
            elseif keyIsDown == 0
                P.T(i).seq = 0;
                P.T(i).key = 'none'; % Key pressed
                P.T(i).RT = 0; % RT
                SendTrigger(P.triggertype,P.trigmiss_TE)
            end
        case 'down'
            if keyIsDown == 0   % stimulus detected
                fprintf ('no keypress detected - stim detected');
                SendTrigger(P.triggertype,P.trigmiss_TE)
                P.T(i).key = 'none'; % Key pressed
                P.T(i).RT = 0; % RT 
                
            elseif keyIsDown == 1 % stimulus NOT detected
                fprintf ('stim missed %d', saveTimes)
                P.T(i).key = KbName(find(firstPress,1)); % Key pressed
                P.T(i).RT = firstPress(find(firstPress,1)) - P.T(i).startSound; % RT
                
                if or(strcmp(P.T(i).key, '1'), strcmp(P.T(i).key, 'a')) % weak
                    SendTrigger(P.triggertype,P.trighit_TE)
                    % quit when a stimulus was NOT felt
                    P.thresholdEst = P.stim_vector(i);
                    break
                end 
            end
    end
    PsychHID('KbQueueFlush', P.index);
    save([P.savePath filesep filename], 'P');
    
end

DrawFormattedText(win, 'End of Experiment', ...
    'center', midY-70, white);
Screen('Flip',win);
fprintf('\n End of the Experiment\n');
fprintf(' Threshold: %.3f\n',P.thresholdEst);
WaitSecs(3);
sca;
PsychPortAudio('Close');

end
