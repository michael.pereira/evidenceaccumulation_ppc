%========================================================
%
%   Model comparison
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
%
%   This script compare models
%
addpath ../conf
addpath functions
clear

config_model;


orig = load(['save/pop_max' opts.confsuffix '_rt.mat']);
resp = load(['save/pop_resp' opts.confsuffix '_rt.mat']);


ci = @(x) std(x)./sqrt(length(x));

llresp(1,:) = sum(orig.pop.llresp);
llresp(2,:) = sum(resp.pop.llresp);

llconf(1,:) = sum(orig.pop.llconf);
llconf(2,:) = sum(resp.pop.llconf);

llerp(1,:) = sum(orig.pop.llerp);
llerp(2,:) = sum(resp.pop.llerp);

bic(1,:) = (orig.pop.bicconf);
bic(2,:) = (resp.pop.bicconf);


fprintf('====================\n');
mysignrank(bic(1,:),bic(2,:),{'conf BIC max','conf BIC resp'});

%%
figure, hold on;

bar(mean(bic,2))
errorbar(mean(bic.'),ci(bic.'),'k.','LineWidth',2);
plot(bsxfun(@plus,[1.2,2.2],0.05*randn(size(bic.'))),bic.','ko','MarkerSize',4);

set(gcf,'Position',[200,200,200,200])
set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:200:800,'YTickLabel',{});
ylim([0,800])
xlim([0.2,2.8])

% save data 
tbl = table(bic(1,:).',bic(2,:).','VariableNames',{'bic_1','bic_2'});
writetable(tbl,'../../supdata/Fig.4h.xls');

%%
for m=1:2
    if m==1
        pop = orig.pop;
    else
        pop = resp.pop;
    end
    for s=1:18
        [cerp(s,m),perp(s,m)] = corr([pop.erp_miss(:,s) ; pop.erp_hit(:,s)],[pop.simerp_miss(:,s) ; pop.simerp_hit(:,s)],'rows','complete','type','Spearman');
        [cerp_hit(s,m),perp_hit(s,m)] = corr(pop.erp_hit(:,s), pop.simerp_hit(:,s),'rows','complete','type','Spearman');
        [cerp_miss(s,m),perp_miss(s,m)] = corr(pop.erp_miss(:,s),pop.simerp_miss(:,s),'rows','complete','type','Spearman');
        
        [chit(s,m),phit(s,m)] = corr(pop.hconfhit(:,s),pop.hsimconfhit(:,s),'rows','complete','type','Spearman');
        [cmiss(s,m),pmiss(s,m)] = corr(pop.hconfmiss(:,s),pop.hsimconfmiss(:,s),'rows','complete','type','Spearman');
        [ccrej(s,m),pcrej(s,m)] = corr(pop.hconfcrej(:,s),pop.hsimconfcrej(:,s),'rows','complete','type','Spearman');
        [cfa(s,m),pfa(s,m)] = corr(pop.hconffa(:,s),pop.hsimconffa(:,s),'rows','complete','type','Spearman');
        
    end
    
    fprintf('====================\n');
    if m==1
        fprintf('= Maximal evidence \n');
    else
        fprintf('= Alternative model \n');
    end
    fprintf('====================\n');
    
    fprintf('ERP fit: R = %.2f ± %.2f\n',mean(cerp(:,m)),ci(cerp(:,m)));
    fprintf('ERP fit for hits: R = %.2f ± %.2f\n',mean(cerp_hit(:,m)),ci(cerp_hit(:,m)));
    fprintf('ERP fit for misses: R = %.2f ± %.2f\n',mean(cerp_miss(:,m)),ci(cerp_miss(:,m)));
    fprintf('Conf (hit) fit: R = %.2f ± %.2f\n',mean(chit(:,m)),ci(chit(:,m)));
    fprintf('Conf (miss) fit: R = %.2f ± %.2f\n',mean(cmiss(:,m)),ci(cmiss(:,m)));
    fprintf('Conf (crej) fit: R = %.2f ± %.2f\n',mean(ccrej(:,m)),ci(ccrej(:,m)));
    fprintf('Conf (fa) fit: R = %.2f ± %.2f\n',nanmean(cfa(:,m)),ci(cfa(:,m)));
end
mysignrank(chit(:,1),chit(:,2),{'conf BIC max','conf BIC resp'});
%%
