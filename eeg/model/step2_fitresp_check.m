%========================================================
%
%   Check model fit
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

clear all
addpath ../conf/
addpath functions
config_model;
addpath(opts.dir.toolbox_misc);

initparams = paramset();

recompute = 0;

if ~exist('save','dir')
    mkdir('save');
end
for s = [1:6,10:16]%1:length(opts.subjects)
    %%
    
    subj = opts.subjects{s};
    matfile = ['save/' subj '.mat'];
    if recompute || ~exist(matfile,'file')
        data = loadsubjects_eeg(opts,subj);
        save(['save/' subj '.mat'],'data');
    else
        load(['save/' subj '.mat'],'data');
    end
    modelfile = ['models/' subj '_fitresp_' opts.type '.mat'];
    fprintf(' [ stage 3 ] loading parameters from %s\n',modelfile);
    load(modelfile,'params','stat','out','allparams','allstat','allout');
    
    %%
    loglik = [allout.ll];
    [srt,isrt] = sort(loglik);
    for i = 1:10
        j = isrt(i);
        p = allparams(j);
        fprintf(' [ stage 3 ] %s) ll: %.2f -> (B=%.0f, v=%.4f, ndt=%.3f, vvar=%.3f, decay=%.3f, lambda=%.3f\n',...
            subj,loglik(j),p.bound.val, p.drift.val, p.ndt.val, p.driftvar.val, p.decay.val, p.lambda.val);
    end
    title(subj);
    pause(0.1);
end