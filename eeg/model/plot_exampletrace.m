%========================================================
%
%   Check model fit
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

clear all
addpath ../conf/
addpath functions
config_model;
%opts.N = 500;
rng('default');
%opts.N = 10;
addpath(opts.dir.toolbox_misc);


opts.confreadout = 'max';
opts.confsuffix = '';  origparams = 0;
%opts.confsuffix = '_lowleak';origparams = -1;
%opts.confsuffix = '_highleak';origparams = 1;


initparams = paramset();

recompute = 0;

ci = @(x) std(x)./sqrt(length(x));
t = linspace(-0.5,1.5,2000);
t2 = linspace(0,4,4000);

if ~exist('data','dir')
    mkdir('data');
end
%%
s = 2
tic;
subj = opts.subjects{s};



fmodel1 = ['models/' subj '_fitresp_' opts.type '.mat'];
fmodel2 = ['models/' subj '_fitconf_' opts.type '_' opts.confreadout opts.confsuffix '.mat'];

fprintf(' [ stage 5 ] loading %s\n',fmodel1);
load(fmodel1,'allparams','params','allstat','stat','allout','out');
fprintf(' [ stage 5 ] loading %s\n',fmodel2);
load(fmodel2,'params2','stat2','out2');

fprintf(' [ stage 5 ] %d/%d simulating for %s\n',s,length(opts.subjects),opts.subjects{s});
if origparams == 1
    lambda = [allparams.lambda];
    sel0 = find([lambda.val] > 5);
    if length(sel0) < 10
        sel0 = find([lambda.val] > 6);
    end
    
    params = allparams(sel0(out2.iminll));
    out = allout(sel0(out2.iminll));
    stat = allstat(sel0(out2.iminll));
elseif origparams == -1
    lambda = [allparams.lambda];
    sel0 = find([lambda.val] < 1);
    if length(sel0) < 10
        sel0 = find([lambda.val] < 2);
    end
    
    params = allparams(sel0(out2.iminll));
    out = allout(sel0(out2.iminll));
    stat = allstat(sel0(out2.iminll));
end
pop.params(:,s) = [params.bound.val params.drift.val params.driftvar.val ...
    params.lambda.val params.decay.val params.ndt.val ...
    params2.sensitivity.val params2.bias.val params2.readout.val params2.mu.val];

pop.llresp(:,s) = [out.ll1 out.ll2];
pop.llerp(:,s) = [out.ll3 out.ll4];
pop.llconf(:,s) = [out2.ll1 out2.ll2 out2.ll3];
%%

ton = 1.2;
stimon = ton*ones(opts.N,1);
nsimcatch = round(opts.N*0.2);
stimon(1:nsimcatch) = 0;
rng('default');
opts.N = 1000;
params.driftvar.val = 10;
[sim,dv,dvstim] = simconf(stimon,params,params2,opts);


t = linspace(0,3,3000);
hit = find(sim.idxhit);
miss = find(sim.idxmiss);
i = 0;
%%
figure(100); clf; hold on;

i = 1;
tdec = 0:opts.dt/1:1;
vshape = exp(-((tdec-0.1)./params.decay.val).^2);
vshape(1:round(0.1/opts.dt)) = 1;
drift = zeros(size(t));
drift(round(ton./opts.dt)) = 1;
d = 2*conv(drift,vshape); %d(1:500) = [];

ndt = params.ndt.val;
d2 = t*0;
d2(t>ton+ndt & t < ton+ndt+0.1) = 1;
dec = t >= ton+ndt+0.1;
d2(dec) = exp(-((t(dec)-ton-ndt-0.1)./params.decay.val).^2);
%%
plot(t-ton,dv(1:3000,hit(i)),'Color',opts.color.hit,'LineWidth',2);
plot(t-ton,dv(1:3000,miss(i)),'Color',opts.color.miss,'LineWidth',2);
plot([0,3]-ton,params.bound.val*[1 1],'k-.','LineWidth',2);
plot(t-ton+params.ndt.val,d(1:length(t)),'Color','k','LineWidth',4);
plot(t-ton,d2,'Color','r','LineWidth',2);

%plot([0,0],ylim(),'k--');
xlim([-ton,3-ton])
set(gca,'XTick',-0.5:2,'XTickLabel',{},'YTickLabel',{});
set(gcf,'Position',[10,700,400,250])