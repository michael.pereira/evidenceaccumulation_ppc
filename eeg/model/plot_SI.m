%========================================================
%
%   Supplementary Figure 
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
%
% This script plots model fits for every subject
addpath ../conf
addpath functions
config_model

confreadout = 'max';
if ~exist('pop','var')
    load(['save/pop_' confreadout opts.confsuffix '_rt.mat']);
end

nsuj = length(pop.hr);
b = 100;
t = linspace(-0.5,1.5,2000);
figure(101+b); clf; 
figure(102+b); clf; 
figure(103+b); clf; 
ms = 8;
lw = 1;
lw2 = 1;

% save data


tbl = table(pop.hr.',pop.far.',pop.simhr.',pop.simfar.',...
    'VariableNames',{'obsHR','obsFAR','simHR','simFAR'});
writetable(tbl,'../../supdata/SFig.6.xls');

tbl = table(pop.confhit.',pop.confmiss.',pop.confcrej.',pop.conffa.',...
    pop.simconfhit.',pop.simconfmiss.',pop.simconfcrej.',pop.simconffa.',... 
    'VariableNames',{'obsconfhit','obsconfmiss','obsconfcrej','obsconffa','simconfhit','simconfmiss','simconfcrej','simconffa'});
writetable(tbl,'../../supdata/SFig.7.xls');

for s=1:nsuj
    figure(101+b); subplot(4,5,s); hold on;
    plot(1,pop.hr(s),'ko','MarkerSize',ms','LineWidth',lw);
    plot(2,pop.far(s),'ko','MarkerSize',ms','LineWidth',lw);
    plot(1,pop.simhr(s),'kx','MarkerSize',ms','LineWidth',lw);
    plot(2,pop.simfar(s),'kx','MarkerSize',ms','LineWidth',lw);
    plot([0.5,2.5],[0.5,0.5],'k--');
    xlim([0.5,2.5]);
    ylabel('P(yes)');
    ylim([0,1])
    set(gca,'XTick',1:2,'XTickLabel',{'HR','FAR'},'YTick',0:0.2:1);
    title(opts.subjects{s});
    %set(gcf,'Position',[100,100,900,700])

    figure(102+b); subplot(4,5,s); hold on;
    plot(1,pop.confhit(s),'ko','MarkerSize',ms','LineWidth',lw,'Color',opts.color.hit);
    plot(2,pop.confmiss(s),'ko','MarkerSize',ms','LineWidth',lw,'Color',opts.color.miss);
    plot(3,pop.confcrej(s),'ko','MarkerSize',ms','LineWidth',lw,'Color',opts.color.crej);
    plot(4,pop.conffa(s),'ko','MarkerSize',ms','LineWidth',lw,'Color',opts.color.fa);

    plot(1,pop.simconfhit(s),'kx','MarkerSize',ms','LineWidth',lw,'Color',opts.color.hit);
    plot(2,pop.simconfmiss(s),'kx','MarkerSize',ms','LineWidth',lw,'Color',opts.color.miss);
    plot(3,pop.simconfcrej(s),'kx','MarkerSize',ms','LineWidth',lw,'Color',opts.color.crej);
    plot(4,pop.simconffa(s),'kx','MarkerSize',ms','LineWidth',lw,'Color',opts.color.fa);
    xlim([0.5,4.5])
    ylim([0,1]) 
    ylabel('Confidence');
    set(gca,'XTick',1:4,'XTickLabel',{'H','M','CR','FA'},'YTick',0:0.2:1);
    title(opts.subjects{s});
    %set(gcf,'Position',[100,100,900,700])

    figure(103+b); subplot(4,5,s); hold on;
    plot(t,pop.erp_hit(:,s),'k','Color',0.5*[1 1 1],'LineWidth',lw2);
    plot(t,pop.erp_miss(:,s),'k','Color',0.5*[1 1 1],'LineWidth',lw2);
    plot(t,pop.simerp_hit(:,s),'k','Color',opts.color.hit,'LineWidth',lw2);
    plot(t,pop.simerp_miss(:,s),'k','Color',opts.color.miss,'LineWidth',lw2);
    tbl = table(t.',pop.erp_hit(:,s),pop.erp_miss(:,s),pop.simerp_hit(:,s),pop.simerp_miss(:,s),...
        'VariableNames',{'t','erphit','erpmiss','simhit','simmiss'});
    writetable(tbl,['Fig.5_' num2str(s) '.xls']);
    xlim([-0.5,1.5])
    %ylim([-0.5,2.5])
    plot([-0.5,1.5],[0 0],'k');
    plot([0 0],ylim(),'k--');
    set(gca,'XTick',-0.5:0.5:1.5,'YTick',0:0.5:2);  
    xlabel('Time [s]');
    ylabel('Amplitude [\mu{V}]');
    title(opts.subjects{s});
    %set(gcf,'Position',[100,100,900,700])
 tbl = table(t.',pop.erp_hit(:,s),pop.erp_miss(:,s),pop.simerp_hit(:,s),pop.simerp_miss(:,s),...
        'VariableNames',{'t','erphit','erpmiss','simhit','simmiss'});
    writetable(tbl,['../../supdata/SFig.5_' num2str(s) '.xls']);
end
figure(103+b);
print('figs/SFig5.eps','-depsc');
figure(101+b);
print('figs/SFig6.eps','-depsc');
figure(102+b);
print('figs/SFig7.eps','-depsc');

%
alpha = [];
figure(110+b); clf; hold on;
cishade(pop.erp_hit1(1:1990,:).',alpha,'k',t(1:1990),'LineWidth',2,'Color',0.5*[1 1 1]);
cishade(pop.erp_hit2(1:1990,:).',alpha,'k',t(1:1990),'LineWidth',2,'Color',0.5*[1 1 1]);
cishade(pop.erp_miss(1:1990,:).',alpha,'k',t(1:1990),'LineWidth',2,'Color',0.5*[1 1 1]);

plot(t,nanmean(pop.simerp_hit1,2),'LineWidth',2,'Color',opts.color.hit2);
plot(t,nanmean(pop.simerp_hit2,2),'LineWidth',2,'Color',opts.color.hit3);
plot(t,nanmean(pop.simerp_miss,2),'LineWidth',2,'Color',opts.color.miss);
 xlim([-0.5,1.5])
 plot([-0.5,1.5],[0 0],'k');
 plot([0 0],ylim(),'k--');
 set(gca,'XTick',-0.5:0.5:1.5,'YTick',-0.5:0.5:2,'XTickLabel',{},'YTickLabel',{});
 
%xlabel('Time from stimulus [s]');
%ylabel('Amplitude [\mu{V}]');
set(gcf,'Position',[500,400,300,200])