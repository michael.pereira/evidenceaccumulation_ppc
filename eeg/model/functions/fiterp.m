%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [ params,stat,out] = fiterp( stimon,resp, erp, params, opts, fit)
% fiterp fitting procedure for first-order model

if nargin < 6
    fit = 2;
end
stat = NaN;
out = [];
eval = 0;

idxhit = resp==1 & stimon > 0;
idxmiss = resp==0 & stimon > 0;
idxcrej = resp==0 & stimon == 0;
idxfa = resp==1 & stimon == 0;
hr = sum(resp==1 & stimon>0)/sum(stimon>0);
far = sum(resp==1 & stimon==0)/sum(stimon==0);

erp = bsxfun(@minus,erp,mean(erp(201:500,:)));
scale = 1e-3*mean(sum((erp(501:1700,:)),1));
erp = erp./scale;

erphit_mean = max(mean(erp(:,idxhit),2),0);
erphit_std = opts.t2llstd+0*std(erp(:,idxhit),[],2);

erpmiss_mean = max(mean(erp(:,idxmiss),2),0);
erpmiss_std = opts.t2llstd+0*std(erp(:,idxmiss),[],2);

t = linspace(-0.5,1.5,length(erphit_mean));

rng('default');
simstimon = zeros(opts.N,1);
simstimon(1:round(opts.N)*0.8) = 2*rand(round(opts.N)*0.8,1);
%% Find which parameters to fit
fields = {'bound','drift','lambda','scale','driftvar','noise','startvar','decay','rbound','ndt'};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params.(fields{f}).fit
        p = p+1;
        % inverse transform of parameters
        param_val(p) = params.(fields{f}).itrans(params.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

if fit>0
    %% Fitting procedure
    %rng('default');
    % Reset random number generator
    
    options = optimset('Display',opts.optimizationdisplay1,'MaxIter',opts.maxiter);%,'TolFun',opts.tolfun);%,'FinDiffRelStep',1e-2);
    [p, stat] = opts.fminfunc(@fitfunc,param_val,options);
else
    stat = fitfunc(param_val);
    return
end
%% Transform parameters back
for f=1:length(param_id)
    params.(param_id{f}).val = params.(param_id{f}).trans(p(f));
end

    function stat = fitfunc(p)
        
        
        
        % transform parameters back to "random-walk space"
        newparams = params;
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
        
        % simulate random walks and corresponding choice accuracy, rt
        [sim,~,dverp] = evacc(simstimon,newparams,opts);
        %%
        sim.stimon = simstimon;
        sim.idxhit = sim.resp==1 & sim.stimon > 0;
        sim.idxmiss = sim.resp==0 & sim.stimon > 0;
        sim.idxcrej = sim.resp==0 & sim.stimon == 0;
        sim.idxfa = sim.resp==1 & sim.stimon == 0;
        sim.hr = sum(sim.resp==1 & sim.stimon>0)/sum(sim.stimon>0);
        sim.far = sum(sim.resp==1 & sim.stimon==0)/sum(sim.stimon==0);
        
        %stat1 = -(log(binopdf(sum(sim.resp==1 & stimon>0),sum(stimon > 0),hr)+eps));
        %stat2 = -(log(binopdf(sum(sim.resp==1 & stimon==0),sum(stimon <= 0),far)+eps));
        stat1 = -log(normpdf(sim.hr,hr,opts.t1llstd)+eps);
        stat2 = -log(normpdf(sim.far,far,opts.t1llstd)+eps);
        
        
        dverp = bsxfun(@minus,dverp,mean(dverp(201:500,:)));
        dvscale = 1e-3*max(mean(sum((dverp(501:1700,:)),1)),0);
        dverp = dverp./dvscale;
        
        
        if sum(sim.idxhit)
            %stat3 = -mean(mean(log(eps+normpdf(dverp(501:end,sim.idxhit),erphit_mean(501:end),erphit_std(501:end)))));
            stat3 = -mean(log(eps+normpdf(mean(dverp(501:1700,sim.idxhit),2),erphit_mean(501:1700),erphit_std(501:1700))));
        else
            stat3 = -log(eps);
        end
        
        if sum(sim.idxmiss)
            %stat4 = -mean(mean(log(eps+normpdf(dverp(501:end,sim.idxmiss),erpmiss_mean(501:end),erpmiss_std(501:end)))));
            stat4 = -mean(log(eps+normpdf(mean(dverp(501:1700,sim.idxmiss),2),erpmiss_mean(501:1700),erpmiss_std(501:1700))));
        else
            stat4 = -log(eps);
        end
        if fit == 1
            stat = double(stat1+stat2);
        else
            stat = double(stat1+stat2+stat3+stat4);
        end
        % save current state
        out.ll = stat;
        out.ll1 = stat1;
        out.ll2 = stat2;
        out.ll3 = stat3;
        out.ll4 = stat4;
        out.sim = sim;
        out.params = params;
        
        eval = eval+1;
        %% plot some stuff
        if mod(eval,opts.doplot)==0
            %%
            
            out.h = figure(100); clf;
            subplot(121); hold on;
            plot([hr,far],'ko','LineWidth',2,'MarkerSize',10);
            plot([sim.hr,sim.far],'k+','LineWidth',2,'MarkerSize',10);
            xlim([0,3]);
            ylim([0,1])
            set(gca,'XTick',1:2,'XTickLabel',{'zHR','zFA'});
            title(sprintf('T1: %.2f / %.2f',stat1,stat2));
            
            
            subplot(122); hold on;
            hit = dverp(:,sim.idxhit);
            hit(:,any(isnan(hit))) = [];
            if size(hit,2) > 2
                stdshade(hit.',0.4,'c',t,'LineWidth',2);
            end
            miss = dverp(:,sim.idxmiss);
            miss(:,any(isnan(miss))) = [];
            if size(miss,2) > 2
                stdshade(miss.',0.4,'r',t,'LineWidth',2);
            end
            plot(xlim(),newparams.bound.val./dvscale*[1 1],'k--');
            
            plot(t,erphit_mean,'c--','LineWidth',2);
            plot(t,erpmiss_mean,'r--','LineWidth',2);
            
            title(sprintf('T2: %.2f / %.2f',stat3,stat4));
            
            pause(0.01);
        end
    end

end

