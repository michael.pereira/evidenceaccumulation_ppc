%========================================================
%
%   Simulate evidence accumulation (Euler)
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

function [ sim, dv, dvstim, dvresp,v] = evacc( stimon, params, opts )
%========================================================
%% Prepare parameters
%========================================================


% extract parameters
if isfield(params,'bound')
    B = params.bound.val;
else
    error('I need a boundary (bound)');
end
if isfield(params,'drift')
    drift = params.drift.val;
else
    error('I need a drift rate (drift)');
end
if isfield(params,'driftvar')
    driftvar = params.driftvar.val;
else
    driftvar = 0;
end
if isfield(params,'decay')
    decay = params.decay.val;
else
    decay = 0;
end
if isfield(params,'noise')
    sigma = params.noise.val;
else
    sigma = 0;
end
if isfield(params,'lambda')
    lambda = params.lambda.val;
else
    lambda = 0;
end
if isfield(params,'ndt')
    ndtfixed = params.ndt.val;
else
    ndtfixed = 0;
end
if isfield(params,'ndtvarperc')
    ndtstdperc = params.ndtvarperc.val;
else
    ndtstdperc = 0;
end
if isfield(params,'ndtmot')
    ndtmot = params.ndtmot.val;
else
    ndtmot = 0;
end
if isfield(params,'ndtvarmot')
    ndtstdmot = params.ndtvarmot.val;
else
    ndtstdmot = 0;
end
if isfield(params,'stimdur')
    stimdur = params.stimdur.val;
else
    stimdur = 0.1;
end
if isfield(params,'maxdur')
    maxdur = params.maxdur.val;
else
    maxdur = Inf;
end
if isfield(params,'start')
    zinit = params.start.val;
else
    zinit = 0;
end
if isfield(params,'startvar')
    zvar = params.startvar.val;
else
    zvar = 0;
end
if zvar > 0 && zinit > 0
    error('Cannot have both start and startvar');
end
if isfield(params,'rB')
    rB = params.rB.val;
else
    rB = 0;
end
dt = opts.dt;
L = opts.L;
K = round(L/dt);
N = size(stimon,1);

if nargout >= 3
    scale = params.scale.val;
end

sim.resp = nan(N,1);
sim.rawresp = nan(N,1);
sim.readout = nan(N,1);
sim.treadout = nan(N,1);

Ldecay = 1;
terp = [-0.5,1.5];
ierp = round(terp./dt);
ierp(2) = ierp(2) - 1;

terp2 = [-0.5,0.5];
ierp2 = round(terp2./dt);

dv = nan(round(L/dt),N);

if nargout >= 3
dvstim = zeros(diff(ierp)+1,N);
end
if nargout >=4
dvresp = zeros(diff(ierp2)+1,N);
end

drift = abs(drift + drift*driftvar*randn(1,N));

if ndtstdperc == 0
    ndt = ndtfixed*ones(1,N);
else
    ndt = ndtfixed + ndtstdperc*randn(1,N);
end
%t = linspace(0,Ldecay,Ldecay/dt);
t = 0:dt/Ldecay:Ldecay;
if decay ~= 0
    % generate decaying drift
    vshape = exp(-((t-0.1)./decay).^2);
    vshape(1:round(stimdur/dt)) = 1;
    mx = (round(maxdur/dt)+1);
    if mx < length(vshape)
        vshape(mx:end) = 0;
    end
else
    vshape = t.*0;
    vshape(1:round(0.1/dt)) = 1;
    vshape(1:round(stimdur/dt)) = 1;
    mx = (round(maxdur/dt)+1);
    if mx < length(vshape)
        vshape(mx:end) = 0;
    end
end
vshape = vshape*lambda;
%vshape = vshape./sum(vshape);

if opts.doprint
    fprintf('B=%.2f, drift=%.3f, driftvar=%.3f, lambda=%.5f, decay=%.3f, ndt=%.2f\n',...
        B,params.drift.val, params.driftvar.val,lambda,params.decay.val,params.ndt.val);
end
v = zeros(N,K);
for i=1:N
    if (stimon(i)>0)
        x = round((stimon(i)+ndt(i))/dt);
        x = min(x,K);
        v(i,x) = drift(i);
    end
end
v(stimon>0,:) = filter(vshape,1,v(stimon>0,:),[],2);
samples = v+sigma*randn(N,K);

% define a random starting point between 0 and B*Zvar
if zvar > 0
    z = B*zvar*rand(N,1);
else
    z = zinit*ones(N,1);
end
for i=1:N
    
    %========================================================
    %% DRIFT DIFFUSION
    %========================================================
    
    % d.v. is the cumulative sum of samples
    %dv(:,i) = bsxfun(@plus,z,cumsum(samples));
    dv(1,i) = z(i);
    for j=2:L/dt
        dv(j,i) = max((1-lambda*dt)*dv(j-1,i) + samples(i,j-1),-rB);
    end
    %dv(:,i) = satcumsum(samples(:,i),dv(:,i),lambda,rB);
    
    %[~,iro_] = max(dv(:,i));
    ibnd = find(dv(1:3000,i) > B,1,'first') + round(ndtmot/dt+ndtstdmot/dt*randn(1));
    if ~isempty(ibnd)
        sim.rt(i) = ibnd.*dt - stimon(i);
    else
        sim.rt(i) = NaN;
    end
    sim.resp(i) = ~isempty(ibnd);
    
    istim = round(stimon(i)/dt);
    
    if nargout >= 3 && ~isnan(stimon(i)) && stimon(i)>0
        imin = istim+ierp(1);
        if (imin<=0)
            dvstim((-imin+2):end,i) = dv(1:(istim+ierp(2)),i)./scale;
        else
            dvstim(:,i) = dv(istim+(ierp(1):ierp(2)),i)./scale;
        end
    end
    if nargout >= 4 && ~isempty(ibnd)
        imin = ibnd+ierp2(1);
        if (imin<=0)
            dvresp((-imin+2):end,i) = dv(1:(ibnd+ierp2(2)),i)./scale;
        else
            dvresp(:,i) = dv(ibnd+(ierp2(1):ierp2(2)),i)./scale;
        end
    end
    
end

end

