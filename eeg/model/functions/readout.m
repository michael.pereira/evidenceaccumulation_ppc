%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [simconf,evidence,ro] = readout(sim,params,params2,dv,dvstim,dvresp,opts)
%readout simulate confidence based on evidence accumulation
%   
%   Inputs:
%   - sim: simulated responses
%   - params: first-order parameters
%   - params2: second-order parameters
%   - dv: decision variable time-locked to the cue
%   - dvstim: decision variable time-locked to the stim (NaN for catchtrials)
%   - dvresp: decision variable time-locked to the resp (NaN for "no" resp)
%
%   Outputs:
%   - simconf: simulated confidence
%   - evidence: readout, in percentage of the bound
%   - ro: raw confidence readout
%   
if strcmp(opts.confreadout,'max')
    % the readout is the maximal evidence across time
    ro = max(dv(1:3000,:),[],1);
end
if strcmp(opts.confreadout,'resp')
    % dvresp is -0.5 to 0.5 post-response
    % so we add the readout timing to t=0
    tro = (0.5+params2.readout.val)./opts.dt;
    % to help the model converge when fitting the readout time
    % we interpolate to compensate for discrete sampling
    itro = ceil(tro);
    alpha = itro - tro;
    itro = min(max(itro,2),1000);
    ro(sim.resp==1) = alpha*dvresp(itro,sim.resp==1) + (1-alpha)*dvresp(itro-1,sim.resp==1);

    % for "no" response, confidence is based on a noise
    mu = params2.mu.val;
    sigma = params2.sigma.val;
    ro(sim.resp==0) = mu+sigma*randn(1,sum(sim.resp==0));  
end

bnd = params.bound.val;
a = params2.sensitivity.val;
b = params2.bias.val;
evidence = (ro-bnd)./bnd;
% pass the evidence through a sigmoid to have 0 < confidence < 1
simconf = sigmoid( evidence.', a, b, sim.resp*2-1, opts.confbiastype);

if opts.doprint2
    fprintf('sensitivity=%.2f, bias=%.2f, readout=%.3f, mu=%.2f, sigma=%.2f, (bound=%.0f)\n',...
        params2.sensitivity.val,params2.bias.val,params2.readout.val,...
        params2.mu.val,params2.sigma.val, bnd);
end
end

