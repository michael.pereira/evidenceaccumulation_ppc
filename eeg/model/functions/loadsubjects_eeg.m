%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [dat,EEG] = loadsubjects_eeg(opts,selsubj,doplot,doclas)
%loadsubjects_eeg: loads EEG data
s = 0;
if nargin < 4
    doclas = 1;
end
if nargin < 3
    doplot = 1;
end
subjects = dir([opts.dir.bids '/sub*']);

ci = @(x,dim) std(x,[],dim)./sqrt(size(x,dim));

fprintf('Loading subjects: ');
% loop through subjects
for s_=1:length(subjects)
    %% skip the two subjects that were excluded due to exessive artifacts
    subj = subjects(s_).name;
    if ~strcmp(subj,selsubj)
        continue
    end
    
    s = s+1;
    %% load eegtoolbox only once
    if isempty(which('pop_loadset'))
        addpath(opts.dir.toolbox_eeglab);
        eeglab;
    end
    
    % load data
    EEG = pop_loadset([subj '_preproc_01hz.set'], ...
        [opts.dir.bids_deriv '/' subj '/ses-01/eeg/']);
    
    %%
    med = median([EEG.epoch(strcmp({EEG.epoch.sdt},'hit')).conf]);
    
    hit1 = strcmp({EEG.epoch.sdt},'hit') & [EEG.epoch.conf] < med;
    hit2 = strcmp({EEG.epoch.sdt},'hit') & [EEG.epoch.conf] >= med;
    miss = strcmp({EEG.epoch.sdt},'miss');
    
    selt = EEG.times>=500 & EEG.times<=1500;
    
    dat.erphit1 = mean(EEG.data(1:62,:,hit1),3).';
    dat.erphit2 = mean(EEG.data(1:62,:,hit2),3).';
    dat.erpmiss = mean(EEG.data(1:62,:,miss),3).';
    
    dat.erphit1_ci = ci(EEG.data(1:62,:,hit1),3).';
    dat.erphit2_ci = ci(EEG.data(1:62,:,hit2),3).';
    dat.erpmiss_ci = ci(EEG.data(1:62,:,miss),3).';
    dat.chanlocs = EEG.chanlocs;
    
    seltrial = ~strcmp({EEG.epoch.sdt},'n/a');
    
    selhitmiss_ = strcmp({EEG.epoch.sdt},'hit') | strcmp({EEG.epoch.sdt},'miss');
    selhitmiss = selhitmiss_(seltrial);
    
    dat.erpstat = squeeze(mean(EEG.data(1:62,selt,seltrial),2));
    dat.erpstat_resp = strcmp({EEG.epoch(seltrial).sdt},'hit');
    dat.erpstat_conf = [EEG.epoch(seltrial).conf];
    dat.erp_time = EEG.times;
    
    %dat.chanlocs = rEEG.chanlocs;
    % rest is selecting behavior
    dat.subj = subj;
    dat.sdt = {EEG.epoch(seltrial).sdt}.';
    
    dat.catch = strcmp(dat.sdt,'crej') | strcmp(dat.sdt,'fa');
    dat.resp = strcmp(dat.sdt,'hit') | strcmp(dat.sdt,'fa');
    
    dat.stimamp = [EEG.epoch(seltrial).stimamp].';
    dat.stimamp(dat.catch) = 0;
    dat.stimon = [EEG.epoch(seltrial).stimonset].';
    dat.stimon(dat.catch) = 0;
    
    dat.conf = [EEG.epoch(seltrial).conf].';
    dat.rt = [EEG.epoch(seltrial).rt].';
    dat.rtconf = [EEG.epoch(seltrial).rt_conf].';
    
    
    if doclas
        %% classification
        % resampling
        rEEG = pop_resample(EEG,50);
        
        % select data
        seltidx = rEEG.times>=-500 & rEEG.times<=1500;
        tidx = rEEG.times(seltidx);
        feat = rEEG.data(1:62,seltidx,selhitmiss);
        label = {rEEG.epoch(selhitmiss).sdt};
        
        [nel,nt,ntr] = size(feat);
        ntest = round(ntr*0.1);
        
        rng('default'); % for reproducibility
        % repeat 10 x 10 fold xval for every time point (fs=50hz)
        cverr = nan(nt,opts.nrep);
        fprintf('Xval: ');
        for rep = 1:opts.nrep
            fprintf('[ %d ]',rep);
            cp = cvpartition(label,'k',opts.nfold);
            for t=1:nt
                % miss-classification rate xval
                X = squeeze(feat(:,t,:)).';
                y = strcmp(label.','hit');
                cverr(t,rep)  = crossval('mcr',X,y,'predfun',@classif,'partition',cp);
            end
        end
        % take time point corresponding to peak performance of the decoder
        dat.mcr = 1-cverr;
        [dat.best_mcr,ibest] = max(mean(dat.mcr,2));
        
        % refit decoder on all data at this time point
        Mdl = fitcdiscr(squeeze(feat(:,ibest,:)).',strcmp(label.','hit'),...
            'SaveMemory','off','FillCoeffs','on',...
            'Prior','uniform','ScoreTransform','logit', ...
            'DiscrimType','pseudolinear','Gamma',0.5);
        
        % classifier predictions
        allfeat = rEEG.data(1:62,:,seltrial);
        dat.weights = Mdl.Coeffs(2).Linear;
        dat.weights_t = tidx(ibest)*1e-3;
        
        pEA = squeeze(sum(bsxfun(@times,allfeat,dat.weights),1) + Mdl.Coeffs(2).Const);
        
        % upsample
        dat.pEA_allwin = resample(double(pEA),20,1);
        
        % take window of interest
        t = linspace(min(EEG.times)*1e-3,max(EEG.times)*1e-3,size(dat.pEA_allwin,1));
        sel = find(t>=-0.5 & t<=1.5);
        sel = [sel(1)-1 sel];
        dat.pEA = dat.pEA_allwin(sel,:);
        
        fprintf('\n');
        
        
        if doplot
            %%
            h = figure();
            subplot(121); hold on;
            erp = bsxfun(@minus,dat.pEA,mean(dat.pEA(201:500,:)));
            cishade(erp(:,dat.resp & ~dat.catch).',0.4,'c',t(sel));
            cishade(erp(:,~dat.resp & ~dat.catch).',0.4,'r',t(sel));
            title(sprintf('Best acc: %.2f  @ %.2f s', dat.best_mcr , dat.weights_t))
            xlim([-0.5,1.5]);
            
            subplot(122);
            topoplot(Mdl.Coeffs(2).Linear,EEG.chanlocs)
            title(dat.subj)
            
            saveas(h,['figs/' subj '_data.png']);
            
        end
    end
end