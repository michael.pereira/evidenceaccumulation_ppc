%========================================================
%   sigmoid.m passes a value through a sigmoid function, adjusting bias (b) and
%   sensitivity (a) and optionally inverts confidence for all w==-1
%
%   Pereira, Faivre, Iturrate et al., 2019, 
%   Disentangling the origins of confidence in speeded 
%      perceptual judgments through multimodal imaging
%
%   preregistration: https://osf.io/a5qmv/  
%   gitlab: https://gitlab.com/meta-eegfmri/  
%      
%   Michael Pereira <michael.pereira@epfl.ch>
%   25/09/2018
%========================================================
function [ simconf, reg ] = sigmoid( dv, a,b, w, biastype)
%   sigmoid computes confidence based on the readout and some parameters
%
%   Inputs:
%   - dv: decision variable from the readout
%   - a: slope
%   - b: bias
%   - w: weight [-1,1] (to condition confidence to a response)
%   - biastype: evidence or conf, whether the bias is before or after
%   conditioning to the response
% 
%   Outputs: 
%   - simconf: simulated confidence
%
if nargin < 5 || isempty(biastype)
    biastype = 'conf';
end
if nargin < 4 || isempty(w)
    w = ones(size(dv));
end
if nargin < 3 || isempty(b)
    b = 0;
end
if nargin < 2 || isempty(a)
    a = 1;
end
%%
% the regression depends on the type of bias we use
if strcmp(biastype,'evidence')
    reg = a*dv - b;
    mult = bsxfun(@times,w,reg);
elseif strcmp(biastype,'conf')
    reg = a*dv;
    mult = bsxfun(@times,w,reg) + b;
else
    error('Unknown bias type');
end
% sigmoid
simconf = exp(mult)./(1+exp(mult));
simconf(isnan(simconf)) = 1;
end

