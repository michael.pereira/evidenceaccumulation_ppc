%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [ params,stat,out,allparams,allstat,allout] = fiterp_wrapper( stimon,resp, erp, params, opts)
% fiterp_wrapper
% This functions does multiple fits with different initial parameters

% set special options (to go faster)
opts2 = opts;
opts2.doplot = 0;
opts2.doprint = 0;
opts2.N = 500;

opts3 = opts;
opts3.N = 500;
opts3.maxiter = opts.maxiter_gridsearch;

ntot = length(params.drift.init)*length(params.driftvar.init)*length(params.decay.init)*length(params.lambda.init);
np = 0;
ngrid = length(params.bound.init)*length(params.ndt.init);%*length(params.drift.init);

% loop through init parameters
for decay = 1:length(params.decay.init)
    params.decay.val = params.decay.init(decay);
    for lambda = 1:length(params.lambda.init)
        params.lambda.val = params.lambda.init(lambda);
        for dvar = 1:length(params.driftvar.init)
            params.driftvar.val = params.driftvar.init(dvar);
            for d = 1:length(params.drift.init)
                params.drift.val = params.drift.init(d);
                tic;
                np = np+1;
                if isfield(opts,'current')
                    fprintf('%s) ',opts.current);
                end
                fprintf('%d/%d Starting grid search (N=%d) for driftvar=%.5f, ndt=%.3f, decay=%.1f, lambda=%.0f\n',np,ntot,ngrid,params.driftvar.val,params.ndt.val,params.decay.val,params.lambda.val);
                
                % we first find the best initial ndt and bound
                for b = 1:length(params.bound.init)
                    params.bound.val = params.bound.init(b);
                    for ndt = 1:length(params.ndt.init)
                        
                        [pinit(b,ndt),ll(b,ndt)] = fiterp( stimon, resp, erp, params, opts2,0);
                        
                    end
                end

                % then we fit with these parameters
                [~,iparam] = min(ll(:));
                pbest = pinit(iparam);
                
                [pbest,ll,out] = fiterp( stimon, resp, erp, pbest, opts3,2);
                
                % just plot final result
                % fiterp( stimon, resp, erp, pbest, opts3,0);
                
                allparams(d,dvar,decay,lambda) = pbest;
                allstat(d,dvar,decay,lambda) = ll;
                allout(d,dvar,decay,lambda) = out;
                
                et = toc;
                fprintf('\nLL: %.2f (elapsed: %.2f)\n',ll,et);
                
                
            end
        end
    end
end

[stat,imin] = min(allstat(:));
params = allparams(imin);
out = allout(imin);
end

