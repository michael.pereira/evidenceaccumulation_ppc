%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [ params2,stat2,out2,allparams2,allstat2,allout2] = fitconf_wrapper( data, params, params2, opts)
% fitconf_wrapper
% This functions does multiple fits with different initial parameters

if strcmp(opts.confreadout,'max')
    params2.mu.fit = 0;
    params2.sigma.fit = 0;
    params2.readout.fit = 0;
    rolist = 1;
    blist = length(params2.bias.init);
    %params2.bias.init = 0;
else
    rolist = 1:length(params2.readout.init);
    blist = length(params2.bias.init);
end

opts3 = opts;
opts3.N = 500;
opts3.maxiter = opts.maxiter_gridsearch;


%params2 = params;
ntot = length(params2.sensitivity.init)*length(params2.bias.init)*length(params2.readout.init);
np = 0;
for sens = 1:length(params2.sensitivity.init)
    params2.sensitivity.val = params2.sensitivity.init(sens);
    for bias = 1:blist
        params2.bias.val = params2.bias.init(bias);
        
        for ro = rolist
            params2.readout.val = params2.readout.init(ro);
            tic;
            np = np+1;
            fprintf(' init %d/%d with sensitivity=%.2f, bias=%.2f and ro=%.3f\n',...
                np,ntot,params2.sensitivity.val,params2.bias.val,params2.readout.val);
           
            [pbest,ll2,out2] = fitconf(data, params, params2, opts3,2);
            
            allparams2(sens,bias,ro) = pbest;
            allstat2(sens,bias,ro) = ll2;
            allout2(sens,bias,ro) = out2;
            
            et = toc;
            fprintf('LL: %.2f (elapsed: %.2f)\n',ll2,et);
        end
    end
end

[stat2,imin] = min(allstat2(:));
params2 = allparams2(imin);
out2 = allout2(imin);
end

