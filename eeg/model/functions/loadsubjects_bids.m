%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [tbl,stats] = loadsubjects_bids(opts,selsubj)
% loads eeg data
s = 0;
subjects = dir([opts.dir.bids '/sub*']);

stats = [];

dat = struct();
dat.subject = {};
dat.subjectid = [];
dat.stimamp = [];
dat.stimon = [];

dat.answer = [];
dat.conf = [];
dat.catch = [];
dat.rt = [];
dat.rtconf = [];

fprintf('Loading subjects: ');
for s_=1:length(subjects)
    %%
    subj = subjects(s_).name;
    if (nargin > 1) && ~strcmp(subj,selsubj)
        continue
    end
    
    s = s+1;
    %%
    fprintf('.');
    copyfile( ...
        [opts.dir.bids '/' subj '/ses-01/eeg/' subj '_ses-01_task-adapt_run-01_events.tsv'],...
        'tmp.csv');
    data = readtable('tmp.csv');
    data(~strcmp(data.trial_type,'stim-adapt'),:) = [];
    
    
    answ = strcmp(data.sdt,'hit') | strcmp(data.sdt,'fa');
    iscatch = strcmp(data.sdt,'fa') | strcmp(data.sdt,'crej');
    
    if iscell(data.stimamp)
        stim = nan(size(data.stimamp));
        stimon = nan(size(data.stimamp));
        conf = nan(size(data.stimamp));
        rt = nan(size(data.stimamp));
        rt2 = nan(size(data.stimamp));
        
        bad = strcmp(data.stimamp,'n/a');
        for i=1:length(data.stimamp)
            if ~bad(i)
                stim(i) = str2num(data.stimamp{i});
                stimon(i) = str2num(data.stimon{i});
                conf(i) = str2num(data.confidence{i});
                rt(i) = str2num(data.response_time{i});
                rt2(i) = str2num(data.response_time2{i});
                
            end
        end
    else
        stim = data.stimamp;
        stimon = data.stimon;
        conf = data.confidence;
        rt = data.response_time;
        rt2 = data.response_time2;
    end
    
    dat.answer = [dat.answer answ.'];
    dat.conf = [dat.conf conf.'];
    dat.rt = [dat.rt rt.'];
    dat.rtconf = [dat.rtconf rt2.'];
    dat.catch = [dat.catch iscatch.'];
    dat.stim = [dat.stimamp stim.'];
    dat.stimon = [dat.stimon stimon.'];
    
    dat.subjectid = [dat.subjectid s*ones(size(iscatch)).'];
    k = length(dat.subject);
    for i=1:length(iscatch)
        dat.subject{k+i} = subj;
    end
    
    
    stim(iscatch) = 0;
    dat.stimamp = [dat.stimamp stim.'];
    
    nhit = sum(dat.answer(dat.subjectid==s)==1 & dat.stimamp(dat.subjectid==s)>0);
    nmiss = sum(dat.answer(dat.subjectid==s)==0 & dat.stimamp(dat.subjectid==s)>0);
    
    nfa = sum(dat.answer(dat.subjectid==s)==1 & dat.stimamp(dat.subjectid==s)==0);
    ncr = sum(dat.answer(dat.subjectid==s)==0 & dat.stimamp(dat.subjectid==s)==0);
    
    stats.hitrate(s) = nhit./(nhit+nmiss);
    stats.farate(s) = nfa./(nfa+ncr);
    
    zfa = norminv((nfa+0.5)./((nfa+0.5)+ncr+1));
    zhr = norminv((nhit+0.5)/((nhit+0.5)+nmiss+1));
    
    stats.crit(s) = -zfa;
    stats.dprime(s) = zhr-zfa;
    
    stats.avgconf_hit(s) = mean(conf(answ==1 & stim>0));
    stats.avgconf_miss(s) = mean(conf(answ==0 & stim>0));
    stats.avgconf_crej(s) = mean(conf(answ==0 & stim==0));
    stats.avgconf_fa(s) = mean(conf(answ==1 & stim==0));
    stats.avgrt(s) = mean(rt);
    stats.avgrtconf(s) = mean(rt2);
    
    if all(double(stim(answ==1)>0)==1)
        stats.aroc_perc(s) = NaN;
    else
        [~,~,~,stats.aroc_perc(s)] = perfcurve(double(stim(answ==1)>0),conf(answ==1),1);
    end
    [~,~,~,stats.aroc_unperc(s)] = perfcurve(double(stim(answ==0)==0),conf(answ==0),1);
    
    %%
   if isempty(which(''))
       addpath(opts.dir.toolbox_eeglab);
       eeglab;
   end
      
   EEG = pop_loadset([subj '_preproc_01hz.set'], ...
        [opts.dir.bids_deriv '/' subj '/ses-01/eeg/']);
    
end


tbl = table(dat.subject.',dat.subjectid.',dat.stimamp.',dat.stimon.',dat.answer.',dat.conf.',dat.rt.',dat.rtconf.',...
    'VariableNames',{'subj','subjid','stimamp','stimon','resp','conf','rt','rtconf'});
fprintf(' [ OK ]\n');
tbl(dat.rt > 5  | dat.rtconf > 5,:) = [];

bad = isnan(tbl.stimamp);
tbl(bad,:) = [];

end