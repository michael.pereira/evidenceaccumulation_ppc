%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function h = plotconf(sim,data,stat,confhit,confmiss,confcrej)
%plotconf helper to see what's going on during the fit of a second-oorder
%model
%   
smplconf = 0:0.1:1;
smplev = -2:0.2:2;
ms = 10;
h = figure(100); clf;
subplot(321); hold on;
hsim = hist(sim.conf(sim.idxhit),smplconf)./sum(sim.idxhit);
hdata = hist(confhit,smplconf)./length(confhit);
plot(smplconf,hdata,'k','LineWidth',2);
plot(smplconf,hsim,'c','LineWidth',2);
title(sprintf('Hits: %.2f',stat(1)));

subplot(322); hold on;
hsim = hist(sim.conf(sim.idxmiss),smplconf)./sum(sim.idxmiss);
hdata = hist(confmiss,smplconf)./length(confmiss);
plot(smplconf,hdata,'k','LineWidth',2);
plot(smplconf,hsim,'r','LineWidth',2);
title(sprintf('Misses: %.2f',stat(2)));

subplot(323); hold on;
hsim = hist(sim.conf(sim.idxcrej),smplconf)./sum(sim.idxcrej);
hdata = hist(confcrej,smplconf)./length(confcrej);
plot(smplconf,hdata,'k','LineWidth',2);
plot(smplconf,hsim,'g','LineWidth',2);
title(sprintf('Crejs: %.2f',stat(3)));

subplot(324); hold on;
h1 = hist(sim.evidence(sim.idxhit),smplev);
h2 = hist(sim.evidence(sim.idxmiss),smplev);
h3 = hist(sim.evidence(sim.idxcrej),smplev);
plot(smplev,h1,'c','LineWidth',2)
plot(smplev,h2,'r','LineWidth',2)
plot(smplev,h3,'g','LineWidth',2)

subplot(325); hold on;
hr = sum(data.resp==1 & data.catch==0)/sum(data.catch==0);
far = sum(data.resp==1 & data.catch==1)/sum(data.catch==1);
sim.hr = sum(sim.resp==1 & sim.stimon>0)/sum(sim.stimon>0);
sim.far = sum(sim.resp==1 & sim.stimon==0)/sum(sim.stimon==0);
plot(1,hr,'ko','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
plot(2,far,'ko','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
plot(1,sim.hr,'kx','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
plot(2,sim.far,'kx','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
ylim([-0.2,1.2]); xlim([0.5,2.5]);

subplot(326); hold on;

h1 = mean(confhit); e1 = std(confhit);%/sqrt(sum(idxhit));
h2 = mean(confmiss); e2 = std(confmiss);%/sqrt(sum(idxmiss));
h3 = mean(confcrej); e3= std(confcrej);%/sqrt(sum(idxcrej));
errorbar(1,h1,e1,'co','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
errorbar(2,h2,e2,'ro','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
errorbar(3,h3,e3,'go','LineWidth',2,'MarkerSize',ms,'MarkerFaceColor','w');
h1 = mean(sim.conf(sim.idxhit));
h2 = mean(sim.conf(sim.idxmiss));
h3 = mean(sim.conf(sim.idxcrej));
plot(1,h1,'cx','LineWidth',2,'MarkerSize',ms)
plot(2,h2,'rx','LineWidth',2,'MarkerSize',ms)
plot(3,h3,'gx','LineWidth',2,'MarkerSize',ms)

ylim([-0.2,1.2]); xlim([0.5,3.5]);
end

