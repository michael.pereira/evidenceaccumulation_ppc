%========================================================
%
%   Classification
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [ytest,rawpred] = classif(x,y,xtest)
%classif Fits a linear discriminant on (x,y) and classifies xtest

Mdl = fitcdiscr(x,y,...
    'SaveMemory','off','FillCoeffs','on',...
    'Prior','uniform','ScoreTransform','logit', ...
    'DiscrimType','pseudolinear','Gamma',0.5);
ytest = Mdl.predict(xtest);
if nargout > 1
    rawpred = xtest*Mdl.Coeffs(2).Linear + Mdl.Coeffs(2).Const;
end
end

