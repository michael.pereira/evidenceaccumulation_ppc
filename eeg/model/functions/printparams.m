%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function printparams(params)
%printparams helper: prints parameters 
for f = fieldnames(params).'
   fprintf('%s = %.4f\n',cell2mat(f),params.(cell2mat(f)).val); 
end
end

