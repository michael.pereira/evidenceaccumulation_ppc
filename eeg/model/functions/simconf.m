%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [sim,dv,dvstim,dvresp] = simconf(stimon,params,params2,opts)
%simconf simulates confidence based on first- and second-order parameters
%
%   Inputs:
%   - stimon: stimulus onsets
%   - params: first-order parameter structure
%   - params2: second-order parameter structure
%   - opts: configurations
%
%   Outputs:
%   - sim: simulated responses and confidence
%   - dv: decision variable time-locked to the cue
%   - dvstim: decision variable time-locked to the stim (NaN for catchtrials)
%   - dvresp: decision variable time-locked to the resp (NaN for "no" resp)

[sim,dv,dvstim,dvresp] = evacc(stimon,params,opts);

% simulate confidence 
sim.stimon = stimon;
sim.idxhit = sim.resp==1 & stimon > 0;
sim.idxmiss = sim.resp==0 & stimon > 0;
sim.idxcrej = sim.resp==0 & stimon == 0;
sim.idxfa = sim.resp==1 & stimon == 0;
sim.hr = sum(sim.resp==1 & stimon>0)/sum(stimon>0);
sim.far = sum(sim.resp==1 & stimon==0)/sum(stimon==0);
[sim.conf,sim.evidence,sim.ro] = readout(sim,params,params2,dv,dvstim,dvresp,opts);

end

