%========================================================
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
function [ params2,stat,out] = fitconf( data,params,params2,opts, fit)
%fitconf fitting procedure for second-order model

if nargin < 6
    fit = 2;
end
stat = NaN;
out = [];
eval = 0;

idxhit = data.resp==1 & data.stimon > 0;
idxmiss = data.resp==0 & data.stimon > 0;
idxcrej = data.resp==0 & data.stimon == 0;
idxfa = data.resp==1 & data.stimon == 0;
hr = sum(data.resp==1 & data.stimon>0)/sum(data.stimon>0);
far = sum(data.resp==1 & data.stimon==0)/sum(data.stimon==0);

confhit = data.conf(idxhit);
confmiss = data.conf(idxmiss);
confcrej = data.conf(idxcrej);

%% Find which parameters to fit
fields = {'sensitivity','bias','readout','mu','sigma'};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params2.(fields{f}).fit
        p = p+1;
        % inverse transform of parameters
        param_val(p) = params2.(fields{f}).itrans(params2.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

if fit>0
    %% Fitting procedure
    %rng('default');
    % Reset random number generator
    rng('default');
    
    options = optimset('Display',opts.optimizationdisplay1,'MaxIter',opts.maxiter);%,'TolFun',opts.tolfun);%,'FinDiffRelStep',1e-2);
    [p, stat] = opts.fminfunc(@fitfunc,param_val,options);
else
    stat = fitfunc(param_val);
    return
end
%% Transform parameters back
for f=1:length(param_id)
    params2.(param_id{f}).val = params2.(param_id{f}).trans(p(f));
end

    function stat = fitfunc(p)
        
        
        % transform parameters back to "random-walk space"
        newparams = params2;
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
        
        % simulate confidence
        stimon = 2*rand(opts.N,1);
        nsimcatch = round(opts.N*0.2);
        stimon(1:nsimcatch) = 0;
        sim = simconf(stimon,params,newparams,opts);
        
        %stat1 = -(log(binopdf(sum(sim.resp==1 & stimon>0),sum(stimon > 0),hr)+eps));
        %stat2 = -(log(binopdf(sum(sim.resp==1 & stimon==0),sum(stimon <= 0),far)+eps));
        [~,p] = kstest2(sim.conf(sim.idxhit),confhit);
        stat1 = -log(p);
        [~,p] = kstest2(sim.conf(sim.idxmiss),confmiss);
        stat2 = -log(p);
        [~,p] = kstest2(sim.conf(sim.idxcrej),confcrej);
        stat3 = -log(p);
        
        
        stat = double(stat1+stat2+stat3)/3;
        % save current state
        out.ll = stat;
        out.ll1 = stat1;
        out.ll2 = stat2;
        out.ll3 = stat3;
        out.sim = sim;
        out.params2 = params2;
        
        eval = eval+1;
        %% plot some stuff
        if mod(eval,opts.doplot)==0
            %%
            out.h = plotconf(sim,data,[stat1 stat2 stat3],confhit,confmiss,confcrej);
            pause(0.01);
        end
    end

end

