%========================================================
%
%   Fit second-order model for maximal evidence confidence readout
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

addpath ../conf/
addpath functions
config_model;
addpath(opts.dir.toolbox_misc);

initparams = paramset();

opts.confreadout = 'max';
opts.confsuffix = '_nobias';

recompute = 0;

if ~exist('save','dir')
    mkdir('save');
end
for s = 1:length(opts.subjects)
    %%
    
    subj = opts.subjects{s};
    matfile = ['save/' subj '.mat'];
    if recompute || ~exist(matfile,'file')
        data = loadsubjects_eeg(opts,subj);
        save(['save/' subj '.mat'],'data');
    else
        load(['save/' subj '.mat'],'data');
    end
    
    load(['models/' subj '_fitresp_' opts.type '.mat'],'params');
    
    %%
    params2 = paramset2();
    
    [ params2,stat2,out2,allparams2,allstat2,allout2] = fitconf_wrapper( data, params, params2, opts);
    
    %%
    opts2 = opts;
    opts2.doplot = 1;
    opts2.doprint = 1;
    opts2.optimizationdisplay1 = 'Iter';
    %opts2.maxiter = 100;
    [ params2,stat2,out2] = fitconf( data, params, params2, opts2);
    
                  
    %%
%       stimon = 2*rand(10000,1);
%       stimon(1:2000) = 0;
%       opts2 = opts;
%       opts2.doprint2 = 0;
%      sim = simconf(2*rand(1),params,params2,opts2);

   
      %%
%       figure(); hold on;
%       plot(flist,simconfhit,'c','LineWidth',2);
%       plot(flist,simconfmiss,'r','LineWidth',2);
%       plot(flist,simconfcrej,'g','LineWidth',2);
%       plot(flist,simconffa,'k','LineWidth',2);
%       plot(flist,simfar,'k--','LineWidth',2);
%       title(subj)
    
    %confhit = data.conf(data.resp==1 & data.catch==0);
    %confmiss = data.conf(data.resp==0 & data.catch==0);
    %confcrej = data.conf(data.resp==0 & data.catch==1);
    %out2.h = plotconf(sim,data,[0 0 0],confhit,confmiss,confcrej);
    %    pause(0.1);

    %%
    save(['models/' subj '_fitconf_' opts.type '_' opts.confreadout opts.confsuffix '.mat'],...
        'params2','stat2','out2','allparams2','allstat2','allout2');
 %   saveas(out2.h,['figs/' subj '_fitconf_' opts.type '.png']);
    
end