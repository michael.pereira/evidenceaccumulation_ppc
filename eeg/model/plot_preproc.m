%========================================================
%
%   Describe preprocessing 
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
%
%   This script describes the number of ICs, trials and channels after
%   preprocessing
%
addpath ../conf/
addpath functions
config_model;
addpath(opts.dir.toolbox_misc);

initparams = paramset();

recompute = 0;

if ~exist('data','dir')
    mkdir('data');
end
if isempty(which('pop_loadset'))
    addpath(opts.dir.toolbox_eeglab);
    eeglab;
end


for s = 1:length(opts.subjects)
    %%
    subj = opts.subjects{s};
    % load and resample data
    EEG = pop_loadset([subj '_preproc_01hz.set'], ...
        [opts.dir.bids_deriv '/' subj '/ses-01/eeg/']);
    pop.nicacom(s) = size(EEG.icaweights,1);
    pop.ntrial(s) = sum(~strcmp({EEG.epoch.sdt},'n/a'));
    pop.nchans(s) = size(EEG.icaweights,2);
    
end

%%
describe(pop.nicacom,'ICs');
describe(pop.ntrial,'trials');
describe(62-pop.nchans,'channels');
