%========================================================
%
%   EEG results in Fig. 2
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
%
% This scripts computes the topoplot and mixed linear models for EEG data
%
clear
addpath ../conf/
addpath functions
config_model;

addpath(opts.dir.toolbox_misc);
addpath(opts.dir.toolbox_eeglab);

short = 1;
if short == 1
    lw = 1;
    figsz = [100,400,150,120];
else
    lw = 2;
    figsz = [100,400,300,250];
end
%eeglab

%%
recompute = 0;

if ~exist('data','dir')
    mkdir('data');
end
tbl = cell(1,62);
for s = 1:length(opts.subjects)
    %%
    
    subj = opts.subjects{s};
    opts.current = subj;
    matfile = ['data/' subj '.mat'];
    
    [data,EEG] = loadsubjects_eeg(opts,subj,0,0);
    
    rng('default');
    
    isstim = strcmp({EEG.epoch.sdt},'hit') | strcmp({EEG.epoch.sdt},'miss');
    iscatch = strcmp({EEG.epoch.sdt},'fa') | strcmp({EEG.epoch.sdt},'crej');
    isyes = strcmp({EEG.epoch.sdt},'hit') | strcmp({EEG.epoch.sdt},'fa');
    isno = strcmp({EEG.epoch.sdt},'miss') | strcmp({EEG.epoch.sdt},'crej');
    
    beh.hr(s) = mean(strcmp({EEG.epoch(isstim).sdt},'hit'));
    beh.far(s) = mean(strcmp({EEG.epoch(iscatch).sdt},'fa'));
    
    beh.confhit(s) = mean([EEG.epoch(strcmp({EEG.epoch.sdt},'hit')).conf]);
    beh.confmiss(s) = mean([EEG.epoch(strcmp({EEG.epoch.sdt},'miss')).conf]);
    beh.confcrej(s) = mean([EEG.epoch(strcmp({EEG.epoch.sdt},'crej')).conf]);
    beh.conffa(s) = mean([EEG.epoch(strcmp({EEG.epoch.sdt},'fa')).conf]);
    
    beh.accyes(s) = mean(strcmp({EEG.epoch(isyes).sdt},'hit'));
    beh.accno(s) = mean(strcmp({EEG.epoch(isno).sdt},'crej'));
    
    beh.confyes(s) = mean([EEG.epoch(isyes).conf]);
    beh.confno(s) = mean([EEG.epoch(isno).conf]);
    
    
    erp.hit1(:,:,s) = data.erphit1;
    erp.hit2(:,:,s) = data.erphit2;
    erp.miss(:,:,s) = data.erpmiss;
    
    erp.hit1_ci(:,:,s) = data.erphit1_ci;
    erp.hit2_ci(:,:,s) = data.erphit2_ci;
    erp.miss_ci(:,:,s) = data.erpmiss_ci;
    
    tbl_ = cell(1,62);
    for el=1:62
        suj = s*ones(size(data.erpstat_resp.'));
        tbl_ = table(suj,double(data.erpstat(el,:)).',data.erpstat_resp.',data.erpstat_conf.', ...
            'VariableNames',{'suj','eeg','resp','conf'});
        if s == 1
            tbl{el} = tbl_;
        else
            tbl{el} = [tbl{el} ; tbl_];
        end
        % tbl{el} =  %
    end
    
end

if ~exist('save','dir')
    mkdir('save');
end
save('save/erpdata.mat','tbl','erp','beh');

%%
for el=1:62
    el
    model{1} = fitlme(tbl{el},'eeg ~ resp*conf + (1|suj)');
    model{2} = fitlme(tbl{el},'eeg ~ resp*conf + (resp|suj)');
    model{3} = fitlme(tbl{el},'eeg ~ resp*conf + (conf|suj)');
    model{4} = fitlme(tbl{el},'eeg ~ resp*conf + (resp*conf|suj)');
    bic = [model{1}.ModelCriterion.BIC model{2}.ModelCriterion.BIC model{3}.ModelCriterion.BIC model{4}.ModelCriterion.BIC];
    
    [~,selmodel] = min(bic);
    
    model{el} = model{selmodel};
    sel(el) = selmodel;
    coeff(:,el) = model{selmodel}.Coefficients.Estimate;
    pval(:,el) = model{selmodel}.Coefficients.pValue;
end

%%
h = figure();
topoplot(coeff(4,:),data.chanlocs,'plotrad',0.51,'style','map');
caxis([0,3]);
colormap('hot');
set(gcf,'Position',[100,300,250,150]);
saveas(h,['figs/fig2f_topo.tiff'],'tiffn');
%%
lw = 1;
figure(); hold on;
[~,elec] = intersect({data.chanlocs.labels},{data.chanlocs(pval(4,:)< 0.05/62).labels});

selt = data.erp_time>-500;
erphit1 = squeeze(mean(erp.hit1(selt,elec,:),2)).';
erphit2 = squeeze(mean(erp.hit2(selt,elec,:),2)).';
erpmiss = squeeze(mean(erp.miss(selt,elec,:),2)).';
cishade(erphit1,[],opts.color.hit2,data.erp_time(selt)*1e-3,'LineWidth',lw);
cishade(erphit2,[],opts.color.hit3,data.erp_time(selt)*1e-3,'LineWidth',lw);
cishade(erpmiss,[],opts.color.miss,data.erp_time(selt)*1e-3,'LineWidth',lw);
plot([0 0],ylim(),'k--');

set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-2:1:30,'YTickLabel',{});
set(gcf,'Position',figsz)
print(['figs/fig2f_erp.eps'],'-depsc');

% save data
miss_ = mean(erpmiss,1);
confhigh_ = mean(erphit2,1);
conflow_ = mean(erphit1,1);

t = data.erp_time(selt)*1e-3;
sel = t>= -0.5 & t < 1.5;
tbl = table(t(sel).',confhigh_(sel).',conflow_(sel).',miss_(sel).','VariableNames',{'time','confhigh','conflow','miss'});

writetable(tbl,'../../supdata/Fig.2f.xls');
