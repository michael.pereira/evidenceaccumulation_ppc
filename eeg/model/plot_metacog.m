%========================================================
%
%   Describe preprocessing 
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   24 Dec 2021
%========================================================
%
%   This script plots the observed versus simulated AROC
%
clear
addpath ../conf/
config_model
load(['save/pop_max' opts.confsuffix '_rt.mat']);

% correlation
[c,p] = corr(pop.aroc_yes.',pop.simaroc_yes.','rows','complete','type','Spearman');
n = length(pop.simaroc_no.');
% permutation test
rng('default');
%perm = nan(size(pop.aroc_yes,1),1000);
clear perm
for i=1:1000
    perm(:,i) = pop.simaroc_yes(randperm(n));
end
rc = corr(pop.aroc_yes.',perm,'rows','complete','type','Spearman');
p = mean((rc) > c);  

fprintf('Corr AROC yes: %.2f (p=%.3f)\n',c,p);
figure(); hold on;
plot(pop.aroc_yes.',pop.simaroc_yes.','ko','MarkerFaceColor','k');
m = fitlm(pop.simaroc_yes.',pop.aroc_yes.');
x = linspace(0.0,1,100).';
[y,yint] = m.predict(x);
plot(x,y,'r','LineWidth',2);
plot([x ; x(end:-1:1)],[yint(:,1) ; yint(end:-1:1,2)],'r','LineWidth',1);
axis([0.45,1,0.45,1])
set(gca,'XTick',0:0.1:1,'YTick',0:0.1:1,'XTickLabel',{},'YTickLabel',{});
set(gcf,'Position',[400,400,150,150]);
print(['figs/aroc_corr_yes.eps'],'-depsc');

%%
[c,p] = corr(pop.aroc_no.',pop.simaroc_no.','rows','complete','type','Spearman');
n = length(pop.simaroc_no.');
rng('default');
for i=1:1000
    perm(:,i) = pop.simaroc_no(randperm(n));
end
rc = corr(pop.aroc_no.',perm,'rows','complete','type','Spearman');
p = mean((rc) > c);    
    fprintf('Corr AROC no: %.2f (p=%.3f)\n',c,p);

    figure(); hold on;
plot(pop.aroc_no.',pop.simaroc_no.','ko','MarkerFaceColor','k');
m = fitlm(pop.aroc_no.',pop.simaroc_no.');
x = linspace(0.45,0.75,100).';
[y,yint] = m.predict(x);
%plot(x,x,'k--')
plot(x,y,'r','LineWidth',2);
plot([x ; x(end:-1:1)],[yint(:,1) ; yint(end:-1:1,2)],'r','LineWidth',1);
axis([0.45,0.75,0.45,1])
set(gca,'XTick',0:0.1:1,'YTick',0:0.1:1,'XTickLabel',{},'YTickLabel',{});
set(gcf,'Position',[400,400,150,150]);
print(['figs/aroc_corr_no.eps'],'-depsc');

% save data
tbl = table(pop.aroc_yes.',pop.aroc_no.',pop.simaroc_yes.',pop.simaroc_no.',...
    'VariableNames',{'arocyes','arocno','simarocyes','simarocno'});
writetable(tbl,'../../supdata/Fig.4g.xls');
%%
[c,p] = corr(pop.simaroc_yes.',pop.params(4,:).','rows','complete','type','spearman');
n = length(pop.params(4,:));
rng('default');
for i=1:1000
    rc(i) = corr(pop.params(4,randperm(n)).',pop.aroc_yes.','rows','complete','type','Spearman');
end

p = mean((rc) > c);    
    fprintf('Corr AROC yes and leakage: %.2f (p=%.3f)\n',c,p);
