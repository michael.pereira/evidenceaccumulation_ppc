%========================================================
%
%   Evaluate models
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

clear
addpath ../conf/
addpath functions
config_model;
opts.N = 10000; 
%opts.N = 10;
addpath(opts.dir.toolbox_misc);
 rng('default');

opts.confreadout = 'resp'; k = 4;
opts.confreadout = 'max'; k = 2;


opts.confsuffix = '';  

initparams = paramset_1storder();

recompute = 0;

ci = @(x) std(x)./sqrt(length(x));
t = linspace(-0.5,1.5,2000);
t2 = linspace(0,4,4000);

if ~exist('save','dir')
    mkdir('save');
end
%%
for s = 1:length(opts.subjects)
    %%
   

    tic;
    subj = opts.subjects{s};
    matfile = ['save/' subj '.mat'];
    if recompute || ~exist(matfile,'file')
        data = loadsubjects_eeg(opts,subj);
        save(['save/' subj '.mat'],'data');
    else
        load(['save/' subj '.mat'],'data');      
    end
    
    fprintf(' [ stage 5 ] analysing data\n');

    pop.bestmcr(s) = data.best_mcr;
    pop.besttime(s) = data.weights_t;
    pop.topo(:,s) = data.weights;
    pop.topo_norm(:,s) = data.weights./std(data.weights);
    
    
    %params2.bias.val = params2.bias.val+0.5;
    idxhit = data.resp==1 & data.stimon > 0;
    idxmiss = data.resp==0 & data.stimon > 0;
    idxcrej = data.resp==0 & data.stimon == 0;
    idxfa = data.resp==1 & data.stimon == 0;
    hr = sum(data.resp==1 & data.stimon>0)/sum(data.stimon>0);
    far = sum(data.resp==1 & data.stimon==0)/sum(data.stimon==0);
    
    idxhit1 = idxhit & data.conf <= median(data.conf(idxhit));
    idxhit2 = idxhit & data.conf > median(data.conf(idxhit));
    idxmiss1 = idxmiss & data.conf <= median(data.conf(idxmiss));
    idxmiss2 = idxmiss & data.conf > median(data.conf(idxmiss));
    
    confhit = data.conf(idxhit);
    confmiss = data.conf(idxmiss);
    confcrej = data.conf(idxcrej);
    conffa = data.conf(idxfa);

    %%
    pop.hr(s) = hr;
    pop.far(s) = far;
    pop.dprime(s) = norminv(hr)-norminv(far);
    pop.crit(s) = -norminv(far);

    pop.confhit(s) = mean(confhit);
    pop.confmiss(s) = mean(confmiss);
    pop.confcrej(s) = mean(confcrej);
    pop.conffa(s) = mean(conffa);
    
    pop.confhit_var(s) = var(confhit);
    pop.confmiss_var(s) = var(confmiss);
    pop.confcrej_var(s) = var(confcrej);
    pop.conffa_var(s) = var(conffa);
    
    pop.hconfhit(:,s) = hist(confhit,0:0.1:1)./sum(idxhit);
    pop.hconfmiss(:,s) = hist(confmiss,0:0.1:1)./sum(idxmiss);
    pop.hconfcrej(:,s) = hist(confcrej,0:0.1:1)./sum(idxcrej);
    pop.hconffa(:,s) = hist(conffa,0:0.1:1)./sum(idxfa);
    
    erp = bsxfun(@minus,data.pEA,mean(data.pEA(201:500,:)));

    pop.erp_hit(:,s) = mean(erp(:,idxhit),2);
    pop.erp_miss(:,s) = mean(erp(:,idxmiss),2);
    
    pop.erp_hit1(:,s) = mean(erp(:,idxhit1),2);
    pop.erp_hit2(:,s) = mean(erp(:,idxhit2),2);
    pop.erp_miss1(:,s) = mean(erp(:,idxmiss1),2);
    pop.erp_miss2(:,s) = mean(erp(:,idxmiss2),2);
    
    % DATA FOR NO
    c = [confmiss ; confcrej];
    lab = [zeros(size(confmiss)) ; ones(size(confcrej))];
    [~,~,~,pop.aroc_no(s)] = perfcurve(lab,c,1);
    
    if ~isempty(conffa)
        c = [confhit ; conffa];
        lab = [ones(size(confhit)) ; zeros(size(conffa))];
        [~,~,~,pop.aroc_yes(s)] = perfcurve(lab,c,1);
    else
        pop.aroc_yes(s) = NaN;
    end
    %%
    
    fmodel1 = ['models/' subj '_fitresp_' opts.type '.mat'];
    fmodel2 = ['models/' subj '_fitconf_' opts.type '_' opts.confreadout opts.confsuffix '.mat'];
    
    fprintf(' [ stage 5 ] loading %s\n',fmodel1);
    load(fmodel1,'allparams','params','allstat','stat','allout','out');
    fprintf(' [ stage 5 ] loading %s\n',fmodel2);
    load(fmodel2,'params2','stat2','out2');
    
    fprintf(' [ stage 5 ] %d/%d simulating for %s\n',s,length(opts.subjects),opts.subjects{s});
   
    pop.params(:,s) = [params.bound.val params.drift.val params.driftvar.val ...
        params.lambda.val params.decay.val params.ndt.val ...
        params2.sensitivity.val params2.bias.val params2.readout.val params2.mu.val];
    
    pop.llresp(:,s) = -[out.ll1 out.ll2];
    pop.llerp(:,s) = -[out.ll3 out.ll4];
    pop.llconf(:,s) = -[out2.ll1 out2.ll2 out2.ll3];
    
    n = length(data.stimamp);

    pop.bicconf(s) = k*log(n)-2*sum(pop.llconf(:,s));
    pop.aicconf(s) = 2*k-2*sum(pop.llconf(:,s));
    
    stimon = 2*rand(opts.N,1);
    nsimcatch = round(opts.N*0.2);
    stimon(1:nsimcatch) = 0;
    
    [sim,dv,dvstim,dvresp] = simconf(stimon,params,params2,opts);
    
    pop.rthist(:,s) = hist(sim.rt,0:0.1:2);
    
    simconfhit = sim.conf(sim.idxhit);
    simconfmiss = sim.conf(sim.idxmiss);
    simconfcrej = sim.conf(sim.idxcrej);
    simconffa = sim.conf(sim.idxfa);

    idxsimconfhit1 = sim.idxhit & sim.conf <= median(sim.conf(sim.idxhit));
    idxsimconfhit2 = sim.idxhit & sim.conf > median(sim.conf(sim.idxhit));
    idxsimconfmiss1 = sim.idxmiss & sim.conf <= median(sim.conf(sim.idxmiss));
    idxsimconfmiss2 = sim.idxmiss & sim.conf > median(sim.conf(sim.idxmiss));

  
    %%
  
    scale = 1e-3*sum(max(mean(erp(501:1700,:),2),0),1);
  
    simerp = bsxfun(@minus,dvstim,mean(dvstim(201:500,:)));
  
    simscale = 1e-3*sum(max(mean(simerp(501:1700,:),2),0),1);
    simerp = simerp./simscale.*scale;
   
    %%
    suj = repmat({subj},size(data.conf));
    typ = ones(size(data.conf));

    tbl = table(suj,typ,data.conf,data.resp,data.stimon,data.stimamp,...
        'VariableNames',{'suj','type','conf','resp','onset','amp'});
    simsuj = repmat({subj},size(sim.conf));
    simtyp = 2*ones(size(sim.conf));
    
    simtbl = table(simsuj,simtyp,sim.conf,sim.resp,sim.stimon,double(sim.stimon>0),...
        'VariableNames',{'suj','type','conf','resp','onset','amp'});
    
    if s==1
        pop.tbl = tbl;
        pop.simtbl = simtbl;
    else
        pop.tbl = [pop.tbl ; tbl];
        pop.simtbl = [pop.simtbl ; simtbl];
    end
    
   
    pop.simhr(s) = sim.hr;
    pop.simfar(s) = sim.far;
    
    pop.simdprime(s) = norminv(sim.hr)-norminv(sim.far);
    pop.simcrit(s) = -norminv(sim.far);

    pop.simconfhit(s) = mean(sim.conf(sim.idxhit));
    pop.simconfmiss(s) = mean(sim.conf(sim.idxmiss));
    pop.simconfcrej(s) = mean(sim.conf(sim.idxcrej));
    pop.simconffa(s) = mean(sim.conf(sim.idxfa));
    
    pop.simconfhit_var(s) = var(sim.conf(sim.idxhit));
    pop.simconfmiss_var(s) = var(sim.conf(sim.idxmiss));
    pop.simconfcrej_var(s) = var(sim.conf(sim.idxcrej));
    pop.simconffa_var(s) = var(sim.conf(sim.idxfa));
    
    
    pop.hsimconfhit(:,s) = hist(sim.conf(sim.idxhit),0:0.1:1)./sum(sim.idxhit);
    pop.hsimconfmiss(:,s) = hist(sim.conf(sim.idxmiss),0:0.1:1)./sum(sim.idxmiss);
    pop.hsimconfcrej(:,s) = hist(sim.conf(sim.idxcrej),0:0.1:1)./sum(sim.idxcrej);
    pop.hsimconffa(:,s) = hist(sim.conf(sim.idxfa),0:0.1:1)./sum(sim.idxfa);
    
    pop.simerp_hit(:,s) = mean(simerp(:,sim.idxhit),2);
    pop.simerp_miss(:,s) = mean(simerp(:,sim.idxmiss),2);
    
    pop.simerp_hit1(:,s) = mean(simerp(:,idxsimconfhit1),2);
    pop.simerp_hit2(:,s) = mean(simerp(:,idxsimconfhit2),2);
    pop.simerp_miss1(:,s) = mean(simerp(:,idxsimconfmiss1),2);
    pop.simerp_miss2(:,s) = mean(simerp(:,idxsimconfmiss2),2);
    
      % SIM FOR NO
    c = [simconfmiss ; simconfcrej];
    lab = [zeros(size(simconfmiss)) ; ones(size(simconfcrej))];
    [~,~,~,pop.simaroc_no(s)] = perfcurve(lab,c,1);
    
    if ~isempty(simconffa)
        c = [simconfhit ; simconffa];
        lab = [ones(size(simconfhit)) ; zeros(size(simconffa))];
        [~,~,~,pop.simaroc_yes(s)] = perfcurve(lab,c,1);
    else
        pop.simaroc_yes(s) = NaN;
    end
        
    
end

save(['save/pop_' opts.confreadout opts.confsuffix '_rt.mat'],'pop');