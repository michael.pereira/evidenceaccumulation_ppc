%========================================================
%
%   Describe preprocessing
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
%
%   This script plots model fits for all subjects
%
clear
addpath ../conf/
addpath functions
config_model;

confreadout = 'max';
if ~exist('pop','var')
    load(['save/pop_' confreadout opts.confsuffix '_rt.mat']);
end

saveplots = 1;
ci = @(x) nanstd(x)./sqrt(length(x));
% compute correlations
for s=1:18
    [cerp(s),perp(s)] = corr([pop.erp_miss(:,s) ; pop.erp_hit(:,s)],[pop.simerp_miss(:,s) ; pop.simerp_hit(:,s)],'rows','complete','type','Spearman');
    [cerp_hit(s),perp_hit(s)] = corr(pop.erp_hit(:,s), pop.simerp_hit(:,s),'rows','complete','type','Spearman');
    [cerp_miss(s),perp_miss(s)] = corr(pop.erp_miss(:,s),pop.simerp_miss(:,s),'rows','complete','type','Spearman');
    
    [chit(s),phit(s)] = corr(pop.hconfhit(:,s),pop.hsimconfhit(:,s),'rows','complete','type','Spearman');
    [cmiss(s),pmiss(s)] = corr(pop.hconfmiss(:,s),pop.hsimconfmiss(:,s),'rows','complete','type','Spearman');
    [ccrej(s),pcrej(s)] = corr(pop.hconfcrej(:,s),pop.hsimconfcrej(:,s),'rows','complete','type','Spearman');
    [cfa(s),pfa(s)] = corr(pop.hconffa(:,s),pop.hsimconffa(:,s),'rows','complete','type','Spearman');
    
end

[chr,phr] = corr(pop.hr.',pop.simhr.','rows','complete','type','Spearman');
[cfar,pfar] = corr(pop.far.',pop.simfar.','rows','complete','type','Spearman');
fprintf('-------------------------\n');
fprintf('-  Model fit\n');
fprintf('-------------------------\n');
ll = sum(pop.llerp)+sum(pop.llresp)+sum(pop.llconf);
fprintf('Loglikehood: %.2f ± %.2f\n',mean(ll),ci(ll));
ll = sum(pop.llresp);
fprintf('Loglikehood for detection: %.2f ± %.2f\n',mean(ll),ci(ll));
ll = sum(pop.llerp);
fprintf('Loglikehood for ERP: %.2f ± %.2f\n',mean(ll),ci(ll));
ll = sum(pop.llconf);
fprintf('Loglikehood for confidence: %.2f ± %.2f\n',mean(ll),ci(ll));

fprintf('ERP fit: R = %.2f ± %.2f\n',mean(cerp),ci(cerp));
fprintf('ERP fit for hits: R = %.2f ± %.2f\n',mean(cerp_hit),ci(cerp_hit));
fprintf('ERP fit for misses: R = %.2f ± %.2f\n',mean(cerp_miss),ci(cerp_miss));
fprintf('HR fit: R = %.2f ± %.2f\n',mean(chr),ci(chr));
fprintf('FAR fit: R = %.2f ± %.2f\n',mean(cfar),ci(cfar));
fprintf('Conf (hit) fit: R = %.2f ± %.2f\n',mean(chit),ci(chit));
fprintf('Conf (miss) fit: R = %.2f ± %.2f\n',mean(cmiss),ci(cmiss));
fprintf('Conf (crej) fit: R = %.2f ± %.2f\n',mean(ccrej),ci(ccrej));
fprintf('Conf (fa) fit: R = %.2f ± %.2f\n',nanmean(cfa),ci(cfa));


%%
fprintf('-------------------------\n');
fprintf('-  Parameter xcor\n');
fprintf('-------------------------\n');
[c,p] = corr(pop.params.','type','Spearman')
c2 = triu(c,1);
p2 = triu(p,1);
sel = find(p2>0);
[~,~,~,pfdr] = fdr_bh(p2(sel));
p(sel) = pfdr.';
fprintf('Max R = %.2f, p = %.3f\n',max(abs(c2(:))),min(pfdr));

%%
fprintf('-------------------------\n');
fprintf('-  Model predictions\n');
fprintf('-------------------------\n');
y = pop.simconfhit_var - pop.simconfmiss_var;
x = pop.confhit_var - pop.confmiss_var;

[psim,~,stsim] = signrank(y);
[pdat,~,stdat] = signrank(x);

fprintf('Variance. conf. sim.: %.3f ± %.3f (hits), %.3f ± %.3f (misses), p = %.4f, z = %.1f\n',...
    mean(pop.simconfhit_var),ci(pop.simconfhit_var),mean(pop.simconfmiss_var),ci(pop.simconfmiss_var),psim,stsim.zval);
fprintf('Variance. conf. data: %.3f ± %.3f (hits), %.3f ± %.3f (misses), p = %.4f, z = %.1f\n',...
    mean(pop.confhit_var),ci(pop.confhit_var),mean(pop.confmiss_var),ci(pop.confmiss_var),pdat,stdat.zval);


y = pop.simconfmiss_var - pop.simconfcrej_var;
x = pop.confmiss_var - pop.confcrej_var ;

[psim,~,stsim] = signrank(y);
[pdat,~,stdat] = signrank(x);

fprintf('Variance. conf. sim.: %.3f ± %.3f (misses), %.3f ± %.3f (c.rej.), p = %.4f, z = %.1f\n',...
    mean(pop.simconfmiss_var),ci(pop.simconfmiss_var),mean(pop.simconfcrej_var),ci(pop.simconfcrej_var),psim,stsim.zval);
fprintf('Variance. conf. data: %.3f ± %.3f (misses), %.3f ± %.3f (c.rej.), p = %.4f, z = %.1f\n',...
    mean(pop.confmiss_var),ci(pop.confmiss_var), mean(pop.confcrej_var),ci(pop.confcrej_var),pdat,stdat.zval);


%%
rel = max(pop.erp_miss)./max(pop.erp_hit);

simrel = max(pop.simerp_miss)./max(pop.simerp_hit);

t = linspace(-0.5,1.5,2000);
[~,imax] = max(pop.simerp_hit);
tmax = t(imax);
%%


%%

fprintf('\n============\n\n');
myttest(pop.confhit,pop.conffa,{'conf (hit)','conf (fa)'});
myttest(pop.simconfhit,pop.simconffa,{'simconf (hit)','simconf (fa)'});
fprintf('\n============\n\n');
myttest(pop.confmiss,pop.confcrej,{'conf (miss)','conf (crej)'});
myttest(pop.simconfmiss,pop.simconfcrej,{'simconf (miss)','simconf (crej)'});
fprintf('\n============\n\n');

[c,p] = corr((pop.hr).',(pop.simhr).','rows','complete','type','Spearman');
fprintf('Corr HR data vs. model R=%f, p=%.3f\n',c,p);
[c,p] = corr((pop.far).',(pop.simfar).','rows','complete','type','Spearman');
fprintf('Corr FAR data vs. model R=%f, p=%.3f\n',c,p);

%%
figure(501); clf; hold on

cishade(pop.erp_hit(1:1990,:).',[],[1 1 1]*0.5,t(1:1990),'LineWidth',2);
plot(t,mean(pop.simerp_hit,2),'Color',opts.color.hit,'LineWidth',2);

cishade(pop.erp_miss(1:1990,:).',[],[1 1 1]*0.5,t(1:1990),'LineWidth',2);
plot(t,mean(pop.simerp_miss,2),'Color',opts.color.miss,'LineWidth',2);
xlim([-0.5,1.5])
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',-10:0.5:10,'YTickLabel',{})
set(gcf,'Position',[200,300,300,200])
if strcmp(confreadout,'max');
    print('figs/Fig4c.eps','-depsc');
end

% save data
erphit_ = mean(pop.erp_hit(1:1990,:),2);
erpmiss_ = mean(pop.erp_miss(1:1990,:),2);
simhit_ = mean(pop.simerp_hit(1:1990,:),2);
simmiss_ = mean(pop.simerp_miss(1:1990,:),2);
tbl = table(t(1:1990).',erphit_,erpmiss_,simhit_,simmiss_,...
    'VariableNames',{'t','erphit','erpmiss','simhit','simmiss'});
writetable(tbl,'../../supdata/Fig.4c.xls');
%%
figure(502); clf; hold on;
ms = 8;
lw = 1.5;
plot(1,mean(pop.hr),'o','MarkerSize',ms,'LineWidth',lw,'Color','k');
plot(2,mean(pop.far),'o','MarkerSize',ms,'LineWidth',lw,'Color','k');
plot(1,mean(pop.simhr),'x','MarkerSize',ms,'LineWidth',lw,'Color','k');
plot(2,mean(pop.simfar),'x','MarkerSize',ms,'LineWidth',lw,'Color','k');
xlim([0.5,2.5]); ylim([0,0.6])
set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[200,800,60,60])
if strcmp(confreadout,'max')
    print('figs/Fig4d.eps','-depsc');
end
% save data 
tbl = table(mean(pop.hr),mean(pop.far),mean(pop.simhr),mean(pop.simfar),...
    'VariableNames',{'hr','far','simhr','simfar'});
writetable(tbl,'../../supdata/Fig.4d.xls');

%%
figure(505); clf; hold on;
ms = 8;
lw = 1.5;
plot(1,mean(pop.confhit),'o','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.hit);
plot(2,mean(pop.confmiss),'o','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.miss);
plot(3,mean(pop.confcrej),'o','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.crej);
plot(4,nanmean(pop.conffa),'o','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.fa);

plot(1,mean(pop.simconfhit),'x','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.hit);
plot(2,mean(pop.simconfmiss),'x','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.miss);
plot(3,mean(pop.simconfcrej),'x','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.crej);
plot(4,nanmean(pop.simconffa),'x','MarkerSize',ms,'LineWidth',lw,'Color',opts.color.fa);

xlim([0.5,4.5]); ylim([0.3,1])
set(gca,'XTick',1:2,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
if strcmp(confreadout,'max')
    print('figs/Fig4e.eps','-depsc');
end

% save data
tbl = table(mean(pop.confhit),mean(pop.confmiss),mean(pop.confcrej),nanmean(pop.conffa),...
    mean(pop.simconfhit),mean(pop.simconfmiss),mean(pop.simconfcrej),nanmean(pop.simconffa),...
    'VariableNames',{'confhit','confmiss','confcrej','conffa','simconfhit','simconfmiss','simconfcrej','simconffa'});
writetable(tbl,'../../supdata/Fig.4e.xls');
%%
figure(511); clf; hold on;
x = 0:0.1:1;
bar(x,mean(pop.hconfhit,2),'FaceColor',0.5*[1 1 1]);
errorbar(x,mean(pop.hconfhit.'),ci(pop.hconfhit.'),'k.');
cishade(pop.hsimconfhit.',[],opts.color.hit,x,'LineWidth',2);
set(gca,'XTick',0:0.5:1,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
if strcmp(confreadout,'max');
    print('figs/Fig4f_11.eps','-depsc');
end

figure(512); clf; hold on;
x = 0:0.1:1;
bar(x,mean(pop.hconfmiss,2),'FaceColor',0.5*[1 1 1]);
errorbar(x,mean(pop.hconfmiss.'),ci(pop.hconfmiss.'),'k.');
cishade(pop.hsimconfmiss.',[],opts.color.miss,x,'LineWidth',2);
set(gca,'XTick',0:0.5:1,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
if strcmp(confreadout,'max');
    print('figs/Fig4f_12.eps','-depsc');
end

figure(513); clf; hold on;
x = 0:0.1:1;
bar(x,mean(pop.hconfcrej,2),'FaceColor',0.5*[1 1 1]);
errorbar(x,mean(pop.hconfcrej.'),ci(pop.hconfcrej.'),'k.');
cishade(pop.hsimconfcrej.',[],opts.color.crej,x,'LineWidth',2);
set(gca,'XTick',0:0.5:1,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
if strcmp(confreadout,'max');
    print('figs/Fig4f_21.eps','-depsc');
end

figure(514); clf; hold on;
x = 0:0.1:1;
bad = any(isnan(pop.hconffa)) | any(isnan(pop.hsimconffa));
bar(x,mean(pop.hconffa(:,~bad),2),'FaceColor',0.5*[1 1 1]);
errorbar(x,mean(pop.hconffa(:,~bad).'),ci(pop.hconffa(:,~bad).'),'k.');
cishade(pop.hsimconffa(:,~bad).',[],opts.color.fa,x,'LineWidth',2);
set(gca,'XTick',0:0.5:1,'XTickLabel',{},'YTick',0:0.1:1,'YTickLabel',{})
set(gcf,'Position',[400,800,120,120])
if strcmp(confreadout,'max');
    print('figs/Fig4f_22.eps','-depsc');
end
% save data
confhit = mean(pop.hconfhit,2);
simconfhit = mean(pop.hsimconfhit,2);
confmiss = mean(pop.hconfmiss,2);
simconfmiss = mean(pop.hsimconfmiss,2);
confcrej = mean(pop.hconfcrej,2);
simconfcrej = mean(pop.hsimconfcrej,2);
conffa = nanmean(pop.hconffa,2);
simconffa = nanmean(pop.hsimconffa,2);
tbl = table(confhit,confmiss,confcrej,conffa,simconfhit,simconfmiss,simconfcrej,simconffa,...
   'VariableNames',{'confhit','confmiss','confcrej','conffa','simconfhit','simconfmiss','simconfcrej','simconffa'});
 writetable(tbl,'../../supdata/Fig.4f.xls');

%%
%%
[data,EEG] = loadsubjects_eeg(opts,'sub-01',0,0);

figure(503); clf;
topoplot(mean(pop.topo_norm,2),data.chanlocs,'plotrad',0.5);
colormap('hot')
caxis([0,1.5]);
print('figs/Fig4c_topo.png','-dpng');