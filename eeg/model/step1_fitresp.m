%========================================================
%
%   Fit first-order model
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

addpath ../conf/
addpath functions
config_model;
addpath(opts.dir.toolbox_misc);

initparams = paramset();

recompute = 1;

if ~exist('save','dir')
    mkdir('save');
end
for s = 1:length(opts.subjects)
    %%
   
    subj = opts.subjects{s};
    opts.current = subj;
    matfile = ['save/' subj '.mat'];
    if recompute || ~exist(matfile,'file')
        data = loadsubjects_eeg(opts,subj);
        save(['save/' subj '.mat'],'data');
    else
        load(['save/' subj '.mat'],'data');
    end
    rng('default');
    
    %%
    [ params,stat,out,allparams,allstat,allout] = fiterp_wrapper( data.stimon,data.resp, data.pEA, initparams, opts);
    
    opts2 = opts;
    opts2.doplot = 1;
    opts2.doprint = 1;
    opts2.optimizationdisplay1 = 'Iter';
    %opts2.t1llstd = 0.01;
    %%
    [params,stat,out] = fiterp(data.stimon, data.resp, data.pEA, params, opts2,2);
    
    save(['models/' subj '_fitresp_' opts.type '.mat'],'params','stat','out','allparams','allstat','allout');
    
    saveas(out.h,['figs/' subj '_fitresp_' opts.type '.png']);
    
end