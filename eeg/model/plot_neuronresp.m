%========================================================
%
%   Plot neuronal response (SFig. 8)
%
%   Pereira et al., 2021,
%   Evidence accumulation determines perceptual
%       consciousness and monitoring
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

clear
addpath ../conf/
config_model;

prod = 1;
opts.confreadout = 'max'; k = 2;
opts.confsuffix = '';  

if prod == 1
    alpha = [];
else
    alpha = 0.04;
end
rng('default');


params.bound.val = 4;
params.drift.val = 0.004;
params.driftvar.val = 0.7;
params.noise.val = 0.1;
params.scale.val = 1;
params.decay.val = 0.5;
params.lambda.val = 4;
params.ndt.val = 0.4;
params.ndtvarperc.val = 0.04;
params.ndtmot.val = 0.08;
params.ndtvarmot.val = 0.04;

params.stimdur.val = 0.1;
params.maxdur.val = Inf;
params.rbound.val = 0;
params.start.val = 0;
params.startvar.val = 0;
    
params2 = paramset_2ndorder();
%%

opts.doprint = 1;
opts.N = 300;

stimon = 2*rand(opts.N,1);

[sim,dv,dvstim,dvresp] = simconf(stimon,params,params2,opts);
 mean(sim.resp(sim.stimon>0))

 
q = quantile(sim.rt(sim.resp==1),[0.33,0.66]);
idrt1 = sim.rt < q(1);
idrt2 = sim.rt >= q(1) & sim.rt < q(2);
idrt3 = sim.rt >= q(2);

l1 = min(500+round(1000*median(sim.rt(idrt1.' & sim.resp==1))),2000);
l2 = min(500+round(1000*median(sim.rt(idrt2.' & sim.resp==1))),2000);
l3 = min(500+round(1000*median(sim.rt(idrt3.' & sim.resp==1))),2000);

figure(801); clf; hold on;
t = linspace(-0.5,1.5,2000);
%cishade(dvstim(:,sim.resp==0).',alpha,opts.color.miss,t,'LineWidth',2);
%cishade(dvstim(1:l1,idrt1.' & sim.resp==1).',alpha,opts.color.hit1,t(1:l1),'LineWidth',2);
%cishade(dvstim(1:l2,idrt2.' & sim.resp==1).',alpha,opts.color.hit2,t(1:l2),'LineWidth',2);
%cishade(dvstim(1:l3,idrt3.' & sim.resp==1).',alpha,opts.color.hit3,t(1:l3),'LineWidth',2);
plot(t,mean(dvstim(:,sim.resp==0),2),'Color',opts.color.miss,'LineWidth',2,'LineStyle','-');
plot(t(1:l1),mean(dvstim(1:l1,idrt1.' & sim.resp==1),2),'Color',opts.color.hit1,'LineWidth',2,'LineStyle','-');
plot(t(1:l2),mean(dvstim(1:l2,idrt2.' & sim.resp==1),2),'Color',opts.color.hit2,'LineWidth',2,'LineStyle','-');
plot(t(1:l3),mean(dvstim(1:l3,idrt3.' & sim.resp==1),2),'Color',opts.color.hit3,'LineWidth',2,'LineStyle','-');

plot([0,0],ylim(),'k--');
set(gca,'XTick',-0.5:0.5:1.5,'XTickLabel',{},'YTick',0:2:10,'YTickLabel',{});
set(gcf,'Position',[100,500,250,150])

% save data
miss_ = mean(dvstim(:,sim.resp==0),2).';
rt1_ = nan(size(miss_));
rt2_ = nan(size(miss_));
rt3_ = nan(size(miss_));

rt1_(1:l1) = mean(dvstim(1:l1,idrt1.' & sim.resp==1),2).';
rt2_(1:l2) = mean(dvstim(1:l2,idrt2.' & sim.resp==1),2).';
rt3_(1:l3) = mean(dvstim(1:l3,idrt3.' & sim.resp==1),2).';

sel = t>= -0.5 & t < 1.5;
tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});

writetable(tbl,'../../supdata/SFig.8b.xls');

%%
figure(802); hold on;
t = linspace(-0.5,0.5,1001);
plot(t,mean(dvresp(:,idrt1.' & sim.resp==1),2),'Color',opts.color.hit1,'LineWidth',2);
plot(t,mean(dvresp(:,idrt2.' & sim.resp==1),2),'Color',opts.color.hit2,'LineWidth',2);
plot(t,mean(dvresp(:,idrt3.' & sim.resp==1),2),'Color',opts.color.hit3,'LineWidth',2);
%cishade(dvresp(:,idrt4.' & sim.resp==1).',[],'k',t);

% save data
rt1_ = mean(dvresp(:,idrt1.' & sim.resp==1),2).';
rt2_ = mean(dvresp(:,idrt2.' & sim.resp==1),2).';
rt3_ = mean(dvresp(:,idrt3.' & sim.resp==1),2).';

sel = t>= -0.5 & t < 0.5;
tbl = table(t(sel).',rt1_(sel).',rt2_(sel).',rt3_(sel).',miss_(sel).','VariableNames',{'time','rt1','rt2','rt3','miss'});

writetable(tbl,'../../supdata/SFig.8b_inset.xls');

%
maxall = mean(max(dvresp(601:end,sim.resp==1))>params.bound.val);
max1 = mean(max(dvresp(601:end,idrt1.' & sim.resp==1))>params.bound.val);
max2 = mean(max(dvresp(601:end,idrt2.' & sim.resp==1))>params.bound.val);
max3 = mean(max(dvresp(601:end,idrt3.' & sim.resp==1))>params.bound.val);
yl = [0,6]

plot([0,0],yl,'k--');
fprintf('Max: All: %.2f %%; RT1: %.2f %%;  RT2: %.2f %%;  RT3: %.2f %%\n',maxall*100,max1*100,max2*100,max3*100);

xlabel('Time from RT [s]');
ylabel('Simulated DV [a.u.]');
set(gcf,'Position',[100,500,200,150])

figure(801); ylim(yl);
if prod == 1
    set(gca,'XTickLabel',{},'YTickLabel',{});
    ylabel(''); xlabel('');
end
figure(802); ylim(yl);
if prod == 1
    set(gca,'XTickLabel',{},'YTickLabel',{});
    ylabel(''); xlabel('');
end

