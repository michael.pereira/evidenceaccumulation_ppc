%========================================================
%
%   Parameters for the computational model (first-order)
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

function [ params ] = paramset_1storder(  )

params = struct();

%% FIRST ORDER

% The decision bound (constrained to >0)
params.bound.val = 1;
params.bound.fit = 1;
params.bound.init = 2.^(0:1:10);
params.bound.trans = @(x) exp(x);
params.bound.itrans = @(x) log(x);

% The drift rate (constrained to >0)
params.drift.val = 1;
params.drift.fit = 1;
params.drift.init = 2.^(-12:1:-1);
params.drift.trans = @(x) exp(x+1);
params.drift.itrans = @(x) log(x)-1;

% The variability in the drift rate (constrained to >0)
params.driftvar.val = 4;
params.driftvar.fit = 1;
params.driftvar.init = 10.^(-2:0.5:1);
params.driftvar.trans = @(x) exp(x+1);
params.driftvar.itrans = @(x) log(x)-1;

% The noise 
params.noise.val = 0.1;
params.noise.fit = 0;
params.noise.init = 0.1;
params.noise.trans = @(x) exp(x+1);
params.noise.itrans = @(x) log(x)-1;

params.scale.val = 1;
params.scale.fit = 0;
params.scale.trans = @(x) exp(x);
params.scale.itrans = @(x) log(x);


% The decay of the drift rate over time
% (constrained to >0)
params.decay.val = 0.2;
params.decay.fit = 1;
params.decay.init = 0.1:0.2:0.5;
params.decay.trans = @(x) 1./(1+exp(-x));
params.decay.itrans = @(x) log((x)./(1-x));

% The inhibition paramseter 
params.lambda.val = 0.01;
params.lambda.fit = 1;
params.lambda.init = 1:2:5;
params.lambda.trans = @(x) exp(x+1);
params.lambda.itrans = @(x) log(x)-1;

% The non-decision time (constrained to 0<ndtime<1)
params.ndt.val = 0.1;
params.ndt.fit = 1;
params.ndt.init = 0.1:0.2:0.5;
params.ndt.trans = @(x) 1./(1+exp(-x));
params.ndt.itrans = @(x) log((x)./(1-x));

% The reflection bound (constrained to <0)
params.rbound.val = 0;
params.rbound.fit = 0;
params.rbound.trans = @(x) exp(x+1);
params.rbound.itrans = @(x) log(x)-1;

params.startvar.val = 0;
params.startvar.fit = 0;
params.startvar.trans = @(x) 1./(1+exp(-x));
params.startvar.itrans = @(x) log((x)./(1-x));



end

