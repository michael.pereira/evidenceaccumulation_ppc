%========================================================
%
%   Configuration file for the computational model
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================
if ismac
    user = 'michael';
end
opts.subjects = {'sub-01','sub-02','sub-03','sub-04','sub-05',...
    'sub-06','sub-07','sub-09','sub-10','sub-11','sub-12','sub-13',...
    'sub-14','sub-15','sub-16','sub-17','sub-18','sub-19'};

if strcmp(user,'michael')
    opts.dir.bids = '/Volumes/Data/data/BIDS/perithr_ppc/';
    opts.dir.bids_deriv = '/Volumes/Data/data/BIDS/perithr_ppc//derivatives/eegprep/';
    opts.dir.toolbox_eeglab = '/Users/meaperei/Toolboxes/eeglab/';
    opts.dir.toolbox_misc = '.';
end

% Set a  name to the fit
opts.type = 'rnd2';
% how many times we fit. > 1 can improve results
opts.nfits = 2;
% type of readout: max for maximal evidence, resp for a readout time-locked
% to the evidence reaching the decision boundary
opts.confreadout = 'max';
opts.confbiastype = 'evidence'; % 'conf' or 'evidence'
opts.confsuffix = '';
% To compute a proxy to the evidence accumulation process, we 
% want to find time of maximal discriminability by doing classification
% in a nrep x nfold cross-validation scheme
opts.nrep = 10;
opts.nfold = 10;

% Time between decision and motor output (Van den Berg et al., 2016).
opts.motordelay = 0.08;

% standard devation of the observation noise to compute the log-likelihood
opts.t1llstd = 0.02;
opts.t2llstd = 0.2;

% The number of seconds to simulate after the stimulus (doesn't take
% non-decision time into account so if opts.L=2 and ndtime is fitted to 0.3 will
% simulate RTs up to 2.3 seconds after stimulus.
opts.L = 4;

% Censoring of RTs for the first-order fit (RT + choice), we do not censor RTs that are
% higher than 2 s. 
opts.cens = [0.2,2]; 

% Censoring of RTs for the second-order fit (confidence), here we censor
% RTs > 500 ms. 
opts.cens2 = [0.2,0.5];

% Set time granularity for the random walk
opts.dt = 1e-3; 

% Set number of simulated trials for the second-order fit
opts.N = 10000; 

% For the optimization procedure
opts.fminfunc = @fminsearch; 
opts.maxiter = 500;
opts.maxiter_gridsearch = 30;
opts.times = 100:50:1000;
opts.optimizationdisplay1 = 'None';

% Whether to plot/print fitting information
opts.doplot = 0;
opts.doprint = 0;
opts.doprint2 = 1;

opts.ndt = [];

% colors for plotting
opts.color.nodelay = [109,186,150]./255;
opts.color.delay = [224,144,112]./255;

opts.color.hit = [83,169,212]./255;
opts.color.miss = [186,39,50]./255;
opts.color.crej = [149,189,20]./255;
opts.color.fa = [0 0 0]./255;

opts.color.hit1 = [162,202,211]./255;
opts.color.hit2 = [83,169,212]./255;
opts.color.hit3 = [2,61,79]./255;


