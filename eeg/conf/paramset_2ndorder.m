%========================================================
%
%   Parameters for the computational model (second-order)
%   
%   Pereira et al., 2021, 
%   Evidence accumulation determines perceptual 
%       consciousness and monitoring
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.ch>
%   16 Dec 2020
%========================================================

function [ params2 ] = paramset_2ndorder(readout)

if nargin == 0
    readout = 'max';
end
params2 = struct();

%% FIRST ORDER

% The sensitivity of confidence
params2.sensitivity.val = 1;
params2.sensitivity.fit = 1;
params2.sensitivity.init = 2:2:12;
params2.sensitivity.trans = @(x) exp(x+1);
params2.sensitivity.itrans = @(x) log(x)-1;

% The bias in confidence
params2.bias.val = 0;
params2.bias.fit = 0;
params2.bias.init = 0;
params2.bias.trans = @(x) x;
params2.bias.itrans = @(x) x;

if strcmp(readout,'max')
    % The confidence readout timing (when relevant)
    params2.readout.val = 0.0;
    params2.readout.fit = 0;
    params2.readout.init = 0;
    params2.readout.trans = @(x) exp(x+1);
    params2.readout.itrans = @(x) log(x)-1;
    
    % The mean of evidence in the absence of evidence
    params2.mu.val = 0;
    params2.mu.fit = 0;
    params2.mu.trans = @(x) x;
    params2.mu.itrans = @(x) x;
    
elseif strcmp(readout,'resp')
    % The confidence readout timing (when relevant)
    params2.readout.val = 0.0;
    params2.readout.fit = 1;
    params2.readout.init = [0.25,0.5];
    params2.readout.trans = @(x) exp(x+1);
    params2.readout.itrans = @(x) log(x)-1;
    
    % The mean of evidence in the absence of evidence
    params2.mu.val = 4;
    params2.mu.fit = 1;
    params2.mu.trans = @(x) x;
    params2.mu.itrans = @(x) x;
    
end
% The std of evidence in the absence of evidence
params2.sigma.val = 1;
params2.sigma.fit = 0;
params2.sigma.trans = @(x) exp(x+1);
params2.sigma.itrans = @(x) log(x)-1;


end

